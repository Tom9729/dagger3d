#include <stdlib.h>
#include <dagger3d.h>

float rx = 0.0f, ry = 0.0f;
int wireframe = 0;

void logic()
{
	rx += 1.0f;
	ry += 1.0f;
}

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	// Toggle wireframe view.
	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_W))
	{
		wireframe = !wireframe;
	}
}

void display()
{
	glTranslatef(0.0f, 0.0f, -5.0f);
	glRotatef(rx, 1.0f, 0.0f, 0.0f);
	glRotatef(ry, 0.0f, 1.0f, 0.0f);
	
	// Toggle wireframe mode, also toggles lighting.
	if (wireframe) 
	{
		glDisable(GL_LIGHTING);
		dgWindowSetWireframe(true);
	}
	
	else
	{
		glEnable(GL_LIGHTING);
		 dgWindowSetWireframe(false);
	}
	
	// Render a 3d cube.
	glutSolidCube(1);
	
	logic();
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Cube", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);

	// Just use GL's standard lighting.
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	dgWindowMainLoop();

	return 0;
}
