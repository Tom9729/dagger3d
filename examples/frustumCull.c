#include <stdlib.h>
#include <stdio.h>
#include <dagger3d.h>

int cam1, cam2;
float sphereRad = 1000;
float boxRad = 1500;

void init()
{
	// Create a camera to be the one
	// that we are observing.
	cam1 = dgCameraNew();
	// Set starting position.
	float pos1[3] = { 0, 0, -50000 };
	dgCameraSetPosition(cam1, pos1);
	
	// Create a camera and activate it.
	// This will be our viewing camera.
       	cam2 = dgCameraNew();
	dgCameraSetActive(cam2);
}

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	float ry = 0, rx = 0;
	float rotvel = 0.03f;

	if (dgWindowKeyPressed(DG_KEY_LEFT))
	{
		ry = -rotvel;
	}

	else if (dgWindowKeyPressed(DG_KEY_RIGHT))
	{
		ry = rotvel;
	}
	
	if (dgWindowKeyPressed(DG_KEY_UP))
	{
		rx = rotvel;
	}

	else if (dgWindowKeyPressed(DG_KEY_DOWN))
	{
		rx = -rotvel;
	}
	
	dgCameraRotate(cam1, rx, ry, 0);
}

void display()
{	
	// Draw a cube representing the camera.
	float pos[3];
	dgCameraGetPosition(cam1, pos);
	float rot[4];
	dgCameraGetRotation(cam1, rot);
	
	dgCameraFrustumUpdate(cam1);

	// Gather some information about the current frustum.
	float nearWidth, nearHeight;
	float farWidth, farHeight;
	float nearDist;
	float farDist;

	// Grab camera attributes for the camera we're observing.
	dgCameraGetFrustumAttributes(cam1,
				     NULL,
				     &nearWidth, &nearHeight,
				     &farWidth, &farHeight,
				     &nearDist, &farDist); 

	// Generate vertices for the near and far quads.
	float nearQuad[12];
	float farQuad[12];	
	dgCameraFrustumQuads(cam1, nearQuad, farQuad);

	// Generate plane equations for cam1's frustum.
	float planeqs[24];
	dgCameraFrustumPlanes(cam1, planeqs);

	// A view frustum is pretty big so scale everything down
	// so that we can see it easily.
	float sf = 0.001f;
	glScalef(sf, sf, sf);
	
	glPushAttrib(GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);
	glDisable(GL_LIGHTING);

	// Render near and far planes.
	glBegin(GL_QUADS);

	// Near plane.
	glColor3f(0, 1, 0);
	glVertex3fv(&nearQuad[0]);
	glVertex3fv(&nearQuad[3]);
	glVertex3fv(&nearQuad[6]);
	glVertex3fv(&nearQuad[9]);

	// Far plane.
	/* glColor3f(0.2, 1, 0.6); */
	/* glVertex3fv(&farQuad[0]); */
	/* glVertex3fv(&farQuad[3]); */
	/* glVertex3fv(&farQuad[6]); */
	/* glVertex3fv(&farQuad[9]); */

	// Top plane.
	glColor3f(0, 0.6, 0.5);
	glVertex3fv(&nearQuad[6]);
	glVertex3fv(&nearQuad[9]);
	glVertex3fv(&farQuad[9]);
	glVertex3fv(&farQuad[6]);

	// Bottom plane.
	glColor3f(0.2, 0.3, 0.5);
	glVertex3fv(&nearQuad[0]);
	glVertex3fv(&nearQuad[3]);
	glVertex3fv(&farQuad[3]);
	glVertex3fv(&farQuad[0]);

	// Right plane.
	glColor3f(0.6, 0.3, 0.3);
	glVertex3fv(&nearQuad[0]);
	glVertex3fv(&nearQuad[9]);
	glVertex3fv(&farQuad[9]);
	glVertex3fv(&farQuad[0]);

	// Left plane.
	glColor3f(1.0, 0.6, 0.0);
	glVertex3fv(&nearQuad[6]);
	glVertex3fv(&nearQuad[3]);
	glVertex3fv(&farQuad[3]);
	glVertex3fv(&farQuad[6]);

	glEnd();
	glPopAttrib();

	{
		// Create a fixed "ball" that will change colors depending
		// upon whether or not it's in the view frustum of cam1.
		float ballPos[3] = { 7000, 0, -45000 };
		int inside = dgGeometryFrustumCollidesSphere(ballPos, sphereRad, planeqs);
	
		glPushMatrix();
		glLoadIdentity();
		glScalef(sf, sf, sf);
		glTranslated(ballPos[0], ballPos[1], ballPos[2]);
		
		if (inside == DG_GEOMETRY_INTERSECTS)
		{
			glColor3d(0, 0, 1);
		}
		
		else if (inside == DG_GEOMETRY_CONTAINS)
		{
			glColor3d(0, 1, 0);
		}
		
		else
		{
			glColor3d(1, 0, 0);
		}
		
		glutSolidSphere(sphereRad, 32, 32);
		glPopMatrix();
	}

	{
		// Create a fixed "box" that will change colors depending
		// upon whether or not it's in the view frustum of cam1.
		float boxPos[3] = { -5000, 0, -45000 };
		float boxVerts[24] = {
			boxPos[0] + boxRad, boxPos[1] + boxRad, boxPos[2] + boxRad,
			boxPos[0] + boxRad, boxPos[1] + boxRad, boxPos[2] - boxRad,
			boxPos[0] + boxRad, boxPos[1] - boxRad, boxPos[2] + boxRad,
			boxPos[0] + boxRad, boxPos[1] - boxRad, boxPos[2] - boxRad,
			boxPos[0] - boxRad, boxPos[1] + boxRad, boxPos[2] + boxRad,
			boxPos[0] - boxRad, boxPos[1] + boxRad, boxPos[2] - boxRad,
			boxPos[0] - boxRad, boxPos[1] - boxRad, boxPos[2] + boxRad,
			boxPos[0] - boxRad, boxPos[1] - boxRad, boxPos[2] - boxRad
		};
		int inside = dgGeometryFrustumCollidesAABB(boxVerts, planeqs);

		glPushMatrix();
		glLoadIdentity();
		glScalef(sf, sf, sf);
		glTranslated(boxPos[0], boxPos[1], boxPos[2]);
		
		if (inside == DG_GEOMETRY_INTERSECTS)
		{
			glColor3d(0, 0, 1);
		}

		else if (inside == DG_GEOMETRY_CONTAINS)
		{
			glColor3d(0, 1, 0);
		}
		
		else
		{
			glColor3d(1, 0, 0);
		}
		
		glutSolidCube(boxRad);
		glPopMatrix();
	}
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Camera", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	
	// Setup the cameras.
	init();
	
	dgWindowMainLoop();
	
	return 0;
}
