#include <dagger3d.h>

void *e;
int texture;

void particleLogic(void *emitter, void *particle, double scale)
{	
	double life = dgParticleGetLife(particle);

	// Make particles fade-out.
	if (life < 10) 
	{
		float color[4];
		dgParticleGetColor(particle, color);
		color[3] = life / 10.0;
		dgParticleSetColor(particle, color);
	}
	
	// Apply gravity.
	float velocity[3];
	dgParticleGetVelocity(particle, velocity);
	velocity[1] -= 0.0008 * scale;
	dgParticleSetVelocity(particle, velocity);

	dgParticleSetSize(particle, dgParticleGetSize(particle) + 0.1 * scale);
}

void logic(double scale)
{	
	dgParticleEmitterUpdate(e, scale);
}

void input(double scale)
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}
	
	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_L))
	{
		dgParticleEmitterSetLocal(e, !dgParticleEmitterGetLocal(e));
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_K))
	{
		dgParticleEmitterKill(e);
	}
	
	float position[3], rotation[4];
	
	dgParticleEmitterGetPosition(e, position);
	dgParticleEmitterGetRotation(e, rotation);

	const double speed = 0.05 * scale;

	if (dgWindowKeyPressed(DG_KEY_DOWN)) position[1] -= speed;
	if (dgWindowKeyPressed(DG_KEY_UP)) position[1] += speed;
	if (dgWindowKeyPressed(DG_KEY_LEFT)) position[0] -= speed;
	if (dgWindowKeyPressed(DG_KEY_RIGHT)) position[0] += speed;
	if (dgWindowKeyPressed(DG_KEY_LOWER_Z))
		dgQuaternionApplyEuler(rotation, 0, speed, 0);
	if (dgWindowKeyPressed(DG_KEY_LOWER_X))
		dgQuaternionApplyEuler(rotation, 0, -speed, 0);

	dgParticleEmitterSetPosition(e, position);
	dgParticleEmitterSetRotation(e, rotation);
}

void display()
{
	dgParticleEmitterRender(e);

	/* unsigned char *scr = dgWindowScreenDump(); */
	/* dgFree(scr); */
	
	char *str = NULL;
	dgStringAppend(&str, "Particles: %d",
		       dgParticleEmitterGetNumParticlesAlive(e));
	dgFontPrint(dgFontGetSystemFont(),
		    0, 0, 0, str);
	dgFree(str);
}

void init()
{	
	e = dgParticleEmitterNew();
	
	// Push emitter back so we can see it.
	float position[3];
	dgParticleEmitterGetPosition(e, position);
	position[2] = -20;
	dgParticleEmitterSetPosition(e, position);
	
	// Rotate emitter to get a better view.
	float rotation[4];
	dgParticleEmitterGetRotation(e, rotation);
	dgQuaternionApplyEuler(rotation, M_PI / 2.0, 0, 0);
	dgParticleEmitterSetRotation(e, rotation);

	// Load a texture for the particles.
	texture = dgResLoad(DG_TEXTURE, "smoke.png", 0);
	dgParticleEmitterSetTexture(e, texture);
	
	// Change emission theta to be a cone.
	dgParticleEmitterSetTheta(e, dgMathDegreesToRadians(30));

	// Increase the number of particles.
	dgParticleEmitterSetNumParticles(e, 500);
	dgParticleEmitterSetFrequency(e, 2);

	// Tweak particle properties.
	dgParticleEmitterSetSize(e, 0.1, 0.2);
	dgParticleEmitterSetSpeed(e, 0.06, 0.1);
	dgParticleEmitterSetAcceleration(e, .005, .005);
	dgParticleEmitterSetLife(e, 100, 120);
	dgParticleEmitterSetColor(e,
				  .8, 1,   // Red
				  .8, 1,   // Green
				  .8, 1, // Blue
				  1, 1);  // Alpha

	// Add custom particle logic.
	dgParticleEmitterSetCallback(e, particleLogic);
}

void cleanup()
{
	// Delete emitter.
	dgParticleEmitterDelete(e);
	
	// Delete texture.
	dgResDelete(DG_TEXTURE, texture);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Particles", 800, 600, 60, false);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(logic);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
