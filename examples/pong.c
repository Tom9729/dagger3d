#include <dagger3d.h>

typedef struct
{		
	float sx, sy;
	float vy;
	int score;
	int ai;
} player_t;

typedef struct
{
	float sx, sy;
	float vx, vy;
} ball_t;

const int paddleWidth = 10;
const int paddleHeight = 40;
const float paddleAccel = 3.5f;
const int ballSize = 10;
const float ballSpeedMult = 1.05f;
const float ballSpeedgInit = 1.5f;

int font;
player_t players[2];
ball_t ball;

/**
 * Reposition the ball in the center,
 * and give it a random velocity.
 */
void resetBall()
{
	ball.sx = 0.0f;
	ball.sy = 0.0f;
	ball.vx = ballSpeedgInit * dgRandomSign();
	ball.vy = ballSpeedgInit * dgRandomSign();
}

/**
 * Initialize the players, and the ball.
 */
void init()
{
	// Seed the PRNG so the ball's motion isn't predictable.
	dgRandomSeed();
	
	memset(players, 0, sizeof(player_t) * 2);

	// Move the paddles to the sides.
	players[0].sx = -140.0f;
	players[1].sx = 140.0f;

	// Give the ball a random velocity.
	resetBall();

	// Enable AI for player 2.
	players[1].ai = 1;

	// Get font.
	font = dgFontGetSystemFont();
}

void drawRect(float x, float y, float w, float h)
{
	float hw, hh;
	
	// Half width.
	hw = w / 2.0f;
	// Half height.
	hh = h / 2.0f;

	glPushMatrix();
	glTranslatef(x, y, 0.0f);
	glRectf(-hw, hh, hw, -hh);
	glPopMatrix();
}

void keyboard()
{
	// Move up.
	if (dgWindowKeyPressed(DG_KEY_UP))
	{
		players[0].vy = paddleAccel;
	}
	
	// Move down.
	else if (dgWindowKeyPressed(DG_KEY_DOWN))
	{
		players[0].vy = -paddleAccel;
	}

	// Otherwise stop.
	else
	{
		players[0].vy = 0;
	}
	
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}
}

void logic()
{	
	// Move the ball.
	ball.sx += ball.vx;
	ball.sy += ball.vy;

	// For each player.
	for (int i = 0; i < 2; ++i)
	{
		// Run AI logic if it is enabled for this player.
		if (players[i].ai)
		{
			// Player-ball distance on the y-axis.
			float df = players[i].sy - ball.sy;
			// Sign of the previous.
			int sign = (df < 0.0f ? 1 : -1);
			df = dgMathAbs(df);
			// Keep the AI from overcompensating.
			df = (paddleAccel < df ? paddleAccel : df);
			players[i].vy = sign * df;
		}
		
		// Move player based upon velocity.
		players[i].sy += players[i].vy;
		
		// Keep the player in bounds.
		if ((players[i].sy + paddleHeight / 2.0f) > 111.0f)
		{
			players[i].sy = (111.0f - 0.0001f - paddleHeight / 2.0f);
			players[i].vy = 0.0f;
		}
		
		// Ditto above.
		if ((players[i].sy - paddleHeight / 2.0f) < -111.0f)
		{
			players[i].sy = (-111.0f + 0.0001f + paddleHeight / 2.0f);
			players[i].vy = 0.0f;
		}

		// Check if the ball is within the y-bounds of this paddle.
		if ((ball.sy + ballSize / 2.0f  <= players[i].sy + paddleHeight / 2.0f) &&
		    (ball.sy - ballSize / 2.0f >= players[i].sy - paddleHeight / 2.0f))
		{				
			// Distance between the paddle and the ball.
			float dist = dgMathAbs(ball.sx - players[i].sx);
			
			// Ball-paddle collision.
			if (dist < (ballSize / 2.0f) + (paddleWidth / 2.0f))
			{
				int sign = (ball.sx > 0 ? -1 : 1);

				ball.vx = dgMathAbs(ball.vy) * sign;

				// TODO Ball should get VY from paddle somehow.
				ball.vy = players[i].vy == 0.0f ? -ball.vx : players[i].vy;
			}	
		}
	}
	
	// Ball hits left side, player 2 scores. Reset ball.
	if (ball.sx < -150.0f)
	{
		++players[1].score;
		resetBall();
	}

	// Ball hits right side, player 2 scores. Reset ball.
	else if (ball.sx > 150.0f)
	{
		++players[0].score;
		resetBall();
	}
	
	// Ball hits top or bottom (we don't care).
	if (ball.sy < -110.0f || ball.sy > 110.0f)
	{
		ball.vy = -ball.vy;
	}
}

void display()
{
	// XXX
	glDisable(GL_CULL_FACE);

	// Zoom back so we can see.
	glTranslatef(0.0f, 0.0f, -200.0f);

	// Draw the players.
	for (int i = 0; i < 2; ++i)
	{
		drawRect(players[i].sx, players[i].sy, 
			 paddleWidth, paddleHeight);
	}

	// Draw the ball.
	drawRect(ball.sx, ball.sy, ballSize, ballSize);

	// Draw the score.
	char *p1score = NULL, *p2score = NULL;
	dgStringAppend(&p1score, "%d", players[0].score);
	dgStringAppend(&p2score, "%d", players[1].score);
	dgFontPrint(font, 100, 0, 0, p1score);
	dgFontPrint(font, dgWindowGetWidth() - 100, 0, 0, p2score);
	dgFree(p1score);
	dgFree(p2score);
	
	// Run AI code, bounds checking, collision response, etc.
	logic();
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Pong", 640, 480, 60, 0);
	dgWindowSetInputFunc(&keyboard);
	dgWindowSetDisplayFunc(&display);

	init();

	glutMainLoop();
}
