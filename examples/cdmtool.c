#include <dagger3d.h>

void usage()
{
	fprintf(stderr, 
		"Usage: cdmtool <in> <out>\n");
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	
	if (argc < 3)
	{
		usage();
		exit(-1);
	}

	int mesh = dgResLoad(DG_MESH, argv[1], DG_MESH_NORM_INTERP);

	dgMeshSaveCDM(argv[2], mesh);
	
	dgResDelete(DG_MESH, mesh);

	return EXIT_SUCCESS;
}
