#include <dagger3d.h>

#define UP 1
#define DOWN -1
#define LEFT -2
#define RIGHT 2
#define CYCLE_WIDTH 1.0f
#define CYCLE_HEIGHT 2.0f

typedef struct
{
	float pos[2];
	void *next;
} wall_t;

typedef struct
{
	float pos[2];
	int dir;
	float speed;
	float color[3];
	wall_t *wall;
} cycle_t;

typedef struct
{
	char *name;
	int score;
	cycle_t cycle;
} player_t;

void *players;
int human, human2;

void reset();

void drawWall(wall_t *w)
{
	glBegin(GL_LINE_STRIP);
	glVertex3fv(w->pos);

	while ((w = w->next) != NULL)
	{
		glVertex3fv(w->pos);
	}

	glEnd();
}

void drawCycle(cycle_t *c)
{
	glColor3fv(c->color);
	glPushMatrix();
	glTranslatef(c->pos[0], c->pos[1], 0.0f);
	
	if (c->dir % 2)
	{
		glRectf(-CYCLE_WIDTH, -CYCLE_HEIGHT, 
			CYCLE_WIDTH, CYCLE_HEIGHT);
	}
	
	else
	{
		glRectf(-CYCLE_HEIGHT, -CYCLE_WIDTH, 
			CYCLE_HEIGHT, CYCLE_WIDTH);
	}

	glPopMatrix();

	// Draw the wall.
	glBegin(GL_LINES);
	glVertex2fv(c->pos);
	glVertex2fv(c->wall->pos);
	//glColor3f(0.0f,0.0f,1.0f);
	glEnd();
	drawWall(c->wall);
}

#define VERTICAL 0
#define HORIZONTAL 1

int getLineType(float *p1, float *p2)
{
	if (p1[0] == p2[0] && p1[1] != p2[1])
	{
		return VERTICAL;
	}

	return HORIZONTAL;
}

void doCycleWallCollision(cycle_t *c, float *a, float *b)
{
	int type = getLineType(a, b);
	
// If A is between B and C.
#define IN_BETWEEN(A,B,C) ((A > B && A < C) || (A > C && A < B))
	
	if (type == VERTICAL && 
	    c->dir % 2 == 0 && //Cycle is moving horizontally.
	    IN_BETWEEN(c->pos[1], 
		       a[1],
		       b[1]))
	{
		if (dgMathAbs(c->pos[0] - a[0]) < CYCLE_WIDTH * 2.0f)
		{
			c->speed = 0;
		}
	}
	
	else if (c->dir % 2 && // Cycle is moving vertically.
		 IN_BETWEEN(c->pos[0], 
			    a[0],
			    b[0]))
	{
		if (dgMathAbs(c->pos[1] - a[1]) < CYCLE_WIDTH * 2.0f)
		{
			c->speed = 0;
		}
	}
}

void doWallCollision(cycle_t *c, cycle_t *c2)
{
	wall_t *last, *next;
	
	doCycleWallCollision(c, c2->pos, c2->wall->pos);
	last = c2->wall;

	while ((next = last->next) != NULL)
	{		
		doCycleWallCollision(c, last->pos, next->pos);
		last = next;
	}
}

void doLogic(player_t *p)
{
	cycle_t *c = &p->cycle;
	int size = dgArrayListSize(players);

	// Move the player.
	switch (c->dir)
	{
	case UP:
		c->pos[1] += c->speed;
		break;
	case DOWN:
		c->pos[1] -= c->speed;
		break;
	case LEFT:
		c->pos[0] -= c->speed;
		break;
	case RIGHT:
		c->pos[0] += c->speed;
		break;
	}

	// Keep the player in bounds.
	if (dgMathAbs(c->pos[1]) >= 80.0f - CYCLE_HEIGHT - 1.0f || dgMathAbs(c->pos[0]) >= 110.0f - CYCLE_WIDTH - 1.0f)
	{
		c->speed = 0;
	}

	// Check if the player collided with anyone's walls (including their own).
	for (int i = 0; i < size; ++i)
	{
		if (dgArrayListItemExists(players, i))
		{
			player_t *p2 = dgArrayListGetItem(players, i);

			doWallCollision(c, &p2->cycle);		
		}
	}
}

void display()
{
	glTranslatef(0.0f, 0.0f, -150.0f);

	int size = dgArrayListSize(players);

	for (int i = 0; i < size; ++i)
	{
		if (dgArrayListItemExists(players, i))
		{
			player_t *p = dgArrayListGetItem(players, i);
			
			doLogic(p);
			drawCycle(&p->cycle);
		}
	}

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINE_STRIP);
	glVertex2f(-110.0f, -80.0f);
	glVertex2f(-110.0f, 80.0f);
	glVertex2f(110.0f, 80.0f);
	glVertex2f(110.0f, -80.0f);
	glVertex2f(-110.0f, -80.0f);
	glEnd();
}

void doChangeDirection(cycle_t *c, int dir)
{
	wall_t *w;
	
	c->dir = dir;
	
	dgMalloc(w, wall_t, 1);
	memcpy(w->pos, c->pos, sizeof(float) * 2);
	w->next = c->wall;
	c->wall = w;
}

void keyb()
{
       	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	player_t *p1 = dgArrayListGetItem(players, human);
	cycle_t *c1 = &p1->cycle;

	if (c1->speed != 0.0f)
	{
		if (dgWindowKeyPressedOnce(DG_KEY_UP) && !(c1->dir % 2))
		{
			doChangeDirection(c1, UP);
		}
		
		else if (dgWindowKeyPressedOnce(DG_KEY_DOWN) && !(c1->dir % 2))
		{
			doChangeDirection(c1, DOWN);
		}
		
		else if (dgWindowKeyPressedOnce(DG_KEY_LEFT) && c1->dir % 2)
		{
			doChangeDirection(c1, LEFT);
		}
		
		else if (dgWindowKeyPressedOnce(DG_KEY_RIGHT) && c1->dir % 2)
		{
			doChangeDirection(c1, RIGHT);
		}	
	}

	player_t *p2 = dgArrayListGetItem(players, human2);
	cycle_t *c2 = &p2->cycle;

	if (c2->speed != 0.0f)
	{
		if (dgWindowKeyPressedOnce(DG_KEY_LOWER_W) && !(c2->dir % 2))
		{
			doChangeDirection(c2, UP);
		}
		
		else if (dgWindowKeyPressedOnce(DG_KEY_LOWER_S) && !(c2->dir % 2))
		{
			doChangeDirection(c2, DOWN);
		}
		
		else if (dgWindowKeyPressedOnce(DG_KEY_LOWER_A) && c2->dir % 2)
		{
			doChangeDirection(c2, LEFT);
		}
		
		else if (dgWindowKeyPressedOnce(DG_KEY_LOWER_D) && c2->dir % 2)
		{
			doChangeDirection(c2, RIGHT);
		}	
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_R))
	{
		reset();
	}
}

void deleteWall(cycle_t *c)
{
	if (c->wall->next != NULL)
	{
		wall_t *w = c->wall->next;
		
		while (w != NULL)
		{
			wall_t *temp;
			
			temp = w->next;
			dgFree(w);
			w = temp;
		}
		
		c->wall->next = NULL;
	}

	
}

void reset()
{
	player_t *p1, *p2;

	p1 = dgArrayListGetItem(players, human);
	p1->cycle.pos[0] = 100.0f;
	p1->cycle.pos[1] = 0.0f;
	p1->cycle.color[0] = 1.0f;
	p1->cycle.color[1] = 0.0f;
	p1->cycle.color[2] = 0.0f;
	p1->cycle.speed = 0.5f;
	p1->cycle.dir = UP;
	memcpy(p1->cycle.wall->pos, p1->cycle.pos, sizeof(float) * 2);
	deleteWall(&p1->cycle);
		
	p2 = dgArrayListGetItem(players, human2);
	p2->cycle.pos[0] = -100.0f;
	p2->cycle.pos[1] = 0.0f;
	p2->cycle.color[0] = 0.0f;
	p2->cycle.color[1] = 1.0f;
	p2->cycle.color[2] = 0.0f;
	p2->cycle.speed = 0.5f;
	p2->cycle.dir = UP;
	memcpy(p2->cycle.wall->pos, p2->cycle.pos, sizeof(float) * 2);
	deleteWall(&p2->cycle);
}

void init()
{
	player_t *p1, *p2;
	
	players = dgArrayListNew();
	
	dgMalloc(p1, player_t, 1);
	dgMalloc(p1->cycle.wall, wall_t, 1);
	p1->cycle.wall->next = NULL;
	human = dgArrayListAddItem(players, p1);

	dgMalloc(p2, player_t, 1);
	dgMalloc(p2->cycle.wall, wall_t, 1);
	p2->cycle.wall->next = NULL;
	human2 = dgArrayListAddItem(players, p2);

	reset();
}

void cleanup()
{
	reset();
	
	player_t *p1 = dgArrayListDeleteItem(players, human);
	player_t *p2 = dgArrayListDeleteItem(players, human2);

	dgFree(p1->cycle.wall);
	dgFree(p2->cycle.wall);
	dgFree(p1);
	dgFree(p2);

	dgArrayListDelete(players);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Tron", 640, 480, 60, 0);
	dgWindowSetInputFunc(keyb);
	dgWindowSetDisplayFunc(display);
	dgWindowSetCleanupFunc(cleanup);

	init();

	glutMainLoop();

	return 0;
}
