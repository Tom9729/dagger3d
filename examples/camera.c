#include <dagger3d.h>

typedef struct
{
	int x, y, z, s;
} cube;

#define NUMCUBES 1000

int cam, cam2;
float camVel = 0.0;
cube cb[NUMCUBES];
int useFrustumCulling = true;
int useDotProdOptim = false;
int useConeCulling = false;
int observer = false;

void init()
{	
	dgMouseShowCursor(false);
	dgMouseSetCaptured(true);
	dgMouseSetAutoCapture(true);
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	
	dgWindowDrawFPS(true);

	// Create an octree.
	//float center[3] = { 400, 400, 400 };
	
	// Create a random cube field.
	for (int i = 0; i < NUMCUBES; ++i)
	{		
		cb[i].x = dgRandomInt(0, 800);
		cb[i].y = dgRandomInt(0, 800);
		cb[i].z = dgRandomInt(0, 800);
		cb[i].s = dgRandomInt(1, 20);

		//float pos[3] = { cb[i].x, cb[i].y, cb[i].z };
	}
	
	// Create a camera and activate it.
       	cam = dgCameraNew();
	dgCameraSetActive(cam);

	// Create an observer.
	cam2 = dgCameraNew();
	/* dgCameraSetActive(cam2); */
	
	// Set starting position.
	float initPos[3] = { 400, 400, 400 };
	dgCameraSetPosition(cam, initPos);
	dgCameraSetPosition(cam2, initPos);
}

void cleanup()
{
	
}

void logic()
{	
	float vel[3];
	float pos[3];

	// Get the camera's current position.
	dgCameraGetPosition(cam, pos);

	// Get the camera's forward vector.
	dgCameraGetForward(cam, vel);
	
	// Scale it by the speed.
	dgVectorScale(vel, camVel);
	
	// Add to position.
	dgVectorAdd(pos, vel, pos);

	// Set the camera's new position.
	dgCameraSetPosition(cam, pos);
}

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_F))
	{
		useFrustumCulling = !useFrustumCulling;
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_O))
	{
		if ((observer = !observer))
		{
			dgCameraSetActive(cam2);
		}

		else
		{
			dgCameraSetActive(cam);
		}
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_D) &&
	    dgWindowKeyModifier(DG_KEY_ALT))
	{
		dgMouseSetCaptured(false);
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_Q))
	{
		useDotProdOptim = !useDotProdOptim;
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_C))
	{
		useConeCulling = !useConeCulling;
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_W))
	{
		camVel += 0.1f;
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_S))
	{
		camVel -= 0.1f;

		if (camVel < 0.0)
		{
			camVel = 0.0f;
		}
	}

	float rotvel = 0.006f;
	float rx = 0, ry = 0;
	int dx, dy;
	
	dgMouseGetDelta(&dx, &dy);
	
	static float trx = 0;
	static float M_PI_H = M_PI / 2;

	// Remember that deltaX is really yaw,
	// and deltaY is pitch.
	rx = dy * rotvel;
	ry = dx * rotvel;
	
	// Oops, went over north pole.
	if (trx + rx >= M_PI_H)
	{
		rx = M_PI_H - trx;
	}

	// Oops, went over south pole.
	else if (trx + rx <= -M_PI_H)
	{
		rx = -M_PI_H - trx;
	}

	// Update total yaw.
	trx += rx;

	// Apply new rotation to the camera.
	dgCameraRotate(cam, -rx, ry, 0);
}

void drawFrustum()
{
	float nearQuad[12];
	float farQuad[12];	

	dgCameraFrustumQuads(cam, nearQuad, farQuad);

	glPushAttrib(GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);
	glDisable(GL_LIGHTING);

	// Render near and far planes.
	glBegin(GL_QUADS);

	// Near plane.
	glColor3f(0, 1, 0);
	glVertex3fv(&nearQuad[0]);
	glVertex3fv(&nearQuad[3]);
	glVertex3fv(&nearQuad[6]);
	glVertex3fv(&nearQuad[9]);

	// Top plane.
	glColor3f(0, 0.6, 0.5);
	glVertex3fv(&nearQuad[6]);
	glVertex3fv(&nearQuad[9]);
	glVertex3fv(&farQuad[9]);
	glVertex3fv(&farQuad[6]);

	// Bottom plane.
	glColor3f(0.2, 0.3, 0.5);
	glVertex3fv(&nearQuad[0]);
	glVertex3fv(&nearQuad[3]);
	glVertex3fv(&farQuad[3]);
	glVertex3fv(&farQuad[0]);

	// Right plane.
	glColor3f(0.6, 0.3, 0.3);
	glVertex3fv(&nearQuad[0]);
	glVertex3fv(&nearQuad[9]);
	glVertex3fv(&farQuad[9]);
	glVertex3fv(&farQuad[0]);

	// Left plane.
	glColor3f(1.0, 0.6, 0.0);
	glVertex3fv(&nearQuad[6]);
	glVertex3fv(&nearQuad[3]);
	glVertex3fv(&farQuad[3]);
	glVertex3fv(&farQuad[6]);

	glEnd();
	glPopAttrib();
}

void display()
{	
	float planeqs[24];	

	// Get the camera's position.
	float camPos[3];
	dgCameraGetPosition(cam, camPos);

	// Get the camera's forward vector.
	float camForward[3];
	dgCameraGetForward(cam, camForward);
	dgVectorNorm(camForward);

	// If frustum culling is enabled then we need to update
	// the frustm plane equations.
	if (useFrustumCulling || useConeCulling)
	{
		dgCameraFrustumUpdate(cam);
		dgCameraFrustumPlanes(cam, planeqs);

		// Are we using the observer viewpoint?
		// If so, draw the frustum.
		if (observer)
		{
			drawFrustum();
		}
	}
	
	// Render cube field.
	for (int i = 0; i < NUMCUBES; ++i)
	{		
		float cubePos[3] = { cb[i].x, cb[i].y, cb[i].z };
		float cubeRad = cb[i].s;
		/* float cubeVerts[24] = { */
		/* 	cubePos[0] + cubeRad, cubePos[1] + cubeRad, cubePos[2] + cubeRad, */
		/* 	cubePos[0] + cubeRad, cubePos[1] + cubeRad, cubePos[2] - cubeRad, */
		/* 	cubePos[0] + cubeRad, cubePos[1] - cubeRad, cubePos[2] + cubeRad, */
		/* 	cubePos[0] + cubeRad, cubePos[1] - cubeRad, cubePos[2] - cubeRad, */
		/* 	cubePos[0] - cubeRad, cubePos[1] + cubeRad, cubePos[2] + cubeRad, */
		/* 	cubePos[0] - cubeRad, cubePos[1] + cubeRad, cubePos[2] - cubeRad, */
		/* 	cubePos[0] - cubeRad, cubePos[1] - cubeRad, cubePos[2] + cubeRad, */
		/* 	cubePos[0] - cubeRad, cubePos[1] - cubeRad, cubePos[2] - cubeRad */
		/* }; */
		
		int itemIsVisible = true;

		// Are we using dot product optimizations?	
		if (itemIsVisible && useDotProdOptim)
		{	
			// Get direction from camera to item.
			float camToItem[3];
			dgVectorSub(cubePos, camPos, camToItem);
			
			// Take dotprod of camera's forward vector
			// and camera's to-item vector.
			float dp = dgVectorDotProd(camForward, camToItem);
			
			// If dotprod is negative, item is behind camera.
			if (dp < 0.0)
			{
				itemIsVisible = false;
			}			
		}
		
		// Frustum cone culling.
		if (itemIsVisible && useConeCulling)
		{
			float angle, len;

			dgCameraFrustumCone(cam, &angle, &len);

			// Get direction from camera to item.
			float camToItem[3];
			dgVectorSub(cubePos, camPos, camToItem);
			dgVectorNorm(camToItem);
			
			// Take dotprod of camera's forward vector
			// and camera's to-item vector.
			float dp = dgVectorDotProd(camForward, camToItem);

			if (angle > dp)
			{
				itemIsVisible = false;
			}
		}
		
		// Frustum culling.
		if (itemIsVisible && useFrustumCulling)
		{		
			itemIsVisible = dgGeometryFrustumCollidesSphere(cubePos, 
									cubeRad, 
									planeqs);	
		}	

		// If the item is still visible, draw it!
		if (itemIsVisible)
		{
			glPushMatrix();
			glTranslatef(cb[i].x, cb[i].y, cb[i].z);
			glutSolidSphere(cb[i].s, 24, 24);
			glPopMatrix();
		}	
	}
	
	char *str = NULL;
	float pos[3];
	float rot[4];
	
	// Write some debug information to the screen.
	glColor3f(1, 1, 1);
	dgCameraGetPosition(cam, pos);
	dgCameraGetRotation(cam, rot);
	dgStringAppend(&str, "Position: %f %f %f\n"
		       "Rotation: %f %f %f %f\n"
		       "Speed: %f\n"
		       "Frustum culling: %s\n"
		       "Cone culling: %s\n"
		       "Dot prod optim: %s\n"
		       "Viewpoint: %s\n",
		       pos[0], pos[1], pos[2],
		       rot[0], rot[1], rot[2], rot[3],
		       camVel,
		       useFrustumCulling ? "enabled" : "disabled",
		       useConeCulling ? "enabled" : "disabled",
		       useDotProdOptim ? "enabled" : "disabled",
		       observer ? "camera" : "observer"
		);
	dgFontPrint(dgFontGetSystemFont(),
		    0, dgWindowGetHeight() - 40, 1000, str);
	dgFree(str);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Camera", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(logic);       
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}

