#include <stdlib.h>
#include <stdio.h>
#include <dagger3d.h>

void keyb()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}
	
	if (dgMouseButtonIsDown(DG_BUTTON_LEFT))
	{
		dgWindowSelect(dgMouseGetX(), dgMouseGetY(), 0.1, 0.1);
	}
}

void display()
{
	glTranslatef(0.0f, 0.0f, -5.0f);	

	glColor3f(0.2f, 1.0f, 0.0f);
	glPushName(6);
	glRectd(-1.0, -1.0, 1.0, 1.0);
	glPopName();

	glColor3f(0.0f, 1.0f, 0.4f);
	glPushName(8);
	glRectd(2.0, 2.0, 3.0, 3.0);
	glPopName();
}

void selectFunc(int hits, unsigned int *buf)
{
	while (hits--)
	{
		if (dgWindowSelectGetName(hits, buf) > 0)
		{
			printf("%d\n", dgWindowSelectGetName(hits, buf));
		}
	}
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Select", 320, 320, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(keyb);
	dgWindowSetSelectFunc(selectFunc);

	dgMouseEnabled(true);
	dgMouseShowCursor(true);

	glutMainLoop();

	return 0;
}
