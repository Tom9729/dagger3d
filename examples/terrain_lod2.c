#include <dagger3d.h>

// XXX http://www.flipcode.com/archives/article_geomipmaps.pdf
// XXX glDrawElements(GL_TRIANGLES, numTris * 3, GL_UNSIGNED_INT, tris);

int mipmapLevel = 0;

/*
  Need to make a virtual interface to terrain.

  Have a terrain object we can address that in-turn addresses
  individual terrain blocks. and handles mipmapping on it's own.

  Terrain -> QuadtreeNode -> TerrainBlock -> TerrainMipMap
                  |-> AABB

  2^(2*k) = 2^k * 2^k
  n       = q   * q

  q = number of blocks, size of block
  q = 2^k, q is a power of 2 therefore is divisible by any smaller power of 2

  How should mipmapping be determined?

  Resolution of level w is 2^(k-w)

  Half resolution of previous.

  0 2^6 64
  1 2^5 32
  2 2^4 16
  3 2^3 8
  4 2^2 4
  5 2^1 2
  
  so we have log2(q) mipmaps for each block
  
*/

typedef struct
{
	/** Two opposite points defining an axis-aligned bounding-box.
	    
 	          x-----x max
 	         /|    /|
	        / |   / |
	       x-----x  |
	       |  x--|--x
	       | /   | /
	       |/    |/
           min x-----x
       
	   A couple points
	   - Origin is the center of the AABB
	   - Left handed coordinate system (+X = right, +Y = up, +Z = forward)
	*/
	float min[3], max[3];
} AABB;

typedef struct
{	
	/** Tight-packed array of vertices. Vertices
	    are arranged in a square row-major grid. */
	float *verts;
	/** Tight-packed array of vertex normals 
	    corresponding to the array of vertices. */
	float *norms;
	/** Tight-packed array of UV-coords corresponding
	    to array of vertices. */
	float *texcos;
	/** Indices into array of vertices. Every three
	    indices forms a triangle. */
	unsigned int *tris;
	/** Number of vertices, number of triangles. */
	unsigned int numVerts, numTris;
	/** Mipmap distance. */
	float dist;
} TerrainMipMap;

typedef struct
{
	/** Array of progressively downsampled terrain meshes. 
	    Index 0 indicates the original mesh. */
	TerrainMipMap *mipmaps;
	/** Pointer to visible mipmap level, NULL indicates invisible block. */
	TerrainMipMap *visibleMipmap;
	/** Texture for this terrain block, -1 indicates no texture. */
	int texture;
	/** Axis-aligned box containing this terrain block. */
	AABB aabb;
} TerrainBlock;

typedef struct
{
	/** The four quadrants/leaves of this node (null if NE).
	    
	    x---x---x
	    | 1 | 0 |
	    x---x---x
	    | 2 | 3 |
	    x---x---x	    
	*/
	void *children[4];
	/** Axis-aligned box containing all terrain blocks in this node. */
	AABB aabb;
	/** List of terrain blocks in this node. */
	void *terrain;
	/** True if this leaf is visible. */
	int visible;
} QuadtreeNode;

typedef struct
{
	/** qxq matrix of terrain blocks
	   
	    [00] [01] ... [0q]
	    [10] [11] ... [1q]
	    ...  ...  ... ...
	    [q0] [q1] ... [qq]
	*/
	TerrainBlock ***trnmat;
	/** q, number of blocks in a row/column and qxq res block mipmap level 0 */
	int trnmat_size;
	/** n, e.g. nxn terrain, resolution at mipmap 0 */
	int trn_res;
	/** log2(q), number of mipmap levels per block */
	int numMipmaps;
	float xzScale;
	float yScale;
} Terrain;

TerrainBlock * _dgTerrainBlockNew(Terrain *trn, int i, int j)
{
	const int q = trn->trnmat_size;
	const int n = trn->trn_res;
	const float blockSize = trn->xzScale;
		
	dgDeclMalloc(tbk, TerrainBlock, 1);

	// calculate center of this block
	const float center[] = { 
		i * blockSize + (blockSize / 2.0) - (q / 2) * blockSize,
		0,
		j * blockSize + (blockSize / 2.0) - (q / 2) * blockSize,
	};
	
	// construct pointer to bounding-box for this block
	AABB *aabb = &tbk->aabb;

	/* dgPrintf("(%d,%d) -> (%.1f,%.1f)\n", i, j, center[0], center[1]); */
	
	// construct AABB
	aabb->min[0] = center[0] - (blockSize / 2.0);
	aabb->min[2] = center[2] - (blockSize / 2.0);	
	aabb->max[0] = center[0] + (blockSize / 2.0);	
	aabb->max[2] = center[2] + (blockSize / 2.0);
	
	// XXX
	aabb->min[1] = center[1] - (blockSize / 2.0);
	aabb->max[1] = center[1] + (blockSize / 2.0);

	// create mipmap levels
	dgMalloc(tbk->mipmaps, TerrainMipMap, trn->numMipmaps);
	tbk->visibleMipmap = tbk->mipmaps;

	int n_local = n;
	int q_local = q;

	// initialize all mipmap levels
	for (int w = 0; w < trn->numMipmaps; ++w)
	{
		TerrainMipMap *tmp = &tbk->mipmaps[w];
		
		// calculate number of vertices and triangles
		tmp->numVerts = n_local;
		tmp->numTris = 2 * pow((q_local - 1), 2);
		
		/* dgPrintf("w=%d, numVerts=%d, numTris=%d\n", */
		/* 	 w, tmp->numVerts, tmp->numTris); */
		
		// allocate memory for mesh data
		dgMalloc(tmp->verts, float, 3 * tmp->numVerts);
		dgMalloc(tmp->norms, float, 3 * tmp->numVerts);
		dgMalloc(tmp->texcos, float, 2 * tmp->numVerts);
		dgMalloc(tmp->tris, unsigned int, 3 * tmp->numTris);
		
		// determine vertex spacing
		const float vertSpacing = blockSize / (float) (q_local - 1);
		
		// initalize insertion pointers
		float *verts_ptr = tmp->verts;
		float *norms_ptr = tmp->norms;
		float *texcos_ptr = tmp->texcos;
		unsigned int *tris_ptr = tmp->tris;

		// XXX to make sure we counted right
		int vertsAdded = 0;
		int trisAdded = 0;
		
		// for each row of vertices
		for (int s = 0; s < q_local; ++s)
		{
			// for each column of vertices
			for (int t = 0; t < q_local; ++t)
			{
				// initalize vertex (height=0)
				verts_ptr[0] = aabb->min[0] + s * vertSpacing;
				verts_ptr[2] = aabb->min[2] + t * vertSpacing;
				verts_ptr += 3;
				vertsAdded += 1;
				
				// initialize normal (straight up)
				norms_ptr[1] = 1;
				norms_ptr += 3;
				
				// initialize texture coordinate
				texcos_ptr[0] = s / (float) q_local;
				texcos_ptr[1] = t / (float) q_local;
				texcos_ptr += 2;							
			}
		}

		// for each row of vertices
		for (int s = 1; s < q_local; ++s)
		{
			// for each column of vertices
			for (int t = 1; t < q_local; ++t)
			{
				// initalize indices for the 2 triangles added by this vertex
				tris_ptr[0] = dgIdx(s - 1, t - 1, q_local);
				tris_ptr[1] = dgIdx(s, t - 1, q_local);
				tris_ptr[2] = dgIdx(s, t, q_local);
				tris_ptr[3] = dgIdx(s, t, q_local);
				tris_ptr[4] = dgIdx(s - 1, t, q_local);
				tris_ptr[5] = dgIdx(s - 1, t - 1, q_local);
				tris_ptr += 6;
				trisAdded += 2;
			}
		}
		
		//dgPrintf("\t%d\t%d\t%d\n", tmp->numVerts, trisAdded, q_local);

		// XXX make sure we counted right
		assert(vertsAdded == tmp->numVerts);
		assert(trisAdded == tmp->numTris);

		n_local /= 4;
		q_local /= 2;
	}

	
	return tbk;
}

void _dgTerrainBlockDelete(TerrainBlock *tbk, Terrain *trn)
{
	assert(tbk);

	// deallocate all mipmap levels
	for (int w = 0; w < trn->numMipmaps; ++w)
	{
		TerrainMipMap *tmp = &tbk->mipmaps[w];
		
		dgFree(tmp->verts);
		dgFree(tmp->norms);
		dgFree(tmp->texcos);
		dgFree(tmp->tris);
	}
	
	// deallocate everything else
	dgFree(tbk->mipmaps);
	dgFree(tbk);
}

/**
 * Construct a new nxn heightmap.
 *
 * By default the terrain is initialized to be flat (height=0)
 * and is completely visible.
 *
 * [n = 2^(2*k)] where k is a positive non-zero integer.
 * [q = sqrt(n)] 
 * 
 * Terrain is internally broken into q^2-many blocks of size q.
 *
 * @param[out] trn Terrain object to initialize.
 * @param[in] k Size of heightmap.
 * @param[in] xzScale Scales the distance between points.
 * @param[in] yScale Scales the height of a point.
 */
Terrain *_dgTerrainNew(int k, float xzScale, float yScale)
{
	assert(k > 0);
	assert(xzScale > 0);
	assert(yScale > 0);

	dgDeclMalloc(trn, Terrain, 1);

	const int n = (trn->trn_res = pow(2, 2 * k));
	const int q = (trn->trnmat_size = sqrt(n));       

	// qxq terrain matrix
	dgMalloc(trn->trnmat, TerrainBlock**, q);
	dgMalloc(trn->trnmat[0], TerrainBlock*, n);
	
	// log2(q) number of mipmap levels
	trn->numMipmaps = log(q) / log(2);

	// store scale info
	trn->xzScale = xzScale;
	trn->yScale = yScale;
	
	dgPrintf("n=%d, q=%d, numMipmaps=%d\n", n, q, trn->numMipmaps);
	
	// initialize all terrain blocks
	for (int i = 0; i < q; ++i)
	{
		// initialize row pointer
		trn->trnmat[i] = trn->trnmat[0] + q * i;
		
		// initalize all blocks in row
		for (int j = 0; j < q; ++j) 
		{
			trn->trnmat[i][j] = _dgTerrainBlockNew(trn, i, j);
		}
	}
	
	return trn;
}

/**
 * Delete a heightmap.
 *
 * @param[in] trn
 */
void _dgTerrainDelete(Terrain *trn)
{
	assert(trn);

	const int q = trn->trnmat_size;

	// dealloc all terrain blocks
	for (int i = 0; i < q; ++i)
	{
		// initalize all blocks in row
		for (int j = 0; j < q; ++j) 
		{
			_dgTerrainBlockDelete(trn->trnmat[i][j], trn);
		}
	}
	
	// delete terrain matrix
	dgFree(trn->trnmat[0]);
	dgFree(trn->trnmat);
	
	// delete terrain object
	dgFree(trn);
}

/**
 * Set the value of a point in the heightmap.
 *
 * Note: This does not automatically update
 * any lighting-normals or mipmaps associated with this point. 
 * dgTerrainUpdateMipmaps() must at some point be 
 * called to do this instead.
 *
 * @param[in] trn
 * @param[in] s
 * @param[in] t
 * @param[in] height
 */
void _dgTerrainSetHeight(Terrain *trn, int s, int t, float height)
{	
	assert(trn);
	assert(s >= 0 && s < trn->trn_res);
	assert(t >= 0 && t < trn->trn_res);

	// terrain resolution (nxn)
	const int n = trn->trn_res;
	// number of blocks
	const int q = trn->trnmat_size;
	
	/*
	  notice that a point (s,t) may exist in 1, 2, or 4 blocks
	  
	  remember that s/t are in the range [0,n)
	  
	  q = sqrt(n)
	  	  
	   0   1   2   3
	 0 x---x---x---x
	   |   |   |   |
	 1 x---x---x---x
	   |   |   |   |
	 2 x---x---x---x
	   |   |   |   |
	 3 x---x---x---x
	 
	*/

	const float s_a = s / (float) n;
	const float t_a = s / (float) n;

	dgPrintf("%d,%d\t%f,%f\t%f,%f\n", s, t, s_a, t_a, s_a * q, t_a * q);
}

/**
 * Get the value of a point in the heightmap.
 * 
 * @param[in] trn
 * @param[in] s
 * @param[in] t
 * @return
 */
float _dgTerrainGetHeight(Terrain *trn, int s, int t);

/**
 * Recalculate lighting normals and mipmaps for any "dirty" blocks.
 *
 * @param[in] trn
 */
void _dgTerrainUpdateMipmaps(Terrain *trn);

void _dgTerrainUpdateVisibility(Terrain *trn, float *feqs);

void _dgTerrainRender(Terrain *trn)
{
	assert(trn);

	// XXX descend through quadtree

	


	
	const int q = trn->trnmat_size;
	// for each terrain block in matrix
	for (int i = 0; i < q; ++i)
	{
		for (int j = 0; j < q; ++j)
		{
			TerrainBlock *tbk = trn->trnmat[i][j];

			// XXX draw mesh
			{
				for (int i = 0; i < 2; ++i)
				{
					// first pass, fill terrain
					if (i == 0)
					{					
						glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
						glEnable(GL_LIGHTING); 
						glColor3f(1, 1, 1);
					}
					
					// second pass, render wireframe version
					// with polygon offset
					else
					{					
						glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
						glDisable(GL_LIGHTING); 
						glColor3f(0.2, 0.2, 1);
						glDisable(GL_DEPTH_TEST);
					}
					
					TerrainMipMap *tmp = &tbk->mipmaps[mipmapLevel];
					
					glEnableClientState(GL_VERTEX_ARRAY);
					glVertexPointer(3, GL_FLOAT, 0, tmp->verts);
					glNormalPointer(GL_FLOAT, 0, tmp->norms);
					glTexCoordPointer(2, GL_FLOAT, 0, tmp->texcos);
					glDrawElements(GL_TRIANGLES, tmp->numTris * 3, 
						       GL_UNSIGNED_INT, tmp->tris);
					glDisableClientState(GL_VERTEX_ARRAY);
				}
				
				// clean up
				glEnable(GL_DEPTH_TEST);
			}
			
			// XXX draw AABB
			{
				AABB *aabb = &tbk->aabb;
				/*
				  
				     5-----4
				    /|    /|
				   / |   / |
				  3-----2  |
				  |  6--|--7
				  | /   | /
				  |/    |/
				  0-----1
				   
				*/   
				const float aabb_verts[] = {
					aabb->min[0], aabb->min[1], aabb->min[2],
					aabb->max[0], aabb->min[1], aabb->min[2],
					aabb->max[0], aabb->max[1], aabb->min[2],
					aabb->min[0], aabb->max[1], aabb->min[2],
					
					aabb->max[0], aabb->max[1], aabb->max[2],
					aabb->min[0], aabb->max[1], aabb->max[2],
					aabb->min[0], aabb->min[1], aabb->max[2],
				aabb->max[0], aabb->min[1], aabb->max[2],
				};
				const unsigned int aabb_indices[] = {
					0, 1, 2, 3, // front
					1, 7, 4, 2, // right
					5, 4, 7, 6, // back
					3, 5, 6, 0, // left	
					3, 2, 4, 5, // top
					1, 0, 6, 7, // bottom
				};
				
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				glDisable(GL_LIGHTING);
				glColor3f(1, 0.2, 0.2);

				glEnableClientState(GL_VERTEX_ARRAY);
				glVertexPointer(3, GL_FLOAT, 0, aabb_verts);
				glDrawElements(GL_QUADS, 24, GL_UNSIGNED_INT, aabb_indices);
				glDisableClientState(GL_VERTEX_ARRAY);
			}
		}
	}
}

int cam;
float camVel = 0.0;

void **lights;
int numLights = 1;
float light0[] = {
	0.0, 100.0, 0.0, 1.0,
	1, 0.5, 0.5, 0,
	1, 1, 1, 1
};

int useWireframe = true;

Terrain *trn;

void logic(double scale)
{	
	float vel[3];
	float pos[3];

	// Get the camera's current position.
	dgCameraGetPosition(cam, pos);
	
	// Get the camera's forward vector.
	dgCameraGetForward(cam, vel);
	
	// Scale it by the speed.
	dgVectorScale(vel, scale * camVel);
	
	// Add to position.
	dgVectorAdd(pos, vel, pos);
	
	// Set the camera's new position.
	dgCameraSetPosition(cam, pos);
}

void input(double scale)
{
	const float camAccel = scale * 0.5;

	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_D) &&
	    dgWindowKeyModifier(DG_KEY_ALT))
	{
		dgMouseSetCaptured(false);
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_W))
	{
		camVel += camAccel;
	}
	
	if (dgWindowKeyPressed(DG_KEY_LOWER_S))
	{
		camVel -= camAccel;
	}
	
	if (dgWindowKeyPressedOnce(DG_KEY_QUOTESINGLE)) 
	{
		useWireframe = !useWireframe;
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_M)) 
	{
		mipmapLevel = mipmapLevel + 1 == trn->numMipmaps ? 0 : mipmapLevel + 1;
	}
	
	float rotvel = 0.006f * scale;
	float rx = 0, ry = 0;
	int dx, dy;
	
	dgMouseGetDelta(&dx, &dy);
	
	static float trx = 0;
	static float M_PI_H = M_PI / 2;
	
	// Remember that deltaX is really yaw,
	// and deltaY is pitch.
	rx = dy * rotvel;
	ry = dx * rotvel;
	
	// Oops, went over north pole.
	if (trx + rx >= M_PI_H)
	{
		rx = M_PI_H - trx;
	}
	
	// Oops, went over south pole.
	else if (trx + rx <= -M_PI_H)
	{
		rx = -M_PI_H - trx;
	}

	// Update total yaw.
	trx += rx;

	// Apply new rotation to the camera.
	dgCameraRotate(cam, -rx, ry, 0);
}

void display()
{
	

	// draw terrain
	_dgTerrainRender(trn);
}


void init()
{	
	//dgRandomSeed();

	// generate terrain
	trn = _dgTerrainNew(2, 20000, 1);
	
	_dgTerrainSetHeight(trn, 1, 1, 10);

	dgMouseShowCursor(false);
	dgMouseSetCaptured(true);
	dgMouseSetAutoCapture(true);
	
	// Create a camera and activate it.
       	cam = dgCameraNew();
	dgCameraSetActive(cam);
	
	// Set starting position.
	float initPos[] = { 
//		-71618.453125, 37784.222656, -45566.320312,
		0, 37784, 0,
		-0.462181, -0.075638, -0.871956, -0.142699		
	};
	dgCameraSetPosition(cam, initPos);
	dgCameraSetRotation(cam, initPos + 3);
	
	glEnable(GL_LIGHTING);
	
	// Create lights.
	dgMalloc(lights, void*, numLights);
	
	lights[0] = dgLightNew();
	dgLightSetfv(lights[0], DG_LIGHT_POSITION, light0);
	dgLightSetfv(lights[0], DG_LIGHT_DIFFUSE, light0 + 4);
	dgLightSetfv(lights[0], DG_LIGHT_SPECULAR, light0 + 8);
	
	// Configure lights.
	dgLightConfigure(lights, numLights);

	dgWindowDrawFPS(true);
}

void cleanup()
{
	_dgTerrainDelete(trn);

	// dump camera info for easy copy/paste
	float pos[3], rot[4];
	dgCameraGetPosition(cam, pos);
	dgCameraGetRotation(cam, rot);
	printf("\n%f, %f, %f,\n", pos[0], pos[1], pos[2]);
	printf("%f, %f, %f, %f\n\n", rot[0], rot[1], rot[2], rot[3]);

	// dealloc lights
	for (int i = 0; i < numLights; ++i) dgLightDelete(lights[i]);	
	dgFree(lights);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Terrain LOD2", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(logic);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
