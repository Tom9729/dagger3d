#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <dagger3d.h>

int cam;
void **lights;
int numLights = 12;
float t = 0.0f;
float angle = 180.0f;
int animate = 1;

float lightData[] = {
      -4.0, 0.0, -10.0, 1.0,
      1.0, 1.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      4.0, 0.0, -10.0, 1.0,
      0.0, 1.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      -4.0, 0.0, -6.0, 1.0,
      1.0, 0.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      4.0, 0.0, -6.0, 1.0,
      0.0, 0.0, 1.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      -4.0, 0.0, -2.0, 1.0,
      0.0, 1.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      4.0, 0.0, -2.0, 1.0,
      1.0, 1.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      -4.0, 0.0, 2.0, 1.0,
      0.0, 0.0, 1.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      4.0, 0.0, 2.0, 1.0,
      1.0, 0.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      -4.0, 0.0, 6.0, 1.0,
      1.0, 1.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      4.0, 0.0, 6.0, 1.0,
      0.0, 1.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      -4.0, 0.0, 10.0, 1.0,
      1.0, 0.0, 0.0, 1.0,
      0.5, 0.5, 0.5, 1.0,

      4.0, 0.0, 10.0, 1.0,
      0.0, 0.0, 1.0, 1.0,
      0.5, 0.5, 0.5, 1.0
};

// More or less a port of Mark Kilgard's multilight example.
// http://www.opengl.org/resources/code/samples/mjktips/VirtualizedLights/multilight.c

void logic()
{	
	if (animate)
	{
		// Animates big sphere.
		t += 0.003f;	
	}
}

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}
	
	if (dgWindowKeyPressedOnce(DG_KEY_SPACE))
	{
		animate = !animate;
	}

	if (dgMouseButtonIsDown(0))
	{
		int dx;
		
		dgMouseGetDelta(&dx, NULL);
		angle += dx;
	}
}

void display()
{	
	float objPos[3];
	float camPos[3];

	glTranslatef(0.0f, 0.0f, -18.0f);
	glRotatef(5, 1.0f, 0.0f, 0.0f);
	glRotatef(angle, 0.0, 1.0, 0.0);

	// For each light.
	for (int i = 0; i < numLights; ++i)
	{
		dgLightDraw(lights[i], 0.3);
	}

	// Position of big sphere.
	objPos[0] = cos(t * 12.3) * 2.0;
	objPos[1] = 0.0;
	objPos[2] = sin(t) * 7.0;

	camPos[0] = 16.0 * sin(angle * M_PI / 180.0);
	camPos[1] = 1.0;
	camPos[2] = 16.0 * -cos(angle * M_PI / 180.0);

	// Configure lights for rendering this object.
	//dgLightConfigure(objPos, camPos, 8);
	dgLightSortByIntensity(lights, numLights, objPos, camPos);
	dgLightConfigure(lights, 8);
	
	// Transition and draw the big sphere.
	glPushMatrix();
	glTranslatef(objPos[0], objPos[1], objPos[2]);	
	glutSolidSphere(1.5, 40, 40);
	glPopMatrix();
}

void init()
{
	// Initalize mouse and hide cursor.
	dgMouseEnabled(true);
	dgMouseShowCursor(false);
	
	dgMalloc(lights, void*, numLights);

	// Load some lights.
	for (int i = 0; i < numLights; ++i)
	{
		lights[i] = dgLightNew();
		dgLightSetfv(lights[i], DG_LIGHT_POSITION, &lightData[3 * 4 * i]);
		dgLightSetfv(lights[i], DG_LIGHT_DIFFUSE, &lightData[3 * 4 * i + 4]);
		dgLightSetfv(lights[i], DG_LIGHT_SPECULAR, &lightData[3 * 4 * i + 8]);
	}
	
	// Enable lighting and enter main loop.
	glEnable(GL_LIGHTING);
}

void cleanup()
{
	for (int i = 0; i < numLights; ++i)
	{
		dgLightDelete(lights[i]);
	}
	
	dgFree(lights);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Virtual Lights", 800, 600, 60, false);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(logic);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
