#include <dagger3d.h>

void a()
{
	printf("a\n");
}

void b()
{
	printf("b\n");
}

void c()
{
	printf("c\n");
}

void d()
{
	printf("d\n");
}

void e()
{
	printf("e\n");
}

void f()
{
	printf("f\n");
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);

	enum { A, B, C, D, E, F };
	
	int nodes[] = { A, B, C, D, E, F };
	void (*funcs[])() = { a, b, c, d, e, f };
	int deps[sizeof(nodes) / sizeof(int)][10] = {
		/* A */ { 1, B },
		/* B */ { 3, C,D,E },
		/* C */ { 3, E,F,D },
		/* D */ { 1, F },
		/* E */ { 1, D },
		/* F */ { 0 },
	};
	
	for (int i = 0; i < sizeof(nodes) / sizeof(int); ++i)
	{
		int node = nodes[i];
		dgSysRegister(node, funcs[node], 
			       deps[node][0], deps[node] + 1);
	}

	return 0;
}
