// Originally borrowed from the OpenAL tutorials on DevMaster.

#include <dagger3d.h>

// Maximum data buffers we will need.
#define NUM_BUFFERS 3
// buffers hold sound data.
int buffers[NUM_BUFFERS];
// Position of the listener.
float ListenerPos[] = { 0.0, 0.0, 0.0 };
// Velocity of the listener.
float ListenerVel[] = { 0.0, 0.0, 0.0 };
// Orientation of the listener. (first 3 elements are "at", second 3 are "up")
float ListenerOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
void *timers[NUM_BUFFERS];

int main(int argc, char *argv[])
{
	dgInit(argc, argv);
		
	// Load sound effects. Test caching by
	// trying to load several files multiple times.
	buffers[0] = dgResLoad(DG_AUDIO, "Battle.ogg", 0);
	buffers[1] = dgResLoad(DG_AUDIO, "Gun1.ogg", 0);
	buffers[2] = dgResLoad(DG_AUDIO, "Gun2.ogg", 0);
	
	// Create and initialize timers so that we can
	// playback sounds at different intervals.
	timers[0] = dgTimerNew(3.5);
	timers[1] = dgTimerNew(1.3);
	timers[2] = dgTimerNew(2.5);
	       
	dgAudioSetListener(ListenerPos, ListenerVel, ListenerOri);

	while (true)
	{
		for(int i = 0; i < NUM_BUFFERS; i++)
		{
			if (dgTimerReady(timers[i]))
			{
				// Pick a random position around the 
				// listener to play the source.
				double theta = (double) (rand() % 360) 
					* 3.14 / 180.0;
				float pos[] = {
					-(float)(cos(theta)),
					-(float)(rand()%2),
					-(float)(sin(theta))
				};
				float vel[] = { 0, 0, 0 };

				const int priority = (i == 0 ? 0 : 10);

				//printf("Playing %d\n", buffers[i]);
				dgAudioPlayNoHandle(buffers[i], priority, pos, vel);
			}
		}

		usleep(15);
	}
	
	return 0;
}
