#include <dagger3d.h>

int actor;
int instance;
float angle = 0;
int **shadows = NULL;
void **lights;
const int numLights = 3;
int drawShadowVol = false;

float objPos[] = { 0.0f, 0.0f, 0.0f };
float objRot[] = { 1.0f, 0.0f, 0.0f, 0.0f };
float zoom = 70.0f;

float light0[] = {
	30.0, 0.0, 30.0, 1.0,
	0.0, 1.0, 0.0, 1.0,
	0.5, 0.5, 0.5, 1.0
};

float light1[] = {
	-20.0, 0.0, 30.0, 1.0,
	0.7, 0.0, 0.0, 1.0,
	0.5, 0.5, 0.5, 1.0
};

float light2[] = {
	0.0, 0.0, -20.0, 1.0,
	1.0, 0.5, 0.0, 1.0,
	0.5, 0.5, 0.9, 1.0
};

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	if (dgMouseButtonIsDown(DG_BUTTON_LEFT))
	{
		int dx;
		
		dgMouseGetDelta(&dx, NULL);
		angle += dx;
	}
	
	float pitch = 0;
	float yaw = 0;
	const float rotSpeed = 0.1;

	if (dgWindowKeyPressed(DG_KEY_UP))
	{
		pitch -= rotSpeed;
	}

	if (dgWindowKeyPressed(DG_KEY_DOWN))
	{
		pitch += rotSpeed;
	}

	if (dgWindowKeyPressed(DG_KEY_RIGHT))
	{
		yaw += rotSpeed;
	}

	if (dgWindowKeyPressed(DG_KEY_LEFT))
	{
		yaw -= rotSpeed;
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_A))
	{
		zoom -= 0.6f;
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_Z))
	{
		zoom += 0.6f;
	}
	
	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_R))
	{
		drawShadowVol = !drawShadowVol;
	}

	float adj[4] = { pitch, 0, -yaw };
	
	dgQuaternionFromEuler(adj, adj);
	dgQuaternionMult(objRot, adj, objRot);
}

// FROM NeHe 27 with some modifications.
void drawRoom()
{
	float size = 100.0f;

	glBegin(GL_QUADS);
	// Floor
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-size,-size,-size);
	glVertex3f(-size,-size, size);
	glVertex3f( size,-size, size);
	glVertex3f( size,-size,-size);
	// Ceiling
	glNormal3f(0.0f,-1.0f, 0.0f);
	glVertex3f(-size, size, size);
	glVertex3f(-size, size,-size);
	glVertex3f( size, size,-size);
	glVertex3f( size, size, size);
	// Front Wall
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-size, size,-size);
	glVertex3f(-size,-size,-size);
	glVertex3f( size,-size,-size);
	glVertex3f( size, size,-size);
	// Back Wall
	glNormal3f(0.0f, 0.0f,-1.0f);
	glVertex3f( size, size, size);
	glVertex3f( size,-size, size);
	glVertex3f(-size,-size, size);
	glVertex3f(-size, size, size);
	// Left Wall
	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-size, size, size);
	glVertex3f(-size,-size, size);
	glVertex3f(-size,-size,-size);
	glVertex3f(-size, size,-size);
	// Right Wall
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glVertex3f( size, size,-size);
	glVertex3f( size,-size,-size);
	glVertex3f( size,-size, size);
	glVertex3f( size, size, size);
	glEnd();
}

void renderScene()
{
	drawRoom();
	dgActorInstanceRender(instance, objPos, objRot);
}

void display()
{
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -zoom);
	glRotatef(angle, 0.0, 1.0, 0.0);
		
	int meshes[] = { dgActorGetMesh(actor) };
	int frames[] = { 0 };
	float *objPos2[] = { objPos };
	float *objRot2[] = { objRot };
	
	dgShadowVolumeDrawLights(true);
	dgShadowVolumeDrawVolumes(drawShadowVol);
	dgShadowVolumeDoPass(lights, numLights,
			     meshes, 1, frames,
			     shadows, 
			     objPos2, objRot2,
			     &renderScene,
			     true);

	glPopMatrix();
}

void cleanup()
{
	dgPrintf("Running user cleanup.\n");

	for (int i = 0; i < numLights; ++i)
	{
		dgLightDelete(lights[i]);
		dgShadowVolumeDelete(shadows[i][0]);
		dgFree(shadows[i]);
	}
	
	dgFree(lights);
	dgFree(shadows);

	dgActorInstanceDelete(instance);
	dgActorDelete(actor);
}

int main(int argc, char **argv)
{	
	dgInit(argc, argv);
	dgWindowOpen("Shadows2", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetCleanupFunc(cleanup);
	
	// Enable and load lighting.
	dgMalloc(lights, void*, numLights);

	lights[0] = dgLightNew();
	dgLightSetfv(lights[0], DG_LIGHT_POSITION, light0);
	dgLightSetfv(lights[0], DG_LIGHT_DIFFUSE, light0 + 4);
	dgLightSetfv(lights[0], DG_LIGHT_SPECULAR, light0 + 8);

	lights[1] = dgLightNew();
	dgLightSetfv(lights[1], DG_LIGHT_POSITION, light1);
	dgLightSetfv(lights[1], DG_LIGHT_DIFFUSE, light1 + 4);
	dgLightSetfv(lights[1], DG_LIGHT_SPECULAR, light1 + 8);
	
	lights[2] = dgLightNew();
	dgLightSetfv(lights[2], DG_LIGHT_POSITION, light2);
	dgLightSetfv(lights[2], DG_LIGHT_DIFFUSE, light2 + 4);
	dgLightSetfv(lights[2], DG_LIGHT_SPECULAR, light2 + 8);

	dgMalloc(shadows, int*, numLights);
	
	for (int i = 0; i < numLights; ++i)
	{
		dgMalloc(shadows[i], int, 1);
		shadows[i][0] = dgShadowVolumeNew();
	}

	// Load resources.
	int mesh = dgResLoad(DG_MESH, "monkey.cdm", 0);
	int tex = dgResLoad(DG_TEXTURE, "tom.tga", 0);
	actor = dgActorNew();
	dgActorSetMesh(actor, mesh);
	dgActorSetTexture(actor, tex);

	// Create an instance of our actor and
	// set a default animation.
	instance = dgActorInstanceNew(actor);

	dgWindowDrawFPS(true);
	dgWindowMainLoop();

	return 0;
}
