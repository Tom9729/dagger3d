#include <dagger3d.h>

int skyboxTex[5];
float rot = 0;
int mesh, tex = -1;

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}
}

void display()
{       
	// Adjust the view (we could also use a camera to do this).
	glTranslatef(0.0f, 0.0f, -5.0f);

	dgSkyboxRender();

	glRotatef(rot, 0.0f, 1.0f, 0.0f);
	rot += 1;
	
	dgMeshRender(mesh, tex, 0);
}

char *mesh_fn, *tex_fn = NULL;

void init()
{
	mesh = dgResLoad(DG_MESH, mesh_fn, 0);
	if (tex_fn) tex = dgResLoad(DG_TEXTURE, tex_fn, 0);
	
	skyboxTex[0] = dgResLoad(DG_TEXTURE, "skybox/skyrender0001.png", 0);
	skyboxTex[1] = dgResLoad(DG_TEXTURE, "skybox/skyrender0002.png", 0);
	skyboxTex[2] = dgResLoad(DG_TEXTURE, "skybox/skyrender0004.png", 0);
	skyboxTex[3] = dgResLoad(DG_TEXTURE, "skybox/skyrender0005.png", 0);
	skyboxTex[4] = dgResLoad(DG_TEXTURE, "skybox/skyrender0003.png", 0);
	
	dgSkyboxSide(DG_SKYBOX_FRONT, skyboxTex[0]);
	dgSkyboxSide(DG_SKYBOX_LEFT, skyboxTex[1]);
	dgSkyboxSide(DG_SKYBOX_BACK, skyboxTex[2]);
	dgSkyboxSide(DG_SKYBOX_RIGHT, skyboxTex[3]);
	dgSkyboxSide(DG_SKYBOX_TOP, skyboxTex[4]);
	dgSkyboxSize(1000);
	
	dgWindowDrawFPS(true);
	dgMouseShowCursor(false);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
}

void cleanup()
{	
	dgCondFree(mesh_fn);
	dgCondFree(tex_fn);

	for (int i = 0; i < 5; ++i) 
	{
		dgResDelete(DG_TEXTURE, skyboxTex[i]);
	}

	dgResDelete(DG_MESH, mesh);
}

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: mesh_viewer <mesh> [<texture>]\n");
		exit(-1);
	}

	mesh_fn = dgStringDup(argv[1]);

	if (argc > 2)
	{
		tex_fn = dgStringDup(argv[2]);
	}

	dgInit(argc, argv);
	dgWindowOpen("Mesh Viewer", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
