#include <dagger3d.h>

int cam;
float camVel = 0.0;
int map;

void logic(double scale)
{	
	float vel[3];
	float pos[3];

	// Get the camera's current position.
	dgCameraGetPosition(cam, pos);
	
	// Get the camera's forward vector.
	dgCameraGetForward(cam, vel);
	
	// Scale it by the speed.
	dgVectorScale(vel, scale * camVel);
	
	// Add to position.
	dgVectorAdd(pos, vel, pos);
	
	// Set the camera's new position.
	dgCameraSetPosition(cam, pos);
}

void input(double scale)
{
	const float camAccel = scale * 0.5;

	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_D) &&
	    dgWindowKeyModifier(DG_KEY_ALT))
	{
		dgMouseSetCaptured(false);
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_W))
	{
		camVel += camAccel;
	}
	
	if (dgWindowKeyPressed(DG_KEY_LOWER_S))
	{
		camVel -= camAccel;
	}
	
	float rotvel = 0.006f * scale;
	float rx = 0, ry = 0;
	int dx, dy;
	
	dgMouseGetDelta(&dx, &dy);
	
	static float trx = 0;
	static float M_PI_H = M_PI / 2;
	
	// Remember that deltaX is really yaw,
	// and deltaY is pitch.
	rx = dy * rotvel;
	ry = dx * rotvel;
	
	// Oops, went over north pole.
	if (trx + rx >= M_PI_H)
	{
		rx = M_PI_H - trx;
	}
	
	// Oops, went over south pole.
	else if (trx + rx <= -M_PI_H)
	{
		rx = -M_PI_H - trx;
	}

	// Update total yaw.
	trx += rx;

	// Apply new rotation to the camera.
	dgCameraRotate(cam, -rx, ry, 0);
}

void display()
{

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); 
	dgBspMapRender(map);


	
}


void init()
{	
	map = dgBspMapLoad("coco.bsp");

	

	dgMouseShowCursor(false);
	dgMouseSetCaptured(true);
	dgMouseSetAutoCapture(true);
	
	// Create a camera and activate it.
       	cam = dgCameraNew();
	dgCameraSetActive(cam);
	
	// Set starting position.
	float initPos[] = { 
		-169.411194, 753.016113, -1655.871948,
		0.220566, 0.035610, 0.962266, 0.155358

	};
	dgCameraSetPosition(cam, initPos);
	dgCameraSetRotation(cam, initPos + 3);
	
	

	dgWindowDrawFPS(true);
}

void cleanup()
{
	dgBspMapDelete(map);

	// dump camera info for easy copy/paste
	float pos[3], rot[4];
	dgCameraGetPosition(cam, pos);
	dgCameraGetRotation(cam, rot);
	printf("\n%f, %f, %f,\n", pos[0], pos[1], pos[2]);
	printf("%f, %f, %f, %f\n\n", rot[0], rot[1], rot[2], rot[3]);

	
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("BSP Test 1", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(logic);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
