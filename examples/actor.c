#include <dagger3d.h>

int actor;
int instance;
float rot = 0;
int mesh, tex;

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}
}

void display()
{       
	// Adjust the view (we could also use a camera to do this).
	glTranslatef(0.0f, 0.0f, -100.0f);


	glRotatef(rot, 0.0f, 1.0f, 0.0f);
	glRotatef(-90, 1.0f, 0.0f, 0.0f);
	rot += 1;
	
	// Render actor, the NULL arguments for position and orientation
	// mean to use defaults (basically, render at the origin).
	dgActorInstanceRender(instance, NULL, NULL);	      
}

void init()
{
//	dgMeshRenderNormals(true);

	// Load resources.
	actor = dgActorNew();
	mesh = dgResLoad(DG_MESH, "knight2.cdm", 0);
	tex = dgResLoad(DG_TEXTURE, "knight.tga", 0);
	dgActorSetMesh(actor, mesh);
	dgActorSetTexture(actor, tex);
	
	// Configure mesh animations.
	dgMeshAddAnimation(mesh, "standing", 0, 39, 0.05, true);
	dgMeshAddAnimation(mesh, "running", 40, 45, 0.1, true);
	dgMeshAddAnimation(mesh, "attacking", 46, 52, 0.1, false);
	
	// Create an instance of our actor and
	// set a default animation.
	instance = dgActorInstanceNew(actor);
	dgActorInstanceSetAnimation(instance, "standing");
	
	dgWindowDrawFPS(true);
	dgMouseShowCursor(false);
}

void cleanup()
{	
	dgActorInstanceDelete(instance);
	dgActorDelete(actor);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Mesh", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
