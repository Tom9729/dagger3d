#include <dagger3d.h>

int camera;
int texture;

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}	
}

void display()
{
	dgDrawRect(-2, -2, 1, 2);
	dgDrawRectTextured(3, -2, 4, 3, texture);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Draw", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	
	// Position camera.
	float pos[] = { 0, 0, 20 };
	camera = dgWindowGetSystemCamera();
	dgCameraSetPosition(camera, pos);

	// Load texture.
	texture = dgTextureLoad("tom.tga");

	dgWindowMainLoop();

	return 0;
}
