#include <stdlib.h>
#include <stdio.h>
#include <dagger3d.h>

float ry = 0.0f;
int texture;

void logic()
{
	ry += 1.0f;
}

void input()
{
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}
}

void display()
{
	glTranslatef(0.0f, 0.0f, -3.0f);
	glRotatef(ry, 0.0f, 1.0f, 0.0f);
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, dgTextureGetRefGL(texture));
	glBegin(GL_QUADS);
	
	// Lower left.
	glTexCoord2d(0.0, 0.0);
	glVertex2d(-1.0, -1.0);

	// Upper left.
	glTexCoord2d(0.0, 1.0);
	glVertex2d(-1.0, 1.0);

	// Upper right.
	glTexCoord2d(1.0, 1.0);
	glVertex2d(1.0, 1.0);

	// Lower right.
	glTexCoord2d(1.0, 0.0);
	glVertex2d(1.0, -1.0);
	
	glEnd();
	glDisable(GL_TEXTURE_2D);
	
	logic();
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Texture", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	
	texture = dgTextureLoad("tom.tga");
	/*   texture = dgTextureLoad("tom.png"); */
	/* texture = dgTextureLoad("tom.jpg"); */

	glutMainLoop();

	return 0;
}
