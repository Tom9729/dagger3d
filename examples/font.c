#include <dagger3d.h>

// Font reference for later.
int font;

void input()
{
	if (dgKeyboardPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	if (dgKeyboardPressedOnce(DG_KEY_LOWER_M))
	{
		static int cursor = DG_MOUSE_DEFAULT;
		
		if (cursor == DG_MOUSE_DEFAULT)
		{
			dgMouseCursor(cursor = DG_MOUSE_WAIT);
		}
		
		else
		{
			dgMouseCursor(cursor = DG_MOUSE_DEFAULT);
		}
	}
}

void display()
{
	// You can use dgStringAppend() to print text to an empty string
	// (as opposed to "appending" text to an existing string), just be
	// sure to make it NULL so that Dagger knows it's empty.
       	char *msg = NULL, *timeOfDay;

	// Get the time as a string.
	timeOfDay = dgTimeOfDayStr();
	// Add the time and our text to the string we're gonna print
	// later. ^$ indicates that we want to use fancy-text (basically colors
	// and tabs). The syntax of this command is a lot like that of printf().
	dgStringAppend(&msg, "Hey, the time is now: %s\n"
		      "Also this is a huge paragraph of "
		      "text to test the word wrap. We are "
		      "also testing different colors.", timeOfDay);
	// We've copied timeOfDay's contents to our other string, which
	// means we don't need this version any more.
	dgFree(timeOfDay);

	// Change color to white and print our string.
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	dgFontPrint(font, 100.0f, 400.0f, 260, msg);
	dgFree(msg);
	
	// Again, set it to NULL before we use dgStringAppend() so that
	// Dagger knows it's an empty string.
	msg = NULL;
	dgStringAppend(&msg, "Here is some more text! "
		"Woot.");
	dgFontPrint(font, 200.0f, 100.0f, 220, msg);
	dgFree(msg);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Font", 640, 480, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);

	// Actually using the embedded font (included in libdagger),
	// but we could just as easily say
	font = dgFontGetSystemFont();
	//font = dgResLoad(DG_FONT, "res/fonts/DejaVuSans.ttf", 0);

	dgWindowMainLoop();

	return 0;
}
