#include <dagger3d.h>

int actor;
int instance;
float angle = 0;
int **shadows = NULL;
void **lights;
const int maxNumLights = 4;
int numLights = 1;
int drawShadowVol = false;
int grassTex;

float objSpeed = 0.5;
float objPos[] = { 0.0f, 0.0f, 0.0f };
float objRot[] = { 0.625010, 0.780617, 0, 0 };
float zoom = 180.0f;

float light0[] = {
	30.0, 30.0, 30.0, 1.0,
	0.9921, 0.9058, 0.5450, 1,
	0, 0, 0, 1,
	0.1064453125, 0.1123046875, 0.1337890625, 1
};

float light1[] = {
	60.0, 35.0, 90.0, 1.0,
	1, 1, 1, 1,
	1, 1, 1, 1
};

float light2[] = {
	-55.0, 130.0, 50.0, 1.0,
	1, 1, 1, 1,
	1, 1, 1, 1
};

float light3[] = {
	30, 150, 0, 1,
	1, 1, 1, 1,
	1, 1, 1, 1
};

void input(double scale)
{
	if (dgWindowKeyPressedOnce(DG_KEY_F5))
	{		
		dgWindowScreenDumpToFile("screenshot.tga");
	}
	
	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		printf("ROT\t%f\t%f\t%f\t%f\n",
		       objRot[0], objRot[1], objRot[2], objRot[3]);
		exit(0);
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_R))
	{
		drawShadowVol = !drawShadowVol;
	}
	
	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_L))
	{
		numLights += (numLights == maxNumLights ? -maxNumLights : 1); 
	}
	
	angle += 0.1;
	
	float pitch = 0;
	float yaw = 0;
	float rotSpeed = 0.1 * scale;
	
	if (dgWindowKeyPressed(DG_KEY_UP))
	{
		pitch -= rotSpeed;
	}
	
	if (dgWindowKeyPressed(DG_KEY_DOWN))
	{
		pitch += rotSpeed;
	}	

	if (dgWindowKeyPressed(DG_KEY_RIGHT))
	{
		yaw += rotSpeed;
	}
	
	if (dgWindowKeyPressed(DG_KEY_LEFT))
	{
		yaw -= rotSpeed;
	}
	
	// Update object rotation.
	float adj[4] = { pitch, 0, -yaw };		
	dgQuaternionFromEuler(adj, adj);
	dgQuaternionMult(objRot, adj, objRot);	
}

void update(double scale)
{
	const double s_objSpeed = objSpeed * scale;
	
	float vel[3] = { 0, -1, 0 };
	dgQuaternionRotate(objRot, vel, vel);
	dgVectorScale(vel, s_objSpeed);
	dgVectorAdd(vel, objPos, objPos);
}

void drawGround()
{
	const float size = 4000.0f;
	const float y = -60;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, dgTextureGetRefGL(grassTex));
	
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0, 0);
	glVertex3f(-size,y,-size);
	glTexCoord2f(1, 0);
	glVertex3f(-size,y, size);
	glTexCoord2f(1, 1);
	glVertex3f( size,y, size);
	glTexCoord2f(0, 1);
	glVertex3f( size,y,-size);
	glEnd();
}

void renderScene()
{	
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	

	// Render.
	drawGround();
	dgActorInstanceRender(instance, objPos, objRot);
	
	/* // Draw forward-direction of plane. */
	/* glPushAttrib(GL_ENABLE_BIT); */
	/* glDisable(GL_LIGHTING); */
	/* float dir[3] = { 0, -1, 0 }; */
	/* dgQuaternionRotate(objRot, dir, dir); */
	/* dgVectorScale(dir, 20); */
	/* glPushMatrix(); */
	/* glTranslatef(objPos[0], objPos[1], objPos[2]); */
	/* glBegin(GL_LINES); */
	/* glVertex3f(0, 0, 0); */
	/* glVertex3fv(dir); */
	/* glEnd(); */
	/* glPopMatrix(); */
	/* glPopAttrib(); */
}

void display()
{
	// Setup camera.
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -zoom);
	glRotatef(angle, 0.0, 1.0, 0.0);
	
	int meshes[] = { dgActorGetMesh(actor) };
	int frames[] = { 0 };
	float *objPos2[] = { objPos };
	float *objRot2[] = { objRot };
	
	dgSkyboxRender();
	
	dgShadowVolumeDrawLights(true);
	dgShadowVolumeDrawVolumes(drawShadowVol);
	dgShadowVolumeDoPass(lights, numLights,
			     meshes, 1, frames,
			     shadows, 
			     objPos2, objRot2,
			     &renderScene,
			     true);
	glPopMatrix();

	dgFontPrint(dgFontGetSystemFont(), 15, 75, false,
		    "Arrow keys to fly the plane\n"
		    "L to toggle the number of lights\n"
		    "R to toggle drawing the shadow volumes");
}

int skyboxTex[4];

void init()
{
	skyboxTex[0] = dgResLoad(DG_TEXTURE, "skybox/skyrender0001.png", 0);
	skyboxTex[1] = dgResLoad(DG_TEXTURE, "skybox/skyrender0002.png", 0);
	skyboxTex[2] = dgResLoad(DG_TEXTURE, "skybox/skyrender0004.png", 0);
	skyboxTex[3] = dgResLoad(DG_TEXTURE, "skybox/skyrender0005.png", 0);

	dgSkyboxSide(DG_SKYBOX_FRONT, skyboxTex[0]);
	dgSkyboxSide(DG_SKYBOX_LEFT, skyboxTex[1]);
	dgSkyboxSide(DG_SKYBOX_BACK, skyboxTex[2]);
	dgSkyboxSide(DG_SKYBOX_RIGHT, skyboxTex[3]);
	dgSkyboxSize(1500);

       	/* const float ambient[] = { 0.2, 0.2, 0.3, 1 }; */
	/* glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient); */

	// Create lights.
	dgMalloc(lights, void*, maxNumLights);
	
	lights[0] = dgLightNew();
	dgLightSetfv(lights[0], DG_LIGHT_POSITION, light0);
	dgLightSetfv(lights[0], DG_LIGHT_DIFFUSE, light0 + 4);
	dgLightSetfv(lights[0], DG_LIGHT_SPECULAR, light0 + 8);
	dgLightSetfv(lights[0], DG_LIGHT_AMBIENT, light0 + 4);

	lights[1] = dgLightNew();
	dgLightSetfv(lights[1], DG_LIGHT_POSITION, light1);
	dgLightSetfv(lights[1], DG_LIGHT_DIFFUSE, light1 + 4);
	dgLightSetfv(lights[1], DG_LIGHT_SPECULAR, light1 + 8);
	
	lights[2] = dgLightNew();
	dgLightSetfv(lights[2], DG_LIGHT_POSITION, light2);
	dgLightSetfv(lights[2], DG_LIGHT_DIFFUSE, light2 + 4);
	dgLightSetfv(lights[2], DG_LIGHT_SPECULAR, light2 + 8);

	lights[3] = dgLightNew();
	dgLightSetfv(lights[3], DG_LIGHT_POSITION, light3);
	dgLightSetfv(lights[3], DG_LIGHT_DIFFUSE, light3 + 4);
	dgLightSetfv(lights[3], DG_LIGHT_SPECULAR, light3 + 8);
	
	dgMalloc(shadows, int*, maxNumLights);
	
	for (int i = 0; i < maxNumLights; ++i)
	{
		dgMalloc(shadows[i], int, 1);
		shadows[i][0] = dgShadowVolumeNew();
	}
	
	// Load resources.
	int mesh = dgResLoad(DG_MESH, "jet.cdm", DG_MESH_VBO);
	int tex = dgResLoad(DG_TEXTURE, "jet1_tex.png", 0);
	grassTex = dgResLoad(DG_TEXTURE, "ground.jpg", 0);
//	grassTex = dgResLoad(DG_TEXTURE, "ice.jpg", 0);
       
	actor = dgActorNew();
	dgActorSetMesh(actor, mesh);
	dgActorSetTexture(actor, tex);

	// Create an instance of our actor and
	// set a default animation.
	instance = dgActorInstanceNew(actor);
	
	float fogColor[] = { 0.427, 0.423, 0.478, 1 };	
	glEnable(GL_FOG);
	glFogfv(GL_FOG_COLOR, fogColor);	
	glFogi(GL_FOG_MODE, GL_EXP2);
	glFogf(GL_FOG_DENSITY, 0.001f);

	dgWindowDrawFPS(true);
}

void cleanup()
{
//	printf("Running user cleanup.\n");
	
	for (int i = 0; i < 4; ++i) 
	{
		dgResDelete(DG_TEXTURE, skyboxTex[i]);
	}

	for (int i = 0; i < maxNumLights; ++i)
	{
		dgLightDelete(lights[i]);
		dgShadowVolumeDelete(shadows[i][0]);
		dgFree(shadows[i]);
	}
	
	dgFree(lights);
	dgFree(shadows);

	dgActorInstanceDelete(instance);
	dgActorDelete(actor);
}

int main(int argc, char **argv)
{	
	dgInit(argc, argv);
	dgWindowOpen("Shadows", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(update);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
