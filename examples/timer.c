#include <stdlib.h>
#include <dagger3d.h>

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	
	void *timer = dgTimerNew(5);
	
	// Sleep for 5 seconds.
	while (!dgTimerReady(timer)) {
		printf("%f\n", 5 - dgTimerRemaining(timer));
	}
	
	return EXIT_SUCCESS;
}
