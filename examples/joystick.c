#include <dagger3d.h>

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	
	while (1)
	{
		int *dev;
		int num_dev;
		
		num_dev = dgJoystickDetectDevices(&dev);
		
		printf("Found %d device(s).\n", num_dev);
		
		for (int i = 0; i < num_dev; ++i)
		{
			char name[80];
			int axes;
			int buttons;
			
			// Poll the device.
			if (dgJoystickPoll(dev[i]) == -1)
			{
				// Error, device seems to have
				// been disconnected. Skip it.
				continue;
			}
			
			dgJoystickName(dev[i], name);
			axes = dgJoystickNumAxes(dev[i]);
			buttons = dgJoystickNumButtons(dev[i]);

			char buttonState[buttons];
			int axesState[axes];

			// Read buttons/axes.
			dgJoystickButtons(dev[i], buttonState);
			dgJoystickAxes(dev[i], axesState);

			printf("device\n");
			printf(" ref: %d\n", dev[i]);
			printf(" name: %s\n", name);
			printf(" num axes: %d\n", axes);
			printf(" num buttons: %d\n", buttons);
			
			printf(" buttons:");
			for (int j = 0; j < buttons; ++j)
			{
				printf(" %d", buttonState[j]);
			}
			printf("\n");

			printf(" axes:");
			for (int j = 0; j < axes; ++j)
			{
				printf(" %d", axesState[j]);
			}
			printf("\n");
		}
		
		dgFree(dev);

		puts("");

		if (!num_dev)
		{
			break;
		}
	}

	return 0;
}
