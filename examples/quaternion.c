#include <stdlib.h>
#include <stdio.h>
#include <dagger3d.h>

float q[4];

void logic()
{

}

void input()
{
	// Z-axis.
	float az[4] = { 0.0f, 0.0f, 1.0f, 0.0f };
	// X-axis.
	float ax[4] = { 1.0f, 0.0f, 0.0f, 0.0f };
	// Y-axis.
	float ay[4] = { 0.0f, 1.0f, 0.0f, 0.0f };

	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	if (dgWindowKeyPressed(DG_KEY_LEFT))
	{
		az[3] = 0.1f;
	}

	if (dgWindowKeyPressed(DG_KEY_RIGHT))
	{
		az[3] = -0.1f;
	}
	
	if (dgWindowKeyPressed(DG_KEY_UP))
	{
		ax[3] = -0.1f;
	}

	if (dgWindowKeyPressed(DG_KEY_DOWN))
	{
		ax[3] = 0.1f;
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_Z))
	{
		ay[3] = 0.1f;
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_X))
	{
		ay[3] = -0.1f;
	}

	// Convert each axis/angle to a 
	// temporary quaternion.
	dgQuaternionFromAxisAngle(az, az);
	dgQuaternionFromAxisAngle(ay, ay);
	dgQuaternionFromAxisAngle(ax, ax);

	// Multiply the temporary quaternions with 'q'.
	dgQuaternionMult(q, ax, q);
	dgQuaternionMult(q, ay, q);
	dgQuaternionMult(q, az, q);
}

void display()
{
	float m[16];

	glTranslatef(0.0f, 0.0f, -10.0f);

	dgQuaternionToMatrix(q, m);
	glMultMatrixf(m);
	glutSolidCube(2);

	// Draw axes for the cube's local
	// coordinate system.
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);

	// Draw the X-axis.
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1000.0f, 0.0f, 0.0f);

	// Draw the Y-axis.
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1000.0f, 0.0f);

	// Draw the Z-axis.
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, -1000.0f);

	glEnd();
	glEnable(GL_LIGHTING);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Quaternions", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(logic);

	dgQuaternionIdentity(q);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glutMainLoop();
	
	return 0;
}
