#include <dagger3d.h>

int main(int argc, char **argv)
{
	int buf, pref;

	if (argc != 2)
	{
		printf("Usage: %s [FILE]\n", argv[0]);
		return 1;
	}
	
	dgInit(argc, argv);
	dgSysBinCwd(false);

	// Try to load the file from the command line arg.
	if ((buf = dgResLoad(DG_AUDIO, argv[1], 0)) == -1)
	{
		fprintf(stderr, "Error loading file.\n");
		return 1;
	}

	// Play the audio file.
	pref = dgAudioPlay(buf, 0, NULL, NULL);
	
	while (dgAudioGetState(pref) == DG_AUDIO_PLAYING ||
	       dgAudioGetState(pref) == DG_AUDIO_PAUSED);
	
	dgAudioEnd(pref);
	dgResDelete(DG_AUDIO, buf);
	
	return EXIT_SUCCESS;
}
