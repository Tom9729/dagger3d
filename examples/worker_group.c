/* #include <dagger3d.h> */

/* /\* Multiply together two nxn matrices.  */
/*    C = AB */
/*  *\/ */
/* const int n = 4; */
/* float *a, *b, *c; */

/* void dotProd(void *w) */
/* { */
/* 	int i = dgWorkerGetDataInt(w, 0); */
/* 	int j = dgWorkerGetDataInt(w, 1); */
	
/* 	for (int k = 0; k < n; ++k) */
/* 	{ */
/* 		c[dgIdx(i,j,n)] += a[dgIdx(i,k,n)] * b[dgIdx(k,j,n)]; */
/* 	} */
/* } */

/* void printMatrix(float *mat, int n) */
/* { */
/* 	for (int i = 0; i < (n * n); ++i) */
/* 	{ */
/* 		if (i % n == 0) printf("\n");		 */
/* 		printf("%.0f\t", mat[i]); */
/* 	} */
/* 	printf("\n"); */
/* } */

int main(int argc, char **argv)
{
	/* dgMalloc(a, float, n * n); */
	/* dgMalloc(b, float, n * n); */
	/* dgMalloc(c, float, n * n);	 */

	/* for (int i = 0; i < (n * n); ++i) */
	/* { */
	/* 	a[i] = (float) dgRandomInt(0, 100); */
	/* 	b[i] = (float) dgRandomInt(0, 100); */
	/* } */
		
	/* dgInit(argc, argv); */
	
	/* void *g = dgWorkerGroupNew(dotProd, 1); */
	
	/* for (int i = 0; i < n; ++i) */
	/* { */
	/* 	for (int j = 0; j < n; ++j) */
	/* 	{ */
	/* 		//dgWorkerGroupSetDataInt(g, 0, i); */
	/* 		//dgWorkerGroupSetDataInt(g, 1, j); */
	/* 		dgWorkerGroupPushData(g); */
	/* 	} */
	/* } */

	/* dgWorkerGroupStart(g); */
	
	/* // print results */
	/* printMatrix(a, n); */
	/* printMatrix(b, n); */
	/* printMatrix(c, n); */

	/* // cleanup */
	/* dgWorkerGroupDelete(g); */
	/* dgFree(a); */
	/* dgFree(b); */
	/* dgFree(c); */

	return 0;
}
