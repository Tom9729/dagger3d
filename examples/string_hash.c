#include <dagger3d.h>

void printHash(char *str)
{
	printf("%lu : %s\n", dgStringHash((unsigned char *) str), str);
}

int main(int argc, char ** argv)
{
	dgInit(argc, argv);
	
	printHash("llamaRama");
	printHash("cool pants");
	printHash("ExampleFile1.tga");
	printHash("ExampleFile2.tga");
	printHash("examplefile2.tga");

	return EXIT_SUCCESS;
}
