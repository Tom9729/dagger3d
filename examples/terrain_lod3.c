#include <dagger3d.h>

// XXX http://www.flipcode.com/archives/article_geomipmaps.pdf

/*
  
  (1-dimensional viewpoint)

  n verts
  q blocks
  w verts in a block
  
  naively => q * w
  
  each block shares it's right-most vertex with the next block (except the last)

  that's q-1 shared vertices

  so => n = q * w - (q - 1)
     => n = q * (w - 1) + 1
     
  also note
  
  # quads = (n - 1)^2
  # tris  = 2 * (n - 1)^2
  
  eg.
      q = 2
      w = 3
      
   => n = 2 * (3 - 1) + 1
        = 2 * 2 + 1
	= 5
  
  ---

  we want the ability to downsample (reduce res by 1/2) N-times so 
  w must be a power of 2
  
   => w = 2^k

  so our equation becomes

   => n = q * (2^k - 1) + 1

  ---
  
  we want to be able to provide a terrain 'size', the number of blocks 'q',
  and a resolution 'k' to a function, which will calculate the number of
  vertices (in 1-dimension) and the scale-distance between vertices (xzScale * [distance=1])

  we already have the eqn for n
  
  size is the number of edges times the length of an edge
  
   => size = (n - 1) * xzScale
   
  therefore
  
   => xzScale = size / (n - 1)
   
  f(size,q,k) => [n, xzScale]
                => [ n = q * (2^k - 1) + 1,
                     xzScale = (n - 1) / size ]
		     
  ---

  we want to be able to map coordinates <s,t> into the proper block, and the proper coordinates
  inside that block

  note: s,t in the range [0,n)
  
  f(x) => floor(x * q / n)

  note: what if x is on a boundary between two blocks?
  
   => if x is a multiple of the number of verts in a block ...
   => if x % (w - 1) == 0 ...
   ... 
   => then x is mapped to blocks f(x) and f(x) + 1
   
  obviously though if there is no neighboring block we don't care so
  
   => if x != 0 && x != (n - 1) && x % (w - 1) == 0
      then
          f(x) => [f(x), f(x) + 1]
      else
          f(x) => f(x)
  
  ---
  
  if we take the fractional part of f(x) and scale it by the number of
  verts in a block 'w', we can determine coordinates inside the block

  g(x) => ((x * q / n) - f(x)) * w
  
  so our block is [f(s), f(t)] and inside that block we are at coordinates [g(s), g(t)]
 */

typedef struct
{
	/** Two opposite points defining an axis-aligned bounding-box.
	    
 	          x-----x max
 	         /|    /|
	        / |   / |
	       x-----x  |
	       |  x--|--x
	       | /   | /
	       |/    |/
           min x-----x
       
	   A couple points
	   - Origin is the center of the AABB
	   - Left handed coordinate system (+X = right, +Y = up, +Z = forward)
	*/
	float min[3], max[3];
} AABB;


/*
  http://members.gamedev.net/EvilSteve/JournalStuff/Terrain03.png
  http://www.gamasutra.com/view/feature/1754/binary_triangle_trees_for_terrain_.php?print=1
  
  ---

  TerrainChunk - nxn heightmap using vertex indices and skipping vertices for lower LOD
  
  0 : 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16
  1 : 00    02    04    06    08    10    12    14    16
  2 : 00          04          08          12          16
  3 : 00                      08                      16
  4 : 00                                              16
  
  n = 2^k + 1
  
  k + 1 levels of detail
  
  These are stored in quilt-local coordinates for efficiency.
  
  Each LOD consists of a buffer of indices as detailed above (in one dimension).
  
  ---
  
  TerrainQuilt - virtual nxn heightmap stiching together multiple chunks

  When rendering chunks we must be aware of differences in LOD between two adjacent chunks.
  
  The lower resolution chunk must be "stitched" to the higher resolution chunk.
  
  
  
 */

typedef struct
{	
	/** bounding box containing this chunk */
	AABB aabb;
	/** 
	    parameters to terrain generation
	    
	    n = 2^k + 1	    
	    k + 1 levels of detail
	*/
	unsigned int n, k;	
	/** array of vertices in quilt-local coordinates */
	float *verts;	
} TerrainChunk;


/*
  TriangleBinTree - binary triangle tree
  http://www.gamasutra.com/view/feature/1754/binary_triangle_trees_for_terrain_.php?print=1
 */

typedef struct
{
	unsigned int indices[3];
	void *children[2];
	int level;
	enum {
		TRIANGLEBINTREE_INNER,
		TRIANGLEBINTREE_NORTH, 
		TRIANGLEBINTREE_SOUTH,
		TRIANGLEBINTREE_EAST, 
		TRIANGLEBINTREE_WEST
	} direction;
} TriangleBinTree;

TriangleBinTree *TriangleBinTreeNew(int direction, int n, int level, int a, int b, int c)
{
	int onBorder(int x)
	{
		const int n2 = pow(n, 2);
		return (x >= 0 && x < n) || // Top border.
		       (x >= n2 - n && x < n2) || // Bottom border.
		       (x % n == 0) || // Left border.
		       ((x + 1) % n == 0); // Right border.			
	}
	
	// If triangle does not have two indices on the border, it
	// is an inner triangle.
	if (direction != TRIANGLEBINTREE_INNER &&
	    !((onBorder(a) && onBorder(b)) ||
	      (onBorder(b) && onBorder(c)) ||
	      (onBorder(a) && onBorder(c))))
	{
		direction = TRIANGLEBINTREE_INNER;
	}
	
	const char *directionStr()
	{
		if (direction == TRIANGLEBINTREE_INNER) return "INNER";
		else if (direction == TRIANGLEBINTREE_NORTH) return "NORTH";
		else if (direction == TRIANGLEBINTREE_SOUTH) return "SOUTH";
		else if (direction == TRIANGLEBINTREE_EAST) return "EAST";
		else return "WEST";
	}
	dgPrintf("%d\t%s\t%d\t%d\t%d\n", level, directionStr(), a, b, c);

	// Inherits direction of parent.
	dgDeclMalloc(bt, TriangleBinTree, 1);
	bt->direction = direction;
	bt->level = level;
	
	// By convention, 0,1 are the hypotenuse and indices are CCW.
	bt->indices[0] = a; 
	bt->indices[1] = b;
	bt->indices[2] = c;
	
	// If this triangle can still be tesselated (if
	// any vertices are only 1 apart, then no).
	if (!(abs(a - b) == 1 || abs(a - c) == 1 || abs(b - c) == 1))
	{
		/* 
		   Split this triangle into two triangles.
		   
		   a
		   | \
		   |  \
		   d - c
		   |  /
		   | /
		   b
		   
		   Remember we must preserve these properties
		   - a,b are the hypotenuse
		   - CCW winding
		*/
		const int d = (a + b) / 2;
		bt->children[0] = TriangleBinTreeNew(direction, n, level + 1, c, a, d);
		bt->children[1] = TriangleBinTreeNew(direction, n, level + 1, b, c, d);
	}
	
	return bt;
}


int cam;
float camVel = 0.0;

void **lights;
int numLights = 1;
float light0[] = {
	0.0, 100.0, 0.0, 1.0,
	1, 0.5, 0.5, 0,
	1, 1, 1, 1
};

void logic(double scale)
{	
	float vel[3];
	float pos[3];

	// Get the camera's current position.
	dgCameraGetPosition(cam, pos);
	
	// Get the camera's forward vector.
	dgCameraGetForward(cam, vel);
	
	// Scale it by the speed.
	dgVectorScale(vel, scale * camVel);
	
	// Add to position.
	dgVectorAdd(pos, vel, pos);
	
	// Set the camera's new position.
	dgCameraSetPosition(cam, pos);
}

void input(double scale)
{
	const float camAccel = scale * 0.5;

	if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
	{
		exit(0);
	}

	if (dgWindowKeyPressedOnce(DG_KEY_LOWER_D) &&
	    dgWindowKeyModifier(DG_KEY_ALT))
	{
		dgMouseSetCaptured(false);
	}

	if (dgWindowKeyPressed(DG_KEY_LOWER_W))
	{
		camVel += camAccel;
	}
	
	if (dgWindowKeyPressed(DG_KEY_LOWER_S))
	{
		camVel -= camAccel;
	}
	
	float rotvel = 0.006f * scale;
	float rx = 0, ry = 0;
	int dx, dy;
	
	dgMouseGetDelta(&dx, &dy);
	
	static float trx = 0;
	static float M_PI_H = M_PI / 2;
	
	// Remember that deltaX is really yaw,
	// and deltaY is pitch.
	rx = dy * rotvel;
	ry = dx * rotvel;
	
	// Oops, went over north pole.
	if (trx + rx >= M_PI_H)
	{
		rx = M_PI_H - trx;
	}
	
	// Oops, went over south pole.
	else if (trx + rx <= -M_PI_H)
	{
		rx = -M_PI_H - trx;
	}

	// Update total yaw.
	trx += rx;

	// Apply new rotation to the camera.
	dgCameraRotate(cam, -rx, ry, 0);
}

void display()
{
	
}


void init()
{	
	TriangleBinTree *bt = TriangleBinTreeNew(TRIANGLEBINTREE_WEST, 5, 1, 0, 20, 12);
	/* TriangleBinTree *bt = TriangleBinTreeNew(TRIANGLEBINTREE_EAST, 5, 1, 24, 4, 12); */
	/* TriangleBinTree *bt = TriangleBinTreeNew(TRIANGLEBINTREE_NORTH, 5, 1, 4, 0, 12); */

	//dgRandomSeed();

	// generate terrain

	dgMouseShowCursor(false);
	dgMouseSetCaptured(true);
	dgMouseSetAutoCapture(true);
	
	// Create a camera and activate it.
       	cam = dgCameraNew();
	dgCameraSetActive(cam);
	
	// Set starting position.
	float initPos[] = { 
		0, 37784, 0,
		-0.462181, -0.075638, -0.871956, -0.142699		
	};
	dgCameraSetPosition(cam, initPos);
	dgCameraSetRotation(cam, initPos + 3);
	
	glEnable(GL_LIGHTING);
	
	// Create lights.
	dgMalloc(lights, void*, numLights);
	
	lights[0] = dgLightNew();
	dgLightSetfv(lights[0], DG_LIGHT_POSITION, light0);
	dgLightSetfv(lights[0], DG_LIGHT_DIFFUSE, light0 + 4);
	dgLightSetfv(lights[0], DG_LIGHT_SPECULAR, light0 + 8);
	
	// Configure lights.
	dgLightConfigure(lights, numLights);

	dgWindowDrawFPS(true);
}

void cleanup()
{
	// dump camera info for easy copy/paste
	float pos[3], rot[4];
	dgCameraGetPosition(cam, pos);
	dgCameraGetRotation(cam, rot);
	printf("\n%f, %f, %f,\n", pos[0], pos[1], pos[2]);
	printf("%f, %f, %f, %f\n\n", rot[0], rot[1], rot[2], rot[3]);

	// dealloc lights
	for (int i = 0; i < numLights; ++i) dgLightDelete(lights[i]);	
	dgFree(lights);
}

int main(int argc, char **argv)
{
	dgInit(argc, argv);
	dgWindowOpen("Terrain LOD3", 800, 600, 60, 0);
	dgWindowSetDisplayFunc(display);
	dgWindowSetInputFunc(input);
	dgWindowSetLogicFunc(logic);
	dgWindowSetInitFunc(init);
	dgWindowSetCleanupFunc(cleanup);
	dgWindowMainLoop();
	
	return 0;
}
