#include <stdlib.h>
#include <stdio.h>
#include <dagger3d.h>

int main(int argc, char **argv)
{
	char *timeOfDay;
	
	dgInit(argc, argv);
	timeOfDay = dgTimeOfDayStr();

	printf("The unix time is %f\n", dgTime());
	printf("The 24hr time is %s\n", timeOfDay);

	free(timeOfDay);

	return EXIT_SUCCESS;
}
