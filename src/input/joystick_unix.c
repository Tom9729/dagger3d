/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// @cond

#if defined(LINUX)

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <dagger3d.h>
#include <dagger3d/input/joystick.h>

/*
  In Linux joysticks show up as files. These are just
  guesses, different kernels may name the joysticks 
  differently.
  
  Linux:
  /dev/input/jsX
  /dev/jsX
  
  BSD:
  /dev/joyX

  XXX Support for more than 4 hardcoded devices.
*/

static char *deviceTable[] = {
	"/dev/input/js0",
	"/dev/input/js1",
	"/dev/input/js2",
	"/dev/input/js3",
	"/dev/js0",
	"/dev/js1",
	"/dev/js2",
	"/dev/js3",
	"/dev/joy0",
	"/dev/joy1",
	"/dev/joy2",
	"/dev/joy3",
};
static const int deviceTableSize = sizeof(deviceTable) / sizeof(char *);

void _dgJoystickDelete_unix(int device)
{
	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);

	close(dev->joyDesc);
}

int _dgJoystickDetectDevices_unix(int **devices)
{
	// Number of detected devices.
	int numDetected = 0;

	// For each device in the table.
	for (int i = 0; i < deviceTableSize; ++i)
	{
		// Try to open the device.
		int joy_fd = open(deviceTable[i], O_RDONLY);

		// If the device doesn't exist, skip it.
		if (joy_fd == -1)
		{
			continue;			
		}

		// Got a connection, create a new device.
		dgJoystick *dev = _dgJoystickGetPtr(_dgJoystickNew());

		// Copy over device file name.
		strncpy(dev->joyPath, deviceTable[i], 80);
		dev->joyPath[80 - 1] = 0;
		// Copy over device file descriptor.
		dev->joyDesc = joy_fd;
		// Read number of axes.
		ioctl(joy_fd, JSIOCGAXES, &dev->numAxes);
		// Read number of buttons.
		ioctl(joy_fd, JSIOCGBUTTONS, &dev->numButtons);
		// Read device name.
		ioctl(joy_fd, JSIOCGNAME(80), dev->joyName);
		dev->joyName[80 - 1] = 0;		
		// Enable non-blocking mode.
		fcntl(joy_fd, F_SETFL, O_NONBLOCK);

		// Go to next device.
		++numDetected;
		continue;		
	}
	
	return numDetected;
}

int _dgJoystickPoll_unix(int device)
{
	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);
	
	// If the device is not connected return error.
	if (!dev->connected)
	{
		return -1;
	}
	
	int pollCount = dev->numButtons + dev->numAxes;

	for (int i = 0; i < pollCount; ++i)
	{
		// Read the joystick state.
		int status = read(dev->joyDesc, &dev->joyEvent, sizeof(struct js_event));
		
		// Unable to read state, might not be any events waiting for us.
		if (status == -1)
		{
			break;
		}
		
		// Update buttons/axes depending on what kind of event this is.
		switch (dev->joyEvent.type & ~JS_EVENT_INIT)
		{
		case JS_EVENT_AXIS:
			dev->axes[dev->joyEvent.number] = dev->joyEvent.value;
			break;
		case JS_EVENT_BUTTON:
			dev->buttons[dev->joyEvent.number] = dev->joyEvent.value;
			break;
		}
	}
	
	return 0;
}

#endif

// @endcond
