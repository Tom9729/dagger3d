/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>
#include <dagger3d/input/keyboard.h>

typedef struct
{
	// True if this key is down, otherwise false.
	int state;
	int once;
} dgKeyState;

static const int numKeys = 119;
static dgKeyState *keyState = NULL;

static int modCtrl = false;
static int modAlt = false;
static int modShift = false;

static void _dgKeyboardNormDown(unsigned char k, int x, int y);
static void _dgKeyboardSpecDown(int kc, int x, int y);
static void _dgKeyboardNormUp(unsigned char k, int x, int y);
static void _dgKeyboardSpecUp(int kc, int x, int y);

void _dgKeyboardInit()
{
	dgMalloc(keyState, dgKeyState, numKeys);

	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(_dgKeyboardNormDown);
	glutKeyboardUpFunc(_dgKeyboardNormUp);
	glutSpecialFunc(_dgKeyboardSpecDown);
	glutSpecialUpFunc(_dgKeyboardSpecUp);	
}

void _dgKeyboardCleanup()
{
	dgFree(keyState);
	keyState = NULL;
}

/**
 * Check if a key modifier is active.
 *
 * Valid modifiers are:
 * DG_KEY_CTRL
 * DG_KEY_ALT
 * DG_KEY_SHIFT
 *
 * Some key combinations are intercepted
 * by the window system (for example, Ctrl + D
 * sends keycode 4 aka EOF).
 *
 * @param mod Key modifier to check for.
 * @return True if that modifier is active.
 */
int dgKeyboardModifier(int mod)
{
	switch (mod)
	{
	case DG_KEY_CTRL:
		return modCtrl;
	case DG_KEY_ALT:
		return modAlt;
	case DG_KEY_SHIFT:
		return modShift;
	default:
		return 0;
	}
}

/**
 * Check the state of one of the keys on the keyboard.
 *
 * This function will return true as long as the key is pressed down.
 *
 * @param key Code for the key (ex. DG_KEY_ESCAPE).
 * @return True if the key is down, otherwise false.
 */
int dgKeyboardPressed(int key)
{
	assert(keyState);
	assert(key >= 0 && key < numKeys);
	return (keyState[key].state && !keyState[key].once);
}

/**
 * Check the state of one of the keys on the keyboard.
 *
 * This function will return true only once if the key is pressed down.
 * After that it will return false until the key is released and then
 * repressed.
 *
 * @param key Code for the key (ex. DG_KEY_ESCAPE).
 * @return True if the key is down, otherwise false (except in the case above).
 */
int dgKeyboardPressedOnce(int key)
{
	int pressed;

	assert(keyState);
	assert(key >= 0 && key < numKeys);
	
	if ((pressed = dgKeyboardPressed(key)))
	{	
		keyState[key].once = true;
	}
	
	return pressed;
}

static void _dgKeyboardUpdateMods()
{
	int mod = glutGetModifiers();

	// Set modifiers.
	modCtrl = mod & GLUT_ACTIVE_CTRL;
	modAlt = mod & GLUT_ACTIVE_ALT;
	modShift = mod & GLUT_ACTIVE_SHIFT;
	
	/* printf("%d %d %d\n", modCtrl, modAlt, modShift); */
}

static void _dgKeyboardNormDown(unsigned char k, int x, int y)
{
	_dgKeyboardUpdateMods();

	switch (k)
	{
		/* Upper case letters. */
	case 65:
		keyState[DG_KEY_UPPER_A].state = 1;
		break;
	case 66:
		keyState[DG_KEY_UPPER_B].state = 1;
		break;
	case 67:
		keyState[DG_KEY_UPPER_C].state = 1;
		break;
	case 68:
		keyState[DG_KEY_UPPER_D].state = 1;
		break;
	case 69:
		keyState[DG_KEY_UPPER_E].state = 1;
		break;
	case 70:
		keyState[DG_KEY_UPPER_F].state = 1;
		break;
	case 71:
		keyState[DG_KEY_UPPER_G].state = 1;
		break;
	case 72:
		keyState[DG_KEY_UPPER_H].state = 1;
		break;
	case 73:
		keyState[DG_KEY_UPPER_I].state = 1;
		break;
	case 74:
		keyState[DG_KEY_UPPER_J].state = 1;
		break;
	case 75:
		keyState[DG_KEY_UPPER_K].state = 1;
		break;
	case 76:
		keyState[DG_KEY_UPPER_L].state = 1;
		break;
	case 77:
		keyState[DG_KEY_UPPER_M].state = 1;
		break;
	case 78:
		keyState[DG_KEY_UPPER_N].state = 1;
		break;
	case 79:
		keyState[DG_KEY_UPPER_O].state = 1;
		break;
	case 80:
		keyState[DG_KEY_UPPER_P].state = 1;
		break;
	case 81:
		keyState[DG_KEY_UPPER_Q].state = 1;
		break;
	case 82:
		keyState[DG_KEY_UPPER_R].state = 1;
		break;
	case 83:
		keyState[DG_KEY_UPPER_S].state = 1;
		break;
	case 84:
		keyState[DG_KEY_UPPER_T].state = 1;
		break;
	case 85:
		keyState[DG_KEY_UPPER_U].state = 1;
		break;
	case 86:
		keyState[DG_KEY_UPPER_V].state = 1;
		break;
	case 87:
		keyState[DG_KEY_UPPER_W].state = 1;
		break;
	case 88:
		keyState[DG_KEY_UPPER_X].state = 1;
		break;
	case 89:
		keyState[DG_KEY_UPPER_Y].state = 1;
		break;
	case 90:
		keyState[DG_KEY_UPPER_Z].state = 1;
		break;

		/* Lower case letters. */
	case 97:
		keyState[DG_KEY_LOWER_A].state = 1;
		break;
	case 98:
		keyState[DG_KEY_LOWER_B].state = 1;
		break;
	case 99:
		keyState[DG_KEY_LOWER_C].state = 1;
		break;
	case 100:
		keyState[DG_KEY_LOWER_D].state = 1;
		break;
	case 101:
		keyState[DG_KEY_LOWER_E].state = 1;
		break;
	case 102:
		keyState[DG_KEY_LOWER_F].state = 1;
		break;
	case 103:
		keyState[DG_KEY_LOWER_G].state = 1;
		break;
	case 104:
		keyState[DG_KEY_LOWER_H].state = 1;
		break;
	case 105:
		keyState[DG_KEY_LOWER_I].state = 1;
		break;
	case 106:
		keyState[DG_KEY_LOWER_J].state = 1;
		break;
	case 107:
		keyState[DG_KEY_LOWER_K].state = 1; 
		break;
	case 108:
		keyState[DG_KEY_LOWER_L].state = 1; 
		break;
	case 109:
		keyState[DG_KEY_LOWER_M].state = 1; 
		break;
	case 110:
		keyState[DG_KEY_LOWER_N].state = 1; 
		break;
	case 111:
		keyState[DG_KEY_LOWER_O].state = 1; 
		break;
	case 112:
		keyState[DG_KEY_LOWER_P].state = 1; 
		break;
	case 113:
		keyState[DG_KEY_LOWER_Q].state = 1; 
		break;
	case 114:
		keyState[DG_KEY_LOWER_R].state = 1; 
		break;
	case 115:
		keyState[DG_KEY_LOWER_S].state = 1; 
		break;
	case 116:
		keyState[DG_KEY_LOWER_T].state = 1; 
		break;
	case 117:
		keyState[DG_KEY_LOWER_U].state = 1; 
		break;
	case 118:
		keyState[DG_KEY_LOWER_V].state = 1; 
		break;
	case 119:
		keyState[DG_KEY_LOWER_W].state = 1; 
		break;
	case 120:
		keyState[DG_KEY_LOWER_X].state = 1; 
		break;
	case 121:
		keyState[DG_KEY_LOWER_Y].state = 1; 
		break;
	case 122:
		keyState[DG_KEY_LOWER_Z].state = 1; 
		break;

		/* Number keys (not the numpad). */
	case 48:
		keyState[DG_KEY_0].state = 1;
		break;
	case 49:
		keyState[DG_KEY_1].state = 1;
		break;
	case 50:
		keyState[DG_KEY_2].state = 1;
		break;
	case 51:
		keyState[DG_KEY_3].state = 1;
		break;
	case 52:
		keyState[DG_KEY_4].state = 1;
		break;
	case 53:
		keyState[DG_KEY_5].state = 1;
		break;
	case 54:
		keyState[DG_KEY_6].state = 1;
		break;
	case 55:
		keyState[DG_KEY_7].state = 1;
		break;
	case 56:
		keyState[DG_KEY_8].state = 1;
		break;
	case 57:
		keyState[DG_KEY_9].state = 1;
		break;
		
		/* Miscellaneous keys. */
	case 33:
		keyState[DG_KEY_EXCLAMATION].state = 1;
		break;	
	case 64:
		keyState[DG_KEY_ASPERAND].state = 1;
		break;
	case 35:
		keyState[DG_KEY_HASH].state = 1;
		break;
	case 36:
		keyState[DG_KEY_DOLLAR].state = 1;
		break;
	case 37:
		keyState[DG_KEY_PERCENT].state = 1;
		break;
	case 94:
		keyState[DG_KEY_CARET].state = 1;
		break;
	case 38:
		keyState[DG_KEY_AMPERSAND].state = 1;
		break;
	case 42:
		keyState[DG_KEY_STAR].state = 1;
		break;
	case 40:
		keyState[DG_KEY_PARENLEFT].state = 1;
		break;
	case 41:
		keyState[DG_KEY_PARENRIGHT].state = 1;
		break;
	case 45:
		keyState[DG_KEY_MINUS].state = 1;
		break;
	case 43:
		keyState[DG_KEY_PLUS].state = 1;
		break;
	case 95:
		keyState[DG_KEY_UNDERSCORE].state = 1;
		break;
	case 61:
		keyState[DG_KEY_EQUALS].state = 1;
		break;
	case 123:
		keyState[DG_KEY_BRACELEFT].state = 1;
		break;
	case 125:
		keyState[DG_KEY_BRACERIGHT].state = 1;
		break;
	case 91:
		keyState[DG_KEY_BRACKETLEFT].state = 1;
		break;
	case 93:
		keyState[DG_KEY_BRACKETRIGHT].state = 1;
		break;
	case 58:
		keyState[DG_KEY_COLON].state = 1;
		break;
	case 59:
		keyState[DG_KEY_SEMICOLON].state = 1;
		break;
	case 124:
		keyState[DG_KEY_PIPE].state = 1;
		break;
	case 47:
		keyState[DG_KEY_SLASHFORWARD].state = 1;
		break;
	case 92:
		keyState[DG_KEY_SLASHBACKWARD].state = 1;
		break;
	case 63:
		keyState[DG_KEY_QUESTION].state = 1;
		break;
	case 60:
		keyState[DG_KEY_ANGLELEFT].state = 1;
		break;
	case 62:
		keyState[DG_KEY_ANGLERIGHT].state = 1;
		break;
	case 34:
		keyState[DG_KEY_QUOTEDOUBLE].state = 1;
		break;
	case 96:
		keyState[DG_KEY_QUOTESINGLE].state = 1;
		break;
	case 27:
		keyState[DG_KEY_ESCAPE].state = 1;
		break;
	case 32:
		keyState[DG_KEY_SPACE].state = 1;
		break;
	case 9:
		keyState[DG_KEY_TAB].state = 1;
		break;
	case 13:
		keyState[DG_KEY_RETURN].state = 1;
		break;
	case 126:
		keyState[DG_KEY_TILDE].state = 1;
		break;
	case 39:
		keyState[DG_KEY_APOSTROPHE].state = 1;
		break;
	case 8:
		keyState[DG_KEY_BACKSPACE].state = 1;
		break;
	case 127:
		keyState[DG_KEY_DELETE].state = 1;
		break;
	}
}

static void _dgKeyboardSpecDown(int kc, int x, int y)
{
	_dgKeyboardUpdateMods();

	switch (kc)
	{
	case 101:
		keyState[DG_KEY_UP].state = 1; 
		break;
	case 103:
		keyState[DG_KEY_DOWN].state = 1; 
		break;
	case 100:
		keyState[DG_KEY_LEFT].state = 1; 
		break;
	case 102:
		keyState[DG_KEY_RIGHT].state = 1; 
		break;
	case 1:
		keyState[DG_KEY_F1].state = 1; 
		break;
	case 2:
		keyState[DG_KEY_F2].state = 1; 
		break;
	case 3:
		keyState[DG_KEY_F3].state = 1; 
		break;
	case 4:
		keyState[DG_KEY_F4].state = 1; 
		break;
	case 5:
		keyState[DG_KEY_F5].state = 1; 
		break;
	case 6:
		keyState[DG_KEY_F6].state = 1; 
		break;
	case 7:
		keyState[DG_KEY_F7].state = 1; 
		break;
	case 8:
		keyState[DG_KEY_F8].state = 1; 
		break;
	case 9:
		keyState[DG_KEY_F9].state = 1; 
		break;
	case 10:
		keyState[DG_KEY_F10].state = 1; 
		break;
	case 11:
		keyState[DG_KEY_F11].state = 1; 
		break;
	case 12:
		keyState[DG_KEY_F12].state = 1; 
		break;
	case 108:
		keyState[DG_KEY_INSERT].state = 1; 
		break;
	case 106:
		keyState[DG_KEY_HOME].state = 1; 
		break;
	case 107:
		keyState[DG_KEY_END].state = 1; 
		break;
	case 104:
		keyState[DG_KEY_PAGEUP].state = 1; 
		break;
	case 105:
		keyState[DG_KEY_PAGEDOWN].state = 1; 
		break;
	}
}

static void _dgKeyboardNormUp(unsigned char k, int x, int y)
{
	_dgKeyboardUpdateMods();

	switch (k)
	{
		/* Upper case letters. */
	case 65:
		memset(&keyState[DG_KEY_UPPER_A], 0, sizeof(dgKeyState));
		break;
	case 66:
		memset(&keyState[DG_KEY_UPPER_B], 0, sizeof(dgKeyState)); 
		break;
	case 67:
		memset(&keyState[DG_KEY_UPPER_C], 0, sizeof(dgKeyState)); 
		break;
	case 68:
		memset(&keyState[DG_KEY_UPPER_D], 0, sizeof(dgKeyState)); 
		break;
	case 69:
		memset(&keyState[DG_KEY_UPPER_E], 0, sizeof(dgKeyState)); 
		break;
	case 70:
		memset(&keyState[DG_KEY_UPPER_F], 0, sizeof(dgKeyState)); 
		break;
	case 71:
		memset(&keyState[DG_KEY_UPPER_G], 0, sizeof(dgKeyState)); 
		break;
	case 72:
		memset(&keyState[DG_KEY_UPPER_H], 0, sizeof(dgKeyState)); 
		break;
	case 73:
		memset(&keyState[DG_KEY_UPPER_I], 0, sizeof(dgKeyState)); 
		break;
	case 74:
		memset(&keyState[DG_KEY_UPPER_J], 0, sizeof(dgKeyState)); 
		break;
	case 75:
		memset(&keyState[DG_KEY_UPPER_K], 0, sizeof(dgKeyState)); 
		break;
	case 76:
		memset(&keyState[DG_KEY_UPPER_L], 0, sizeof(dgKeyState)); 
		break;
	case 77:
		memset(&keyState[DG_KEY_UPPER_M], 0, sizeof(dgKeyState)); 
		break;
	case 78:
		memset(&keyState[DG_KEY_UPPER_N], 0, sizeof(dgKeyState)); 
		break;
	case 79:
		memset(&keyState[DG_KEY_UPPER_O], 0, sizeof(dgKeyState)); 
		break;
	case 80:
		memset(&keyState[DG_KEY_UPPER_P], 0, sizeof(dgKeyState)); 
		break;
	case 81:
		memset(&keyState[DG_KEY_UPPER_Q], 0, sizeof(dgKeyState)); 
		break;
	case 82:
		memset(&keyState[DG_KEY_UPPER_R], 0, sizeof(dgKeyState)); 
		break;
	case 83:
		memset(&keyState[DG_KEY_UPPER_S], 0, sizeof(dgKeyState)); 
		break;
	case 84:
		memset(&keyState[DG_KEY_UPPER_T], 0, sizeof(dgKeyState));  
		break;
	case 85:
		memset(&keyState[DG_KEY_UPPER_U], 0, sizeof(dgKeyState));  
		break;
	case 86:
		memset(&keyState[DG_KEY_UPPER_V], 0, sizeof(dgKeyState));  
		break;
	case 87:
		memset(&keyState[DG_KEY_UPPER_W], 0, sizeof(dgKeyState));  
		break;
	case 88:
		memset(&keyState[DG_KEY_UPPER_X], 0, sizeof(dgKeyState));  
		break;
	case 89:
		memset(&keyState[DG_KEY_UPPER_Y], 0, sizeof(dgKeyState));  
		break;
	case 90:
		memset(&keyState[DG_KEY_UPPER_Z], 0, sizeof(dgKeyState));  
		break;

		/* Lower case letters. */
	case 97:
		memset(&keyState[DG_KEY_LOWER_A], 0, sizeof(dgKeyState));  
		break;
	case 98:
		memset(&keyState[DG_KEY_LOWER_B], 0, sizeof(dgKeyState));  
		break;
	case 99:
		memset(&keyState[DG_KEY_LOWER_C], 0, sizeof(dgKeyState));  
		break;
	case 100:
		memset(&keyState[DG_KEY_LOWER_D], 0, sizeof(dgKeyState));  
		break;
	case 101:
		memset(&keyState[DG_KEY_LOWER_E], 0, sizeof(dgKeyState));  
		break;
	case 102:
		memset(&keyState[DG_KEY_LOWER_F], 0, sizeof(dgKeyState));  
		break;
	case 103:
		memset(&keyState[DG_KEY_LOWER_G], 0, sizeof(dgKeyState));  
		break;
	case 104:
		memset(&keyState[DG_KEY_LOWER_H], 0, sizeof(dgKeyState));  
		break;
	case 105:
		memset(&keyState[DG_KEY_LOWER_I], 0, sizeof(dgKeyState));  
		break;
	case 106:
		memset(&keyState[DG_KEY_LOWER_J], 0, sizeof(dgKeyState));  
		break;
	case 107:
		memset(&keyState[DG_KEY_LOWER_K], 0, sizeof(dgKeyState));  
		break;
	case 108:
		memset(&keyState[DG_KEY_LOWER_L], 0, sizeof(dgKeyState));  
		break;
	case 109:
		memset(&keyState[DG_KEY_LOWER_M], 0, sizeof(dgKeyState));  
		break;
	case 110:
		memset(&keyState[DG_KEY_LOWER_N], 0, sizeof(dgKeyState));  
		break;
	case 111:
		memset(&keyState[DG_KEY_LOWER_O], 0, sizeof(dgKeyState));  
		break;
	case 112:
		memset(&keyState[DG_KEY_LOWER_P], 0, sizeof(dgKeyState));  
		break;
	case 113:
		memset(&keyState[DG_KEY_LOWER_Q], 0, sizeof(dgKeyState));  
		break;
	case 114:
		memset(&keyState[DG_KEY_LOWER_R], 0, sizeof(dgKeyState));  
		break;
	case 115:
		memset(&keyState[DG_KEY_LOWER_S], 0, sizeof(dgKeyState));  
		break;
	case 116:
		memset(&keyState[DG_KEY_LOWER_T], 0, sizeof(dgKeyState));  
		break;
	case 117:
		memset(&keyState[DG_KEY_LOWER_U], 0, sizeof(dgKeyState));  
		break;
	case 118:
		memset(&keyState[DG_KEY_LOWER_V], 0, sizeof(dgKeyState));  
		break;
	case 119:
		memset(&keyState[DG_KEY_LOWER_W], 0, sizeof(dgKeyState));  
		break;
	case 120:
		memset(&keyState[DG_KEY_LOWER_X], 0, sizeof(dgKeyState));  
		break;
	case 121:
		memset(&keyState[DG_KEY_LOWER_Y], 0, sizeof(dgKeyState));  
		break;
	case 122:
		memset(&keyState[DG_KEY_LOWER_Z], 0, sizeof(dgKeyState));  
		break;

		/* Number keys (not the numpad). */
	case 48:
		memset(&keyState[DG_KEY_0], 0, sizeof(dgKeyState));  
		break;
	case 49:
		memset(&keyState[DG_KEY_1], 0, sizeof(dgKeyState));  
		break;
	case 50:
		memset(&keyState[DG_KEY_2], 0, sizeof(dgKeyState));  
		break;
	case 51:
		memset(&keyState[DG_KEY_3], 0, sizeof(dgKeyState));  
		break;
	case 52:
		memset(&keyState[DG_KEY_4], 0, sizeof(dgKeyState));  
		break;
	case 53:
		memset(&keyState[DG_KEY_5], 0, sizeof(dgKeyState));  
		break;
	case 54:
		memset(&keyState[DG_KEY_6], 0, sizeof(dgKeyState));  
		break;
	case 55:
		memset(&keyState[DG_KEY_7], 0, sizeof(dgKeyState)); 
		break;
	case 56:
		memset(&keyState[DG_KEY_8], 0, sizeof(dgKeyState)); 
		break;
	case 57:
		memset(&keyState[DG_KEY_9], 0, sizeof(dgKeyState)); 
		break;
		
		/* Miscellaneous keys. */
	case 33:
		memset(&keyState[DG_KEY_EXCLAMATION], 0, sizeof(dgKeyState)); 
		break;	
	case 64:
		memset(&keyState[DG_KEY_ASPERAND], 0, sizeof(dgKeyState)); 
		break;
	case 35:
		memset(&keyState[DG_KEY_HASH], 0, sizeof(dgKeyState)); 
		break;
	case 36:
		memset(&keyState[DG_KEY_DOLLAR], 0, sizeof(dgKeyState)); 
		break;
	case 37:
		memset(&keyState[DG_KEY_PERCENT], 0, sizeof(dgKeyState)); 
		break;
	case 94:
		memset(&keyState[DG_KEY_CARET], 0, sizeof(dgKeyState)); 
		break;
	case 38:
		memset(&keyState[DG_KEY_AMPERSAND], 0, sizeof(dgKeyState)); 
		break;
	case 42:
		memset(&keyState[DG_KEY_STAR], 0, sizeof(dgKeyState)); 
		break;
	case 40:
		memset(&keyState[DG_KEY_PARENLEFT], 0, sizeof(dgKeyState)); 
		break;
	case 41:
		memset(&keyState[DG_KEY_PARENRIGHT], 0, sizeof(dgKeyState)); 
		break;
	case 45:
		memset(&keyState[DG_KEY_MINUS], 0, sizeof(dgKeyState)); 
		break;
	case 43:
		memset(&keyState[DG_KEY_PLUS], 0, sizeof(dgKeyState)); 
		break;
	case 95:
		memset(&keyState[DG_KEY_UNDERSCORE], 0, sizeof(dgKeyState)); 
		break;
	case 61:
		memset(&keyState[DG_KEY_EQUALS], 0, sizeof(dgKeyState)); 
		break;
	case 123:
		memset(&keyState[DG_KEY_BRACELEFT], 0, sizeof(dgKeyState)); 
		break;
	case 125:
		memset(&keyState[DG_KEY_BRACERIGHT], 0, sizeof(dgKeyState)); 
		break;
	case 91:
		memset(&keyState[DG_KEY_BRACKETLEFT], 0, sizeof(dgKeyState)); 
		break;
	case 93:
		memset(&keyState[DG_KEY_BRACKETRIGHT], 0, sizeof(dgKeyState)); 
		break;
	case 58:
		memset(&keyState[DG_KEY_COLON], 0, sizeof(dgKeyState)); 
		break;
	case 59:
		memset(&keyState[DG_KEY_SEMICOLON], 0, sizeof(dgKeyState)); 
		break;
	case 124:
		memset(&keyState[DG_KEY_PIPE], 0, sizeof(dgKeyState)); 
		break;
	case 47:
		memset(&keyState[DG_KEY_SLASHFORWARD], 0, sizeof(dgKeyState)); 
		break;
	case 92:
		memset(&keyState[DG_KEY_SLASHBACKWARD], 0, sizeof(dgKeyState)); 
		break;
	case 63:
		memset(&keyState[DG_KEY_QUESTION], 0, sizeof(dgKeyState)); 
		break;
	case 60:
		memset(&keyState[DG_KEY_ANGLELEFT], 0, sizeof(dgKeyState)); 
		break;
	case 62:
		memset(&keyState[DG_KEY_ANGLERIGHT], 0, sizeof(dgKeyState)); 
		break;
	case 34:
		memset(&keyState[DG_KEY_QUOTEDOUBLE], 0, sizeof(dgKeyState)); 
		break;
	case 96:
		memset(&keyState[DG_KEY_QUOTESINGLE], 0, sizeof(dgKeyState)); 
		break;
	case 27:
		memset(&keyState[DG_KEY_ESCAPE], 0, sizeof(dgKeyState)); 
		break;
	case 32:
		memset(&keyState[DG_KEY_SPACE], 0, sizeof(dgKeyState)); 
		break;
	case 9:
		memset(&keyState[DG_KEY_TAB], 0, sizeof(dgKeyState)); 
		break;
	case 13:
		memset(&keyState[DG_KEY_RETURN], 0, sizeof(dgKeyState)); 
		break;
	case 126:
		memset(&keyState[DG_KEY_TILDE], 0, sizeof(dgKeyState)); 
		break;
	case 39:
		memset(&keyState[DG_KEY_APOSTROPHE], 0, sizeof(dgKeyState)); 
		break;
	case 8:
		memset(&keyState[DG_KEY_BACKSPACE], 0, sizeof(dgKeyState)); 
		break;
	case 127:
		memset(&keyState[DG_KEY_DELETE], 0, sizeof(dgKeyState)); 
		break;
	}
}

static void _dgKeyboardSpecUp(int kc, int x, int y)
{
	_dgKeyboardUpdateMods();

	switch (kc)
	{
	case 101:
		memset(&keyState[DG_KEY_UP], 0, sizeof(dgKeyState));  
		break;
	case 103:
		memset(&keyState[DG_KEY_DOWN], 0, sizeof(dgKeyState));  
		break;
	case 100:
		memset(&keyState[DG_KEY_LEFT], 0, sizeof(dgKeyState));  
		break;
	case 102:
		memset(&keyState[DG_KEY_RIGHT], 0, sizeof(dgKeyState));  
		break;
	case 1:
		memset(&keyState[DG_KEY_F1], 0, sizeof(dgKeyState));  
		break;
	case 2:
		memset(&keyState[DG_KEY_F2], 0, sizeof(dgKeyState));  
		break;
	case 3:
		memset(&keyState[DG_KEY_F3], 0, sizeof(dgKeyState));  
		break;
	case 4:
		memset(&keyState[DG_KEY_F4], 0, sizeof(dgKeyState));  
		break;
	case 5:
		memset(&keyState[DG_KEY_F5], 0, sizeof(dgKeyState));  
		break;
	case 6:
		memset(&keyState[DG_KEY_F6], 0, sizeof(dgKeyState));  
		break;
	case 7:
		memset(&keyState[DG_KEY_F7], 0, sizeof(dgKeyState));  
		break;
	case 8:
		memset(&keyState[DG_KEY_F8], 0, sizeof(dgKeyState));  
		break;
	case 9:
		memset(&keyState[DG_KEY_F9], 0, sizeof(dgKeyState));  
		break;
	case 10:
		memset(&keyState[DG_KEY_F10], 0, sizeof(dgKeyState));  
		break;
	case 11:
		memset(&keyState[DG_KEY_F11], 0, sizeof(dgKeyState));  
		break;
	case 12:
		memset(&keyState[DG_KEY_F12], 0, sizeof(dgKeyState));  
		break;
	case 108:
		memset(&keyState[DG_KEY_INSERT], 0, sizeof(dgKeyState));  
		break;
	case 106:
		memset(&keyState[DG_KEY_HOME], 0, sizeof(dgKeyState));  
		break;
	case 107:
		memset(&keyState[DG_KEY_END], 0, sizeof(dgKeyState));  
		break;
	case 104:
		memset(&keyState[DG_KEY_PAGEUP], 0, sizeof(dgKeyState)); 
		break;
	case 105:
		memset(&keyState[DG_KEY_PAGEDOWN], 0, sizeof(dgKeyState)); 
		break;
	}
}
