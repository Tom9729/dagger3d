/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

#if defined(DARWIN)
#include <Carbon/Carbon.h>
#endif

static void dgMouseFunc(int button, int state, int x, int y);
static void dgMouseMotionFunc(int x, int y);
static void dgMousePassiveMotionFunc(int x, int y);

static int mouseAutoCapture = false;
static int mouseShowCursor = 0;
static int mouseEnabled = 0;
static int mouseX = 0, mouseY = 0;
static int mouseDeltaX = 0, mouseDeltaY = 0;
static int mouseButtons[3] = { 0, 0, 0 };
static int mouseCaptured = false;
static int mouseJustCaptured = false;
static const int mouseWidth = 4;
static const int mouseHeight = 12;
static int mouseCursor = DG_MOUSE_DEFAULT;

static int init = 0;

static void dgMouseInit()
{
	if (!init)
	{
		//printf("Initializing the mouse.\n");
				
		glutMouseFunc(&dgMouseFunc);
		glutMotionFunc(&dgMouseMotionFunc);
		glutPassiveMotionFunc(&dgMousePassiveMotionFunc);
		glutSetCursor(GLUT_CURSOR_NONE);

		init = 1;
	}
}

/**
 * Get the relative position of the mouse cursor
 * since the last time this function was called.
 * 
 * This function resets the current delta values to zero.
 *
 * @param x Pointer for x offset.
 * @param y Pointer for y offset.
 */
void dgMouseGetDelta(int *x, int *y)
{
	assert(init);
	
	if (x) *x = mouseDeltaX;
	if (y) *y = mouseDeltaY;

	mouseDeltaX = 0;
	mouseDeltaY = 0;
}

/**
 * Check if a specific mouse button is down.
 *
 * @param button Mouse button to check (0-2).
 * @return True if the mouse button is down.
 */
int dgMouseButtonIsDown(int button)
{
	assert(button >= 0);
	assert(button < 3);
	assert(init);

	return mouseButtons[button];
}

/**
 * Set whether the mouse is enabled or disabled.
 *
 * @param enabled True if the mouse should be enabled.
 */
void dgMouseEnabled(int enabled)
{
	dgMouseInit();
	glutSetCursor(GLUT_CURSOR_NONE);
	mouseEnabled = enabled;
	//dgMouseSetShowCursor(mouseShowCursor);
}
/**
 * Get the X coordinate of the mouse cursor.
 *
 * @return X coordinate.
 */
int dgMouseGetX()
{
	assert(init);
	return mouseX;
}

/**
 * Get the Y coordinate of the mouse cursor.
 *
 * @return Y coordinate.
 */
int dgMouseGetY()
{
	assert(init);
	return mouseY;
}

/**
 * Set the X coordinate of the mouse cursor.
 *
 * @param x X coordinate.
 */
void dgMouseSetX(int x)
{
	assert(init);
	mouseX = x;
}

/**
 * Set the Y coordinate of the mouse cursor.
 *
 * @param y Y coordinate.
 */
void dgMouseSetY(int y)
{
	assert(init);
	mouseY = y;
}

/**
 * Render the mouse cursor at it's current position,
 * if the mouse is enabled.
 */
void dgMouseRender()
{
	if (init && mouseEnabled && mouseShowCursor)
	{
		glPushAttrib(GL_COLOR_BUFFER_BIT);
		dgWindowCoordinateMode(DG_WINDOW_MODE_SCREEN);
		glPushMatrix();
		glLoadIdentity();

		glTranslatef(mouseX, mouseY, 0.0);
		
		if (mouseCursor == DG_MOUSE_DEFAULT)
		{
			glRotatef(35.0f, 0.0f, 0.0f, 1.0f);
		
			// Draw the mouse cursor.
			glColor4f(0.2f, 0.2f, 0.2f, 1.0f);
			glBegin(GL_TRIANGLES);
			glVertex2f(0.0f, 0.0f);
			glVertex2f(-mouseWidth, -mouseHeight);
			glVertex2f(mouseWidth, -mouseHeight);
			glEnd();
			
			// Draw the mouse cursor outline.
			glColor4f(0.7f, 0.7f, 1.0, 1.0f);
			glBegin(GL_LINE_STRIP);
			glVertex2f(0.0f, 0.0f);
			glVertex2f(-mouseWidth, -mouseHeight);
			glVertex2f(mouseWidth, -mouseHeight);
			glVertex2f(0.0f, 0.0f);
			glEnd();
		}

		else if (mouseCursor == DG_MOUSE_WAIT)
		{	      
			const float radius = (mouseWidth + mouseHeight) / 2;
			static float cursorWaitRot = 0;
			const float cursorWaitRotSpeed = 5;

			glTranslatef(mouseWidth / 2, -mouseHeight / 2, 0);

			// Outline.
			glColor4f(0.7f, 0.7f, 1.0, 1.0f);
			dgDrawArc(radius, 360, 128);
			
			glRotatef(cursorWaitRot, 0, 0, 1);
			cursorWaitRot += cursorWaitRotSpeed;
			
			// Inner dark quadrants.
			glColor4f(0.2f, 0.2f, 0.2f, 1.0f);
			dgDrawArc(radius, 90, 16);
			glRotatef(180, 0, 0, 1);
			dgDrawArc(radius, 90, 16);
		}
		
		glBegin(GL_TRIANGLES);
		glVertex2i(0, 0);
		glVertex2i(0, 0);
		glVertex2i(0, 0);
		glEnd();
		
		glPopMatrix();
		dgWindowCoordinateMode(DG_WINDOW_MODE_WORLD);
		glPopAttrib();
	}
}

static void dgMouseFunc(int button, int state, int x, int y)
{
	int pressed, bidx;

	/* if (state == GLUT_UP) */
	/* { */
	/* 	if (button == GLUT_WHEEL_UP) */
	/* 	{ */
	/* 		printf("Wheel Up\n"); */
	/* 	} */
		
	/* 	else if(button == GLUT_WHEEL_DOWN) */
	/* 	{ */
	/* 		printf("Wheel Down\n"); */
	/* 	} */
	/* } */

	pressed = (state == GLUT_DOWN) ? 1 : 0;
	
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		bidx = 0;
		break;
	case GLUT_MIDDLE_BUTTON:
		bidx = 1;
		break;
	case GLUT_RIGHT_BUTTON:
		bidx = 2;
		break;
	default:
		return;
	}

	mouseButtons[bidx] = pressed;
}

/**
 * Set whether the mouse is confined to this window.
 *
 * @param cap True if the mouse should be captured.
 */
void dgMouseSetCaptured(int cap)
{
	assert(init);
	mouseCaptured = cap;
}

/**
 * Check if the mouse is confined to this window.
 *
 * @return True if the mouse is captured.
 */
int dgMouseGetCaptured()
{
	assert(init);
	return mouseCaptured;
}

static void mouseAutoCaptureCallback(int state)
{
	// Capture the mouse if it has entered
	// the window.
	if (state == GLUT_ENTERED)
	{
		dgMouseSetCaptured(true);
		mouseJustCaptured = true;
	}
}

/**
 * Get whether the mouse should be automatically
 * captured when the window is given focus.
 *
 * @return True if it should.
 */
int dgMouseGetAutoCapture()
{
	assert(init);
	return mouseAutoCapture;
}

/**
 * Set whether the mouse should be automatically
 * captured when the window is given focus.
 *
 * @param enabled True if it should.
 */
void dgMouseSetAutoCapture(int enabled)
{
	assert(init);

	// Enable automatic capturing
	// of the mouse.
	if ((mouseAutoCapture = enabled))
	{
		glutEntryFunc(mouseAutoCaptureCallback);
	}

	// Disable automatic capturing.
	else
	{
		glutEntryFunc(NULL);
	}
}

static void dgMouseUpdatePosition(int x, int y)
{
        #if !defined(DARWIN)
	// State variable used to avoid a feedback loop.
	static int mouseUpdateDisabled = false;
	#endif
	
	// Get window dimensions.
	int width = dgWindowGetWidth();
	int height = dgWindowGetHeight();

	// Get real y.
	y = height - y;

	//printf("%d %d\n", mouseX, mouseY);

        
	if (mouseCaptured)
	{
                #if defined(DARWIN)

		int dx, dy;
		CGGetLastMouseDelta(&dx, &dy);
		
		mouseX += dx;
		mouseY -= dy;
		
		mouseDeltaX += dx;
		mouseDeltaY -= dy;
		
		CGPoint pos;
		pos.x = glutGet(GLUT_WINDOW_X) + (width / 2);
		pos.y = glutGet(GLUT_WINDOW_Y) + (height / 2);
		CGWarpMouseCursorPosition(pos);

		#else

		// Avoid glutWarpPointer() feedback loop.
		if (mouseUpdateDisabled)
		{
			mouseUpdateDisabled = false;
		}
		
		// Update virtual mouse position.
		else
		{		
			// Calculate the center of the window (approx).
			int centerX = width / 2;
			int centerY = height / 2;
			
			// Get mouse position relative to center
			// of the window.
			x -= centerX;
			y -= centerY;

			// Coincidentally this is also the mouse delta.
			mouseDeltaX += x;
			mouseDeltaY += y;
		
			// Relative position to virtual mouse position.
			mouseX += x;
			mouseY += y;
			
			// Warp pointer back to center.
			glutWarpPointer(centerX, centerY);
		
			// Next update will be from glutWarpPointer(),
			// ignore it or we will have a feedback loop.
			mouseUpdateDisabled = true;
		}

		#endif

		if (mouseJustCaptured)
		{
			mouseDeltaX = 0;
			mouseDeltaY = 0;
			mouseJustCaptured = false;
		}
	}

	// No mouse capture, meaning the mouse can leave the window
	// (but at least our job is much simpler).
	else
	{
		// Save old mouse coordinates.
		int lastX = mouseX, lastY = mouseY;

		// Set new mouse positions.
		mouseX = x;
		mouseY = y;		

		// Calculate mouse delta.
		mouseDeltaX += (mouseX - lastX);
		mouseDeltaY += (mouseY - lastY);
	}
	
	// Clip mouse position to window bounds.
	if (mouseX > width)
	{
		mouseX = width;
	}
	
	else if (mouseX < 0)
	{
		mouseX = 0;
	}
	
	if (mouseY > height)
	{
		mouseY = height;
	}
	
	else if (mouseY < 0)
	{
		mouseY = 0;
	}
}

static void dgMouseMotionFunc(int x, int y)
{	
	dgMouseUpdatePosition(x, y);
}

static void dgMousePassiveMotionFunc(int x, int y)
{	
	dgMouseUpdatePosition(x, y);
}

/**
 * Set whether or not the cursor should be displayed.
 *
 * @param show True if the cursor should be displayed.
 */
void dgMouseShowCursor(int show)
{
	assert(init);
	mouseShowCursor = show;
}

void dgMouseCursor(int cursor)
{
	assert(cursor == DG_MOUSE_DEFAULT ||
	       cursor == DG_MOUSE_WAIT);
	mouseCursor = cursor;
}
