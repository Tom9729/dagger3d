/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <fcntl.h>
#include <dagger3d.h>
#include <dagger3d/input/joystick.h>

/** List of enumerated joysticks. */
static void *joy_list = NULL;

static void dgJoystickCleanup()
{
	printf("Running joystick cleanup.\n");
	
	dgArrayListForEach(dgJoystick, j, joy_list, idx,
			   dgJoystickDelete(idx);
		);

	// Delete list of joysticks.
	dgArrayListDelete(joy_list);
	joy_list = NULL;

	#if defined(WIN32)
	_dgJoystickCleanup_win32();
	#endif
}

static void dgJoystickInit()
{
	if (joy_list == NULL)
	{
		printf("Initializing joystick system.\n");

		// Create a new list of joysticks.
		joy_list = dgArrayListNew();

		dgSysRegister(DG_SYS_JOYSTICK, dgJoystickCleanup, 0, NULL);
		
		#if defined(WIN32)
		_dgJoystickInit_win32();
		#endif
	}
}

// @cond

int _dgJoystickNew()
{
	assert(joy_list != NULL);

	// Allocate memory for a joystick device.
	dgJoystick *joy;
	dgMalloc(joy, dgJoystick, 1);

	// Add joystick to list and return a reference.
	return dgArrayListAddItem(joy_list, joy);
}

dgJoystick *_dgJoystickGetPtr(int joystick)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, joystick));
	
	return dgArrayListGetItem(joy_list, joystick);
}

// @endcond

/**
 * Delete a joystick device.
 *
 * @param[in] joystick Joystick reference.
 */
void dgJoystickDelete(int joystick)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, joystick));

	// Get device.
	dgJoystick *dev = _dgJoystickGetPtr(joystick);
	
	dgFree(dev->buttons);
	dgFree(dev->axes);

	#if defined(WIN32)
	_dgJoystickDelete_win32(joystick);
	#elif defined(LINUX)
	_dgJoystickDelete_unix(joystick);
	#endif

	dgFree(dev);
}

/**
 * Detect all connected joystick devices.
 *
 * @param[out] devices System allocated array of device references.
 * @return Number of devices detected.
 */
int dgJoystickDetectDevices(int **devices)
{
	dgJoystickInit();

	int numDetected = 0;

	// Delete all old devices.
	dgArrayListForEach(dgJoystick, dev, joy_list, idx,
			   dgJoystickDelete(idx);
			   dgArrayListDeleteItem(joy_list, idx);
		);

	#if defined(WIN32)
	numDetected = _dgJoystickDetectDevices_win32(devices);
	#elif defined(LINUX)
	numDetected =  _dgJoystickDetectDevices_unix(devices);
	#else
	numDetected = 0;
	#endif

	// Allocate memory to hold references for all the devices
	// we know about.
	dgMalloc(*devices, int, numDetected);

	// For each device in our list...
	int devIndex = 0;
	dgArrayListForEach(dgJoystick, dev, joy_list, idx,
			   // Save device ref for user.
			   (*devices)[devIndex++] = idx;

			   // Allocate memory for axes/buttons
			   dgMalloc(dev->axes, int, dev->numAxes);
			   dgMalloc(dev->buttons, char, dev->numButtons);

			   // Set device connected.
			   dev->connected = true;
		);

	return numDetected;
}

/**
 * Poll a joystick device to update it's state.
 *
 * @param[in] device Device to poll.
 * @return False if there is no problem, -1 if something went wrong.
 */
int dgJoystickPoll(int device)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, device));

	#if defined(WIN32)
	return _dgJoystickPoll_win32(device);
	#elif defined(LINUX)
	return _dgJoystickPoll_unix(device);
	#else
	return -1;
	#endif
}

/**
 * Query the number of buttons on a connected device.
 *
 * @param[in] device Device reference.
 * @return The number of buttons on the device.
 */
int dgJoystickNumButtons(int device)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, device));
	
	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);
	return dev->numButtons;
}

/**
 * Query the number of axes on a connected device.
 *
 * @param[in] device Device reference.
 * @return The number of axes on the device.
 */
int dgJoystickNumAxes(int device)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, device));

	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);
	return dev->numAxes;
}

/**
 * Get the status of all buttons on a device.
 *
 * @param[in] device Device reference.
 * @param[out] buttons Array of dgJoystickNumButtons()-many chars.
 */
void dgJoystickButtons(int device, char *buttons)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, device));
	assert(buttons);
	
	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);
	memcpy(buttons, dev->buttons, sizeof(char) * dev->numButtons);
}

/**
 * Get the status of all axes on a device.
 *
 * @param[in] device Device reference.
 * @param[out] axes Array of dgJoystickNumAxes()-many ints.
 */
void dgJoystickAxes(int device, int *axes)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, device));
	assert(axes);
	
	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);
	memcpy(axes, dev->axes, sizeof(int) * dev->numAxes);
}

/**
 * Get the name of a device.
 *
 * @param[in] device Device reference.
 * @param[out] name 80-char array to store name.
 */
void dgJoystickName(int device, char *name)
{
	assert(joy_list != NULL);
	assert(dgArrayListGetItem(joy_list, device));	
	assert(name);

	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);

	// Copy over device name.
	strncpy(name, dev->joyName, 80);
	name[79] = 0;
}

// @cond
void *_dgJoystickGetList()
{
	return joy_list;
}
// @endcond
