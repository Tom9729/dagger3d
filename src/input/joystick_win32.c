/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// @cond

#if defined(WIN32)

#include <dagger3d.h>
#include <dagger3d/input/joystick.h>

void _dgJoystickDelete_win32(int joystick)
{
	
}

void _dgJoystickInit_win32()
{
	
}

void _dgJoystickCleanup_win32()
{
	
}

// http://support.microsoft.com/kb/133065
int _dgJoystickDetectDevices_win32(int **devices)
{
	JOYINFOEX info;
	MMRESULT status;
	int id = 0;
	int detected = 0;

	info.dwSize = sizeof(info);

	// Guess joystick id's until we get an error, basically.
	while ((status = joyGetPosEx(id, &info)) != JOYERR_PARMS)
	{
		// No error, meaning we found a valid device.
		if (status == JOYERR_NOERROR)
		{
			// Get joystick capabilities.
			JOYCAPS caps;
			MMRESULT hr = joyGetDevCaps(id, &caps, sizeof(caps));

			switch (hr)
			{
			case JOYERR_NOERROR:
				break;
			case MMSYSERR_NODRIVER:
				fprintf(stderr, "Error: no joystick driver (apparently).\n");
				break;
			case JOYERR_PARMS:
			case MMSYSERR_INVALPARAM:
				fprintf(stderr, "Error: invalid parameter(s) passed to joyGetDevCaps().\n");
				break;
			default:
				fprintf(stderr, "Error: unknown error in _dgJoystickDetectDevices_win32().\n");
				break;
			}

			// Create a new joystick and add it to our list
			// of detected devices.
			dgJoystick *js = _dgJoystickGetPtr(_dgJoystickNew());
			// Save joystick descriptor/id.
			js->joyDesc = id;
			
			// Read number of buttons.
			js->numButtons = caps.wNumButtons;
			// Read number of axes.
			js->numAxes = caps.wNumAxes;
			// Read device name.
			strncpy(js->joyName, caps.szPname, 80);
			js->joyName[80-1] = 0;

			UINT axesMin[] = {
				caps.wXmin,
				caps.wYmin,
				caps.wZmax,
				caps.wRmax,
				caps.wUmax,
				caps.wVmax,
			};

			UINT axesMax[] = {
				caps.wXmax,
				caps.wYmax,
				caps.wZmin,
				caps.wRmin,
				caps.wUmin,
				caps.wVmin,
			};

			// Calculate and save axes centers.
			for (int i = 0; i < js->numAxes; ++i)
			{
				js->axesCenters[i] = (UINT) ((axesMax[i] + axesMin[i]) / 2.0);
			}

			++detected;
			++id;
		}
	}
	
	return detected;
}

int _dgJoystickPoll_win32(int device)
{
	// Get the device.
	dgJoystick *dev = _dgJoystickGetPtr(device);

	// Query it.
	JOYINFOEX info;
	info.dwSize = sizeof(info);
	info.dwFlags = JOY_RETURNALL;
	MMRESULT hr = joyGetPosEx(dev->joyDesc, &info);

	switch (hr)
	{
	case JOYERR_NOERROR:
		break;
	case MMSYSERR_NODRIVER:
		fprintf(stderr, "Error: no joystick driver (apparently).\n");
		return -1;
		break;
	case JOYERR_PARMS:
	case MMSYSERR_INVALPARAM:
		fprintf(stderr, "Error: invalid parameter(s) passed to joyGetPosEx().\n");
		return -1;
	case JOYERR_UNPLUGGED:
		fprintf(stderr, "Error: joystick unplugged.\n");
		return -1;
	default:
		fprintf(stderr, "Error: unknown error in _dgJoystickPoll_win32().\n");
		return -1;
	}

	// Query button statuses (the hard way).
	// Max supported is 32.
	for (int i = 0; i < dev->numButtons; ++i)
	{
		dev->buttons[i] = info.dwButtons & (1 << i);
	}

	// Put these in an array to make
	// it more convenient to access them.
	DWORD devAxes[] = { 
		info.dwXpos,
		info.dwYpos,
		info.dwZpos,
		info.dwRpos,
		info.dwUpos,
		info.dwVpos,
	};

	// Get axis statuses.
	// Max supported is 6.
	for (int i = 0; i < dev->numAxes; ++i)
	{
		dev->axes[i] = devAxes[i];

		// Center this axis on zero (MM axes are on a scale of 0-MAX).
		dev->axes[i] -= dev->axesCenters[i];
	}
	
	return 0;
}

#endif

// @endcond
