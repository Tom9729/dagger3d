/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

typedef struct
{
	GLuint id;
	int mode;
	char *fn;
	int w, h;
} dgTexture;

static void *list = NULL;

static void dgTextureCleanup()
{       
	//printf("Running texture cleanup.\n");

	// For each texture we have...
	dgArrayListForEach(dgTexture, tex, list, idx,
			   dgTextureDelete(idx);
		);

	dgArrayListDelete(list);
	list = NULL;
}

static void dgTextureInit()
{
	if (list == NULL)
	{
		//printf("Initializing texture system.\n");

		list = dgArrayListNew();
		
		int deps[] = { DG_SYS_WINDOW };
		dgSysRegister(DG_SYS_TEXTURE, dgTextureCleanup, sizeof(deps) / sizeof(int), deps);
	}
}

static int dgTextureNew()
{
	dgTextureInit();

	dgTexture *tex;
	dgMalloc(tex, dgTexture, 1);

	return dgArrayListAddItem(list, tex);
}

/**
 * Get a OpenGL texture reference from a Dagger texture.
 *
 * <b>Example:</b>
 * \code
 * int texture = dgTextureLoad("example.png"); 
 *
 * glBindgTexture(GL_TEXTURE_2D, dgTextureGetRefGL(texture));
 * \endcode
 *
 * @param ref Dagger texture reference.
 * @return OpenGL texture reference.
 */
GLuint dgTextureGetRefGL(int ref)
{
	assert(list != NULL);
	assert(dgArrayListItemExists(list, ref));
		
	return ((dgTexture *) dgArrayListGetItem(list, ref))->id;
}

/**
 * Delete a texture from memory.
 *
 * @param ref Dagger texture reference.
 * @param force If true, this texture is deleted immediately
 * even if there is still a chance that it is still being used.
 */
void dgTextureDelete(int ref)
{	
	assert(list != NULL);
	assert(dgArrayListItemExists(list, ref));
	
	dgTexture *tex = dgArrayListDeleteItem(list, ref);
	
	glDeleteTextures(1, &tex->id);
	
	dgCondFree(tex->fn);
	dgFree(tex);	
}

/**
 * Load a texture.
 *
 * <b>Supported Formats:</b><br>
 * <ul>
 * <li>TGA (non-RLE)</li>
 * <li>JPEG/JPG</li>
 * <li>PNG</li>
 * </ul>
 *
 * <b>Example:</b>
 * \code
 * int texture = dgTextureLoad("example.png");
 * \endcode
 *
 * @param fn Filename of the texture to load.
 * @return Dagger texture reference, or -1 if something went wrong.
 */
int dgTextureLoad(char *fn)
{
	dgTextureInit();

	assert(fn != NULL);

	dgTexture *tex = NULL;

	// Check the file extension first.
	char *ext = strrchr(fn, '.');
	int format;

	if (ext == NULL)
	{
		fprintf(stderr, "File has no extension, "
			"skipping it. (%s)\n", fn);
		return -1;
	}
		
	else if (!strcasecmp(ext, ".tga"))
	{
		format = DG_TEXTURE_TGA;
	}

	else if (!strcasecmp(ext, ".png"))
	{
		format = DG_TEXTURE_PNG;
	}

	else if (!strcasecmp(ext, ".jpg") || !strcasecmp(ext, ".jpeg")) 
	{
		format = DG_TEXTURE_JPG;
	}

	else
	{
		fprintf(stderr, "Unsupported file format! (%s)\n", 
			fn);
		return -1;
	}

	// Texture has not been loaded yet, so let's do this.
	int index = dgTextureNew();
	tex = dgArrayListGetItem(list, index);
	tex->fn = dgStringDup(fn);
	
	int status = 0;

	switch (format)
	{
	case DG_TEXTURE_TGA:
		status = dgTextureLoadTGA(fn, index);
		break;		
#ifndef NO_PNG		
	case DG_TEXTURE_PNG:
		status = dgTextureLoadPNG(fn, index);
		break;
#endif
	case DG_TEXTURE_JPG:
		status = dgTextureLoadJPG(fn, index);
		break;
	}

	// Problem loading texture, delete it and return unhappily.
	if (status)
	{
		dgTextureDelete(index);
		return -1;
	}
	
	// Home free if we got here (hopefully).
	return index;
}

/**
 * Load texture data into Dagger.
 *
 * This is meant to be used by Dagger internally.
 *
 * @param ref Dagger texture reference.
 * @param data Pixel data.
 * @param mode Components per pixel (ie. 3=RGB, 4=RGBA).
 * @param w Width of the texture in pixels.
 * @param h Height of the texture in pixels.
 * @param format Texture pixel format.
 */
void dgTextureData(int ref, unsigned char *data, int mode, int w, int h, int format)
{
	dgTexture *tex;
	
	assert(data != NULL);
	assert(w > 0);
	assert(h > 0);
	assert(list != NULL);
	assert(dgArrayListItemExists(list, ref));

	// Set the real format.
	switch (format)
	{
	case DG_TEXTURE_RGB:
		format = GL_RGB;
		break;
	case DG_TEXTURE_RGBA:
		format = GL_RGBA;
		break;
	case DG_TEXTURE_BGR:
		format = GL_BGR;
		break;
	case DG_TEXTURE_BGRA:
		format = GL_BGRA;
		break;
	case DG_TEXTURE_GREY:
		format = GL_LUMINANCE;
	}

	// If we don't have NPOT support then complain.
	// TODO Automatically resize the picture.
	if (!dgWindowNPOTSupported() && 
	    !(dgMathIsPowerOfTwo(w) && dgMathIsPowerOfTwo(h)))
	{
		fprintf(stderr,
			"Attempting to load non-"
			"power-of-two texture\n"
			" but you do not seem to have hardware support. Some \n"
			" textures may appear blank. To avoid this please use\n"
			" power-of-two textures.\n");
	}
	
	// Get the texture, and copy over w/h/mode.
	tex = dgArrayListGetItem(list, ref);
	tex->w = w;
	tex->h = h;
	tex->mode = mode;
	
	glPushAttrib(GL_ENABLE_BIT |
		     GL_TEXTURE_BIT);

	// Enable textures.
	glEnable(GL_TEXTURE_2D);       
	
	// Create a new texture and make it active.
	glGenTextures(1, &tex->id);
	glBindTexture(GL_TEXTURE_2D, tex->id);
  	/* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP); */
  	/* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// Enable anisotropic filtering and max it out.
	// http://developer.nvidia.com/object/Anisotropic_Filtering_OpenGL.html
	// TODO The AF level should be modifiable. All textures will
	// TODO have to be reloaded for the change to take effect however.
	#ifndef NO_GLEXT
	GLfloat maxAF;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAF);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAF);
	#endif

	// This generates mipmaps for us without GLU.
  	#ifdef WIN32
  	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
  	#else
  	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
  	#endif
	
	// Load the texture data into video memory.
  	glTexImage2D(GL_TEXTURE_2D, 0, mode, w, h, 0, format, GL_UNSIGNED_BYTE, data);
	
	glPopAttrib();
}
