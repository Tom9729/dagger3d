/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

#ifdef DG_BIG_ENDIAN
#include <dagger3d/compat/endian.h>
#endif

// XXX http://www.lighthouse3d.com/opengl/terrain/index.php3?tgalib
// XXX http://local.wasp.uwa.edu.au/~pbourke/dataformats/tga/

int dgTGASave(char *fn, unsigned char *data, int mode,
	      short int width, short int height, int format)
{
	assert(fn);
	assert(data);
	assert(width > 0);
	assert(height > 0);
	assert(format == DG_TEXTURE_GREY ||
	       format == DG_TEXTURE_BGR ||
	       format == DG_TEXTURE_BGRA);
	
	FILE *fh;
	const unsigned char type = (format == DG_TEXTURE_GREY ? 3 : 2);
	// Greyscale = 1 * 8 = 8
	// BGR       = 3 * 8 = 24
	// BGRA      = 4 * 8 = 32
	const unsigned char depth = mode * 8;
	
	if (!(fh = fopen(fn, "wb")))
	{
		fprintf(stderr, "Error: Could not open file. (%s)\n", fn);
		return -1;
	}

	#ifdef DG_BIG_ENDIAN
	width = letobe16(width);
	height = letobe16(height);
	#endif

	putc(0, fh);                              // ID field length
	putc(0, fh);                              // 0 = no colour map
	putc(type, fh);                           // 2 = RGB(A), 3 = greyscale
	putc(0, fh);                              // colour map origin
	putc(0, fh);
	putc(0, fh);                              // colour map length
	putc(0, fh);
	putc(0, fh);                              // colour map depth
	putc(0, fh);                              // X-origin = 0
	putc(0, fh);
	putc(0, fh);                              // Y-origin = 0
	putc(0, fh);
	fwrite(&width, sizeof(short int), 1, fh); // Width (pixels)
	fwrite(&height, sizeof(short int), 1, fh); // Height (pixels)
	putc(depth, fh);                          // Pixel depth (8, 24, or 32).
	putc(0, fh);                              // Image descriptor

	// Image data should already be BGR(A) so we can just
	// dump it to disk without doing any kind of conversion. :)
	fwrite(data, sizeof(unsigned char), width * height * mode, fh);
	fclose(fh);

	return 0;
}

/**
 * Load a TGA texture.
 *
 * @param fn Filename of the texture.
 * @param data Image data.
 * @param mode Bytes per pixel.
 * @param width Width of image in pixels.
 * @param height Height of image in pixels.
 * @param format Texture format (eg. RGBA).
 */
int dgTGALoad(char *fn, unsigned char **data, int *mode,
	      short int *width, short int *height, int *format)
{
	assert(fn);
	assert(data);
	assert(mode);
	assert(width);
	assert(height);
	assert(format);

	FILE* fh = NULL;
	int bytes = 0;
	unsigned char depth;
	char type;
	char idlen;
	
	// Problem opening file :-(
	if ((fh = fopen(fn, "rb")) == NULL)
	{
		fprintf(stderr, "Error: Could not open file. (%s)\n", fn);
		return -1;
	}
	
	// Length of image id field
	fseek(fh, 0, SEEK_SET);
	fread(&idlen, sizeof(char), 1, fh);

	// Image type
	fseek(fh, 2, SEEK_SET);
	fread(&type, sizeof(char), 1, fh);
	
	// 1 = colour mapped.
	// 2 = RGB(A) uncompressed.
	// 3 = greyscale uncompressed.
	// 9 = greyscale RLE.
	// 10 = RGB(A) RLE.
	if (type != 2 && 
	    type != 3)
	{
		fprintf(stderr, "Error: Unsupported TGA type. (type=%d, %s)\n", type, fn);
		return -1;
	}
	
	// Width
	fseek(fh, 12, SEEK_SET);
	fread(width, sizeof(short int), 1, fh);
	
	// Height
	fseek(fh, 14, SEEK_SET);
	fread(height, sizeof(short int), 1, fh);	

	#ifdef DG_BIG_ENDIAN
	*width = letobe16(*width);
	*height = letobe16(*height);
	#endif

	// Depth
	fseek(fh, 16, SEEK_SET);
	fread(&depth, sizeof(char), 1, fh);
	
	switch (depth)
	{
	case 8:
		*format = DG_TEXTURE_GREY;
		break;
	case 24:
		*format = DG_TEXTURE_BGR;
		break;
	case 32:
		*format = DG_TEXTURE_BGRA;
		break;
	default:
		fprintf(stderr, "Error: Unsupported TGA type! (type=%d, depth=%d, %s)\n", type, depth, fn);
		return -1;
	}

	/* printf("%d %d %d\n", *width, *height, depth); */

	// Mode = bytes per pixel.
	*mode = depth / 8;
	
	// Size = w * h * mode.
	bytes = (*width) * (*height) * (*mode);
	
	// Allocate memory for the image data.
	dgMalloc(*data, unsigned char, bytes);

	// Seek to the image data.
	fseek(fh, 18 + idlen, SEEK_SET);
	fread(*data, bytes, 1, fh);
	
	// Done reading, close the file.
	fclose(fh);
	
	return 0;
}

/**
* Load a TGA image.
*
* @param fn Filename of the image.
* @param ref Dagger texture reference.
*/
int dgTextureLoadTGA(char *fn, int ref)
{
	unsigned char *data = NULL;
	short int width, height;
	int format, mode;

	// Load TGA, return if something went wrong.
	if (dgTGALoad(fn, &data, &mode, &width, 
			      &height, &format))
	{
		return -1;
	}
	
	dgTextureData(ref, data, mode, (int) width, (int) height, format);
	dgFree(data);

	return 0;	
}
