/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <jpeglib.h>
#include <dagger3d.h>

/**
 * Load a JPEG image.
 *
 * This function is meant to be called by Dagger internally.
 * Use dgTextureLoad() instead of this.
 * 
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.korone.net/bbs/board.php?bo_table=etc_misc&wr_id=168</li>
 * <li>http://svn.ghostscript.com/ghostscript/tags/jpeg-6b/example.c</li>
 * </ul>
 *
 * @param fn Filename of the image.
 * @param ref Dagger texture reference.
 * @return Zero indicates success, non-zero indicates failure!
 */
int dgTextureLoadJPG(char *fn, int ref)
{
	FILE* fh = NULL;
	int w, h, mode, format;
	unsigned char *data, *data_ptr;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr error;

	// problem opening file :-(
	if ((fh = fopen(fn, "rb")) == NULL)
	{
		fprintf(stderr, "Error: Could not open file (%s).\n", fn);
		return -1;
	}

	// Set up libjpeg's error handling.
	cinfo.err = jpeg_std_error(&error);

	// Initialize the structures we'll need
	// to load a JPEG.
	jpeg_create_decompress(&cinfo);	
	jpeg_stdio_src(&cinfo, fh);
	jpeg_read_header(&cinfo, 1);
	
	jpeg_start_decompress(&cinfo);

	// Get image properties.
	w = cinfo.output_width;
	h = cinfo.output_height;
	mode = cinfo.output_components;

	// JPEG is always RGB as far as we are concerned.
	format = DG_TEXTURE_RGB;

	// Allocate memory to hold image data.
	data = malloc(sizeof(unsigned char) * w * h * mode);
	// Start at the end of data (minus one row).
	data_ptr = data + (w * h * mode) - (mode * cinfo.output_width);	

	// Read the data in backwards so it is oriented correctly.
	while(cinfo.output_scanline < cinfo.output_height)
	{
		jpeg_read_scanlines(&cinfo, &data_ptr, 1);
		data_ptr -= mode * cinfo.output_width;
	}
	
	// Clean up.
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	fclose(fh);
	
	dgTextureData(ref, data, mode, w, h, format);
	free(data);

	return 0;
}
