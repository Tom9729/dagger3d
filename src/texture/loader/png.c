/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

#ifndef NO_PNG
#include <png.h>

/**
 * Load a PNG image.
 *
 * <b>References:</b><br>
 * <ul> 
 * <li>http://zarb.org/~gc/html/libpng.html</li>
 * <li>http://en.wikibooks.org/wiki/OpenGL_Programming/Intermediate/Textures</li>
 * </ul>
 * 
 * @param fn Filename of the image.
 * @param ref Dagger texture reference.
 * @return 0 for success, otherwise -1.
 */
int dgTextureLoadPNG(char *fn, int ref)
{
	FILE *fh = NULL;
	int w, h, mode, rowbytes;
	png_structp png_ptr;
	png_infop info_ptr;
	png_byte *data;
	png_bytep *rowptrs;
	unsigned char header[8];
	int format;
	
	if ((fh = fopen(fn, "rb")) == NULL)
	{
		fprintf(stderr, "Error: could not open file (%s)\n", fn);
		return -1;
	}

	// Read in the header.
	fread(header, sizeof(char), 8, fh);

	if (png_sig_cmp(header, 0, 8))
	{
		fprintf(stderr, "Error: image does not appear to be a PNG.\n");
		return -1;
	}

	if (!(png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)))
	{
		fprintf(stderr, "Error: png_create_read_struct failed.\n");
		return -1;
	}
	

	if (!(info_ptr = png_create_info_struct(png_ptr)))
	{
		fprintf(stderr, "Error: png_create_info_struct failed.\n");
		return -1;
	}

	// Jump back here if png_init_io fails.
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fprintf(stderr, "Error: init_io failure while loading PNG.\n");
		return -1;
	}

	png_init_io(png_ptr, fh);
	png_set_sig_bytes(png_ptr, 8);
	png_read_info(png_ptr, info_ptr);

	w = png_get_image_width(png_ptr, info_ptr);
	h = png_get_image_height(png_ptr, info_ptr);

	switch(png_get_color_type(png_ptr, info_ptr))
	{
	case 0:
		mode = 1;
		break;
	case 2:
		mode = 3;
		break;
	case 6:
		mode = 4;
		break;
	default:
		fprintf(stderr, "Error: bad color type\n");
		return -1;
	}

	format = (mode == 3 ? DG_TEXTURE_RGB : DG_TEXTURE_RGBA);

	// Row size (bytes).
	rowbytes = png_get_rowbytes(png_ptr, info_ptr);

	// Allocate memory to hold image data.
	data = malloc(sizeof(png_byte) * rowbytes * h);
	rowptrs = malloc(sizeof(png_bytep) * h);

	// libpng requires that we have pointers to the beginning
	// of each row in the image.
	for (int i = 0; i < h; i++)
	{
		rowptrs[h - i - 1] = data + (i * rowbytes);
	}

	// Jump back here if png_read_image fails.
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fprintf(stderr, "Error: read_image failure while loading PNG.\n");
		return -1;
	}

	// Read image data in.
	png_read_image(png_ptr, rowptrs);
	fclose(fh);

	dgTextureData(ref, data, mode, w, h, format);

	// Clean up.
	free(rowptrs);
	free(data);
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	return 0;
}
#endif
