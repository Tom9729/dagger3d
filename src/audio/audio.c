/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

#ifdef DARWIN
#include <OpenAL/MacOSX_OALExtensions.h>
#endif

/*
  int buffer = dgResLoad(DG_RES_AUDIO, "music.ogg");
    
  int ref = dgAudioPlay(buffer, 0);

  while (dgAudioGetState(ref) == DG_AUDIO_PLAYING);

  dgAudioEnd(ref);
 */

/*
  Streaming

  int buf = dgResLoad(DG_RES_AUDIO, "foo.ogg");

  buf => dgAudioBuffer, which shall contain a list of fixed
  sized AL buffers.

  Audio loaders will be threaded providing buffers containing 
  (up to) BUFFER_SIZE bytes to Dagger (main thread), which will
  append them to the buffer list. 

  -------
 
  Ick, the problem with this is that is solves the loading-delay problem
  for audio but not the massive-memory-usage problem. We could easily
  kill off old buffers (especially since we don't allow restarting) but
  resources are supposed to be cached.

  int sbuf = dgResLoad(DG_RES_AUDIO_STREAM, "foo.ogg");

  Could create an uncached resource (loading through dgResLoad() just
  for consistency).

  -------

  Similar to dgAudioPlay()...

  dgAudioStream(char *fn, int priority, float *pos, float *vel);
 */

/*
  
  dgAudioPlay() can lead to either INTERRUPTED or PLAYING

  PLAYING can go to PAUSED
 
  Life of a playback source, in BNF :-)

  dgAudioPlay := INTERRUPTED | PLAYING
  PLAYING     := INTERRUPTED | PAUSED | FINISHED
  PAUSED      := INTERRUPTED | PLAYING | FINISHED
  
  INTERRUPTED and FINISHED are final states.

  INTERRUPTED indicates playback was abrubtly halted.

  FINISHED indicates that playback successfully finished.

  After a playback source reaches a final state,
  dgAudioEnd() should be called on it.
  
 */

typedef struct
{
	/*
	  0 : Uninterruptable
	  1 : Unlikely to be interrupted
	  .
	  .
	  .
	  n : Very likely to be interrupted

	  An audio source can be interrupted if there is a higher priority
	  request waiting for a source to become available.
	 */
	int priority;
	// AL source id.
	ALuint id;
	// Playback source using this.
	void *pbSource;
} dgAudioSource;

typedef struct
{
	// Filename this buffer was loaded from.
	char *fn;
	// AL buffer id.
	ALuint id;
	// List of playback sources using this.
	void *pbSourceList;
} dgAudioBuffer;

typedef struct
{
	// State of this playback source.
	int state;
	// Audio source.
	dgAudioSource *source;
	// Audio buffer.
	dgAudioBuffer *buffer;
	int pbSourceListRef;
	int deleteOnInterrupt;
} dgAudioPlayback;

// OpenAL supports a max of 16 simultaneous sources.
static const int alNumSources = 16;
static dgAudioSource sources[16];

// List of unused sources.
static void *freeSources = NULL;

// List of audio buffers.
static void *buffers = NULL;

// List of playback sources.
static void *pbSources = NULL;

// OpenAL device/context.
static ALCdevice *device;
static ALCcontext *context;

/**
 * Interrupting a playback source detaches it's buffer 
 * and source, and sets it's state to DG_AUDIO_INTERRUPTED.
 *
 * @param[in/out] pb Playback source.
 */
static void _dgAudioInterrupt(dgAudioPlayback *pb)
{
	assert(pb->state != DG_AUDIO_INTERRUPTED);
	assert(pb->source);
	assert(pb->buffer);

	// Stop audio if playing.
	if (pb->state == DG_AUDIO_PLAYING ||
	    pb->state == DG_AUDIO_PAUSED)
	{
		alSourceStop(pb->source->id);
	}

	// Detach buffer.
	alSourcei(pb->source->id, AL_BUFFER, 0);
	dgArrayListDeleteItem(pb->buffer->pbSourceList,
			      pb->pbSourceListRef);
	pb->buffer = NULL;

	// Detach source.
	pb->source->pbSource = NULL;
        dgArrayListAddItem(freeSources, pb->source);
	pb->source = NULL;
	
	// Set state to interrupted.
	pb->state = DG_AUDIO_INTERRUPTED;
	
	if (pb->deleteOnInterrupt)
	{
		dgFree(pb);
	}
}

static void _dgAudioDelete(dgAudioBuffer *buf)
{
	assert(buf);

	// Interrupt playback sources using this buffer.
	dgArrayListForEach(dgAudioPlayback, pb, buf->pbSourceList, idx,
			   
			   // Found a source using this buffer.
			   if (pb->buffer == buf)
			   {
				   _dgAudioInterrupt(pb);
			   }
		);
		
	// Delete list of playback sources.
	dgArrayListDelete(buf->pbSourceList);

	// Deallocate OpenAL buffer.
	alDeleteBuffers(1, &buf->id);
	
	// Delete the buffer itself.
	dgCondFree(buf->fn);
	dgFree(buf);
}

static void _dgAudioEnd(dgAudioPlayback *pb)
{
	// Interrupt playback if it hasn't already happened.
	if (pb->state != DG_AUDIO_INTERRUPTED)
	{
		_dgAudioInterrupt(pb);
	}

	// Delete self.
	dgFree(pb);
}

static void dgAudioCleanup()
{
	printf("Running audio cleanup.\n");
	
	dgArrayListForEach(dgAudioBuffer, buf, buffers, idx,
			   _dgAudioDelete(buf);
		);

	dgArrayListForEach(dgAudioPlayback, pb, pbSources, idx,
			   _dgAudioEnd(pb);
		);

	dgArrayListDelete(freeSources);
	dgArrayListDelete(buffers);
	dgArrayListDelete(pbSources);
	
	// Destroying the context will cause OpenAL to do
	// all of our cleanup for us.
	alcMakeContextCurrent(NULL);
	alcDestroyContext(context);
	alcCloseDevice(device);
}

static void dgAudioInit()
{
	if (!buffers)
	{
		printf("Initializing audio system.\n");
		
		buffers = dgArrayListNew();
		freeSources = dgArrayListNew();
		pbSources = dgArrayListNew();
		
		device = alcOpenDevice(NULL);
		context = alcCreateContext(device, NULL);
		alcMakeContextCurrent(context);
		alGetError();
		
		// XXX Enable HRTF only for headphones.
		// XXX GIT Openal-soft should support HRTF.
		#ifdef DARWIN
		alcMacOSXRenderingQualityProcPtr alcMacOSXRenderingQuality = 
			alcGetProcAddress(NULL, (const ALCchar*) "alcMacOSXRenderingQuality");		
		alcMacOSXRenderingQuality(ALC_MAC_OSX_SPATIAL_RENDERING_QUALITY_HIGH);
		#endif

		// Initialize all.
		for (int i = 0; i < alNumSources; ++i)
		{
			// Create the source.
			alGenSources(1, &sources[i].id);
			
			// Add source to free list.
			dgArrayListAddItem(freeSources, &sources[i]);
		}
		
		int deps[] = { };
		dgSysRegister(DG_SYS_AUDIO, dgAudioCleanup, 
			       sizeof(deps) / sizeof(int), deps);
	}
}


static int _dgAudioGetState(dgAudioPlayback *pb)
{
	if (pb->state != DG_AUDIO_INTERRUPTED)
	{
		ALint playing;
		alGetSourcei(pb->source->id, AL_SOURCE_STATE, &playing);
		
		switch (playing)
		{
		case AL_PLAYING:
			pb->state = DG_AUDIO_PLAYING;
			break;			
		case AL_PAUSED:
			pb->state = DG_AUDIO_PAUSED;
			break;
		case AL_STOPPED:
			pb->state = DG_AUDIO_FINISHED;
			break;
		}
	}
	
	return pb->state;	
}

/**
 * Attempt to get an AL source.
 *
 * 1. Get source from free-list if possible.
 * 2. Otherwise get source with highest priority
 *    value, and if our 'priority' is smaller
 *    then return that source.
 * 3. Otherwise return NULL.
 *
 * @param[in] priority Capped to [0,inf), 0 = uninterruptable,
 *                     lower is better.
 * @return The AL source or NULL if no source is available.
 */
static dgAudioSource *_dgAudioGetSource(int priority)
{
	assert(buffers);
	assert(priority >= 0);
	
	//printf("Free sources = %d\n", dgArrayListSize(freeSources));

	// Need to try to free a source for us to use.
	if (!dgArrayListSize(freeSources))
	{
		//puts("No free sources, need to interrupt one...");
		
		// Candidate source, we want to find a source
		// such that it's priority value is higher than
		// our's.
		dgAudioSource *csrc = NULL;
		
		// For each source...
		for (dgAudioSource *asrc = sources;
		     asrc != sources + (alNumSources - 1); ++asrc)
		{	
			// Update status of this source.
			_dgAudioGetState(asrc->pbSource);
	
			// Found a stopped source, we can go ahead
			// an interrupt this bastard.
			if (((dgAudioPlayback *) 
			     asrc->pbSource)->state == 
			    DG_AUDIO_FINISHED)
			{
				//printf("Interrupting source %p\n", asrc);
				_dgAudioInterrupt((dgAudioPlayback *)
						  asrc->pbSource);
			}
			
			else
			{
				csrc = (!csrc || asrc->priority > csrc->priority ?
					asrc : csrc);	
			}
		}
		
		// If our priority value is smaller than the largest
		// priority source, then we can preempt it.
		if (priority < csrc->priority)
		{							
			_dgAudioInterrupt((dgAudioPlayback *) 
					  csrc->pbSource);
		}
	}
	
	return dgArrayListPopItem(freeSources);
}

/**
 * Inquire upon the status of a playback source.
 *
 * DG_AUDIO_INTERRUPTED : Playback was abrubtly ended either
 *                        because someone else hijacked our
 *                        source or because the buffer being
 *                        used was deleted.
 * DG_AUDIO_PLAYING
 * DG_AUDIO_PAUSED
 * DG_AUDIO_STOPPED     : Playback ended because there is nothing
 *                        left to play.
 *
 * @param[in] pref Playback reference.
 * @return Status enum as defined above.
 */
int dgAudioGetState(int pref)
{
	assert(buffers);
	assert(dgArrayListItemExists(pbSources, pref));

	return _dgAudioGetState(dgArrayListGetItem(pbSources, pref));
}

/**
 * Toggle a playback source between the playing/paused states.
 *
 * @param[in] pref Playback reference.
 * @return -1 if the source is in a final state and thus cannot
 *         be played/paused.
 */
int dgAudioPausePlay(int pref)
{
	assert(buffers);
	assert(dgArrayListItemExists(pbSources, pref));

	dgAudioPlayback *pb = dgArrayListGetItem(pbSources, pref);

	// Cannot play/pause audio if it's in a final state.
	if (pb->state == DG_AUDIO_INTERRUPTED || 
	    pb->state == DG_AUDIO_FINISHED)
	{
		return -1;
	}

	if (pb->state == DG_AUDIO_PLAYING)
	{
		alSourcePause(pb->source->id);
		pb->state = DG_AUDIO_PAUSED;
	}

	else
	{
		alSourcePlay(pb->source->id);
		pb->state = DG_AUDIO_PLAYING;
	}

	return 0;
}

/**
 * Called to delete a playback source.
 *
 * If the source is not currently in the DG_AUDIO_INTERRUPTED
 * state then this function will first interrupt it.
 *
 * A call to this function should follow each call to dgAudioPlay(),
 * perhaps with some kind of monitoring loop in-between so that
 * the audio is given a chance to play before it is terminated.
 *
 * @param[in] pref Playback reference.
 */
void dgAudioEnd(int pref)
{
	assert(buffers);
	assert(dgArrayListItemExists(pbSources, pref));

	_dgAudioEnd(dgArrayListDeleteItem(pbSources, pref));	
}

/**
 * Load audio data into a buffer.
 *
 * @param[in] ref Buffer reference.
 * @param[in] data Raw audio data.
 * @param[in] bytes Length of audio data in bytes.
 * @param[in] format Audio data format enumeration.
 * @param[in] freq Frequency of audio data (in hertz).
 */
void dgAudioData(int bref, char *data, long bytes, int format, int freq)
{
	assert(buffers);
	assert(data);
	assert(dgArrayListItemExists(buffers, bref));
	assert(bytes > 0);
	assert(freq > 0);

	ALenum alFormat;
	
	switch (format)
	{
	case DG_AUDIO_MONO_8:
		alFormat = AL_FORMAT_MONO8;
		break;
	case DG_AUDIO_STEREO_8:
		alFormat = AL_FORMAT_STEREO8;
		break;
	case DG_AUDIO_MONO_16:
		alFormat = AL_FORMAT_MONO16;
		break;
	case DG_AUDIO_STEREO_16:	
		alFormat = AL_FORMAT_STEREO16;
		break;
	default:
		fprintf(stderr, "Error: Unknown internal audio format.\n");
		return;
	}
	
	alBufferData(((dgAudioBuffer *) 
		      dgArrayListGetItem(buffers, bref))->id, 
		     alFormat, data, bytes, freq);
}

/**
 * Play a sound (optionally with 3D effects).
 *
 * A lower priority means that this playback source is less
 * likely to be interrupted by another source. A playback
 * priority of zero indicates that this source is uninterruptable
 * although it can still be paused/stopped/etc via dgAudioEnd()
 * and dgAudioPausePlay().
 *
 * Distance attenuation : sounds playing far away will be softer
 * than sounds playing nearby.
 *
 * Doppler effect : sounds played from a rapidly moving source
 * will be distorted.
 *
 * The listener's position/velocity/direction must be updated
 * with alListener3f() or alListener3fv() by using AL_POSITION,
 * AL_VELOCITY, and AL_ORIENTATION appropriately.
 * 
 * Passing a NULL value to either the position or velocity parameters
 * will disable 3D effects for this playback source.
 *
 * Eg. To play a sound in a 2D game something like
 *     dgAudioPlay(buffer, priority, NULL, NULL);
 * 
 * @param[in] bref Buffer reference.
 * @param[in] priority Playback priority.
 * @param[in] pos Position vector {x,y,z} for distance attenuation.
 * @param[in] vel Velocity vector {x,y,z} for doppler effect.
 * @return Playback reference.
 */
int dgAudioPlay(int bref, int priority, float *pos, float *vel)
{
	assert(buffers);
	assert(dgArrayListItemExists(buffers, bref));
	assert(priority >= 0);
	
	// Create a new playback source.
	dgAudioPlayback * psrc;
	dgMalloc(psrc, dgAudioPlayback, 1);
	
	// If we were unsuccessful in obtaining a source.
	if (!(psrc->source = _dgAudioGetSource(priority)))
	{
		printf("Unable to get a source (bref=%d, priority=%d)\n",
		     bref, priority);
		psrc->state = DG_AUDIO_INTERRUPTED;
	}
	
	// We got a source, start playback.
	else
	{
		psrc->state = DG_AUDIO_PLAYING;
		psrc->source->pbSource = psrc;
		psrc->source->priority = priority;
		psrc->buffer = dgArrayListGetItem(buffers, bref);
		dgArrayListAddItem(psrc->buffer->pbSourceList, psrc);
		
		// Disable 3D effects.
		if (pos == NULL || vel == NULL)
		{
			alSourcei(psrc->source->id, AL_SOURCE_RELATIVE, 
				  AL_TRUE);
			alSourcef(psrc->source->id, AL_ROLLOFF_FACTOR, 0);
			alSource3f(psrc->source->id, AL_POSITION, 0, 0, 0);
			alSource3f(psrc->source->id, AL_VELOCITY, 0, 0, 0);
		}
		
		// Enable 3D effects.
		else
		{
			alSourcei(psrc->source->id, AL_SOURCE_RELATIVE, 
				  AL_FALSE);
			alSourcef(psrc->source->id, AL_ROLLOFF_FACTOR, 1);
			alSourcefv(psrc->source->id, AL_POSITION, pos);
			alSourcefv(psrc->source->id, AL_VELOCITY, vel);
		}

//		printf("%d\n", psrc->buffer->id);
		alSourcei(psrc->source->id, AL_BUFFER, psrc->buffer->id);
		alSourcePlay(psrc->source->id);
	}

	return dgArrayListAddItem(pbSources, psrc);
}

/**
 * This function is identical to dgAudioPlay() except that
 * it does not return a playback handle/reference. This means
 * that there is no way to monitor or control playback, but also
 * that there is no responsibility to call dgAudioEnd().
 * 
 * @param[in] bref Buffer reference.
 * @param[in] priority Playback priority.
 * @param[in] pos Position vector {x,y,z} for distance attenuation.
 * @param[in] vel Velocity vector {x,y,z} for doppler effect.
 */ 
void dgAudioPlayNoHandle(int bref, int priority,
				float *pos, float *vel)
{
	// Delete handle from list (leaving the object in-memor)
	dgAudioPlayback *pb = 	       
		dgArrayListDeleteItem(pbSources, 
				      dgAudioPlay(bref, priority, pos, vel));
	pb->deleteOnInterrupt = true;
}

/**
 * Create a new audio buffer and return a reference to it.
 *
 * @param[in] Optional filename.
 * @return Buffer reference.
 */
int dgAudioNew(char *fn)
{
	dgAudioInit();	
	dgDeclMalloc(buf, dgAudioBuffer, 1);
	
	// Copy filename.
	if (fn) buf->fn = dgStringDup(fn);
	
	// List of playback sources using this.
	buf->pbSourceList = dgArrayListNew();
	
	// Allocate AL buffer.
	alGenBuffers(1, &buf->id);
	
	return dgArrayListAddItem(buffers, buf);
}

/**
 * Delete an audio buffer. This will also interrupt any currently
 * playing sources using this buffer.
 *
 * @param[in] bref Buffer reference.
 */
void dgAudioDelete(int bref)
{
	assert(buffers);
	assert(dgArrayListItemExists(buffers, bref));

	_dgAudioDelete(dgArrayListDeleteItem(buffers, bref));	
}

/**
 * Load an audio file.
 *
 * @param[in] fn Name of file to load.
 * @return Buffer reference, or -1 indicating error.
 */
int dgAudioLoad(char *fn)
{
	assert(fn);

	dgAudioInit();

	// Check the file extension first.
	const char *ext = strrchr(fn, '.');
	
	if (!ext)
	{
		fprintf(stderr, "Error: File has no extension, "
			"skipping it. (%s)\n", fn);
		return -1;
	}
	
	const int bref = dgAudioNew(fn);
	int fail = true;

	if (!strcasecmp(ext, ".ogg")) 
	{
		fail = dgAudioLoadOGG(fn, bref);
	}
	
	else if (!strcasecmp(ext, ".wav")) 
	{
		fail = dgAudioLoadWAV(fn, bref);
	}

	else
	{
		fprintf(stderr, "Error: Unsupported file format! "
			"(%s)\n", fn);
	}

	if (fail)
	{
		fprintf(stderr, "Error: Loading audio file. (%s)\n", fn);
		dgAudioDelete(bref);
		return -1;
	}
	
	return bref;
}

/**
 * Update the position/velocity for a sound being played with 3D effects.
 *
 * @param[in] pref Playback reference.
 * @param[in,optional] pos {X,Y,Z} position of sound source.
 * @param[in,optional] vel {X,Y,Z} velocity of sound source.
 * @return -1 if the source is in a final state and thus cannot
 *         be updated.
 */
int dgAudioSetSource(int pref, float *pos, float *vel)
{
	assert(buffers);
	assert(dgArrayListItemExists(pbSources, pref));

	dgAudioPlayback *pb = dgArrayListGetItem(pbSources, pref);

	// Cannot play/pause audio if it's in a final state.
	if (pb->state == DG_AUDIO_INTERRUPTED || 
	    pb->state == DG_AUDIO_FINISHED)
	{
		return -1;
	}
	
	// http://osdir.com/ml/lib.openal.devel/2005-04/msg00029.html
	alcSuspendContext(context);
	if (pos) alSourcefv(pb->source->id, AL_POSITION, pos);
	if (vel) alSourcefv(pb->source->id, AL_VELOCITY, vel);
	alcProcessContext(context);

	return 0;
}

/**
 * Update the listener's position/velocity/orientation for 3D effects.
 *
 * @param[in,optional] pos {X,Y,Z} position of listener.
 * @param[in,optional] vel {X,Y,Z} position of listener.
 * @param[in,optional] ori {FX,FY,FZ,UX,UY,UZ} 'at' and 'up' vectors.
 */
void dgAudioSetListener(float *pos, float *vel, float *ori)
{
	dgAudioInit();
	
	// http://osdir.com/ml/lib.openal.devel/2005-04/msg00029.html
	alcSuspendContext(context);
	if (pos) alListenerfv(AL_POSITION, pos);
	if (vel) alListenerfv(AL_VELOCITY, vel);
	if (ori) alListenerfv(AL_ORIENTATION, ori);
	alcProcessContext(context);
}
