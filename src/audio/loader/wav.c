/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <dagger3d.h>

#ifdef DG_BIG_ENDIAN
#include <dagger3d/compat/endian.h>
#endif

/**
 * Load a WAVE audio file.
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://crownandcutlass.svn.sourceforge.net/viewvc/crownandcutlass/trunk/Protocce/src/soundutil.cpp?revision=914&view=markup</li>
 * <li>http://ccrma.stanford.edu/courses/422/projects/WaveFormat/</li>
 * </ul>
 *
 * @param fn The filename.
 * @param ref Dagger buffer reference.
 * @return Error status.
 */
int dgAudioLoadWAV(char *fn, int ref)
{
	int format;
	int freq;
	FILE *f;
	char *data;
	short int channels, bps;
	int bytes;

	if ((f = fopen(fn, "rb")) == NULL)
	{
		fprintf(stderr, "Error: Could not open file. (%s)\n", fn);
		return -1;
	}

	// Find out how much raw sound data there is (in bytes).
	// <raw data size> = <chunk size> - 36
	fseek(f, 4, SEEK_SET);
	fread(&bytes, sizeof(int), 1, f);

	#ifdef DG_BIG_ENDIAN
	bytes = letobe32(bytes);
	#endif

	bytes -= 36;
	
	// Allocate memory for the raw sound data.
	dgMalloc(data, char, bytes);

	// Read number of channels.
	fseek(f, 22, SEEK_SET);
	fread(&channels, sizeof(short int), 1, f);

	// Read frequency (sample rate).
	fseek(f, 24, SEEK_SET);
	fread(&freq, sizeof(int), 1, f);

	// Read bits per sample (BPS).
	fseek(f, 34, SEEK_SET);
	fread(&bps, sizeof(short int), 1, f);

	// Read the actual sound data.
	fseek(f, 44, SEEK_SET);
	fread(data, sizeof(char) * bytes, 1, f);
	
	#ifdef DG_BIG_ENDIAN
	freq = letobe32(freq);
	channels = letobe16(channels);
	bps = letobe16(bps);
	#endif

	if (channels == 1)
	{
		format = (bps == 8 ? DG_AUDIO_MONO_8 : DG_AUDIO_MONO_16);
	}
	
	else
	{
		format = (bps == 8 ? DG_AUDIO_STEREO_8 : DG_AUDIO_STEREO_16);
	}

	fclose(f);

	dgAudioData(ref, data, bytes, format, freq);
	dgFree(data);

	return 0;
}
