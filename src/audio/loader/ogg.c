/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <vorbis/vorbisfile.h>
#include <dagger3d.h>

/**
 * Attempt to load an OGG Vorbis audio file.
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.gamedev.net/reference/articles/article2031.asp</li>
 * </ul>
 * 
 * @param fn The filename.
 * @param ref Dagger audio buffer reference.
 * @return Error status.
 */
int dgAudioLoadOGG(char *fn, int ref)
{	
	OggVorbis_File oggFile;
	
	if (ov_fopen(fn, &oggFile))
	{
		fprintf(stderr, "Error: could not open file (%s)\n", fn);
		return -1;
	}
	
	vorbis_info *pInfo = ov_info(&oggFile, -1);
	ALenum format = (pInfo->channels == 1 ? DG_AUDIO_MONO_16 : DG_AUDIO_STEREO_16);
	ALsizei freq = pInfo->rate;

	/* printf("channels: %d\n" */
	/*        "rate: %ld\n" */
	/*        "upper bitrate: %ld\n" */
	/*        "nominal bitrate: %ld\n" */
	/*        "lower bitrate: %ld\n" */
	/*        "window bitrate: %ld\n", */
	/*        pInfo->channels, */
	/*        pInfo->rate, */
	/*        pInfo->bitrate_upper, */
	/*        pInfo->bitrate_nominal, */
	/*        pInfo->bitrate_lower, */
	/*        pInfo->bitrate_window); */
	
	const long bufferSizeInit = 1024 * 1024; // 1mb
	long bufferSize = bufferSizeInit;
	long bytesReadTotal = 0; // Total bytes read.	
	long bytesRead = 0;
	
	dgDeclMalloc(buffer, char, bufferSize);

	do 
	{
		int bitstream;
		
		#ifdef DG_BIG_ENDIAN
		const int isBigEndian = 1;
                #else
		const int isBigEndian = 0;
	        #endif
				
		// Read in a max of 'bufferSizeInit'.
		bytesRead = ov_read(&oggFile, buffer + bytesReadTotal,
				    bufferSizeInit, isBigEndian, 2, 1, &bitstream);
		
		// If we read something..
		if (bytesRead > 0)
		{
			bytesReadTotal += bytesRead;

			if ((bufferSize - bytesReadTotal) < bufferSizeInit )
			{
				bufferSize += bufferSizeInit;
				/* dgPrintf("growing buffer to %ld bytes\n", bufferSize); */
				dgRealloc(buffer, char, bufferSize);
			}
		}
		
	} while (bytesRead > 0);

	// Done reading, close the file.
	ov_clear(&oggFile);
		
	// Load the sound data.
	dgAudioData(ref, buffer, bytesReadTotal, format, freq);
	
	// Can free this now.
	dgFree(buffer);

	return 0;
}
