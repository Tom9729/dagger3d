/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/**
 * Check if a point falls within the bounds of a rectangle.
 *
 * @param point The point {x,y}.
 * @param rect The rectangle {x1,y1,x2,y2}, where (x1,y1) is
 * the upper left corner of the rectangle and (x2,y2) is the lower
 * right corner.
 * @return True if the point is within the rectangle.
 */
int dgGeometryPointInRectangle(float *point, float *rect)
{
	assert(point);
	assert(rect);

	return (point[0] > rect[0] &&
		point[0] < rect[2] &&
		point[1] > rect[3] &&
		point[1] < rect[1]);
}

/**
 * Calculate a plane from three points.
 *
 * Equation of a plane is Ax + By + Cz + D = 0, 
 * and the plane normal is {A,B,C}.
 *
 * <b>References:</b>
 * <ul>
 * <li>http://local.wasp.uwa.edu.au/~pbourke/geometry/planeeq/</li>
 * </ul>
 *
 * @param points The three {x,y,z} points.
 * @param plane The plane coefs {A,B,C,D}.
 */
void dgGeometryPlaneFromPoints(float *points, float *plane)
{
	assert(points);
	assert(plane);
	assert(points != plane);
	
	// @cond

	#define X1 points[0]
	#define Y1 points[1]
	#define Z1 points[2]
	#define X2 points[3]
	#define Y2 points[4]
	#define Z2 points[5]
	#define X3 points[6]
	#define Y3 points[7]
	#define Z3 points[8]

	#define A plane[0]
	#define B plane[1]
	#define C plane[2]
	#define D plane[3]

	A = Y1 * (Z2 - Z3) + Y2 * (Z3 - Z1) + Y3 * (Z1 - Z2);
	B = Z1 * (X2 - X3) + Z2 * (X3 - X1) + Z3 * (X1 - X2);
	C = X1 * (Y2 - Y3) + X2 * (Y3 - Y1) + X3 * (Y1 - Y2);
	D = -(X1 * (Y2 * Z3 - Y3 * Z2) + 
	      X2 * (Y3 * Z1 - Y1 * Z3) + 
	      X3 * (Y1 * Z2 - Y2 * Z1));

	#undef A
	#undef B
	#undef C
	#undef D

	#undef X1
	#undef Y1
	#undef Z1
	#undef X2
	#undef Y2
	#undef Z2
	#undef X3
	#undef Y3
	#undef Z3
	// @endcond
}

/**
 * Calculate a plane from three points.
 *
 * Equation of a plane is Ax + By + Cz + D = 0, 
 * and the plane normal is {A,B,C}.
 *
 * <b>References:</b>
 * <ul>
 * <li>http://local.wasp.uwa.edu.au/~pbourke/geometry/planeeq/</li>
 * </ul>
 *
 * @param u First vertex.
 * @param v Second vertex.
 * @param w Third vertex.
 * @param plane The plane coefs {A,B,C,D}.
 */
void dgGeometryPlaneFromPoints2(float *u, float *v, float *w, float *plane)
{
	assert(u);
	assert(v);
	assert(w);
	assert(plane);
	assert(u != plane && v != plane && w != plane);
	
	// @cond

	#define X1 u[0]
	#define Y1 u[1]
	#define Z1 u[2]
	#define X2 v[0]
	#define Y2 v[1]
	#define Z2 v[2]
	#define X3 w[0]
	#define Y3 w[1]
	#define Z3 w[2]

	#define A plane[0]
	#define B plane[1]
	#define C plane[2]
	#define D plane[3]

	A = Y1 * (Z2 - Z3) + Y2 * (Z3 - Z1) + Y3 * (Z1 - Z2);
	B = Z1 * (X2 - X3) + Z2 * (X3 - X1) + Z3 * (X1 - X2);
	C = X1 * (Y2 - Y3) + X2 * (Y3 - Y1) + X3 * (Y1 - Y2);
	D = -(X1 * (Y2 * Z3 - Y3 * Z2) + 
	      X2 * (Y3 * Z1 - Y1 * Z3) + 
	      X3 * (Y1 * Z2 - Y2 * Z1));

	#undef A
	#undef B
	#undef C
	#undef D

	#undef X1
	#undef Y1
	#undef Z1
	#undef X2
	#undef Y2
	#undef Z2
	#undef X3
	#undef Y3
	#undef Z3
	// @endcond
}

/**
 * Generate the near and far quads of a frustum.
 *
 * <b>References:</b>
 * <ul>
 * <li>http://www.lighthouse3d.com/opengl/viewfrustum/index.php?gaplanes</li>
 * </ul>
 * 
 * @param nearQuad Generated vertices of the near plane quad (12 floats).
 * @param farQuad Generated vertices of the far plane quad (12 floats). 
 * @param pos Position of the apex of the pyramid that the frustum is part of.
 * @param rot Orientation of the frustum {w,x,y,z}.
 * @param nearDist Distance to the near plane.
 * @param nearWidth Width of the near plane.
 * @param nearHeight Height of the near plane.
 * @param farDist Distance to the far plane.
 * @param farWidth Width of the far plane.
 * @param farHeight Height of the far plane.
 */
void dgGeometryFrustumQuads(float *nearQuad, float *farQuad,
			     float *pos, float *rot,
			     float nearDist, float nearWidth, float nearHeight,
			     float farDist, float farWidth, float farHeight)
{
	assert(nearQuad != farQuad);
	
	nearHeight /= 2;
	nearWidth /= 2;
	farHeight /= 2;
	farWidth /= 2;
	
	// Determine forward vector of orientation.
	float forward[3];
	dgQuaternionToForward(rot, forward);

	// Determine up vector of orientation.
	float up[3];
	dgQuaternionToUp(rot, up);

	// Determine right vector of orientation.
	float right[3];
	dgQuaternionToRight(rot, right);

	// Center point of near plane.
	float np[3];
	memcpy(np, forward, sizeof(float) * 3);
	dgVectorScale(np, nearDist);
	dgVectorAdd(pos, np, np);

	// Center point of far plane.
	float fp[3];
	memcpy(fp, forward, sizeof(float) * 3);
	dgVectorScale(fp, farDist);
	dgVectorAdd(pos, fp, fp);

	// Up vector scaled for near plane.
	float nup[3];
	memcpy(nup, up, sizeof(float) * 3);
	dgVectorScale(nup, nearHeight);

	// Up vector scaled for far plane.
	float fup[3];
	memcpy(fup, up, sizeof(float) * 3);
	dgVectorScale(fup, farHeight);
	
	// Right vector scaled for near plane.
	float nright[3];
	memcpy(nright, right, sizeof(float) * 3);
	dgVectorScale(nright, nearWidth);
	
	// Right vector scaled for far plane.
	float fright[3];
	memcpy(fright, right, sizeof(float) * 3);
	dgVectorScale(fright, farWidth);
	
	// Calculate points of near plane.
	dgVectorSub(np, nup, &nearQuad[0]);  // Bottom left.
	dgVectorSub(&nearQuad[0], nright, &nearQuad[0]); 
	dgVectorSub(np, nup, &nearQuad[3]);  // Bottom right.
	dgVectorAdd(&nearQuad[3], nright, &nearQuad[3]); 
	dgVectorAdd(np, nup, &nearQuad[6]);  // Upper right.	
	dgVectorAdd(&nearQuad[6], nright, &nearQuad[6]); 
	dgVectorAdd(np, nup, &nearQuad[9]);  // Upper left.
	dgVectorSub(&nearQuad[9], nright, &nearQuad[9]); 

	// Calculate points of far plane.
	dgVectorSub(fp, fup, &farQuad[0]);  // Bottom left.
	dgVectorSub(&farQuad[0], fright, &farQuad[0]); 
	dgVectorSub(fp, fup, &farQuad[3]);  // Bottom right.
	dgVectorAdd(&farQuad[3], fright, &farQuad[3]); 
	dgVectorAdd(fp, fup, &farQuad[6]);  // Upper right.
	dgVectorAdd(&farQuad[6], fright, &farQuad[6]); 
	dgVectorAdd(fp, fup, &farQuad[9]);  // Upper left.
	dgVectorSub(&farQuad[9], fright, &farQuad[9]); 
}

/**
 * Calculate the distance of a point from an arbitrary plane.
 *
 * @param[in] pt {x,y,z} point.
 * @param[in] pl Equation of the plane.
 * @return Distance from point to plane.
 */
float dgGeometryPointPlaneDistance(float *pt, float *pl)
{
	assert(pt);
	assert(pl);
	return (pl[0] * pt[0] + pl[1] * pt[1] + pl[2] * pt[2] + pl[3]) / dgVectorMag(pl);
}

/**
 * Check if a frustum contains a point.
 *
 * @param[in] pt {x,y,z} point.
 * @param[in] frustum 6 plane equations, one for each face of the frustum.
 * @return True if the point is contained, otherwise false.
 */
int dgGeometryFrustumContainsPoint(float *pt, float *frustum)
{
	assert(pt);
	assert(frustum);

	for (int i = 0; i < 24; i += 4)
	{
		if (dgGeometryPointPlaneDistance(pt, frustum + i) > 0.0)
		{
			return false;
		}
	}

	return true;
}

/**
 * Collide a frustum with a sphere and return the results.
 *
 * @param[in] pt {x,y,z} position of the center of the sphere.
 * @param[in] rad Radius of the sphere.
 * @param[in] frustum 6 plane equations, one for each face of the frustum.
 * @return DG_GEOMETRY_CONTAINS if the sphere is fully contained in the frustum,
 * DG_GEOMETRY_INTERSECTS if the sphere is not contained in but intersects the frustum,
 * or DG_GEOMETRY_NO_COLLISION if there is no collision.
 */
int dgGeometryFrustumCollidesSphere(float *pt, float rad, float *frustum)
{
	assert(pt);
	assert(frustum);

	int notContained = false;
	
	/*
	  Check the sphere against each plane. If we find it's not contained by
	  a plane, keep checking (it might be intersecting). If we find it's not
	  intersecting then return no-collision.
	 */ 
	for (int i = 0; i < 24; i += 4)
	{
		//if (i == 4) continue;

		// If the sphere isn't contained (it might be intersecting)...
		if (dgGeometryPointPlaneDistance(pt, frustum + i) > -rad)
		{
			notContained = true;
		}
		
		// If the sphere isn't intersecting the plane
		// and is on the wrong side...
		if (dgGeometryPointPlaneDistance(pt, frustum + i) > rad)
		{
			return DG_GEOMETRY_NO_COLLISION;
		}
	}

	return (notContained ? DG_GEOMETRY_INTERSECTS : DG_GEOMETRY_CONTAINS);
}

/**
 * Collide a frustum with an AABB and return the results.
 *
 * Note:
 * - For efficiency this method makes some assumptions
 *   that will occasionally lead to false-positives (eg. things
 *   that are not really intersecting will be said to).
 *
 * @param[in] aabb_verts 8 vertices of the AABB.
 * @param[in] frustum_planeqs 6 plane equations, one for each face of the frustum.
 * @return DG_GEOMETRY_CONTAINS if the AABB is fully contained in the frustum,
 * DG_GEOMETRY_INTERSECTS if the AABB is not contained in but intersects the frustum,
 * or DG_GEOMETRY_NO_COLLISION if there is no collision.
 */
int dgGeometryFrustumCollidesAABB(float *aabb_verts, float *frustum_planeqs)
{
	/*	 
	  References:
	  - http://www.lighthouse3d.com/opengl/viewfrustum/index.php?gatest2

	  First, we test each vertex of the AABB against each plane of
	  the frustum. There are several possible results.
	  
	  Note: We make the distinction between containment and intersection here because
	  this function is mainly intended to be used for frustum culling (via octrees), 
	  and we have to do a lot more work if a AABB is merely intersected rather
	  than contained.

	  1) All vertices are on the wrong side of the same plane.
	     - Then the AABB does not intersect the frustum.
	     - Return DG_GEOMETRY_NO_COLLISION

	  2) All vertices are on the right sides of all planes.
	     - The AABB is contained in the frustum.
	     - Return DG_GEOMETRY_CONTAINS

	  3) Some but not all vertices are on the right sides of all planes.
	     - Then the AABB intersects the frustum.
	     - Return DG_GEOMETRY_INTERSECTS

	  4) All vertices are outside the frustum, but not on the wrong
	     side of the same plane.
	     - Quite simply this sucks. The AABB could be outside the 
	     frustum, or it could contain the frustum, or it could intersect
	     the edge of the frustum.
	     - Lighthouse suggests just accepting these is the most efficient
	     (albeit not correct) solution.
	     - We could perform more tests (this is the correct but expensive
	     solution).
	     - An intelligent solution may be to somehow rate how "expensive"
	     an AABB may be to accept, eg. a false positive containing an
	     complicated 3d mesh that will be very costly to render may warrant
	     further tests, whereas simple point sprites may be less of a concern.
	     - However we are going with the first solution and will consider
	     the more complex algorithmic solution as an optimisation later if it
	     is necessary.
	     - Return DG_GEOMETRY_INTERSECTS
	*/	

	/*
	  Our early-exit condition is:
	  If we have a mix of good/bad vertices, we have an intersection.

	  If we reach the end then we have one of three cases:
	  1) All vertices good -> DG_GEOMETRY_CONTAINS
	  2) All vertices bad for same plane -> DG_GEOMETRY_NO_COLLISION
	  3) All vertices bad on differen tplanes -> DG_GEOMETRY_INTERSECTS
	 */
	
	int planeStats[6] = { false, false, false, false, false, false };
	int goodVerts = false;
	int badVerts = false;

	// For each vert...
	for (int i = 0; i < 24; i += 3)
	{	       
		int vertContained = true;

		// For each plane...
		for (int j = 0; j < 24; j += 4)
		{
			// Skip checking against front plane.
			//if (j == 8) continue;

			// True if this vertex is on the correct side of this plane.
			int vertIsGood = (dgGeometryPointPlaneDistance(aabb_verts + i, frustum_planeqs + j) < 0.0);

			// If we find at least one good vertex for a plane
			// then we know that plane cannot be a candidate for
			// the NO_COLLISION condition.
			planeStats[j / 4] |= vertIsGood;	
			
			// A vertex is only contained if it is on the correct
			// side for every plane.
			vertContained &= vertIsGood;
		}

		// If we have at least one contained vertex.
		goodVerts |= vertContained;
		
		// If we have at least one bad vertex.
		badVerts |= !vertContained;
		
		// If we have both good and bad verts, then we must have an intersection.
		if (goodVerts && badVerts)
		{
			return DG_GEOMETRY_INTERSECTS;
		}		
	}

	// If we only have good verts, then we have containment.
	if (goodVerts && !badVerts)
	{
		return DG_GEOMETRY_CONTAINS;
	}

	// If we have any plane that all vertices are on the wrong side of, then
	// we have no collision. We can AND these together, if the result is false then
	// that means at least one plane meets our conditions (we don't care which one).
	if (!(planeStats[0] && planeStats[1] && planeStats[2] && planeStats[3] && planeStats[4] && planeStats[5]))
	{
		return DG_GEOMETRY_NO_COLLISION;
	}

	// Otherwise we could have intersection or no-collision. The easiest thing to do here
	// is just say intersection, although this is not technically a correct answer.
	return DG_GEOMETRY_INTERSECTS;
}
