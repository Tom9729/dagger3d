/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/**
 * Convert a quaternion to a 4x4 homogenous matrix. 
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.gamedev.net/reference/articles/article1095.asp</li>
 * <li>http://jerome.jouvie.free.fr/OpenGl/Lessons/Lesson2.php#Modelview</li>
 * </ul>
 *
 * @param q The quaternion { w, x, y, z }.
 * @param m The matrix (16 floats).
 */
void dgQuaternionToMatrix(float *q, float *m)
{
	assert(q);
	assert(m);

	// @cond
	#define w q[0]
	#define x q[1]
	#define y q[2]
	#define z q[3]
	// @endcond
	
	// First column.
	m[0] = 1 - 2 * (y * y + z * z);
	m[1] = 2 * (x * y + w * z);
	m[2] = 2 * (x * z - w * y);
	m[3] = 0.0f;

	// Second column.
	m[4] = 2 * (x * y - w * z);
	m[5] = 1 - 2 * (x * x + z * z);
	m[6] = 2 * (y * z + w * x);
	m[7] = 0.0f;

	// Third column.
	m[8] = 2 * (x * z + w * y);
	m[9] = 2 * (y * z - w * x);
	m[10] = 1 - 2 * (x * x + y * y);
	m[11] = 0.0f;
	
	// Fourth column.
	m[12] = 0.0f;
	m[13] = 0.0f;
	m[14] = 0.0f;
	m[15] = 1.0f;
}

/**
 * Multiply two quaternions together and store the result in 'c'.
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.gamedev.net/reference/articles/article1095.asp</li>
 * <li>http://jerome.jouvie.free.fr/OpenGl/Lessons/Lesson2.php#Modelview</li>
 * </ul>
 *
 * @param a,b Quaternions to multiply together.
 * @param c Where to store the result (a quaternion).
 */
void dgQuaternionMult(float *a, float *b, float *c)
{
	assert(a);
	assert(b);
	assert(c);

	// A temporary quaternion to store the results
	// just in case a == c or b == c.
	float temp[4];

	// @cond
	#undef w
	#undef x
	#undef y
	#undef z
	#define w temp[0]
	#define x temp[1]
	#define y temp[2]
	#define z temp[3]
	#define w1 a[0]
	#define x1 a[1]
	#define y1 a[2]
	#define z1 a[3]
	#define w2 b[0]
	#define x2 b[1]
	#define y2 b[2]
	#define z2 b[3]
	// @endcond

	w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2;
	x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2;
	y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2;
	z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2;

	// Copy the results over.
	memcpy(c, temp, sizeof(float) * 4);

	#undef w
	#undef x
	#undef y
	#undef z
	#undef w1
	#undef x1
	#undef y1
	#undef z1
	#undef w2
	#undef x2
	#undef y2
	#undef z2
}

/**
 * Reset a quaternion to it's multiplicative identity { 1, 0, 0, 0 }.
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.gamedev.net/reference/articles/article1095.asp</li>
 * </ul>
 *
 * @param q The quaternion.
 */
void dgQuaternionIdentity(float *q)
{
	assert(q);
	
	q[0] = 1.0f;
	q[1] = 0.0f;
	q[2] = 0.0f;
	q[3] = 0.0f;
}

/**
 * Convert an orientation from an axis/angle combination,
 * to a quaternion.
 *
 * Angle theta is measured in radians.
 * 
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.gamedev.net/reference/articles/article1095.asp</li>
 * </ul>
 *
 * @param q The quaternion { w, x, y, z }.
 * @param a The axis/angle { x, y, z, theta }.
 */
void dgQuaternionFromAxisAngle(float *q, float *a)
{
	assert(q);
	assert(a);
	
	float halfTheta;
	float cosTheta;
	float sinTheta;

	// Temporary quaternion just in case q == a.
	float temp[4];

	dgVectorNorm(a);
	
	/* // If the axis is a zero vector, return  */
	/* // the identity quaternion. */
	/* if (dgMathFloatEquals(a[0] + a[1] + a[2], 0, 0.00001)) */
	/* { */
	/* 	puts("eek"); */
	/* 	dgQuaternionIdentity(q); */
	/* 	return; */
	/* } */

	/* // Otherwise normalise the axis. */
	/* else */
	/* { */
	/* 	dgVectorNorm(a); */
	/* } */

	// Calculate these values once.
	halfTheta = a[3] / 2.0f;
	cosTheta = cos(halfTheta);
	sinTheta = sin(halfTheta);
	
	// Calculate the quaternion.
	temp[0] = cosTheta;
	temp[1] = a[0] * sinTheta;
	temp[2] = a[1] * sinTheta;
	temp[3] = a[2] * sinTheta;

	// Copy over final result.
	memcpy(q, temp, sizeof(float) * 4);
}

/**
 * Convert a quaternion to it's axis/angle
 * representation.
 *
 * Angle theta is measured in radians.
 * 
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.gamedev.net/reference/articles/article1095.asp</li>
 * </ul>
 *
 * @param q The quaternion { w, x, y, z }.
 * @param a The axis/angle { x, y, z, theta }.
 */
void dgQuaternionToAxisAngle(float *q, float *a)
{
	assert(q);
	assert(q);
	
	// Scale is like the magnitude of the vector
	// component of the quaternion.
	float scale;
	// Temporary axis/angle just in case q == a.
	float temp[4];

	// Scale = sqrt(x^2 + y^2 + z^2)
	// where {x,y,z} are from the quaternion 'q'.
	// If scale == 0 then there is no rotation,
	// which means the axis can be anything and
	// the angle is zero.
	if (dgMathFloatEquals(q[1] + q[2] + q[3], 0, 0.00001))
	{
		// a = { x, y, z, t } 
		//   = { 0, 0, 1, 0 }
		a[0] = a[1] = a[3] = 0.0f;
		a[2] = 1.0f;
		return;
	}

	// Calculate the angle.
	temp[3] = 2 * acos(q[0]);
	// Calculate the scale.
	scale = sqrt(q[1] * q[1] +
		     q[2] * q[2] +
		     q[3] * q[3]);
	// Calculate the axis now.
	temp[0] = q[1] / scale;
	temp[1] = q[2] / scale;
	temp[2] = q[3] / scale;

	// Copy result back over.
	memcpy(a, temp, sizeof(float) * 4);
}
/**
 * Apply a quaternion rotation to a vector.
 *
 * @param q The quaternion {w,x,y,z}.
 * @param vector The vector to rotate {x,y,z}.
 * @param result Vector to store the result.
 */
void dgQuaternionRotate2(float *q, float *vector, float *result)
{
	assert(q);
	assert(vector);
	assert(result);

	// Save magnitude of vector first.
	float mag = dgVectorMag(vector);

	// Apply rotation.
	dgQuaternionRotate(q, vector, result);

	// Rescale result to original length.
	dgVectorScale(result, mag);
}

/**
 * Apply a quaternion rotation to a unit vector.
 * 
 * <b>This will normalise the result. If you would like to
 * apply a rotation around the origin to a quaternion while
 * preserving magnitude then use dgQuaternionRotate2().</b>
 *
 * @param q The quaternion {w,x,y,z}.
 * @param vector The vector to rotate {x,y,z}.
 * @param result Vector to store the result.
 */
void dgQuaternionRotate(float *q, float *vector, float *result)
{
	float vectorQ[4];
	float conj[4];

	// Make a copy of the original vector and normalise it.
	memcpy(vectorQ + 1, vector, sizeof(float) * 3);
	dgVectorNorm(vectorQ + 1);
	
	// Treat vectorQ as a quaternion, set w=0.
	vectorQ[0] = 0.0f;

	// Get the conjugate of the quaternion we are using to rotate.
	dgQuaternionConjugate(q, conj);

	// Result = q * vectorQ * q'.
	dgQuaternionMult(q, vectorQ, vectorQ);
	dgQuaternionMult(vectorQ, conj, vectorQ);

	// Copy over vector part to result.
	memcpy(result, vectorQ + 1, sizeof(float) * 3);
}

/**
 * Calculate the conjugate/inverse of a quaternion.
 *
 * @param q The quaternion {w,x,y,z}.
 * @param qp Place to store the quaternion's conjugate.
 */
void dgQuaternionConjugate(float *q, float *qp)
{
	assert(q);
	assert(qp);
	
	qp[0] = q[0];
	qp[1] = -q[1];
	qp[2] = -q[2];
	qp[3] = -q[3];
}

/**
 * Apply a euler angle to a quaternion.
 *
 * Angle measurements are in radians.
 *
 * @param q The quaternion.
 * @param x Pitch.
 * @param y Raw.
 * @param z Roll.
 */
void dgQuaternionApplyEuler(float *q, float x, float y, float z)
{
	assert(q);
	
	float ax[4] = { 1, 0, 0, x };
	float ay[4] = { 0, 1, 0, y };
	float az[4] = { 0, 0, 1, z };
	
	// Convert axis/angles to quaternions.
	dgQuaternionFromAxisAngle(ax, ax);
	dgQuaternionFromAxisAngle(ay, ay);
	dgQuaternionFromAxisAngle(az, az);
	
	// Concatenate rotations into one.
	dgQuaternionMult(ax, q, q);
	dgQuaternionMult(ay, q, q);
	dgQuaternionMult(az, q, q);

	// Normalise the resulting quaternion.
	dgQuaternionNorm(q);
}

/**
 * Construct a quaternion from a euler angle.
 *
 * @param q The quaternion {w,x,y,z}.
 * @param e The euler angle {p,y,r}.
 */
void dgQuaternionFromEuler(float *q, float *e)
{
	assert(q);
	assert(e);
	
	float a = e[0] / 2.0;
	float c = e[1] / 2.0;
	float b = e[2] / 2.0;
	
	float Qx[4] = { cos(a), sin(a), 0, 0 };
	float Qy[4] = { cos(b), 0, sin(b), 0 };
	float Qz[4] = { cos(c), 0, 0, sin(c) };
	
	dgQuaternionMult(Qx, Qy, Qx);
	dgQuaternionMult(Qx, Qz, Qx);
	
	memcpy(q, Qx, sizeof(float) * 4);
}

/**
 * Calculate and return the length/magnitude of a quaternion.
 *
 * @param q The quaternion.
 */
float dgQuaternionLength(float *q)
{
	assert(q);
	return sqrt(q[0] * q[0] +
		    q[1] * q[1] +
		    q[2] * q[2] +
		    q[3] * q[3]);
}

/**
 * Normalise a quaternion.
 *
 * @param q The quaternion.
 */
void dgQuaternionNorm(float *q)
{
	assert(q);

	// First get the length of q.
	float l = dgQuaternionLength(q);

	// Divide each component by l.
	q[0] /= l;
	q[1] /= l;
	q[2] /= l;
	q[3] /= l;
}

/**
 * Get the forward vector of a rotation quaternion.
 *
 * @param q The quaternion.
 * @param v The vector.
 */
void dgQuaternionToForward(float *q, float *v)
{
	assert(q);
	assert(v);
	
	float conj[4] = { 1, 0, 0, 0 };
	
	v[0] = 0;
	v[1] = 0;
	v[2] = -1;	

	// Calculate the conjugate of the camera's orientation.
	dgQuaternionConjugate(q, conj);
	
	// Forward vector v = rot' * { 0, 0, -1 }
	dgQuaternionRotate(conj, v, v);
}

/**
 * Get the up vector of a rotation quaternion.
 *
 * @param q The quaternion.
 * @param v The vector.
 */
void dgQuaternionToUp(float *q, float *v)
{
	assert(q);
	assert(v);
	
	float conj[4] = { 1, 0, 0, 0 };
	
	v[0] = 0;
	v[1] = 1;
	v[2] = 0;	

	// Calculate the conjugate of the camera's orientation.
	dgQuaternionConjugate(q, conj);
	
	// Up vector v = rot' * { 0, 1, 0 }
	dgQuaternionRotate(conj, v, v);
}

/**
 * Get the right vector of a rotation quaternion.
 *
 * @param q The quaternion.
 * @param v The vector.
 */
void dgQuaternionToRight(float *q, float *v)
{
	assert(q);
	assert(v);
	
	float conj[4] = { 1, 0, 0, 0 };
	
	v[0] = 1;
	v[1] = 0;
	v[2] = 0;	

	// Calculate the conjugate of the camera's orientation.
	dgQuaternionConjugate(q, conj);
	
	// Right vector v = rot' * { 1, 0, 0 }
	dgQuaternionRotate(conj, v, v);
}

void dgQuaternionApply(float *q)
{
	assert(q);
	
	float aarot[4];
	dgQuaternionToAxisAngle(q, aarot);
	glRotatef(dgMathRadiansToDegrees(aarot[3]), aarot[0], 
		  aarot[1], aarot[2]);
}
