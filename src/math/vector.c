/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/**
 * Find the sum of two 3D vectors.
 * 
 * <b>Example usage:</b>
 * \code
 * float *a = { 1.0f, 2.0f, 3.0f };
 * float *b = { 3.0f, 2.0f, 1.0f };
 *
 * // a = { 4.0f, 4.0f, 4.0f }
 * dgVectorAdd(a, b, a);
 * \endcode
 *
 * @param a,b The vectors.
 * @param c Place to store the resulting vector.
 */
void dgVectorAdd(float *a, float *b, float *c)
{
	assert(a);
	assert(b);
	assert(c);
	c[0] = a[0] + b[0];
	c[1] = a[1] + b[1];
	c[2] = a[2] + b[2];
}

/**
 * Find the difference of two vector.
 * 
 * <b>Example usage:</b>
 * \code
 * float *a = { 3.0f, 2.0f, 1.0f };
 * float *b = { 0.5f, 0.5f, 0.5f };
 *
 * // a = { 2.5f, 1.5f, 0.5f }
 * dgVectorSub(a, b, a);
 * \endcode
 *
 * @param a,b The vectors.
 * @param c Place to store the resulting vector.
 */
void dgVectorSub(float *a, float *b, float *c)
{
	assert(a);
	assert(b);
	assert(c);
	c[0] = a[0] - b[0];
	c[1] = a[1] - b[1];
	c[2] = a[2] - b[2];
}

/**
 * Find the product of two vectors.
 * 
 * Vectors are multiplied component by component.
 * 
 * <b>Example usage:</b>
 * \code
 * float *a = { 3.0f, 2.0f, 1.0f };
 * float *b = { 0.5f, 0.5f, 0.5f };
 *
 * // a = { 1.5f, 1.0f, 0.5f }
 * dgVectorMult(a, b, a);
 * \endcode
 *
 * @param a,b The vectors.
 * @param c Place to store the resulting vector.
 */
void dgVectorMult(float *a, float *b, float *c)
{
	assert(a);
	assert(b);
	assert(c);
	c[0] = a[0] * b[0];
	c[1] = a[1] * b[1];
	c[2] = a[2] * b[2];
}

/**
 * Multiply each component of a vector by a scalar.
 * 
 * <b>Example usage:</b>
 * \code
 * float *a = { 3.0f, 2.0f, 1.0f };
 *
 * // a = { 6.0f, 4.0f, 2.0f }
 * dgVectorScale(a, 2.0f);
 * \endcode
 *
 * @param a The vector.
 * @param sf The scalar.
 */
void dgVectorScale(float *a, float sf)
{
	assert(a);
	a[0] *= sf;
	a[1] *= sf;
	a[2] *= sf;
}

/**
 * Find the magnitude of a vector.
 *
 * Formula:<br>
 * <i>mag = sqrt(a^2 + b^2 + c^2)</i>
 *
 * @param a The vector.
 * @return The magnitude.
 */
float dgVectorMag(float *a)
{
	assert(a);
	return sqrt(a[0] * a[0] + 
		    a[1] * a[1] + 
		    a[2] * a[2]);
}

/**
 * Normalise a vector so that it's magnitude is exactly equal to 1.0f.
 *
 * A vector is normalised by calculating it's magnitude, then dividing
 * each component by it.
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://www.fundza.com/vectors/normalize/index.html</li>
 * </ul>
 *
 * <b>Example usage:</b><br>
 * \code
 * float *a = { 3.0f, 1.0f, 2.0f };
 *
 * // a = { 0.802f, 0.267f, 0.534f };
 * dgVectorMag(a);
 * \endcode
 *
 * @param a The vector.
 */
void dgVectorNorm(float *a)
{
	float mag;
	
	assert(a);
	
	// Calculate the magnitude of
	// the vector.
	mag = dgVectorMag(a);

	// Prevent a possible DBZ fail.
	mag = (mag == 0.0f ? 1.0f : mag);

	// Divide each xyz component by
	// the magnitude.
	a[0] /= mag;
	a[1] /= mag;
	a[2] /= mag;
}

/**
 * Make a copy of a vector.
 *
 * @param a The vector.
 * @return A copy.
 */
float *dgVectorClone(float *a)
{
	float *b;

	assert(a);	
	dgMalloc(b, float, 3);
	memcpy(b, a, sizeof(float) * 3);

	return b;
}

/**
 * Set the xyz components of a vector.
 *
 * <b>Example usage:</b>
 * \code
 * float *a = { 0.0f, 0.0f, 0.0f };
 *
 * // a = { 1.0f, 2.0f, 3.0f }
 * dgVectorSet(a, 1.0f, 2.0f, 3.0f);
 * \endcode
 *
 * @param a The vector.
 * @param x,y,z
 */
void dgVectorSet(float *a, float x, float y, float z)
{
	assert(a);

	a[0] = x;
	a[1] = y;
	a[2] = z;	
}

/**
 * Find the dot/scalar product of two vectors.
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://en.wikipedia.org/wiki/Dot_product</li>
 * </ul>
 * 
 * @param a,b The vectors.
 * @return Scalar product.
 */
float dgVectorDotProd(float *a, float *b)
{
	assert(a);
	assert(b);
	
	return (a[0] * b[0]) +
	       (a[1] * b[1]) +
	       (a[2] * b[2]);
}

static inline float dgVectorCrossProdComputef(float a, float b, float c, float d)
{
	return ((a * b) - (c * d));
}

/**
 * Find the cross product of two vectors.
 *
 * @param a,b The vectors.
 * @param c Where to store the result.
 */
void dgVectorCrossProd(float *a, float *b, float *c)
{
	float temp[3];
	
	assert(a);
	assert(b);
	assert(c);

	temp[0] = dgVectorCrossProdComputef(a[1], b[2], a[2], b[1]);
	temp[1] = dgVectorCrossProdComputef(a[2], b[0], a[0], b[2]);
	temp[2] = dgVectorCrossProdComputef(a[0], b[1], a[1], b[0]);

	memcpy(c, temp, sizeof(float) * 3);
}

/**
 * Calculate the dot product of two 4D vectors.
 *
 * @param a The first vector.
 * @param b The second vector.
 * @return a.b
 */
float dgVectorDotProd4(float *a, float *b)
{
	assert(a);
	assert(b);
	
	// http://www.cs.uiuc.edu/class/sp07/cs232/section/Discussion13/disc13.pdf
	// http://gcc.gnu.org/onlinedocs/gcc-3.3.1/gcc/X86-Built-in-Functions.html#X86%20Built-in%20Functions
	
	/* typedef float v4sf __attribute__ ((mode(V4SF))); */
	
	/* float temp[4]; */
	/* v4sf acc, X, Y; // 4x32-bit float registers */
	
	/* X = __builtin_ia32_loadups(a); */
	/* Y = __builtin_ia32_loadups(b); */
	
	/* acc = __builtin_ia32_mulps(X, Y); */
	
	/* __builtin_ia32_storeups(temp, acc); */
	
	/* return temp[0] + temp[1] + temp[2] + temp[3]; */
	
	return (a[0] * b[0]) +
	       (a[1] * b[1]) +
	       (a[2] * b[2]) +
	       (a[3] * b[3]);
}
