/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>
#include <time.h>

/**
 * Seed the PRNG with the system clock.
 */
void dgRandomSeed()
{
	srand(time(NULL) / 2.0);
}

/**
 * Return a random integer in a range.
 *
 * @param min Lower bound (inclusive).
 * @param max Upper bound (inclusive).
 * @return The integer between min and max.
 */
int dgRandomInt(unsigned int min, unsigned int max)
{
	return min + ((rand() / (double) RAND_MAX) * (max - min));
}

/**
 * Get a pseudorandomly signed nonzero integer coefficient clamped to [-1, 1].
 *
 * @return I don't think anything more has to be said.
 */
int dgRandomSign()
{
	return ((dgRandomInt(0, 100) % 2 == 0) ? -1 : 1);
}

float dgRandomf(float min, float max)
{
	assert(min >= 0);
	assert(max >= 0);

	if (min > max)
	{
		const float tmp = max;
		max = min;
		min = tmp;
	}

	return min + ((rand() / (double) RAND_MAX) * (max - min));
}
