/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/**
 * Properly round a float to the nearest integer.
 *
 * @param n The float.
 * @return n rounded to the nearest integer.
 */
float dgMathRoundf(float n)
{
	int d;
	
	d = (int) n;
	n -= d;
	
	return d + ((n >= 0.5f) ? 1 : 0);
}

/**
 * Get the absolute value of a float.
 *
 * @param v Float value.
 * @return Absolute value of v.
 */
float dgMathAbsf(float v)
{
	return (v >= 0.0f ? v : -v);
}

float dgMathMaxf(float v1, float v2)
{
	return (v1 > v2 ? v1 : v2);
}

/**
 * Clamp an angle (radians) to [0, 2*PI).
 *
 * @param ang The angle in radians.
 */
void dgMathFixAngleRad(float *ang)
{
	if (*ang < 0.0f)
	{
		*ang += M_2PI;
	}

	else if (*ang >= M_2PI)
	{
		*ang -= M_2PI;
	}
}

/**
 * Clamp an angle (degrees) to [0, 360).
 *
 * @param ang The angle.
 */
void dgMathFixAngleDeg(float *ang)
{
	if (*ang < 0.0f)
	{
		*ang += 360.0f;
	}

	else if (*ang >= 360.0f)
	{
		*ang -= 360.0f;
	}
}

/**
 * Check if a number is a power of two.
 *
 * <b>References:</b><br>
 * <ul>
 * <li>http://stackoverflow.com/questions/600293/how-to-check-if-a-number-is-a-power-of-2</li>
 * </ul>
 *
 * @param n The number to check.
 * @return True if n is a POT, otherwise false.
 */
int dgMathIsPowerOfTwo(long n)
{
	// Get a positive version of n.
	unsigned long x = (n < 0 ? -n : n);

	return (x && !(x & (x - 1)));
}

/**
 * Calculate the next power-of-two after a value.
 *
 * @param v The value.
 * @return The next power of two > v.
 */
long dgMathNextPowerOfTwo(long v)
{
	long rval = 1;
	
	while (rval < v)
	{
		rval <<= 1;
	}

	return rval;
}

/**
 * Convert an angle from degrees to radians.
 *
 * @param deg The angle in degrees.
 * @return The angle in radians.
 */
float dgMathDegreesToRadians(float deg)
{
	return deg * M_PI / 180.0f;
}

/**
 * Convert an angle from radians to degrees.
 *
 * @param rad The angle in radians.
 * @return The angle in degrees.
 */
float dgMathRadiansToDegrees(float rad)
{
	return rad * 180.0f / M_PI;
}

/**
 * Check if two floating point numbers are relatively
 * equal to each other. That is to say, if the difference
 * between two numbers 'a' and 'b' is less than epsilon
 * value 'es', then 'a' == 'b' as far as we are concerned.
 *
 * @param a,b The numbers to compare.
 * @param es Epsilon value.
 * @return True if the two numbers are equal.
 */
int dgMathFloatEquals(float a, float b, float es)
{
	return (dgMathAbs(a - b) < es);
}

/**
 * Check which quadrant a 2D cartesian coordinate is in.
 *
 * Quadrants are numbered counter-clockwise starting from
 * positive x/y, like so:
 *<pre>         
 *       2  |  1
 *     -----------
 *       3  |  4
 *</pre>
 * @param x,y The 2D point.
 * @return Which quadrant the point is in.
 */
int dgMathQuadrant(float x, float y)
{
	if (x >= 0.0f)
	{
		if (y >= 0.0f)
		{
			return 1;
		}

		else
		{
			return 4;
		}
	}

	else if (y >= 0.0f)
	{
		return 2;
	}

	else
	{
		return 3;
	}
}

/**
 * Converts a cartesian coordinate to a polar
 * coordinate and returns the angle component.
 *
 * The angle will be in radians and should
 * always be clamped to [0, 360).
 *
 * @param x,y The cartesian coordinate.
 * @return Polar angle component in radians.
 */
float dgMathGetPolarAngle(float x, float y)
{
	// In radians.
	float theta = atan(y / x);
	// Quadrant this point is in.
	int quadrant = dgMathQuadrant(x, y);
	
	if (quadrant == 2 || quadrant == 3)
	{
		theta += M_PI;
	}

	else if (quadrant == 4)
	{
		theta += M_2PI;
	}

	return theta;
}
