/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE
#include <dagger3d.h>

/**
 * \mainpage API Documentation
 * \image html logo-small.png
 * <b>Readme:</b>
 * \verbinclude README
 * <b>Todo:</b>
 * \verbinclude TODO
 * <b>Changes:</b>
 * \verbinclude CHANGES
 * <b>Authors:</b>
 * \verbinclude AUTHORS
 */

/*
  Known system cleanup priority. Low priorities are deleted first!

  XXX Some thought should perhaps go into rearranging these.
  XXX Seriously, this is a clusterfuck.

 -10 : res
  -9 : res_prefetch
  -8 : res_worker_queue
  -7 : res_worker
  0  : audio, light, joystick
  1  : window
  2  : camera, socket
  3  : font
  9  : actor, shadow_volumes
  10 : mesh, texture, bsp
  55 : timer
  62 : frame
  125 : unique

 */

static void dgAtExit();

// Number of command line arguments.
static int _dgArgc;
// Pointer to command line arguments.
static char ** _dgArgv = NULL;

/**
 * Initialize Dagger3D.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 */
void dgInit(int argc, char **argv)
{			
	/* #if defined(WIN32) */
	/* printf("System: Windows"); */
	/* #elif defined(LINUX) */
	/* printf("System: Linux"); */
	/* #elif defined(DARWIN) */
	/* printf("System: Darwin"); */
	/* #endif */

	/* #if defined(DG_BIG_ENDIAN) */
	/* printf(" (big endian)\n"); */
	/* #else */
	/* printf(" (little endian)\n"); */
	/* #endif */

	// Save command line args.
	_dgArgc = argc;
	_dgArgv = argv;
	
	// Use binary relative path by default.
	dgSysBinCwd(true);

	// Register function to run when Dagger exits.
	#ifndef NO_ATEXIT
	atexit(dgAtExit);
	#endif
	
	//printf("exec (%s)\n", argv[0]);
	
	// Process command line args
	for (int i = 0; i < argc; ++i)
	{
		if (!strcmp(argv[i], "-fullscreen")) dgWindowOpenOverrideFullscreen(true);
		else if (!strcmp(argv[i], "-window")) dgWindowOpenOverrideWindow(true);
		else if (!strcmp(argv[i], "-size") && (argc - i) == 3)
		{
			const int width = atoi(argv[i + 1]);
			const int height = atoi(argv[i + 2]);

			if (width > 0 && height > 0)
			{
				dgWindowOpenOverrideResolution(width, height);
			}

			i += 2;
		}
	}
	
	int deps[] = {
		DG_SYS_ACTOR,
		DG_SYS_AUDIO,
		DG_SYS_BSP,
		DG_SYS_CAMERA,
		DG_SYS_FONT,
		DG_SYS_JOYSTICK,
		DG_SYS_MESH,
		DG_SYS_RES,
		DG_SYS_SHADOW_VOLUME,
		DG_SYS_SOCKET,
		DG_SYS_TEXTURE,
		DG_SYS_UNIQUE,
		DG_SYS_WINDOW
	};
	
	// Bind a dummy cleanup function to DG_INIT_USER, and place dependencies
	// on all Dagger systems. This will ensure that the user's cleanup code
	// will always be run before Dagger's is.
	dgSysRegister(DG_SYS_USER, NULL, sizeof(deps) / sizeof(int), deps);
}

/**
 * Retrieve command line arguments passed to Dagger via dgInit().
 *
 * @param argc
 * @param argv
 */
void dgInitGetArgs(int *argc, char ***argv)
{
	*argc = _dgArgc;
	*argv = _dgArgv;
}

static void dgAtExit()
{
	/* printf("Running cleanup.\n"); */

	//dgCondFree(binPath);
	
	dgSysCleanup();
	
	#ifdef MEMPROF
	const unsigned long  bytesNotFreed  = dgMemProfBytesAllocated();
	if (bytesNotFreed > 0)
	{
		printf("%lu bytes not freed!\n", bytesNotFreed);
		dgMemProfPrintRecords();
	}
	#endif
	
	/* printf("Bye\n"); */
}
