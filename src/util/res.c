/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

typedef struct
{
	// Filename of the resource.
	char *fn;
	// Type of resource (eg. DG_MESH...).
	int type;
	// Options used when loading this.
	int opts;
	// Dagger reference to resource.
	int ref;
	// Number of references to this resource. Basically this gets
	// incremented every time the resource is loaded and decremented
	// every time it is deleted. The resource does not _actually_ 
	// get deleted until it has no more references.
	int numRefs;
} dgRes;

static void *resList = NULL;

static void dgResInit();
static void dgResCleanup();
static dgRes *_dgResNew(int type, char *fn, int opts);
static void _dgResDelete(dgRes *res);
static dgRes *_dgResGetPtrFromFilename(int type, char *fn, int opts);
static dgRes *_dgResGetPtrFromRef(int type, int ref, int *resRef);

static dgRes *_dgResNew(int type, char *fn, int opts)
{
	dgPrintf("msg=cache miss, type=%d, "
		 "opts=%d, fn=%s\n",
		 type,
		 opts,
		 fn);

	// Dagger reference.
	int ref = -1;
	
	switch (type)
	{
	case DG_MESH:
		ref = dgMeshLoadOpts(fn, opts);
		break;
	case DG_TEXTURE:
		ref = dgTextureLoad(fn);
		break;
	case DG_AUDIO:
		ref = dgAudioLoad(fn);
		break;
	case DG_FONT:
		ref = dgFontLoad(fn, 12);
		break;
	}

	// Failure to load resource for whatever reason.
	if (ref == -1)
	{
		fprintf(stderr, "Error: failed to load resource (%s)\n", fn);
		return NULL;
	}
	
	dgRes *res;
	dgMalloc(res, dgRes, 1);
	dgArrayListAddItem(resList, res);	

	// Copy over filename, type, and reference.
	res->fn = dgStringDup(fn);
	res->type = type;
	res->opts = opts;
	res->ref = ref;
	
	return res;	
}

static void _dgResDelete(dgRes *res)
{
	assert(res);

	if (res->type == DG_MESH)
	{
		dgMeshDelete(res->ref);
	}

	else if (res->type == DG_TEXTURE)
	{
		dgTextureDelete(res->ref);
	}
	
	else if (res->type == DG_FONT)
	{
		dgFontDelete(res->ref);
	}

	else if (res->type == DG_AUDIO)
	{
		dgAudioDelete(res->ref);
	}

	dgFree(res->fn);
	dgFree(res);
}

static dgRes *_dgResGetPtrFromFilename(int type, char *fn, int opts)
{
	assert(
		type == DG_MESH ||
		type == DG_TEXTURE ||
		type == DG_AUDIO ||
		type == DG_FONT
		);
	
	// Search through list of loaded resource.
	dgArrayListForEach(dgRes, res, resList, idx,
			   
			   // Got a match!
			   if (res->type == type &&
			       res->opts == opts &&
			       !strcmp(res->fn, fn))
			   {
				   dgPrintf("msg=cache miss, type=%d, "
					    "opts=%d, fn=%s\n",
					    type,
					    opts,
					    fn);
				   
				   // Return resource.
				   return res;
			   }
		);

	return _dgResNew(type, fn, opts);
}

static dgRes *_dgResGetPtrFromRef(int type, int ref, int *resRef)
{
	dgArrayListForEach(dgRes, res, resList, idx,
			   
			   // Found it!
			   if (res->type == type && res->ref == ref)
			   {
				   *resRef = idx;
				   return res;
			   }
		);
	
	*resRef = -1;
	return NULL;
}

static void dgResInit()
{
	if (resList == NULL)
	{
		// Create resource list and register cleanup function.
		resList = dgArrayListNew();

		int deps[] = { DG_SYS_MESH, DG_SYS_TEXTURE, 
			       DG_SYS_AUDIO, DG_SYS_FONT };
		dgSysRegister(DG_SYS_RES, dgResCleanup, 
			       sizeof(deps) / sizeof(int), deps);
	}
}

static void dgResCleanup()
{
//	printf("Running resource cleanup.\n");

	// Delete each resource in the list.
	dgArrayListForEach(dgRes, res, resList, idx,
			   //printf("%s\n", res->fn);
			   _dgResDelete(res);
		);

	// Delete resource list.
	dgArrayListDelete(resList);
	resList = NULL;
}

int dgResLoad(int type, char *fn, int opts)
{
	assert(fn);
	dgResInit();
		
	// Fetch the resource for this file.
	dgRes *res = _dgResGetPtrFromFilename(type, fn, opts);
	assert(res);
	
	// Increment reference count.
	++(res->numRefs);
	
	// Return resource.
	return res->ref;
}

void dgResDelete(int type, int ref)
{
	/*
	  So here's how this works. When loading a resource,
	  options are used as an additional key. If we load
	  "foo.tga" twice with different options we will receive
	  two different texture references. When deleting a resource
	  we don't need to worry about the options because the two
	  texture references will already be different.
	 */
	
	// Resource system uninitialised, return.
	if (resList == NULL) return;
	
	assert(ref >= 0);
	
	// Fetch the resource with a particular type and reference.
	int resRef;
	dgRes *res = _dgResGetPtrFromRef(type, ref, &resRef);
	assert(res);
	
	// Decrement reference count.
	--(res->numRefs);
	
	// No more references, cull resource.
	// XXX Perhaps only do this if we hit a certain memory threshold.
	if (!res->numRefs)
	{
		//printf("Eek, deleting %s\n", res->fn);
		_dgResDelete(res);
		dgArrayListDeleteItem(resList, resRef);
	}
}

int dgResExists(int type, char *fn, int opts)
{
	dgArrayListForEach(dgRes, res, resList, idx,
			   if (res->type == type &&
			       res->opts == opts &&
			       !strcmp(res->fn, fn))
			   {
				   return true;
			   }
		);
	
	return false;
}
