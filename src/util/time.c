/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>
#include <time.h>
#include <sys/time.h>

/**
 * Get the Unix time (microsecond precision).
 *
 * <b>Example:</b>
 * \code
 * double curTime = dgTime();
 * \endcode
 * @return Time in seconds since the epoch.
 */
double dgTime()
{
	struct timeval time;
	
	gettimeofday(&time, NULL);

	return time.tv_sec + (time.tv_usec / 1e6);
}

/**
 * Get the 24 hour time as a string in the format HH:MM:SS.  
 *
 * Returned string looks like <b>11:39:44</b>, and should be
 * freed to prevent memory leaks.
 *
 * <b>Example:</b>
 * \code
 * char *time = dgTimeOfDayStr();
 *
 * // Prints "11:39:44".
 * puts(time);
 *
 * free(time);
 * \endcode
 * @return The time as a string.
 */
char* dgTimeOfDayStr()
{
	time_t raw_time;
	struct tm* time_info;
	char* buffer;
	
	time(&raw_time);
	time_info = localtime(&raw_time);
	
	dgMalloc(buffer, char, 80);
	strftime(buffer, 80, "%H:%M:%S", time_info);
	
	return buffer;
}
