#include <dagger3d.h>

/*
One problem with worker groups is that memory that is allocated and pushed
onto the job stack will be leaked if the worker group does not consume the entire
stack for some reason.

Some sort of function driving a group needs to be written similar to the 
function that drives a worker.

dgWorkerGroupGetData()
dgWorkerGroupPopData()
dgWorkerGroupGetSignal()

void foo(void *g) {
    void *worker;
    while ((worker = dgWorkerGroupPopData())) {
        ...
    }
}

Basically a worker group will be treated just like a worker, except
with the capability to process multiple jobs simultaneously.

 */

// class WorkerGroup
// {
// private:
// 	void **_workers;
// 	int _num;

// private:
// 	static void funcImpl(void *worker)
// 	{
		
// 	}

// public:
// 	WorkerGroup(void (*func)(void *worker), int num)
// 	{
// 		_num = num;
// 		dgMalloc(_workers, void*, _num);
// 		for (int i = 0; i < _num; ++i) 
// 		{
// 			_workers[i] = dgWorkerNew(func);
// 		}
// 	}

// 	~WorkerGroup()
// 	{
// 		for (int i = 0; i < _num; ++i) 
// 		{
// 			dgWorkerDelete(_workers[i]);
// 		}
// 		dgFree(_workers);
// 	}

// 	void start()
// 	{
		
// 	}
	
// 	void signal(int signal)
// 	{
		
// 	}
	
// 	void event(int event, void (*handler)(void *worker))
// 	{
		
// 	}
	
// 	void data(int slot, void *data)
// 	{
		
// 	}

// 	void push()
// 	{
		
// 	}
// };

// WorkerGroup *_dgWorkerGroupGetPtr(void *group)
// {
// 	return (WorkerGroup*) group;
// }

// void *dgWorkerGroupNew(void (*func)(void *worker), int num)
// {
// 	assert(func);
// 	assert(num > 0);
// 	return new WorkerGroup(func, num);
// }

// void dgWorkerGroupDelete(void *group)
// {
// 	assert(group);
// 	delete _dgWorkerGroupGetPtr(group);
// }

// void dgWorkerGroupStart(void *group)
// {
// 	assert(group);
// 	_dgWorkerGroupGetPtr(group)->start();
// }

// void dgWorkerGroupSetSignal(void *group, int signal)
// {
// 	assert(group);
// 	_dgWorkerGroupGetPtr(group)->signal(signal);
// }

// void dgWorkerGroupSetEvent(void *group, int event, 
// 			   void (*handler)(void *worker))
// {
// 	assert(group);
// 	assert(handler);
// 	_dgWorkerGroupGetPtr(group)->event(event, handler);
// }

// void dgWorkerGroupSetData(void *group, int slot, void *data)
// {
// 	assert(group);
// 	assert(data);
// 	_dgWorkerGroupGetPtr(group)->data(slot, data);
// }

// void dgWorkerGroupPushData(void *group)
// {
// 	assert(group);
// 	_dgWorkerGroupGetPtr(group)->push();
// }

// /*

// func(int worker) => supplied by user
// func2(int worker) => wrapper passed to worker

// The user passes in func(), and data frames.

// Data frames stored in stack.

// func2() pops data frame off stack, and runs func().

//  */
