/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/**
 * Create a formatted string.
 * 
 * This is essentially a more complex version of the stdlib function sprintf().
 * If str is null then this function will create a new string
 * using the format string, and any arguments provided along
 * with it.
 * If str is not null then this function will create a formatted string
 * and append it to the current string (overwriting the original string's
 * NUL terminator.
 *
 * Example usage:
 * \code
 * char *str = NULL;
 *
 * // str == "Hello there Sally.\n"
 * dgStringAppend(&str, "Hello there %s.\n", "Sally");
 * \endcode
 *
 * @param str Original string (can be NULL).
 * @param fmt Formatting string (cannot be NULL).
 * @param ... Variables to splice into the formatting string.
 */
void dgStringAppend(char **str, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	dgStringAppend2(str, fmt, ap);
	va_end(ap);
}


void dgStringAppend2(char **str, const char *fmt, va_list ap)
{
	int len;
	char *ptr;

	assert(fmt != NULL);

	va_list ap2;
	va_copy(ap2, ap);
	len = vsnprintf(NULL, 0, fmt, ap2) + 1;
	va_end(ap2);
	
	if (*str == NULL)
	{
//		*str = malloc(sizeof(char) * len);
		dgMalloc(*str, char, len);
		ptr = *str;
	}

	else
	{
		int orig;

		orig = strlen(*str);
//		*str = realloc(*str, (len + orig + 1) * sizeof(char));
		dgRealloc(*str, char, len + orig + 1);
		ptr = &(*str)[orig];
	}
	
	va_list ap3;
	va_copy(ap3, ap);
	vsnprintf(ptr, len, fmt, ap3);
	va_end(ap3);
}

/**
 * Count the number of characters in a string.
 *
 * Example usage:
 * \code
 * char *str = "Hello, cool!";
 * int count;
 *
 * // count == 3
 * count = dgStringCountChar(str, 'l');
 *
 * \endcode
 *
 * @param str The string.
 * @param ch The character to count.
 * @return The number of times 'ch' appeared in the string.
 */
int dgStringCountChar(char *str, char ch)
{
	int cnt = 0;
	char *ptr = str;

	while (*ptr++ != '\0')
	{
		if (*ptr == ch)
		{
			++cnt;
		}
	}

	return cnt;
}

unsigned long dgStringHash(unsigned char *str)
{
	// http://cboard.cprogramming.com/tech-board/114650-string-hashing-algorithm.html
        unsigned long hash = 5381;
        int c;
	
        while ((c = *str++))
	{
		hash = ((hash << 5) + hash) + c; // hash * 33 + c
//		hash = ((hash << 5) + hash) ^ c; // hash * 33 ^ c
	}

        return hash;
}

char *dgStringDup(const char *s)
{
	const int len = strlen(s);
	return dgStringDup2(s, len);
}

char *dgStringDup2(const char *s, size_t n)
{
	char *cpy;
	dgMalloc(cpy, char, n + 1);
	return memcpy(cpy, s, n);
}

char *dgStringCat(char *str1, char *str2)
{
	assert(str1);
	assert(str2);

	const int len1 = strlen(str1);
	const int len2 = strlen(str2);
	
	char *str3;
	dgMalloc(str3, char, len1 + len2 + 1);
	
	memcpy(str3, str1, sizeof(char) * len1);
	memcpy(str3 + len1, str2, sizeof(char) * len2);
	
	return str3;
}
