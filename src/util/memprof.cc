/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>
#include <map>

#ifndef NO_PTHREAD
#include <pthread.h>
#endif

using std::map;

typedef struct
{
	size_t bytes;
	char *file;
	char *func;
	int line;
} dgMemProfAllocRecord;

static map<void*, dgMemProfAllocRecord*> allocMap;
static size_t bytesAllocated = 0;

#ifndef NO_PTHREAD
static pthread_mutex_t memprof_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

static void eraseRecord(void *ptr)
{
	dgMemProfAllocRecord *record = allocMap[ptr];

	bytesAllocated -= record->bytes;

	free(record->file);
	free(record->func);
	free(record);
	allocMap.erase(ptr);
}

static void createRecord(void *ptr, size_t bytes, const char *file,
			 const char *func, const int line)
{	
	dgMemProfAllocRecord *record = (dgMemProfAllocRecord *) 
		malloc(sizeof(dgMemProfAllocRecord));
	record->bytes = bytes;
	record->file = strdup(file);
	record->func = strdup(func);
	record->line = line;
	allocMap[ptr] = record;
	
	bytesAllocated += bytes;
}

size_t dgMemProfBytesAllocated()
{	
	int bytes;

	#ifndef NO_PTHREAD
	pthread_mutex_lock(&memprof_mutex);
	#endif
	{
		bytes = bytesAllocated;
	}
	#ifndef NO_PTHREAD
	pthread_mutex_unlock(&memprof_mutex);
	#endif

	return bytes;	
}

void dgMemProfPrintRecords()
{
	#ifndef NO_PTHREAD
	pthread_mutex_lock(&memprof_mutex);
	#endif
	{
		for (map<void*,dgMemProfAllocRecord*>::iterator i = allocMap.begin(); 
		     i != allocMap.end(); ++i)
		{
			dgMemProfAllocRecord *r = (*i).second;
			printf("%p\t%lu%s%s:%s:%d\n", 
			       (*i).first, (unsigned long) r->bytes, 
			       (r->bytes >= 10000000 ? "\t" : "\t\t"),
			       r->file, r->func, r->line);
		}
	}
	#ifndef NO_PTHREAD
	pthread_mutex_unlock(&memprof_mutex);
	#endif
}

void *dgMemProfAlloc(size_t bytes, const char *file, const char *func, const int line)
{
	// Allocate memory.
	void *ptr = malloc(bytes);
	memset(ptr, 0, bytes);

	// printf("%s:%s:%d - allocating %d bytes\n", file, func, line, bytes);

	// Create a new record for this allocation and
	// fill in the necessary information.
	#ifndef NO_PTHREAD
	pthread_mutex_lock(&memprof_mutex);
	#endif
	{
		createRecord(ptr, bytes, file, func, line);
	}
	#ifndef NO_PTHREAD
	pthread_mutex_unlock(&memprof_mutex);
	#endif

	return ptr;
}

void *dgMemProfRealloc(void *orig_ptr, size_t bytes, const char *file, 
		       const char *func, const int line)
{
	void *ptr;

	#ifndef NO_PTHREAD
	pthread_mutex_lock(&memprof_mutex);
	#endif
	{
		if (!allocMap[orig_ptr])
		{
			fprintf(stderr, "%s:%s:%d - attempt to realloc memory not allocated by Dagger. "
				"This could indicate a memory leak or a bug, or you may be trying to "
				"use dgRealloc() on a pointer allocated with malloc().\n",
				file, func, line);
		}
		
		else
		{
			eraseRecord(orig_ptr);
		}

		ptr = realloc(orig_ptr, bytes);
		createRecord(ptr, bytes, file, func, line);
	}
	#ifndef NO_PTHREAD
	pthread_mutex_unlock(&memprof_mutex);
	#endif

	return ptr;
}

void dgMemProfFree(void *ptr, const char *file, const char *func, const int line)
{	
	#ifndef NO_PTHREAD
	pthread_mutex_lock(&memprof_mutex);
	#endif
	{
		if (!allocMap[ptr])
		{
			fprintf(stderr, "%s:%s:%d - attempt to free memory not allocated by Dagger. "
				"This could indicate a memory leak or a bug, or you may be trying to "
				"use dgFree() on a pointer allocated with malloc().\n",
				file, func, line);	       
		}
		
		else
		{
			eraseRecord(ptr);
		}

		free(ptr);
	}
	#ifndef NO_PTHREAD
	pthread_mutex_unlock(&memprof_mutex);
	#endif
}
