/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

typedef struct
{
	/** Next id available. */
	int next;
	/** Arraylist of released id's. */
	void *id_list;
} dgUniqueSource;

static void *src_list = NULL;

static void _dgUniqueSourceDelete(dgUniqueSource *src)
{
	assert(src);	

	// Normally we would have to delete each item of the list,
	// but we are storing integer values as pointers which
	// is hackish but makes our job a lot easier.
	dgArrayListDelete(src->id_list);
	
	// Release memory used by source.
	dgFree(src);
}

static void dgUniqueCleanup()
{
	printf("Running unique-id cleanup.\n");

	// For each source...
	dgArrayListForEach(dgUniqueSource, src, src_list, idx,
			   _dgUniqueSourceDelete(src);
		);
	
	// Delete list of sources.
	dgArrayListDelete(src_list);
	src_list = NULL;
}

static void dgUniqueInit()
{
	if (src_list == NULL)
	{
		src_list = dgArrayListNew();
		dgSysRegister(DG_SYS_UNIQUE, dgUniqueCleanup, 0, NULL);
	}
}

static dgUniqueSource *_dgUniqueGetPtr(int src)
{
	assert(src_list != NULL);
	assert(dgArrayListItemExists(src_list, src));
	return dgArrayListGetItem(src_list, src);
}

/**
 * Create a new unique-id source and return a reference.
 *
 * Basically a unique-id source is a generator that manages
 * numbers. The user asks for a number and the generator gives him
 * one. When the user is done with a number he returns it to the source.
 * The key is that the source guarantees that all the numbers it has given
 * to the user are unique, in that it will never give the user the same
 * number twice without the user first releasing the number.
 *
 * This is a useful way to manage references, because it enables
 * reuse of old references (remember we have a finite number of references).
 *
 * @return Reference to a unique-id source.
 */
int dgUniqueSourceNew()
{
	dgUniqueInit();
	
	// Create a new source.
	dgUniqueSource *src;
	dgMalloc(src, dgUniqueSource, 1);
	// List of reused id's.
	src->id_list = dgArrayListNew();
	// XXX We start at 1 because (to avoid lots of malloc/frees)
	// XXX we store ints as void*'s, and NULL is 0. :)
	src->next = 1;

	// Add source to list.
	return dgArrayListAddItem(src_list, src);
}

/**
 * Delete a unique-id source.
 *
 * @param[in] src Reference to the source.
 */
void dgUniqueSourceDelete(int src)
{
	_dgUniqueSourceDelete(_dgUniqueGetPtr(src));
	dgArrayListDeleteItem(src_list, src);
}

/**
 * Get a unique id from a source.
 *
 * @param[in] src Reference to the source.
 * @return Unique id.
 */
int dgUniqueGetId(int src)
{
	dgUniqueSource *s = _dgUniqueGetPtr(src);

	// Check list for free id's.
	const int id = (int) dgArrayListPopItem(s->id_list);       
	// Return id if we got one, otherwise increment counter
	return id ? id : s->next++;
}

/**
 * Release an id that has been given out.
 *
 * @param[in] src Reference to the source.
 * @param[in] id Id to release.
 */
void dgUniqueReleaseId(int src, int id)
{
	dgUniqueSource *s = _dgUniqueGetPtr(src);
	
	// Add released id to list.
	dgArrayListAddItem(s->id_list, (void *) id);
}
