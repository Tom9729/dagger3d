/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/**
   SparseArrayList 

   Overview - This data structure is comparable to Java's ArrayList, with two
   key differences. First, the insertItem() operation is not supported. Second,
   delItem() leaves holes in the array, and adds the indices of the holes to a
   linked-list of open references (eg. openRefs). addItem() then uses this list
   to quickly fill the holes. The major downside to this approach is that as the
   list becomes very sparse (high fragmentation ratio), iteration through it will
   become less efficient (because you will be stepping through a ton of blank
   spaces to get to your data). The solution to this is to provide a sweep()
   operation that essentially defragments the array. It is left up to the
   user to call this operation when appropriate (eg. when the fragmentation
   ratio is high and they wish to iterate).
       Java's ArrayList does not have this fragmentation problem because their
   delItem() operation shifts the succeeding elements to the left immediately to
   fill the hole. This is undesirable when removing a large number of elements,
   which is why Java also provides a bulk-remove operation, although it operates
   on a list of values instead of a list of keys.
       I created this data-structure because I wanted an accesible dynamic-array
   class that I could use in C. Keys or references are returned to the user
   following an addItem() operation, which map to values stored in the array.
   the SAL provides add/remove operations with efficiency comprable to that of 
   a linked list, with better memory locality and random access. Assuming a little
   preprocessing is done first, the SAL is just as efficient as a LL for iteration.
   
   getItem() - As far as we are concerned, this is an array
   so getItem() is O(1) because we already know the address (reference)
   of the element we wish to access.

   delItem() - No shrinking or reordering is done when an item is
   removed, it's reference is simply added to the head of the openRefs
   list which is O(1), therefore this operation is O(1).
   
   addItem() - The first thing we do when adding an item is to check the
   openRefs list, if it is not empty then we remove the first node and
   use the reference stored in it as an index into the array, then we 
   return that index to the user (this is O(1)). If the openRefs list is
   empty then we use the nextInsert as an index into the array - this is O(1). 
   Either way the numElements counter gets incremented. Unfortunately
   if the list is full when we try to add an item
   (eg. numElements = arrayCapacity) then we need to realloc() the arrayData
   which is an expensive O(n) operation. This means that most of the time
   addItem() will be O(1), but worst-case is O(n).
       One very important assumption that is made in the implementation
   is that a NULL pointer indicates an empty spot in the array, and
   because of this the addItem() function implicitly disallows the insertion
   of a NULL.

   sweep() - This operation is necessary because of the inevitable sparseness
   of our array. After many delItem() operations the fragmentation ratio will be high. 
   If we are going to be iterating through the array many times it may be worthwhile 
   to "sweep" elements from right to left to fill up the holes. This operation 
   will empty the openRefs list reducing the fragmentation ratio to zero.
   
   Fragmentation ratio - defined as numOpenRefs / numItems.

   Additional notes - This data structure is not suitable for concurrent access 
   so care must be taken to ensure data integrity. This is left to the user.
       The nextInsert index is the "frontier", meaning that there should never
   be a node index >= nextInsert. sweep() resets nextInsert to one-after the last
   element.
   
   Two use cases 
   1. As a simple key/value map where we don't care what the key is
      - Iteration will be infefficient, sweep() will screw up mapping.
   2. As a dynamic array
*/
   
typedef struct
{
	/** Reference to an open slot in an array. */
	int ref;
	/** Pointer to next node. */
	void *next;
} dgRefList;

typedef struct
{
	/** Array of pointers. */
	void **arrayData;
	/** Length of arrayData in pointers. */
	int arrayCapacity;
	/** Initial length of arrayData (in pointers). */
	int initialCapacity;
	/** Number of elements currently in this array. */
	int numElements;
	/** Index of next insert. */
	int nextInsert;
	/** Linked list of unallocated references. */
	dgRefList *openRefs;
	int numOpenRefs;
} dgArray;

static dgRefList *_dgRefListNew(int ref, dgRefList *next);
static void _dgRefListDelete(dgRefList *next);
static int _dgArrayListGetOpenRef(dgArray *list);

static const int defaultInitialCapacity = 100;

/**
 * Construct a new node for a linked list of references.
 *
 * @param[in] ref Reference to save in node.
 * @param[in] next Pointer to next node in list.
 * @return Pointer to the new node.
 */
static dgRefList *_dgRefListNew(int ref, dgRefList *next)
{
	dgRefList *rlist;
	dgMalloc(rlist, dgRefList, 1);
	rlist->ref = ref;
	rlist->next = next;
	return rlist;
}

/**
 * Delete a linked list of references.
 *
 * @param[in] next Node to delete.
 */
static void _dgRefListDelete(dgRefList *next)
{
	if (next)
	{
		_dgRefListDelete(next->next);
		dgFree(next);
	}
}

static int _dgArrayListPopRefList(dgArray *list)
{
	if (list->openRefs)
	{	
		// Get a reference from the list and then
		// replace it's container with the next one
		// in the list.
		dgRefList *refNode = list->openRefs;		
		const int ref = refNode->ref;
		--(list->numOpenRefs);

		// Link next node in place.
		list->openRefs = refNode->next;
		// Delete old node.
		dgFree(refNode);		
		
		return ref;
	}
	
	return -1;
}

static int _dgArrayListGetOpenRef(dgArray *list)
{
	assert(list);

	// Check for open references.
	if (list->openRefs)
	{
		return _dgArrayListPopRefList(list);
	}
	
	// Do we need to resize the array?
	else if (list->nextInsert == list->arrayCapacity)
	{
		// Double array capacity.
		list->arrayCapacity *= 2;
		dgRealloc(list->arrayData, void*, list->arrayCapacity);
	}

	return list->nextInsert++;
}

void *dgArrayListNew()
{
	return dgArrayListNew2(defaultInitialCapacity);
}

void *dgArrayListNew2(int initialCapacity)
{
	assert(initialCapacity > 0);
	
	// Alloc and zero memory for array structure
	// and it's underlying members (including the interal
	// backing array).
	dgDeclMalloc(arr, dgArray, 1);
	dgMalloc(arr->arrayData, void*, initialCapacity);
	arr->initialCapacity = initialCapacity;
	arr->arrayCapacity = initialCapacity;
	arr->openRefs = NULL;
	
	return arr;
}

void dgArrayListDelete(void *list)
{
	assert(list);
	dgArray *_list = (dgArray *) list;	
	_dgRefListDelete(_list->openRefs);
	dgFree(_list->arrayData);
	dgFree(_list);
}

int dgArrayListSize(void *list)
{	
	assert(list);
	dgArray *_list = (dgArray *) list;
	return _list->numElements;
}

int dgArrayListCapacity(void *list)
{	
	assert(list);
	dgArray *_list = (dgArray *) list;
	return _list->arrayCapacity;
}

void dgArrayListSort(void *list, int (*compare)(const void *, const void *))
{
	//dgArray *_list = (dgArray *) list;
	dgPrintf("%s() not implemented\n", __func__);
}

int dgArrayListAddItem(void *list, void *ptr)
{
	assert(list);
	assert(ptr);
	
	dgArray *_list = (dgArray *) list;	
	
	// Find a place to insert this item, resizing the
	// list if necessary!
	const int ref = _dgArrayListGetOpenRef(_list);
	_list->arrayData[ref] = ptr;
	++(_list->numElements);
	
	return ref;
}

void *dgArrayListDeleteItem(void *list, int ref)
{
	assert(list);
	assert(ref >= 0);
	
	dgArray *_list = (dgArray *) list;	
	assert(ref < _list->arrayCapacity);
	
	// Eek, looks like there is nothing here.
	if (!_list->arrayData[ref])
	{
		return NULL;
	}
	
	// Save pointer to item.
	void *item = _list->arrayData[ref];
	_list->arrayData[ref] = NULL;
	--(_list->numElements);
	
	// Add now-available reference to the list of open references.
	_list->openRefs = _dgRefListNew(ref, _list->openRefs);
	++(_list->numOpenRefs);
	
	return item;
}

void *dgArrayListGetItem(void *list, int ref)
{
	assert(list);
	assert(ref >= 0);
	dgArray *_list = (dgArray *) list;
	assert(ref < _list->arrayCapacity);
	return _list->arrayData[ref];
}

int dgArrayListFindItem(void *list, void *ptr)
{
	dgArrayListForEach(void, ptr2, list, idx,
			   if (ptr2 == ptr)
			   {
				   return idx;
			   }
		);

	return -1;
}

void *dgArrayListPopItem(void *list)
{
	dgArrayListForEach(void, ptr, list, idx,
			   if (ptr)
			   {
				   dgArrayListDeleteItem(list, idx);
				   return ptr;
			   }
		);
	
	return NULL;
}

int dgArrayListItemExists(void *list, int ref)
{
	return (dgArrayListGetItem(list, ref) != NULL);
}

void dgArrayListSweep(void *list)
{
	assert(list);
	dgArray *_list = (dgArray *) list;
	
	// For each open reference
	for (int ref = _dgArrayListPopRefList(_list); ref != -1; 
	     ref = _dgArrayListPopRefList(_list))
	{
		// Find next item (going backwards)
		while (!_list->arrayData[_list->nextInsert]) --_list->nextInsert;
		
		// Ignore references that are past our frontier
		if (ref > _list->nextInsert) continue;

		_list->arrayData[ref] = _list->arrayData[_list->nextInsert];
		_list->arrayData[_list->nextInsert] = NULL;
	}
}

void *dgArrayListSetItem(void *list, int ref, void *ptr)
{	
	assert(list);
	assert(ref >= 0);
	assert(ptr);
	dgArray *_list = (dgArray *) list;
	assert(_list->arrayData[ref]);
	
	void *old_ptr = _list->arrayData[ref];
	_list->arrayData[ref] = ptr;
	return old_ptr;
}

float dgArrayListFragRatio(void *list)
{	
	assert(list);
	dgArray *_list = (dgArray *) list;
	return (float) _list->numOpenRefs / _list->nextInsert;
}

void dgArrayListTrim(void *list)
{
	assert(list);
	dgArray *_list = (dgArray *) list;

	_list->arrayCapacity = _list->nextInsert;
	dgRealloc(_list->arrayData, void*, _list->arrayCapacity);
}
