#include <dagger3d.h>

static char *_dgSysGetUserDir()
{
	static char userDir[4096];
	static bool userDirSet = false;

	if (!userDirSet)
	{
		getcwd(userDir, 4096);
		userDirSet = true;
	}

	return userDir;
}

static char *_dgSysGetBinDir()
{
	static char binDir[4096];
	static bool binDirSet = false;

	if (!binDirSet)
	{
		int argc;
		char **argv;
		dgInitGetArgs(&argc, &argv);

		size_t pathlen;
		
                #ifdef WIN32
		GetFullPathNameA(argv[0], 4096, binDir, NULL);
		pathlen = (strrchr(binDir, '\\') - binDir) + 1;
		#else
		realpath(argv[0], binDir);
		pathlen = (strrchr(binDir, '/') - binDir) + 1;
		#endif
		
		binDir[pathlen] = 0;
		binDirSet = true;
	}

	return binDir;
}

/**
 * Change the CWD (Current Working Directory). This effects file
 * loading.
 *
 * @param[in] b If true, set the current working directory to
 *              the binary's directory. Otherwise use the user's CWD.
 */
void dgSysBinCwd(int b)
{	
	//printf("%s\n%s\n", _dgSysGetUserDir(), _dgSysGetBinDir());
	
	// Cache and store the original path before we change it.
	static bool first = true;
	if (first) {
		_dgSysGetUserDir();
		first = false;
	}

	chdir((b ? _dgSysGetBinDir() : _dgSysGetUserDir()));
}

#include <set>
#include <map>

using std::set;
using std::map;

typedef int Edge;

class Node
{
public:
	void (*cleanup_func)();
	set<Edge> edges_out, edges_in;

public:
	Node()
	{
		cleanup_func = NULL;
	}
};

typedef map<Edge, Node> Digraph;
static Digraph init_graph;

void dgSysRegister(int name, void (*cleanup)(), int ndeps, int *deps)
{
	Digraph &graph = init_graph;
	
	// Attach cleanup function to this node.
	graph[name].cleanup_func = cleanup;

	for (int i = 0; i < ndeps; ++i)
	{		
		// Create an edge between this node and it's dependency.
		graph[name].edges_out.insert(deps[i]);
		graph[deps[i]].edges_in.insert(name);
	}
}

void dgSysCleanup()
{
	Digraph &graph = init_graph;
	
	while (!graph.empty())
	{		
		bool cycle = true;
		
		// For each node in graph.
		for (Digraph::iterator i = graph.begin(); i != graph.end(); ++i)
		{	
			Node &node = graph[(*i).first];
			
			// If there are no edges coming into this node,
			// then we can safely delete it because no one
			// depends on it.
			if (node.edges_in.empty())
			{
				// Call cleanup function.
				if (node.cleanup_func) node.cleanup_func();
				
				// Remove this node from the edge-in lists of
				// all the nodes it has edges going to. Basically,
				// unlink this node from the graph.
				set<Edge> &edges_out = node.edges_out;
				for (set<Edge>::iterator j = edges_out.begin();
					 j != edges_out.end(); ++j)
				{
					graph[(*j)].edges_in.erase((*i).first);
				}
				
				// Remove this node from the graph.
				graph.erase(i);
				cycle = false;
				break;
			}
		}	

		if (cycle)
		{
			dgPrintf("warning: circular dependencies detected, "
				 "aborting cleanup\n");
			graph.clear();
		}
	}	
}

void dgPrintf(const char *fmt, ...)
{
	va_list ap;
	char *str = NULL;
	
	va_start(ap, fmt);
	dgStringAppend(&str, "[%s:%d] ", __FILE__, __LINE__);
	dgStringAppend2(&str, fmt, ap);
	va_end(ap);
	
	fputs(str, stdout);
	dgFree(str);
}
