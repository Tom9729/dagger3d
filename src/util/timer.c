/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

typedef struct
{
	// Seconds this timer is set for.
	double sleep;
	// When this timer was started, or reset.
	double start;
} dgTimer;

/**
 * Start a microsecond-precision timer.
 *
 * A microsecond is 1/1000000 of a second.
 *
 * @param[in] sleep Duration this timer is set for, in seconds.
 * @return Timer.
 */
void *dgTimerNew(double sleep)
{
	dgTimer *t;
	
	dgMalloc(t, dgTimer, 1);
	t->sleep = sleep;
	t->start = dgTime();

	return t;
}

/**
 * Delete a timer.
 *
 * @param[in] Timer.
 */
void dgTimerDelete(void *timer)
{
	assert(timer);
	dgFree(timer);
}

/**
 * Check if a timer is ready, and reset it if it is.
 *
 * @param[in] timer
 * @return True if the time this timer was set for is up, otherwise false.
 */
int dgTimerReady(void *timer)
{
	dgTimer *t = (dgTimer *) timer;
	const double now = dgTime();

	if (now - t->start >= t->sleep)
	{
		t->start = now;
		return true;
	}
	
	return false;
}

/**
 * Check how much time is left until a timer goes off.
 *
 * @param[in] timer
 * @return Seconds until timer goes off.
 */
double dgTimerRemaining(void *timer)
{
	dgTimer *t = (dgTimer *) timer;
	const double delta = dgTime() - t->start;
	return (delta > 0 ? delta : 0);
}
