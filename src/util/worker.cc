#include <dagger3d.h>
#include <errno.h>
#include <map>

// using std::map;

// class Worker
// {
// private:
// 	pthread_t _thread;
// 	pthread_mutex_t _mutex;
// 	pthread_cond_t _cond;
// 	void (*_func)(void *worker);
// 	int _signal_in, _signal_out;
// 	union _data_u {
// 		int idata;
// 		float fdata;
// 		void *pdata;
// 	} _data[DG_WORKER_MAX_DATA];
// 	map<int,void(*)(void*)> _events;
// 	bool _active;

// private:
// 	static void *threadImpl(void *arg)
// 	{
// 		// Get a pointer to the worker.
// 		Worker *w = (Worker*) arg;
		
// 		// Call worker's func()
// 		w->exec();
		
// 		// Set worker inactive, then notify
// 		// anyone waiting on us.
// 		w->lock();
// 		w->active(false);
// 		w->notify();
// 		w->unlock();
		
// 		return NULL;
// 	}

// public:
// 	Worker(void (*func)(void *worker))
// 	{
// 		pthread_cond_init(&_cond, NULL);
// 		pthread_mutexattr_t attr;
// 		pthread_mutexattr_init(&attr);
// 		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
// 		pthread_mutex_init(&_mutex, &attr);
// 		pthread_mutexattr_destroy(&attr);
		
// 		_func = func;
// 		_signal_in = _signal_out = DG_WORKER_SIGNAL_NONE;
// 		memset(_data, 0, sizeof(_data_u) * DG_WORKER_MAX_DATA);
// 		_active = false;
// 	}
	
// 	~Worker()
// 	{      
// 		// Signal thread to stop, then wait.
// 		// If thread is not running then wait()
// 		// will return immediately.
// 		signal(DG_WORKER_SIGNAL_STOP);
// 		wait();
		
// 		pthread_mutex_destroy(&_mutex);
// 		pthread_cond_destroy(&_cond);
// 	}
	
// 	void exec() { _func(this); }
// 	void lock() { pthread_mutex_lock(&_mutex); }	
// 	void unlock() { pthread_mutex_unlock(&_mutex); }

// 	void active(bool active)
// 	{
// 		lock();
// 		_active = active;
// 		unlock();
// 	}

// 	bool active()
// 	{
// 		lock();
// 		bool active = _active;
// 		unlock();
// 		return active;
// 	}

// 	void start()
// 	{		
// 		if (active()) 
// 		{
// 			// Thread is already running.
// 			return;
// 		}
		
// 		// Create detached threads so that we don't need
// 		// to do any cleanup when they exit.
// 		pthread_attr_t attr;
// 		pthread_attr_init(&attr);
// 		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
// 		pthread_create(&_thread, &attr, Worker::threadImpl, this);
// 		pthread_attr_destroy(&attr);
// 		active(true);
// 	}

// 	void signal(int signal)
// 	{
// 		lock();
		
// 		// If the calling thread is the worker, then
// 		// set the output signal.
// 		if (pthread_equal(pthread_self(), _thread))
// 		{
// 			_signal_out = signal;
// 		}
		
// 		// Otherwise set the input signal.
// 		else
// 		{
// 			_signal_in = signal;
// 		}

// 		unlock();
// 	}

// 	int signal()
// 	{
// 		int signal;

// 		lock();
// 		// If the calling thread is the worker, then
// 		// return the input signal.
// 		if (pthread_equal(pthread_self(), _thread))
// 		{
// 			signal = _signal_in;
// 			_signal_in = DG_WORKER_SIGNAL_NONE;
// 		}
		
// 		// Otherwise return the output signal.
// 		else
// 		{
// 			signal = _signal_out;
// 			_signal_out = DG_WORKER_SIGNAL_NONE;
// 		}

// 		unlock();
		
// 		return signal;
// 	}

// 	void data(int slot, void *data)
// 	{
// 		assert(slot >= 0);
// 		assert(slot < DG_WORKER_MAX_DATA);

// 		lock();
// 		_data[slot].pdata = data;
// 		unlock();
// 	}
	
// 	void dataInt(int slot, int data)
// 	{
// 		assert(slot >= 0);
// 		assert(slot < DG_WORKER_MAX_DATA);

// 		lock();
// 		_data[slot].idata = data;
// 		unlock();
// 	}

// 	void dataFloat(int slot, float data)
// 	{
// 		assert(slot >= 0);
// 		assert(slot < DG_WORKER_MAX_DATA);

// 		lock();
// 		_data[slot].fdata = data;
// 		unlock();
// 	}
	
// 	void *data(int slot)
// 	{
// 		assert(slot >= 0);
// 		assert(slot < DG_WORKER_MAX_DATA);
		
// 		lock();
// 		void *data = _data[slot].pdata;
// 		_data[slot].pdata = NULL;
// 		unlock();
// 		return data;
// 	}

// 	int dataInt(int slot)
// 	{
// 		assert(slot >= 0);
// 		assert(slot < DG_WORKER_MAX_DATA);
		
// 		lock();
// 		int data = _data[slot].idata;
// 		_data[slot].idata = NULL;
// 		unlock();
// 		return data;
// 	}

// 	float dataFloat(int slot)
// 	{
// 		assert(slot >= 0);
// 		assert(slot < DG_WORKER_MAX_DATA);
		
// 		lock();
// 		float data = _data[slot].fdata;
// 		_data[slot].fdata = NULL;
// 		unlock();
// 		return data;
// 	}
	
// 	void event(int event, void (*handler)(void *worker))
// 	{
// 		lock();
// 		_events[event] = handler;
// 		unlock();
// 	}
	
// 	void event(int event)
// 	{
// 		lock();
// 		map<int,void(*)(void*)>::iterator it = _events.find(event);
// 		if (it != _events.end()) (it->second)(this);
// 		unlock();
// 	}
	
// 	void wait()
// 	{
// 		lock();
// 		if (active()) { pthread_cond_wait(&_cond, &_mutex); }
// 		unlock();
// 	}
	
// 	void notify()
// 	{
// 		lock();
// 		pthread_cond_signal(&_cond);
// 		unlock();
// 	}
// };

// Worker *_dgWorkerGetPtr(void *worker)
// {
// 	return (Worker*) worker;
// }

// void *dgWorkerNew(void (*func)(void *worker))
// {
// 	assert(func);
// 	return new Worker(func);
// }

// void dgWorkerDelete(void *worker)
// {
// 	assert(worker);
// 	delete _dgWorkerGetPtr(worker);
// }

// void dgWorkerStart(void *worker)
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->start();
// }

// void dgWorkerSetSignal(void *worker, int signal)
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->signal(signal);
// }

// int dgWorkerGetSignal(void *worker)
// {
// 	assert(worker);
// 	return _dgWorkerGetPtr(worker)->signal();
// }

// void dgWorkerSetData(void *worker, int slot, void *data)
// {
// 	assert(worker);
// 	assert(data);
// 	_dgWorkerGetPtr(worker)->data(slot, data);
// }

// void dgWorkerSetData(void *worker, int slot, int data)
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->dataInt(slot, data);
// }

// void dgWorkerSetData(void *worker, int slot, float data)
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->dataFloat(slot, data);
// }

// void *dgWorkerGetData(void *worker, int slot)
// {
// 	assert(worker);
// 	return _dgWorkerGetPtr(worker)->data(slot);
// }

// int dgWorkerGetDataInt(void *worker, int slot)
// {
// 	assert(worker);
// 	return _dgWorkerGetPtr(worker)->dataInt(slot);
// }

// float dgWorkerGetDataFloat(void *worker, int slot)
// {
// 	assert(worker);
// 	return _dgWorkerGetPtr(worker)->dataFloat(slot);
// }

// void dgWorkerWait(void *worker)
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->wait();
// }

// void dgWorkerNotify(void *worker)
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->notify();
// }

// void dgWorkerSetEvent(void *worker, int event, 
// 		      void (*handler)(void *worker))
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->event(event, handler);
// }

// void dgWorkerTriggerEvent(void *worker, int event)
// {
// 	assert(worker);
// 	_dgWorkerGetPtr(worker)->event(event);
// }
