/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <ft2build.h>

#include <freetype2/freetype.h>
#include <freetype2/ftglyph.h>
#include <freetype2/ftoutln.h>
#include <freetype2/fttrigon.h>

#include <dagger3d.h>

#ifndef _DOXYGEN_
// Embed a truetype font so that we can use it for stuff like
// FPS display without making the user have to worry about distributing
// yet another file with all their applications.
static unsigned char system_font[] = {
  //	#include "../../res/fonts/DejaVuSans.ttf.h"
      #include "../../res/fonts/chemreb.ttf.h"
  //    #include "../../res/fonts/slicker.ttf.h"
  //    #include "../../res/fonts/walkway.ttf.h"
  //    #include "../../res/fonts/FortuneCity.ttf.h"
};
#endif

static int system_font_ref = -1;

// See NeHe Lesson 43

typedef struct
{
	// Height of this font.
	float height;
	// Individual widths (in pixels) of
	// each character in this fontset.
	int *widths;
	// Array of textures (one per character).
	GLuint *textures;
	GLuint listBase;
} dgFont;

static void *list = NULL;

/**
 * Get a reference to the internal system font.
 * The system font is embedded into Dagger at compile-time.
 *
 * @return Reference to the system font.
 */
int dgFontGetSystemFont()
{
	if (system_font_ref == -1)
	{
		system_font_ref = dgFontLoadMemory(system_font, 12,
						   sizeof(system_font));
	}
	
	return system_font_ref;
}

/**
 * Delete a font from memory.
 *
 * @param font Dagger font reference.
 */
void dgFontDelete(int font)
{
	dgFont *f;

	assert(list != NULL);
	assert(dgArrayListItemExists(list, font));

	f = dgArrayListDeleteItem(list, font);
	glDeleteLists(f->listBase, 128);
	glDeleteTextures(128, f->textures);
	dgFree(f->widths);
	dgFree(f->textures);
	dgFree(f);
}

static void dgFontCleanup()
{
	//printf("Running font cleanup.\n");
	assert(list);
	
	dgArrayListForEach(dgFont, f, list, idx,
			   dgFontDelete(idx);
		);

	dgArrayListDelete(list);
	list = NULL;
}

static const enum { DG_FONT_FILE, DG_FONT_MEMORY } dgFontSource;

static int dgFontLoadImpl(char *p, unsigned int height, int size, int source)
{
	FT_Library library;
	FT_Face face;
	dgFont *f;
	
	assert(source == 0 || source ==1);	
	
	if (list == NULL)
	{
		list = dgArrayListNew();
		
		// Anything that uses GL/GLUT depends on 'gfx/window.c'.
		int deps[] = { DG_SYS_WINDOW };
		dgSysRegister(DG_SYS_FONT, dgFontCleanup, 
			      sizeof(deps) / sizeof(int), deps);
	}
	
	if (FT_Init_FreeType(&library))
	{
		fprintf(stderr, "Problem initializing" 
			"FreeType library.\n");
		return -1;
	}

	switch (source)
	{
	case DG_FONT_FILE:
		if (FT_New_Face(library, p, 0, &face))
		{
			fprintf(stderr, "Problem loading font (%s).\n", 
				p);
			FT_Done_FreeType(library);
			return -1;
		}
		break;

	case DG_FONT_MEMORY:
		if (FT_New_Memory_Face(library, (unsigned char *) p, 
				       size, 0, &face))
		{
			fprintf(stderr, "Problem loading font (%p).\n", 
				p);
			FT_Done_FreeType(library);
			return -1;
		}
		break;

	default:
		// This shouldn't happen.
		assert(true);
		break;
	}
	
	// Allocate memory for font and list of
	// textures to use to store glyphs.
	dgMalloc(f, dgFont, 1);
	dgMalloc(f->textures, GLuint, 128);
	dgMalloc(f->widths, int, 128);
	f->height = height;

	// Get next-power-of-two of original height.
	//const int origHeightNPOT = dgMathNextPowerOfTwo(f->height);

	// FreeType measures font sizes in 1/64th's of a pixel,
	// so multiply by 64 (shift left 6 bits).
	height <<= 6;
	
	FT_Set_Char_Size(face, height, height, 96, 96);
	f->listBase = glGenLists(128);
	glGenTextures(128, f->textures);

	// Generate a display list for each character.
	for (unsigned char i = 0; i < 128; ++i)
	{
		/* // Create a display list for each character */
		/* // and save the width. */
		/* f->widths[ch] = dgFontMakeDisplayList(face, ch, f->listBase,  */
		/* 				      f->textures); */

		FT_Glyph glyph;
		FT_BitmapGlyph bitmap_glyph;
		FT_Bitmap *bitmap;

		if (FT_Load_Glyph(face, FT_Get_Char_Index(face, i), FT_LOAD_DEFAULT))
		{
			fprintf(stderr, "Error: loading glyph for character (%c).\n", (char) i);
			continue;
		}

		// Render glyph to bitmap.
		FT_Get_Glyph(face->glyph, &glyph);
		FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, 1);
		bitmap = &(bitmap_glyph = (FT_BitmapGlyph) glyph)->bitmap;
		
		const int width = dgMathNextPowerOfTwo(bitmap->width);
		const int height = dgMathNextPowerOfTwo(bitmap->rows);
		const int size = width * height * 2;

		GLubyte *edata;
		dgMalloc(edata, GLubyte, size);

		// Copy over the bitmap that FreeType rendered for us.
		for (int j = 0; j < bitmap->rows; ++j)
		{
			for (int k = 0; k < bitmap->width; ++k)
			{
				const int size = 2 * (k + j * width);
				edata[size] = bitmap->buffer[k + bitmap->width * j];
				edata[size + 1] = edata[size];
			}
		}

		// Save glyph to a texture.
		glBindTexture(GL_TEXTURE_2D, f->textures[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, 
			     GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, edata);

		// Delete local data.
		dgFree(edata);

		// Save bitmap width (pixels).
		f->widths[i] = face->glyph->advance.x >> 6;

		// Get texture coords for JUST the character (exclude padding).
		const float x = (float) bitmap->width / (float) width;
		const float y = (float) bitmap->rows / (float) height;

		glNewList(f->listBase + i, GL_COMPILE);
		glBindTexture(GL_TEXTURE_2D, f->textures[i]);			
		
		// Translate right to leave space between this character
		// and the next one.
		glTranslatef(bitmap_glyph->left, 0.0f, 0.0f);
		
		glPushMatrix();	
		
		glTranslatef(0, (bitmap_glyph->top - bitmap->rows), 0);
		
		const float verts[] = {
			0, bitmap->rows,
			0, 0,
			bitmap->width, 0,
			bitmap->width, bitmap->rows
		};
		
		const float texCoords[] = {
			0, 0,
			0, y,
			x, y,
			x, 0
		};
		
		// Render quad.
		glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS);
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glVertexPointer(2, GL_FLOAT, 0, verts);
		glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
		glDrawArrays(GL_QUADS, 0, 4);
		glPopClientAttrib();
		
		glPopMatrix();

		// Space the fonts.
		glTranslatef(face->glyph->advance.x >> 6, 0, 0);

		glBindTexture(GL_TEXTURE_2D, 0);
		glEndList();
		
		FT_Done_Glyph(glyph);
	}
	
	// Cleanup!
	FT_Done_Face(face);
	FT_Done_FreeType(library);
	
	return dgArrayListAddItem(list, f);
}

/**
 * Load a TrueType font and return a Dagger font reference.
 *
 * <b>Example:</b><br>
 * <pre>
 * int font = dgFontLoad("example.ttf", 12);
 * </pre>
 *
 * @param fname Filename of the font to load.
 * @param height Font size (in pixels).
 * @return Reference to a Dagger font.
 */
int dgFontLoad(char *fname, unsigned int height)
{
	assert(fname);
	
	dgPrintf("msg=loading font, fn=%s\n", fname);
	return dgFontLoadImpl(fname, height, 0, DG_FONT_FILE);
}

/**
 * Load a TrueType font from memory and return a Dagger font reference.
 *
 * @param buf Pointer to buffer containing TrueType font verbatim.
 * @param height Font size (in pixels).
 * @return Reference to a Dagger font.
 */
int dgFontLoadMemory(unsigned char *buf, unsigned int height, int size)
{
	assert(buf);
	assert(size > 0);
	
	dgPrintf("msg=loading font from memory, addr=%p\n", buf);
	return dgFontLoadImpl((char *) buf, height, size, DG_FONT_MEMORY);
}

struct Substr
{
	char *from_str, *to_str;
	float spacing;
};

static void Substr_new(struct Substr *_this, char *from_str, dgFont *fnt)
{
	_this->from_str = from_str;
	// Spacing is partially a magic number from
	// the NeHe tutorial. Figure out a way to do
	// this that looks good with all fonts.
	_this->spacing = fnt->height / 0.63f;
}

static void Substr_to(struct Substr *_this, char *to_str)
{
	_this->to_str = to_str;
}

static void Substr_print(struct Substr *_this)
{
	glPushMatrix();
	glCallLists(_this->to_str - _this->from_str, 
		    GL_UNSIGNED_BYTE, _this->from_str);	
	glPopMatrix();
	glTranslatef(0, -_this->spacing, 0);
}

/**
 * Print a string on the screen.
 * 
 * @param[in] font Dagger font to use.
 * @param[in] x
 * @param[in] y
 * @param[in] wrap Wrap to the next line after this many pixels.
 * @param[in] str String to print.
 * @return Lines written.
 */
int dgFontPrint(int font, float x, float y, unsigned int wrap, char *str)
{	
	assert(list != NULL);
	assert(str);
	assert(dgArrayListItemExists(list, font));
	assert(wrap >= 0);

	dgFont *f = dgArrayListGetItem(list, font);
	
	glPushMatrix();
	glLoadIdentity();

	// Switch to screen coordinates where (0,0) the origin, is
	// the lower left corner of the window.
	dgWindowCoordinateMode(DG_WINDOW_MODE_SCREEN);
	
	glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | 
		     GL_ENABLE_BIT | GL_TRANSFORM_BIT |
		     GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT | 
		     GL_POLYGON_BIT | GL_TEXTURE_BIT);
	
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glListBase(f->listBase);

	// Turn off wireframe mode.
	dgWindowSetWireframe(false);

	glTranslatef(x, y, 0);

	
	
	int linesWritten = 0;
	
	// Until we reach the end of the string...
	for (int areWeThereYet = false; !areWeThereYet; )
	{
		// Start a substring here.
	 	struct Substr substr;
		Substr_new(&substr, str, f);
		// Pointer to the last space we found,
		// or NULL if we have yet to find one.
		char *last_space = NULL;
		// Running substr length in pixels.
		int len_px = 0;

		// For each character in the string from the 
		// base of the substring to the end of the string.
		for (char *ci = str; *ci != '\0'; ++ci)
		{			
			// Save this position if it is a space.
			last_space = (*ci == ' ' ? ci : last_space);
				
			// Increment running length.
			len_px += f->widths[(int) *ci];
			
			// If we hit the wrap length.
			if (wrap && len_px >= wrap)
			{				
				// If haven't seen a space yet then we are midword.
				last_space = (!last_space ? ci : last_space + 1);
				
				// Extend the substring to this point, then print.
				Substr_to(&substr, last_space);
				Substr_print(&substr);
				str = last_space;

				++linesWritten;

				break;
			}		 

			// If we hit the end of the string.
			else if (*(ci + 1) == '\0')
			{
				Substr_to(&substr, ci + 1);
				Substr_print(&substr);
				len_px = 0;
				areWeThereYet = true;

				++linesWritten;

				break;
			}

			// If we hit a newline.
			else if (*ci == '\n')
			{
				// Extend the substring to this point, then print.
				Substr_to(&substr, ci);
				Substr_print(&substr);
				str = ci + 1;

				++linesWritten;

				break;
			}
		}
	}
	
	glPopAttrib();
	dgWindowCoordinateMode(DG_WINDOW_MODE_WORLD);
	glPopMatrix();

	return linesWritten;

	// ------------------
/*	

	// Running line length in characters.
	int rlen = 0;
	// Pointer to the current line.
	char *line = str;
	// Y-offset
	int yo = 0;
	// Running line length in pixels.
	int rlen_px = 0;

	// Go through the string char by char.
	for (char *s = str; *s != '\0'; ++s)
	{
		// Advance running length by one character.
		++rlen;
		// Advance running length by the number
		// of pixels in the current character.
		rlen_px += f->widths[(int) *s] + 1;
		
		// Replace tabs with spaces.
		if (!fancyText && *s == '\t')
		{
			*s = ' ';
		}

		glPushMatrix();

		// Print a line.
		if (*s == '\n' || // Found a new line char.
		    *(s + 1) == '\0' || // Found the end of the string.
		    (*s == ' ' &&rlen_px > wrap)) // Word wrap, only wrap on spaces.
		{
			// Don't print the newline character.
			if (*s == '\n')
			{
				rlen = rlen - 1;
			}		      
						
			glTranslatef(x, y - f->height * 1.5 * yo++, 0);

			// If fancy text is disabled, do this the old
			// fashioned way...
			if (!fancyText)
			{
				glCallLists(rlen, GL_UNSIGNED_BYTE, line);
			}
			
			// Otherwise print text with pretty colors! And tab
			// support!
			else
			{
				// Print the line character by character.
				for (int i = 0; i < rlen; ++i)
				{
					// If we found part of a color code...
					if (line[i] == '^' && (i == 0 || line[i - 1] != '^'))
					{
						// It was a color code!
						if ((i + 1 != rlen) && isdigit(line[i + 1]))
						{
							char color = line[i + 1];
							
							switch (color)
							{
							case '1':
								glColor3ub(255, 0, 0);
								break;
							case '2':
								glColor3ub(0, 255, 0);
								break;
							case '3':
								glColor3ub(255, 255, 0);
								break;
							case '4':
								glColor3ub(70, 130, 180);
								break;
							case '5':
								glColor3ub(32, 178, 170);
								break;
							case '6':
								glColor3ub(176, 48, 96);
								break;
							case '7':
								glColor3ub(0, 0, 0);
								break;
							case '8':
								glColor3ub(139, 131, 120);
								break;
							case '9':
								glColor3ub(255, 255, 255);
								break;
							case '0':
								glColor3ub(255, 165, 0);
								break;
							}
							
							// Skip the next two characters.
							++i;
						}
					}
					
					// Found a tab, print four spaces.
					else if (line[i] == '\t')
					{
						static const char tabs[8] = { ' ', ' ', ' ', ' ',
									      ' ', ' ', ' ', ' ' };
						glCallLists(8, GL_UNSIGNED_BYTE, tabs);
					}
					
					// Otherwise print the current character.
					else
					{
						glCallLists(1, GL_UNSIGNED_BYTE, &line[i]);
					}
				}
			}					
			
			rlen_px = 0;
			rlen = 0;
			line = s + 1;
		}

		glPopMatrix(); 
	}

	// Restore saved attributes.
	glPopAttrib();

	// Switch back to world coordinates.
	dgWindowCoordinateMode(DG_WINDOW_MODE_WORLD);
	glPopMatrix();
	
	return yo;
*/
}

/**
 * Get the approximate height of a loaded font.
 *
 * @param font Font reference.
 * @return Approximate height of the font in pixels.
 */
float dgFontHeight(int font)
{
	dgFont *f;

	assert(list != NULL);
	assert(dgArrayListItemExists(list, font));

	// Get the font.
	f = dgArrayListGetItem(list, font);

	/* // Return the height of the font + a little spacing. */
	/* return f->height * 1.5; */
	return f->height;
}
