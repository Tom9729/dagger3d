/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// @cond
#define GL_GLEXT_PROTOTYPES
// @endcond

#include <dagger3d/gfx/light.h>
#include <dagger3d/compat/glext.h>
#include <dagger3d/mesh/mesh.h>
#include <dagger3d/mesh/animation.h>
#include <dagger3d.h>

static void *list = NULL;

static void dgMeshCleanup()
{
	//printf("Running mesh cleanup.\n");

	// For each mesh in the list...
	dgArrayListForEach(dgMesh, m, list, idx,
			   dgMeshDelete(idx);
		);

	dgArrayListDelete(list);
	list = NULL;
}

static void dgMeshInit()
{
	if (list == NULL)
	{
		//printf("Initializing mesh system.\n");

		list = dgArrayListNew();
		
		// Anything that uses GL/GLUT depends on 'gfx/window.c'.
		int deps[] = { DG_SYS_WINDOW };
		dgSysRegister(DG_SYS_MESH, dgMeshCleanup, 
			       sizeof(deps) / sizeof(int), deps);
	}
}

/**
 * Get a list of all loaded meshes.
 *
 * @return List of meshes.
 */
void *_dgMeshGetList()
{
	return list;
}

/**
 * Set the filename of a mesh. This is used for
 * caching purposes.
 * 
 * @param mesh Mesh reference.
 * @param fn Filename of the mesh.
 */
void dgMeshSetFilename(int mesh, char *fn)
{
	assert(list != NULL);
	assert(fn != NULL);
	assert(dgArrayListItemExists(list, mesh));

	dgMesh *m = dgArrayListGetItem(list, mesh);

	dgCondFree(m->fn.mesh);
	m->fn.mesh = dgStringDup(fn);
}

/**
 * Get the filename associated with a mesh.
 *
 * @param mesh Mesh reference.
 */
char *dgMeshGetFilename(int mesh)
{
	assert(list != NULL);
	assert(dgArrayListItemExists(list, mesh));

	return ((dgMesh *) dgArrayListGetItem(list, mesh))->fn.mesh;
}

/**
 * Create a new mesh structure.
 *
 * @return Mesh reference.
 */
int dgMeshNew()
{
	dgMeshInit();
	
	dgMesh *m;
	dgMalloc(m, dgMesh, 1);

	m->animations = dgArrayListNew();

	return dgArrayListAddItem(list, m);
}

/**
 * Dereference and possibly delete a mesh.
 *
 * A mesh will be deleted permanently when it runs out
 * of references.
 *
 * @param ref Mesh reference.
 */
void dgMeshDelete(int ref)
{
	dgMesh *m;

	assert(list != NULL);
	assert(dgArrayListItemExists(list, ref));

	m = dgArrayListGetItem(list, ref);
	
	// Delete this mesh's filename if it has one.
	dgCondFree(m->fn.mesh);
	
	dgArrayListForEach(dgMeshAnimation, anim, m->animations, idx,
			   dgMeshDeleteAnimation(ref, 
						 idx);
		);
	
	// Delete animation list.
	dgArrayListDelete(m->animations);
	
	// Free face data.
	dgCondFree(m->faces);
	dgCondFree(m->faceConnInfo);
	
	// Free memory used by vertex data (includes normals & texcos)
	dgFree(m->coords.verts);

	// VBO enabled when loading this mesh?
	if (m->opts & DG_MESH_VBO)
	{
		#ifndef NO_GLEXT
		// Delete texture VBO.
		glDeleteBuffers(1, &m->vbo.tex);
		
		// Delete vert/norm VBOs for each frame.
		glDeleteBuffers(m->num.frames, m->vbo.verts);
		glDeleteBuffers(m->num.frames, m->vbo.norms);
		
		// Now free the actual memory being used
		// by their references...
		dgFree(m->vbo.verts);
		dgFree(m->vbo.norms);
		#endif
	}		

	// Remove this mesh from the list.
	dgArrayListDeleteItem(list, ref);

	// Free the rest of the mesh.
	dgFree(m);	
}

/**
 * Load a mesh.
 *
 * @param[in] fn Filename of the mesh to load.
 * @return -1 if something went wrong, otherwise a mesh reference.
 */
int dgMeshLoad(char *fn)
{
	return dgMeshLoadOpts(fn, 0);
}

/**
 * Load a mesh with options.
 *
 * Options: 
 * DG_MESH_NORM_INTERP - Interpolate normals on load for 
 *                       smoother lighting (slow)
 * DG_MESH_VBO - Use a VBO if available (be careful using this for 
 *               large meshes on older systems)
 * DG_MESH_NO_FACE_INFO - Don't calculate/store mesh face metadata.
 *                        Speeds up loads but breaks silhoutte 
 *                        determination.
 *
 * @param[in] fn Filename of the mesh to load.
 * @return -1 if something went wrong, otherwise a mesh reference.
 */
int dgMeshLoadOpts(char *fn, int opts)
{
	assert(fn);
	dgMeshInit();
	
	int index = 0;
	dgMesh *m = NULL;
	
	// Check the file extension first.
	char *ext = strrchr(fn, '.');
	int format;

	if (!ext)
	{
		fprintf(stderr, "File has no extension, "
			"skipping it <%s>.\n", fn);
		return -1;
	}
	
	else if (!strcasecmp(ext, ".cdm"))
	{
		format = DG_MESH_CDM;
	}
	
	else
	{
		fprintf(stderr, "Unsupported file format (%s).\n", fn);
		return -1;
	}
       
	// Mesh has not been loaded yet, so let's do this.
	m = dgArrayListGetItem(list, (index = dgMeshNew()));
	m->fn.mesh = dgStringDup(fn);
		
	// Copy over mesh options.
	m->opts = opts;

	int status = 0;
	dgPrintf("msg=loading mesh, fn=%s\n", fn);
	
	switch (format)
	{
	case DG_MESH_CDM:
		status = dgMeshLoadCDM(fn, index);
		break;
	}
	
	// Problem loading mesh, delete it and return unhappily.
	if (status)
	{
		dgMeshDelete(index);
		return -1;
	}
	
	//printf("\tverts\t%d\n", m->num.verts);
	
	// Home free if we got here (hopefully).
	return index;
}

void dgMeshGetData(int ref, float **data, size_t *num, 
		   unsigned int *nverts, unsigned int *nframes)
{
	assert(list);
	assert(dgArrayListItemExists(list, ref));
	assert(data && nverts && nframes);
	
	dgMesh *m = dgArrayListGetItem(list, ref);
	
	*num = (m->num.verts * m->num.frames * 3 * 2) + (2 * m->num.verts);
	
	dgMalloc(*data, float, *num);
	memcpy(*data, m->coords.verts, sizeof(float) * *num);
	*nverts = m->num.verts;
	*nframes = m->num.frames;
}

static void _dgMeshGenFaceInfo(dgMesh *m)
{
	// Number of faces = number vertices / 3.
	m->num.faces = (int) (m->num.verts / 3);
	
	// Allocate memory for face data.
	dgMalloc(m->faces, dgMeshFace, m->num.faces * m->num.frames);
	
	// For each frame.
	for (int i = 0; i < m->num.frames; ++i)
	{
		// For each face.
		for (int j = 0; j < m->num.faces; ++j)
		{
			// Get the current face.
			// i * numFaces = offset from beginning
			// 'j' is obv the index of the current face in this frame.
			dgMeshFace *face = &m->faces[i * m->num.faces + j];
			
			// Vertices forming this plane.
			// 3 * numVerts * i = offset from beginning.
			// 3 vertices per face, and 3 components per vertex so
			// 9 * j = 3 * 3 * j = offset from start of current frame.
			float *verts = &m->coords.verts[(3 * m->num.verts * i) + 
							(9 * j)];
			
			// Calculate this face's plane.
			dgGeometryPlaneFromPoints(verts, face->planeEq);
		}
	}
		
	// Allocate memory for connectivity information.
	dgMalloc(m->faceConnInfo, dgMeshFaceConnectivity, m->num.faces);
	// No faces should have neighbors yet.
	memset(m->faceConnInfo, -1, sizeof(dgMeshFaceConnectivity) * m->num.faces);
	
	// @cond
	// Compare two 3D vertices and return 1 if they are identical.
        #define VERTS_MATCH(v1,v2) !memcmp(v1, v2, sizeof(float) * 3)
	// @endcond
	
	// For each face.
	for (int fi = 0; fi < m->num.faces; ++fi)
	{
		// For each edge of that face
		for (int ei = 0; ei < 3; ++ei)
		{
			// If that edge doesn't have a neighbor yet
			if (m->faceConnInfo[fi].neighbors[ei] == -1)
			{
				// Get vertices of that edge
				float *ei_v1 = m->coords.verts + (9 * fi) + 3 * ei;
				float *ei_v2 = m->coords.verts + (9 * fi) + 3 * ((ei + 1) % 3);
				
				// For each face after that one
				for (int fj = fi + 1; fj < m->num.faces; ++fj)
				{
					int found = false;
					
					// For each edge of the current face
					for (int ej = 0; ej < 3; ++ej)
					{
						// If the edge also doesn't have a neighbor yet
						if (m->faceConnInfo[fj].neighbors[ej] == -1)
						{
							// Get vertices of edge
							float *ej_v1 = m->coords.verts + (9 * fj) + 3 * ej;
							float *ej_v2 = m->coords.verts + (9 * fj) + 3 * ((ej + 1) % 3);
							
							// If they are adjacent to the vertices of
							// our first edge, then the edges are connected
							// and these two faces are neighbors
							if ((VERTS_MATCH(ei_v1, ej_v1) && VERTS_MATCH(ei_v2, ej_v2)) ||
							    (VERTS_MATCH(ei_v1, ej_v2) && VERTS_MATCH(ei_v2, ej_v1)))
							{			
								m->faceConnInfo[fi].neighbors[ei] = fj;
								m->faceConnInfo[fj].neighbors[ej] = fi;
								found = true;
								break;
							}
						}
					}
					
					if (found)
					{
						break;
					}
				}
			}
		}
	}
	
	#undef VERTS_MATCH
}

static void _dgMeshNormInterp(dgMesh *m)
{
	// @cond
	// Compare two 3D vertices and return 1 if they are identical.
        #define VERTS_MATCH(v1,v2) !memcmp(v1, v2, sizeof(float) * 3)
	// @endcond

	// For every frame
	for (int i = 0; i < m->num.frames; ++i)
	{
		const int frameOffset = m->num.verts * 3 * i;
		
		// For every vert/normal
		for (int j = 0; j < m->num.verts; ++j)
		{
			float *v1 = m->coords.verts + frameOffset + 3 * j;
			float *n1 = m->coords.norms + frameOffset + 3 * j;
			
			// For every OTHER vert/normal
			for (int k = j + 1; k < m->num.verts; ++k)
			{
				float *v2 = m->coords.verts + frameOffset + 3 * k;
				float *n2 = m->coords.norms + frameOffset + 3 * k;
				
				if (VERTS_MATCH(v1, v2))
				{
					dgVectorAdd(n1, n2, n1);
				}
			}
			
			dgVectorNorm(n1);
			
			// For every OTHER vert/normal
			for (int k = j + 1; k < m->num.verts; ++k)
			{
				float *v2 = m->coords.verts + frameOffset + 3 * k;
				float *n2 = m->coords.norms + frameOffset + 3 * k;
				
				if (VERTS_MATCH(v1, v2))
				{
					memcpy(n2, n1, sizeof(float) * 3);
				}
			}
		}
	}
	
	#undef VERTS_MATCH
}

/**
 * Copy data to a mesh, generally used internally by mesh
 * loaders.
 *
 * @param[in] ref Mesh reference.
 * @param[in] data Vertex data.
 * @param[in] nverts Number of vertices.
 * @param[in] nframes Number of frames.
 */
void dgMeshData(int ref, float *data, unsigned int nverts, unsigned int nframes)
{
	dgMeshDataOpts(ref, data, nverts, nframes, 0);
}

/**
 * Copy data to a mesh, generally used internally by mesh
 * loaders.
 *
 * @param[in] ref Mesh reference.
 * @param[in] data Vertex data.
 * @param[in] nverts Number of vertices.
 * @param[in] nframes Number of frames.
 * @param[in] opts
 */
void dgMeshDataOpts(int ref, float *data, unsigned int nverts, unsigned int nframes, int opts)
{
	assert(data);
	assert(list);
	assert(dgArrayListItemExists(list, ref));

	dgMesh *m = dgArrayListGetItem(list, ref);
	const int offset = 3 * nverts * nframes;

	// Add additional options.
	m->opts |= opts;
	
	m->coords.verts = data;
	m->coords.norms = &m->coords.verts[offset];
	m->coords.tex = &m->coords.norms[offset];
	m->num.verts = nverts;
	m->num.frames = nframes;
	
	// XXX Smooth out rough keyframe animation.
	
	// Should consist entirely of triangles.
	assert(!(m->num.verts % 3));
	
	// Generate face connectivity information.
	if (!(m->opts & DG_MESH_NO_FACE_INFO)) _dgMeshGenFaceInfo(m);
	
	// Interpolate mesh normals for smooth lighting.
	if (m->opts & DG_MESH_NORM_INTERP) _dgMeshNormInterp(m);
	
	// Is VBO support compiled in?	
	#ifndef NO_GLEXT
	// If VBO is supported on this machine, and is enabled for this mesh.
	if (dgWindowVBOSupported() && (m->opts & DG_MESH_VBO))
	{
		dgPrintf("msg=using VBO, mesh=%d\n", ref);

		glGenBuffers(1, &m->vbo.tex);
		glBindBuffer(GL_ARRAY_BUFFER, m->vbo.tex);
		glBufferData(GL_ARRAY_BUFFER, 
			     sizeof(float) * nverts * 2, m->coords.tex, 
			     GL_STATIC_DRAW);
		
		dgMalloc(m->vbo.verts, GLuint, nframes);
		glGenBuffers(nframes, m->vbo.verts);
		
		dgMalloc(m->vbo.norms, GLuint, nframes);
		glGenBuffers(nframes, m->vbo.norms);
		
		// For each frame...
		for (int i = 0; i < nframes; ++i)
		{
			// Load vertex data for this frame into a VBO.
			glBindBuffer(GL_ARRAY_BUFFER, m->vbo.verts[i]);
			glBufferData(GL_ARRAY_BUFFER, 
				     sizeof(float) * m->num.verts * 3,
				     &m->coords.verts[3 * nverts * i],
				     GL_STATIC_DRAW);
			// Same for normal data.
			glBindBuffer(GL_ARRAY_BUFFER, m->vbo.norms[i]);
			glBufferData(GL_ARRAY_BUFFER, 
				     sizeof(float) * m->num.verts * 3,
				     &m->coords.norms[3 * nverts * i],
				     GL_STATIC_DRAW);
		}
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);	
	}
	#endif
}

/**
 * Render one frame of a mesh.
 * 
 * @param[in] mesh Mesh reference.
 * @param[in] tex Texture reference.
 * @param[in] cframe Current frame index.
 */
void dgMeshRender(int mesh, int tex, int cframe)
{
	dgMeshRenderOpts(mesh, tex, cframe, 0);
}

/**
 * Render one frame of a mesh with options.
 * 
 * Options:
 * DG_MESH_RENDER_NORMALS - Draw a visualisation of each face's surface normal.
 * 
 * @param[in] mesh Mesh reference.
 * @param[in] tex Texture reference.
 * @param[in] cframe Current frame index.
 * @param[in] opts
 */
void dgMeshRenderOpts(int mesh, int tex, int cframe, int opts)
{
	assert(list);
	
	GLuint texGL;
	const int useTexture = (tex >= 0);	
	
	// Get the mesh.
	dgMesh *m = _dgMeshGetPtr(mesh);
	const int offset = m->num.verts * 3 * cframe;

	glPushAttrib(GL_ENABLE_BIT);

	// If textures are enabled and we have a texture.
	if (useTexture)
	{
		// Get the OpenGL texture reference.
		texGL = dgTextureGetRefGL(tex);
	
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texGL);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	}
		
	glEnableClientState(GL_NORMAL_ARRAY);	
	glEnableClientState(GL_VERTEX_ARRAY);
	
	// If VBO is supported, was enabled for this mesh at load, and is not
	// turned off for this render.
	if (dgWindowVBOSupported() && (m->opts & DG_MESH_VBO) && !(opts & DG_MESH_RENDER_NO_VBO))
	{
		#ifndef NO_GLEXT
		if (useTexture)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m->vbo.tex);
			glTexCoordPointer(2, GL_FLOAT, 0, NULL);
		}
		
		glBindBuffer(GL_ARRAY_BUFFER, m->vbo.norms[cframe]);
		glNormalPointer(GL_FLOAT, 0, NULL);		
		
		glBindBuffer(GL_ARRAY_BUFFER, m->vbo.verts[cframe]);
		glVertexPointer(3, GL_FLOAT, 0, NULL);
		#endif
	}
	
	// Otherwise fallback to plain vertex arrays.
	else
	{
		if (useTexture) glTexCoordPointer(2, GL_FLOAT, 0, m->coords.tex);		
		glNormalPointer(GL_FLOAT, 0, &m->coords.norms[offset]);		
		glVertexPointer(3, GL_FLOAT, 0, &m->coords.verts[offset]);
	}
	
	// Draw mesh.
	glDrawArrays(GL_TRIANGLES, 0, _dgMeshGetPtr(mesh)->num.verts);
	
	#ifndef NO_GLEXT
	// Unbind last buffer.
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	#endif
	
	if (useTexture)
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisable(GL_TEXTURE_2D);
	}
	
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	
	// XXX draw vertex normals for frame 0
	if (opts & DG_MESH_RENDER_NORMALS)
	{		
		for (int i = 0; i < m->num.verts; ++i)
		{
			float *v = &m->coords.verts[offset + 3*i];
			float *n = &m->coords.norms[offset + 3*i];
			float vnorm[3];
			memcpy(vnorm, n, sizeof(float) * 3);
			dgVectorAdd(v, vnorm, vnorm);
			
			glBegin(GL_LINES);
			glVertex3fv(v);
			glVertex3fv(vnorm);
			glEnd();
		}
	}

	glPopAttrib();
}
 
// @cond
dgMesh *_dgMeshGetPtr(int mesh)
{
	assert(list != NULL);
	assert(dgArrayListItemExists(list, mesh));
	
	return dgArrayListGetItem(list, mesh);
}
// @endcond
