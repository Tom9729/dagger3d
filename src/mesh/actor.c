/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

typedef struct
{
	// Name of this actor.
	char *name;
	// Dagger mesh reference.
	int mesh;
	// Dagger texture reference.
	int texture;
} dgActor;

typedef struct
{
	// The actor this is an instance of.
	dgActor *parent;
	// Frame timer for animations.
	void *timer;
	// Frame index for animations.
	int frame;
	// Reference to the current animation.
	int curAnim;
} dgActorInstance;

// List of actors.
static void *actor_list = NULL;
// List of actor instances.
static void *instance_list = NULL;

static dgActor *_dgActorGetPtr(int actor)
{
	assert(actor_list != NULL);
	assert(dgArrayListItemExists(actor_list, actor));
	return (dgActor *) dgArrayListGetItem(actor_list, actor);
}

/**
 * Actor cleanup function.
 * 
 * Deletes all actors and actor instances.
 */
static void dgActorCleanup()
{
	assert(actor_list != NULL && instance_list != NULL);

//	printf("Running actor cleanup.\n");
	
	// For each actor.
	dgArrayListForEach(dgActor, a, actor_list, idx,
			   dgActorDelete(idx);
		);
	
	// For each instance.
	dgArrayListForEach(dgActorInstance, ai, instance_list, idx,
			   dgActorInstanceDelete(idx);
		);

	// Delete lists and reset to inital state.
	dgArrayListDelete(actor_list);
	dgArrayListDelete(instance_list);
	actor_list = NULL;
	instance_list = NULL;
}

/**
 * Initialise the actor system.
 *
 * Creates lists to hold actors/instances,
 * registers a cleanup function.
 */
static void dgActorInit()
{
	if (actor_list == NULL)
	{
		actor_list = dgArrayListNew();
		instance_list = dgArrayListNew();

		dgSysRegister(DG_SYS_ACTOR, dgActorCleanup, 0, NULL);
	}
}

/**
 * Get an actor's mesh reference.
 *
 * @param actor The actor.
 * @return Mesh reference.
 */
int dgActorGetMesh(int actor)
{
	return _dgActorGetPtr(actor)->mesh;
}

/**
 * Get an actor's texture reference.
 *
 * @param actor The actor.
 * @return Dagger texture reference.
 */
int dgActorGetTexture(int actor)
{
	return _dgActorGetPtr(actor)->texture;
}

/**
 * Create a new actor.
 *
 * <b>Example:</b>
 * \code
 * int actor = dgActorNew();
 * \endcode
 *
 * @return A reference to the newly created actor.
 */
int dgActorNew()
{
	dgActorInit();

	dgDeclMalloc(a, dgActor, 1);

	return dgArrayListAddItem(actor_list, a);
}

/**
 * Delete an actor and dereference any resources
 * associated with it.
 *
 * @param actor The actor to unload.
 */
void dgActorDelete(int actor)
{
	dgActor *a;
	
	assert(actor_list != NULL);
	assert(dgArrayListItemExists(actor_list, actor));

	// Get the actor.
	a = dgArrayListGetItem(actor_list, actor);

	// Delete it from the list.
	dgArrayListDeleteItem(actor_list, actor);
	
	// Delete name if it has one.
	dgCondFree(a->name);
	
	// Delete associated mesh/texture.
	//dgMeshDelete(a->mesh);
	//dgTextureDelete(a->texture);
//	dgResDelete(DG_MESH, a->mesh);
//	dgResDelete(DG_TEXTURE, a->texture);

	// Free memory.
	dgFree(a);
}

/**
 * Create a new instance of an actor.
 *
 * An actor instance keeps track of animation data.
 * 
 * @param actor Reference to the actor to instantiate.
 * @return New actor instance id.
 */
int dgActorInstanceNew(int actor)
{
	dgActor *a;
	dgActorInstance *ai;
	
	assert(actor_list != NULL && instance_list != NULL);
	assert(dgArrayListItemExists(actor_list, actor));

	// Get a ptr to the actor.
	a = dgArrayListGetItem(actor_list, actor);

	// Create a new instance.
	dgMalloc(ai, dgActorInstance, 1);

	// Initial animation = -1, meaning no animation.
	ai->curAnim = -1;
	
	// Set parent.
	ai->parent = a;
	
	// Add instance to list and return ref.
	return dgArrayListAddItem(instance_list, ai);
}

/**
 * Delete a instance of an actor.
 *
 * @param inst Actor instance reference.
 */
void dgActorInstanceDelete(int inst)
{
	assert(actor_list != NULL && instance_list != NULL);
	assert(dgArrayListItemExists(instance_list, inst));

	dgActorInstance *ai = dgArrayListDeleteItem(instance_list, inst);
	if (ai->timer) dgTimerDelete(ai->timer);
	dgFree(ai);
}

/**
 * Render an actor instance.
 *
 * @param inst Actor instance reference.
 */
void dgActorInstanceRender(int inst, float *pos, float *rot)
{
	dgActorInstance *ai;

	assert(actor_list != NULL && instance_list != NULL);
	assert(dgArrayListItemExists(instance_list, inst));

	// Get the instance.
	ai = dgArrayListGetItem(instance_list, inst);

	// If this instance has an animation.
	if (ai->curAnim != -1)
	{
		int start, end, loop;

		// Get some necessary information about this animation...
		dgMeshGetAnimationData(ai->parent->mesh, ai->curAnim,
				      &start, &end, NULL, &loop);
		
		// Next frame?
		if (dgTimerReady(ai->timer))
		{
			// Advance to next frame.
			if (ai->frame < end - 1)
			{
				ai->frame++;
			}

			// At the last frame, restart
			// animation if we are looping.
			else if (loop)
			{
				ai->frame = start;
			}

			// Otherwise we are stuck on the last frame until the
			// animation is reset. :-(
		}
	}
	
	float pos2[] = { 0.0f, 0.0f, 0.0f };
	float rot2[] = { 1.0f, 0.0f, 0.0f, 0.0f };

	// If either is null just reset to identity.
	pos = (pos ? pos : pos2);
	rot = (rot ? rot : rot2);
	
	glPushMatrix();
	glTranslatef(dgVectorComponents3(pos));
	dgQuaternionApply(rot);
	
	// Render with the actor's mesh/texture, and our current frame.
	dgMeshRender(ai->parent->mesh, ai->parent->texture, ai->frame);

	glPopMatrix();
}

/**
 * Attempt to set the current animation.
 *
 * @param inst Actor instance reference.
 * @param name Name of the animation to use.
 * @return 0 if nothing went wrong, -1 if the animation
 * could not be found.
 */
int dgActorInstanceSetAnimation(int inst, char *name)
{
	dgActorInstance *ai;
	int anim;
	float duration;

	assert(name);
	assert(actor_list != NULL && instance_list != NULL);
	assert(dgArrayListItemExists(instance_list, inst));

	// Get the instance.
	ai = dgArrayListGetItem(instance_list, inst);

	// Try to get the animation ref for 'name'.
	anim = dgMeshGetAnimation(ai->parent->mesh, name);

	// If we didn't get one, abort.
	if (anim == -1)
	{
		return -1;
	}

	dgPrintf("msg=set animation, name=%s\n", name);

	// Otherwise update our instance appropriately.
	ai->curAnim = anim;
	dgMeshGetAnimationData(ai->parent->mesh, anim, 
			       &ai->frame, NULL, 
			       &duration, NULL);

	// (Re)start frame timer.
	if (ai->timer) dgTimerDelete(ai->timer);
	ai->timer = dgTimerNew(duration);
	
	return 0;
}

void dgActorSetMesh(int actor, int mesh)
{
	_dgActorGetPtr(actor)->mesh = mesh;
}

void dgActorSetTexture(int actor, int tex)
{
	_dgActorGetPtr(actor)->texture = tex;
}
