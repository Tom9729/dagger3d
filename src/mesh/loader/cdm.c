/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

// Unfortunately CDM is a little endian format
// which means we have to do some conversions...
#ifdef DG_BIG_ENDIAN
#include <dagger3d/compat/endian.h>
#endif

// Magic number calculated by dgMagicGen().
static int const magic_CDM2 = 0x324D4443;

static int dgMeshLoadCDM2(FILE *fh, int ref)
{
	size_t num, off;
	unsigned int nverts, nframes;
	float *data;
	
	/* printf("\tversion: 2\n"); */
	
	// Read the vertex count.
	fread((unsigned int *) &nverts, sizeof(int), 1, fh);
	/* printf("\tvertices (per frame): %d\n", nverts); */
	
	// Read the frame count.
	fread((unsigned int *) &nframes, sizeof(int), 1, fh);
	/* printf("\tframes: %d\n", nframes); */
	
	#ifdef DG_BIG_ENDIAN
       	nverts = letobe32(nverts);
	nframes = letobe32(nframes);
	#endif

	num = (nverts * nframes * 3 * 2) + (2 * nverts);
	off = 3 * nverts * nframes;
	
	// Allocate memory for verts/norms/texcoords 
	// as one big block. Then we can read all 
	// the mesh data in with one fread().
	/* printf("\tallocating %ld bytes for %d floats\n", */
	/*        (long) (sizeof(float) * num), num); */
	dgMalloc(data, float, num);
	
	// Read in all of the mesh data at once.
	fread(data, sizeof(float), num, fh);
	
	#ifdef DG_BIG_ENDIAN
	for (int i = 0; i < num; ++i)
	{
		data[i] = letobe32_float(data[i]);
	}
	#endif

	// Pass the mesh data to dagger.	
	dgMeshData(ref, data, nverts, nframes);
	
	return 0;
}

/**
 * Load a CDM mesh.
 *
 * This is meant to be used internally. Use dgMeshLoad() instead.
 *
 * @param fn Filename of the mesh.
 * @param ref Dagger mesh reference.
 * @return Error status.
 */
int dgMeshLoadCDM(char *fn, int ref)
{
	int magic = 0;
	FILE *fh;
	int status = -1;
	
	// XXX 10.3.9 has CWD as directory in which binary is being run.
	//char buf[120];
	//puts(getcwd(buf, 120));
	
	if ((fh = fopen(fn, "rb")) == NULL)
	{
		fprintf(stderr, "Error: Cannot open file. (%s)\n", fn);
		return status;
	}
	
	// First we need to determine which version we are trying to load,
	// then branch off accordingly.
	fread(&magic, sizeof(int), 1, fh);

	#ifdef DG_BIG_ENDIAN
	magic = letobe32(magic);
	#endif

	if (magic == magic_CDM2)
	{
		status = dgMeshLoadCDM2(fh, ref);
	}

	else
	{
		fprintf(stderr, "Error: Unsupported CDM version! (%x)\n", magic);
	}
	
	// Done reading, close thie file.
	fclose(fh);
		
	return status;
}

int dgMeshSaveCDM(char *fn, int ref)
{
	assert(fn);
	assert(ref >= 0);
	
	FILE *fh;

	if ((fh = fopen(fn, "wb")) == NULL)
	{
		fprintf(stderr, "Error: Cannot open file. (%s)\n", fn);
		return -1;
	}

	int magic = magic_CDM2;
	unsigned int nverts, nframes;
	size_t num;
	float *data;
	
	dgMeshGetData(ref, &data, &num, &nverts, &nframes);
	
	#ifdef DG_BIG_ENDIAN
	magic = letobe32(magic);
       	nverts = letobe32(nverts);
	nframes = letobe32(nframes);

	for (int i = 0; i < num; ++i)
	{
		data[i] = letobe32_float(data[i]);
	}
	#endif
	
	// Write magic number.
	fwrite(&magic, sizeof(int), 1, fh);
	
	// Write the vertex count.
	fwrite(&nverts, sizeof(int), 1, fh);
	
	// Write the frame count.
	fwrite(&nframes, sizeof(int), 1, fh);
	
	// Write mesh data.
	fwrite(data, sizeof(float), num, fh);

	// Done writing, close thie file.
	fclose(fh);
	
	dgFree(data);

	return 0;
}
