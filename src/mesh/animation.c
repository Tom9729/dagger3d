/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d/compat/glext.h>
#include <dagger3d/mesh/mesh.h>
#include <dagger3d/mesh/animation.h>
#include <dagger3d.h>

/**
 * Get a reference to a mesh animation by name.
 *
 * @param mesh Mesh reference.
 * @param name Name of the animation we're looking for.
 * @return The animation reference or -1 if we could not
 * find the animation.
 */
int dgMeshGetAnimation(int mesh, char *name)
{       
	void *list = _dgMeshGetList();	
	dgMesh *m = dgArrayListGetItem(list, mesh);
	
	assert(name);
	assert(dgArrayListItemExists(list, mesh));

	dgArrayListForEach(dgMeshAnimation, a, m->animations, idx,
			   if (a->name && !strcmp(a->name, name))
			   {
				   return idx;
			   }
		);

	return -1;
}

/**
 * Retrieve data about an animation.
 *
 * Fields aside from 'mesh' and 'anim' can be null if you
 * don't care about them.
 *
 * @param mesh Mesh reference.
 * @param anim Animation reference.
 * @param start Starting index of the animation.
 * @param end Ending index of the animation.
 * @param duration Duration of each frame in seconds.
 * @param loop Boolean value, whether or not the animation repeats.
 */
void dgMeshGetAnimationData(int mesh, int anim, int *start, 
			    int *end, float *duration, int *loop)
{
	dgMesh *m;
	dgMeshAnimation *a;
	void *list = _dgMeshGetList();

       	assert(list != NULL);
	assert(dgArrayListItemExists(list, mesh));

	m = dgArrayListGetItem(list, mesh);
	
	assert(dgArrayListItemExists(m->animations, anim));

	a = dgArrayListGetItem(m->animations, anim);

	if (start) *start = a->start;
	if (end) *end = a->end;
	if (duration) *duration = a->duration;
	if (loop) *loop = a->loop;
}

/**
 * Add a new animation to a mesh.
 *
 * If an animation with the same name already is associated
 * with the mesh, then this does nothing.
 *
 * @param mesh Mesh reference.
 * @param name Animation name.
 * @param start Starting frame.
 * @param end Ending frame.
 * @param duration Frame duration in seconds.
 * @param loop Boolean value, true if this animation repeats.
 * @return Reference to animation 'name'.
 */
int dgMeshAddAnimation(int mesh, char *name, int start, int end, float duration, int loop)
{
	dgMesh *m;
	dgMeshAnimation *a;
	void *list = _dgMeshGetList();

	assert(list != NULL);
	assert(name);
	assert(start >= 0 && end >= 0);
	assert(end >= start);
	assert(!(duration < 0.0));
	assert(dgArrayListItemExists(list, mesh));
		
	// Get the mesh this is going to be added to.
	m = dgArrayListGetItem(list, mesh);
	
	// Create the animation structure.
	dgMalloc(a, dgMeshAnimation, 1);

	dgPrintf("msg=add animation, mesh=%s, name=%s\n", 
		 m->fn.mesh, name);
	/* printf("\tname: %s\n" */
	/*        "\tstart: %d\n" */
	/*        "\tend: %d\n" */
	/*        "\tduration: %f\n" */
	/*        "\tloop: %d\n", */
	/*        mesh, name, start, end, duration, loop); */

	// Copy over the name...
	a->name = dgStringDup(name);

	// as well as everything else.
	a->start = start;
	a->end = end;
	a->duration = duration;
	a->loop = loop;

	// Add this animation to the list of animations.
	return dgArrayListAddItem(m->animations, a);
}

/**
 * Delete an animation from a mesh.
 *
 * @param mesh Mesh reference.
 * @param animation Animation reference.
 */
void dgMeshDeleteAnimation(int mesh, int animation)
{
	dgMeshAnimation *a;
	dgMesh *m;
	void *list = _dgMeshGetList();

	assert(list != NULL);
	assert(dgArrayListItemExists(list, mesh));
	
	m = dgArrayListGetItem(list, mesh);

	assert(dgArrayListItemExists(m->animations, animation));

	a = dgArrayListGetItem(m->animations, animation);
	
	// Delete animation name if present.
	dgCondFree(a->name);
	
	// Remove animation from list.
	dgArrayListDeleteItem(m->animations, animation);

	// Free memory used by animation.
	dgFree(a);
}
