/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/*  
  http://graphics.cs.brown.edu/games/quake/quake3.html
  http://www.mralligator.com/q3/
  http://www.mralligator.com/q3/trimesh/
  http://www.misofruit.co.kr/seojewoo/programming/opengl/Quake3Format.htm

  Rendering bezier patches with gl evalulators
  http://www.opengl.org/resources/code/samples/mjktips/grid/index.html

  BSP data types are little endian. Strings are ASCII, but not
  necessarily NUL-terminated.
  
  Integrating BSP with Bullet physics.
    
  http://code.google.com/p/bullet/source/browse/?r=2244#svn%2Ftrunk%2FDemos%2FBspDemo
 */

typedef struct
{
	char *fn;
	
	struct
	{
		int verts;
		int meshverts;
		int faces;
		int textures;
	} num;
	
	struct
	{
		float *verts;
		float *norms;
		float *texcos0;
		float *texcos1;
		unsigned char *colors;
		unsigned int *meshverts;
	} coords;

	struct _dgBspMapTexture
	{
		char name[64];
		int flags;
		int contents;
		int ref;
	} *textures;

	struct _dgBspMapFace
	{
		int texture;
		int effect;
		int type;
		int vert;
		int n_verts;
		int meshvert;
		int n_meshverts;
		int lm_index;
		int lm_start[2];
		int lm_size[2];
		float lm_origin[3];
		float lm_vecs0[3];
		float lm_vecs1[3];
		float norm[3];
		int size[2];
	} *faces;
} dgBspMap;

static void *bsp_list = NULL;

static void _dgBspCleanup()
{
	dgArrayListDelete(bsp_list);
	bsp_list = NULL;
}

static void _dgBspInit()
{
	if (!bsp_list)
	{
		bsp_list = dgArrayListNew();
		dgSysRegister(DG_SYS_BSP, _dgBspCleanup, 0, NULL);
	}
}

int dgBspMapLoad(char *fn)
{	
	_dgBspInit();
	assert(fn);
	
	typedef enum
	{
		DG_BSP_ENTITIES = 0, // Game-related object descriptions
		DG_BSP_TEXTURES = 1, // Surface descriptions
		DG_BSP_PLANES = 2, // Planes used by map geometry
		DG_BSP_NODES = 3, // BSP tree nodes
		DG_BSP_LEAVES = 4, // BSP tree leaves
		DG_BSP_LEAF_FACES = 5, // Lists of face indices
		DG_BSP_LEAF_BRUSHES = 6, // Lists of brush indices
		DG_BSP_MODELS = 7, // Descriptions of rigid world geometry
		DG_BSP_BRUSHES = 8, // Convex polyhedra used to describe solid space
		DG_BSP_BRUSH_SIDES = 9, // Brush surfaces
		DG_BSP_VERTS = 10, // Vertices to describe faces
		DG_BSP_MESHVERTS = 11, // Vertex indices describing meshes
		DG_BSP_EFFECTS = 12, // List of shaders
		DG_BSP_FACES = 13, // Surface geometry
		DG_BSP_LIGHT_MAPS = 14, // Packed lightmap data
		DG_BSP_LIGHT_VOLS = 15, // Local illumination data
		DG_BSP_VIS_DATA = 16, // Cluster-cluster visibility data
	} dgBspLump;

	typedef struct
	{
		int magic;
		int version;
		struct
		{
			int offset; // Offset to start of lump in bytes (from beginning of file)
			int length; // Length of lump in bytes (multiple of 4)
			int num; // Number of records in this lump
		} lump[17];
	} dgBspHeader;

	// Size of each lump structure _on_disk_ (in bytes). We
	// can't use sizeof() without playing around with compiler
	// struct packing. Note: these values are hand calculated.
	const int lumpSize[] = {
		/* DG_BSP_ENTITIES */ 0, 
		/* DG_BSP_TEXTURE */ sizeof(char) * 64 + sizeof(int) * 2, 
		/* DG_BSP_PLANES */ sizeof(float) * 4,
		/* DG_BSP_NODES */ sizeof(int) * 9,
		/* DG_BSP_LEAVES */ sizeof(int) * 12,
		/* DG_BSP_LEAF_FACES */ sizeof(int),
		/* DG_BSP_LEAF_BRUSHES */ sizeof(int),
		/* DG_BSP_MODELS */ sizeof(float) * 6 + sizeof(int) * 4,
		/* DG_BSP_BRUSHES */ sizeof(int) * 3,
		/* DG_BSP_BRUSH_SIDES */ sizeof(int) * 2,
		/* DG_BSP_VERTS */ sizeof(float) * 10 + sizeof(unsigned char) * 4,
		/* DG_BSP_MESHVERTS */ sizeof(int),
		/* DG_BSP_EFFECTS */ sizeof(char) * 64 + sizeof(int) * 2,
		/* DG_BSP_FACES */ sizeof(int) * 14 + sizeof(float) * 12,
		/* DG_BSP_LIGHT_MAPS */ sizeof(unsigned char) * 128 * 128 * 3,
		/* DG_BSP_LIGHT_VOLS */ sizeof(unsigned char) * 8,
		/* DG_BSP_VIS_DATA */ 0,
	};
		
	// Open BSP file.
	FILE *fh = fopen(fn, "rb");

	// Create header
	dgDeclMalloc(h, dgBspHeader, 1);

	// Read BSP version
	fread(&h->magic, sizeof(int), 1, fh);
	fread(&h->version, sizeof(int), 1, fh);	
	dgPrintf("fn=%s, magic=%d, version=%d\n", fn, h->magic, h->version);

	// If this is not a proper Quake 3 BSP, abort.
	if (h->magic != dgMagicGen("IBSP") || h->version != 46)
	{
		dgPrintf("msg=not a proper quake 3 bsp, fn=%s\n", fn);
		dgFree(h);
		return -1;
	}

	// Read the lump directory.
	for (int i = 0; i < 17; ++i)
	{
		// Offset
		fread(&h->lump[i].offset, sizeof(int), 1, fh);
		// Length
		fread(&h->lump[i].length, sizeof(int), 1, fh);
		
		// Calculate the number of records in this lump.
		if (i == DG_BSP_ENTITIES || i == DG_BSP_VIS_DATA) h->lump[i].num = 1;
		else h->lump[i].num = h->lump[i].length / lumpSize[i];		

		// Print BSP header information
		const char *_lumpToStr(int lump)
		{
			if (lump == DG_BSP_ENTITIES) return "entities";
			else if (lump == DG_BSP_TEXTURES) return "textures";
			else if (lump == DG_BSP_PLANES) return "planes";
			else if (lump == DG_BSP_NODES) return "nodes";
			else if (lump == DG_BSP_LEAVES) return "leaves";
			else if (lump == DG_BSP_LEAF_FACES) return "leaf faces";
			else if (lump == DG_BSP_LEAF_BRUSHES) return "leaf brushes";
			else if (lump == DG_BSP_MODELS) return "models";
			else if (lump == DG_BSP_BRUSHES) return "brushes";
			else if (lump == DG_BSP_BRUSH_SIDES) return "brush sides";
			else if (lump == DG_BSP_VERTS) return "vertices";
			else if (lump == DG_BSP_MESHVERTS) return "meshverts";
			else if (lump == DG_BSP_EFFECTS) return "effects";
			else if (lump == DG_BSP_FACES) return "faces";
			else if (lump == DG_BSP_LIGHT_MAPS) return "light maps";
			else if (lump == DG_BSP_LIGHT_VOLS) return "light volumes";
			else if (lump == DG_BSP_VIS_DATA) return "visiblity data";
			else return NULL;
		}
		/* dgPrintf("i=%d, lump=%s, offset=%d, length=%d, num=%d\n", */
		/* 	 i, _lumpToStr(i), h->lump[i].offset, h->lump[i].length, h->lump[i].num); */
	}
	
	// Create map
	dgDeclMalloc(m, dgBspMap, 1);
	m->fn = dgStringDup(fn);
	
	// Swap Y/Z to correct for Q3's weird
	// coordinate system...
	void _swizzle3(float *v)
	{
		const float temp = v[1];
		v[1] = v[2];
		v[2] = temp;
	}

	// Load vertices
	{
		// Copy over the number of vertics
		m->num.verts = h->lump[DG_BSP_VERTS].num;
		
		// Allocate memory for vertex data
		dgMalloc(m->coords.verts, float, m->num.verts * 3);
		dgMalloc(m->coords.norms, float, m->num.verts * 3);
		dgMalloc(m->coords.texcos0, float, m->num.verts * 2);
		dgMalloc(m->coords.texcos1, float, m->num.verts * 2);
		dgMalloc(m->coords.colors, unsigned char, m->num.verts * 4);
		
		// Insertion ponters
		float *verts_ptr = m->coords.verts;
		float *norms_ptr = m->coords.norms;
		float *texcos0_ptr = m->coords.texcos0;
		float *texcos1_ptr = m->coords.texcos1;
		unsigned char *colors_ptr = m->coords.colors;
		
		// Seek to start of vertices lump
		fseek(fh, h->lump[DG_BSP_VERTS].offset, SEEK_SET);
		
		// For each vertex
		for (int i = 0; i < m->num.verts; ++i)
		{
			fread(verts_ptr, sizeof(float), 3, fh);
			fread(norms_ptr, sizeof(float), 3, fh);
			fread(texcos0_ptr, sizeof(float), 2, fh);
			fread(texcos1_ptr, sizeof(float), 2, fh);
			fread(colors_ptr, sizeof(unsigned char), 4, fh);
			
			_swizzle3(verts_ptr);
			_swizzle3(norms_ptr);

			/* dgPrintf("i=%d, vert=<%.1f,%.1f,%.1f>\n", i, dgVectorComponents3(verts_ptr)); */
			
			verts_ptr += 3;
			norms_ptr += 3;
			texcos0_ptr += 2;
			texcos1_ptr += 2;
			colors_ptr += 4;
		}
		
		// Quick sanity check
		assert(m->num.verts * 3 == verts_ptr - m->coords.verts);
	}
	
	// Load meshverts
	{
		// Copy over number of indices
		m->num.meshverts = h->lump[DG_BSP_MESHVERTS].num;

		// Allocate memory
		dgMalloc(m->coords.meshverts, unsigned int, m->num.meshverts);

		// Seek to start of lump
		fseek(fh, h->lump[DG_BSP_MESHVERTS].offset, SEEK_SET);
		
		// Read indices
		fread(m->coords.meshverts, sizeof(unsigned int), m->num.meshverts, fh);

		/* for (int i = 0; i < m->num.meshverts; ++i) */
		/* { */
		/* 	dgPrintf("i=%d, meshvert=%d\n", i, m->coords.meshverts[i]); */
		/* } */
	}

	// Load faces
	{
		// Copy over number of faces
		m->num.faces = h->lump[DG_BSP_FACES].num;
				
		// Allocate memory
		dgMalloc(m->faces, struct _dgBspMapFace, m->num.faces);
		
		// Seek to start of lump
		fseek(fh, h->lump[DG_BSP_FACES].offset, SEEK_SET);
		
		// For each face
		for (int i = 0; i < m->num.faces; ++i)
		{
			// Current face
			struct _dgBspMapFace *f = m->faces + i;
			
			fread(&f->texture, sizeof(int), 1, fh);
			fread(&f->effect, sizeof(int), 1, fh);
			fread(&f->type, sizeof(int), 1, fh);
			fread(&f->vert, sizeof(int), 1, fh);
			fread(&f->n_verts, sizeof(int), 1, fh);
			fread(&f->meshvert, sizeof(int), 1, fh);
			fread(&f->n_meshverts, sizeof(int), 1, fh);
			fread(&f->lm_index, sizeof(int), 1, fh);
			fread(f->lm_start, sizeof(int), 2, fh);
			fread(f->lm_size, sizeof(int), 2, fh);
			fread(f->lm_origin, sizeof(float), 3, fh);
			fread(f->lm_vecs0, sizeof(float), 3, fh);
			fread(f->lm_vecs1, sizeof(float), 3, fh);
			fread(f->norm, sizeof(float), 3, fh);
			fread(f->size, sizeof(int), 2, fh);

			_swizzle3(f->lm_origin);
			_swizzle3(f->lm_vecs0);
			_swizzle3(f->lm_vecs1);
			_swizzle3(f->norm);

			/* dgPrintf("face=%d, type=%d, vert=%d, n_verts=%d, meshvert=%d, n_meshverts=%d\n", */
			/* 	 i, f->type, f->vert, f->n_verts, f->meshvert, f->n_meshverts); */
		}
	}

	// Load textures
	{
		// Copy over number of textures
		m->num.textures = h->lump[DG_BSP_TEXTURES].num;

		// Allocate memory
		dgMalloc(m->textures, struct _dgBspMapTexture, m->num.textures);
		
		// Seek to start of lump
		fseek(fh, h->lump[DG_BSP_TEXTURES].offset, SEEK_SET);
		
		// For each texture
		for (int i = 0; i < m->num.textures; ++i)
		{
			// Current texture
			struct _dgBspMapTexture *t = m->textures + i;

			fread(t->name, sizeof(char), 64, fh);
			fread(&t->flags, sizeof(int), 1, fh);
			fread(&t->contents, sizeof(int), 1, fh);
			
			dgPrintf("i=%d, fn=%s, flags=%d, contents=%d\n",
				 i, t->name, t->flags, t->contents);

			t->ref = -1;
		}
		
		/* // XXX */
		/* int a = dgResLoad(DG_TEXTURE, "tom.jpg"); */
		/* m->textures[0].ref = a; */
		/* m->textures[1].ref = a; */
		/* m->textures[2].ref = a; */
		/* m->textures[3].ref = a; */
		/* m->textures[4].ref = a; */
	}
	
	// Done reading from file.
	dgFree(h);
	fclose(fh);

	return dgArrayListAddItem(bsp_list, m);
}

void dgBspMapDelete(int map)
{
	dgBspMap *m = dgArrayListGetItem(bsp_list, map);
	
	dgFree(m->fn);
	dgFree(m->coords.verts);
	dgFree(m->coords.norms);
	dgFree(m->coords.texcos0);
	dgFree(m->coords.texcos1);
	dgFree(m->coords.colors);
	dgFree(m->coords.meshverts);
	dgFree(m->textures);
	dgFree(m->faces);
	dgFree(m);
}

void dgBspMapRender(int map)
{
	dgBspMap *m = dgArrayListGetItem(bsp_list, map);

	/* glDisable(GL_LIGHTING); */
	/* glDisable(GL_CULL_FACE); */
	/* glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); */

	
	

	// Draw vertices
	/* glPointSize(3); */
	/* glEnableClientState(GL_VERTEX_ARRAY); */
	/* glVertexPointer(3, GL_FLOAT, 0, m->coords.verts); */
	/* glDrawArrays(GL_LINE_STRIP, 0, m->num.verts); */
	/* glDisableClientState(GL_VERTEX_ARRAY);	 */	

	// For each face
	for (int i = 0; i < m->num.faces; ++i)
	{
		struct _dgBspMapFace *f = m->faces + i;
		
		if (f->type == 1 || f->type == 3)
		{
			const int hasTex = m->textures[f->texture].ref != -1;

			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_NORMAL_ARRAY);
			
			glVertexPointer(3, GL_FLOAT, 0, m->coords.verts + 3 * f->vert);
			glNormalPointer(GL_FLOAT, 0, m->coords.norms + 3 * f->vert);
	
			if (hasTex)
			{
				const GLuint texGL = dgTextureGetRefGL(
					m->textures[f->texture].ref);				
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, texGL);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				glTexCoordPointer(2, GL_FLOAT, 0, m->coords.texcos1 + 2 * f->vert);
			}
		
			/* glClientActiveTextureARB(GL_TEXTURE0); */
			
			
			/* glClientActiveTextureARB(GL_TEXTURE1); */
			/* glTexCoordPointer(2, GL_FLOAT, 0, m->coords.texcos1 + 2 * f->vert); */
			
			glDrawElements(GL_TRIANGLES,
				       f->n_meshverts,
				       GL_UNSIGNED_INT,
				       m->coords.meshverts + f->meshvert);
			
			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_NORMAL_ARRAY);

			if (hasTex)
			{
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
				glDisable(GL_TEXTURE_2D);
			}
		}

	}
}
