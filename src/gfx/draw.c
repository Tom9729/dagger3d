/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

/**
 * Draw a rectangle.
 *
 * @param[in] x X-coordinate of the center.
 * @param[in] y Y-coordinate of the center.
 * @param[in] width Width of the rectangle.
 * @param[in] height Height of the rectangle.
 */
void dgDrawRect(float x, float y, float width, float height)
{
	assert(width > 0);
	assert(height > 0);

	// Save the current settings.
	glPushMatrix();

	// Apply a transformation so we draw at the current position.
	glTranslatef(x, y, 0);

	// Draw the quad with our image stretched out on it.
	glBegin(GL_QUADS);
	glVertex3f(-width / 2, -height / 2, 0);
	glVertex3f(width / 2, -height / 2, 0);
	glVertex3f(width / 2, height / 2, 0);
	glVertex3f(-width / 2, height / 2, 0);
	glEnd();

	// Restore the old settings.
	glPopMatrix();		
}

/**
 * Draw a texture-mapped rectangle.
 *
 * @param[in] x X-coordinate of the center.
 * @param[in] y Y-coordinate of the center.
 * @param[in] width Width of the rectangle.
 * @param[in] height Height of the rectangle.
 * @param[in] texture Dagger texture reference.
 */
void dgDrawRectTextured(float x, float y, float width, float height, int texture)
{
	assert(width > 0);
	assert(height > 0);

	// Save the current settings.
	glPushMatrix();
	glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TEXTURE_BIT);

	// Apply a transformation so we draw at the current position.
	glTranslatef(x, y, 0);

	// OpenGL uses it's own reference system that is
	// not compatible with Dagger. Basically we need
	// to trade in our Dagger image reference for an OpenGL
	// texture reference.
	int gl_texture = dgTextureGetRefGL(texture);

	// Tell OpenGL to use our texture.
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, gl_texture);

	// Draw the quad with our image stretched out on it.
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3f(-width / 2, -height / 2, 0);
	glTexCoord2f(1, 0);
	glVertex3f(width / 2, -height / 2, 0);
	glTexCoord2f(1, 1);
	glVertex3f(width / 2, height / 2, 0);
	glTexCoord2f(0, 1);
	glVertex3f(-width / 2, height / 2, 0);
	glEnd();

	// Restore the old settings.
	glPopAttrib();
	glPopMatrix();	
}

/**
 * Draw an arc (part of a circle).
 *
 * @param[in] radius Radius of the circle.
 * @param[in] theta Theta of the arc in degrees.
 * @param[in] sub
 */
void dgDrawArc(float radius, float theta, int sub)
{
	assert(radius > 0);
	assert(theta > 0 && theta <= 360);
	assert(sub > 0);

	const float thetaRad = dgMathDegreesToRadians(theta);
	const float thetaInc = thetaRad / sub;

	glPushMatrix();	
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0, 0);

	for (float th = 0; th < thetaRad; th += thetaInc)
	{
		glVertex2f(cos(th) * radius,
			   sin(th) * radius);
	}

	glVertex2f(radius, 0);
	glEnd();
	glPopMatrix();
}
