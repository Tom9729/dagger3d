#include <dagger3d.h>

typedef struct
{
	// XYZ position of particle
	float position[3];
	// XYZ direction unit vector
	float direction[3];
	// Particle speed
	float speed;
	// Particle acceleration
	float acceleration;
	// RGBA color of particle
	float color[4];
	// Amount of life this particle has remaining.
	double life;
	// Radius of this particle
	float radius;
} dgParticle;

typedef struct
{
	// [{0,0,0}] XYZ position of emitter
	float position[3];
	// [{0,0,0}] Velocity at which emitter is moving
	float velocity[3];
	// Array of all particles
	dgParticle *particles;
	// [100] Number of particles controlled by this emitter
	int numParticles;	
	// Array of batched living particles.
	dgParticle **particlesAlive;
	// [0] Number of particles currently alive and batched.
	int numParticlesAlive;
	// [DG_EMITTER_POINT] Type of emitter (e.g. DG_EMITTER_POINT)
	int type;	
	// [{1,0,0,0}] Quaternion orientation of emitter.
	float rotation[4];
	// [pi] Radians
	float theta;
	// [1] Radius of emitter, e.g. used for DG_EMITTER_SPHERE).
	float radius;
	// [0] Accumulated delta for particle emission.
	double emissionDelta;
	// [1.0] Particle emission period.
	double emissionPeriod;
	// [-1] Texture to use for emitted particles, or -1 for no texture.
	int texture;
	// [null] Logic callback for particles.
	void (*callback)(void *emitter, void *particle, double scale);
	// [100, 100] Particle life range.
	double minLife, maxLife;
	// [0.6, 0.6] Particle radius range.
	float minSize, maxSize;
	// [{0,0,0,1}, {1,1,1,1}] Particle color range.
	float minColor[4], maxColor[4];
	// [0.1, 0.1] Particle speed range.
	float minSpeed, maxSpeed;
	// [0, 0] Particle acceleration range.
	float minAccel, maxAccel;
	// [false] If true, particles in emitter-local coordinates.
	int local;
} dgEmitter;

static void _dgParticleEmitterNewParticle(dgEmitter *e, dgParticle *p)
{
	// Restore this particle to factory defaults. :)
	memset(p, 0, sizeof(dgParticle));
	
	if (e->type == DG_EMITTER_PLANE)
	{
		p->position[0] = dgRandomSign() * dgRandomf(0, e->radius);
		p->position[1] = dgRandomSign() * dgRandomf(0, e->radius);
	}
		
	else if (e->type == DG_EMITTER_RING)
	{
		p->position[0] = dgRandomSign() * dgRandomf(0, 100);
		p->position[1] = dgRandomSign() * dgRandomf(0, 100);
		
		dgVectorNorm(p->position);
		dgVectorScale(p->position, e->radius);
	}
		
	else if (e->type == DG_EMITTER_BOX)
	{
		p->position[0] = dgRandomSign() * dgRandomf(0, e->radius);
		p->position[1] = dgRandomSign() * dgRandomf(0, e->radius);
		p->position[2] = dgRandomSign() * dgRandomf(0, e->radius);
	}
	
	else if (e->type == DG_EMITTER_SPHERE)
	{
		p->position[0] = dgRandomSign() * dgRandomf(0, 100);
		p->position[1] = dgRandomSign() * dgRandomf(0, 100);
		p->position[2] = dgRandomSign() * dgRandomf(0, 100);
		
		dgVectorNorm(p->position);
		dgVectorScale(p->position, e->radius);
	}
	
	// Apply emitter orientation to particle's initial
	// position. For point emitters this would be a waste
	// of time because the initial local coordinates for
	// a particle are going to be {0,0,0} regardless of
	// emitter orientation.
	if (e->type != DG_EMITTER_POINT)
	{
		// XXX It would be more efficient to not do this, or
		// XXX to do it less often.
		float m[16];
		dgQuaternionToMatrix(e->rotation, m);

		p->position[0] = m[0] * p->position[0] + 
			         m[1] * p->position[1] + 
			         m[2] * p->position[2] + 
			         m[3];
		p->position[1] = m[4] * p->position[0] + 
			         m[5] * p->position[1] + 
			         m[6] * p->position[2] + 
			         m[7];
		p->position[2] = m[8] * p->position[0] + 
			         m[9] * p->position[1] + 
			         m[10] * p->position[2] + 
			         m[11];
	}
	
	// Get particle position in world coordinates.
	if (!e->local) dgVectorAdd(e->position, p->position, p->position);
	
	/*
	  Basically our emitter is a cone. The central axis of the
	  cone is a vector from the center of the emitter, and a theta
	  defines how wide the cone is. Giving the particle a random
	  direction unit vector inside this cone is a 2 step process.

	  In local coordinates, the cone's axis is the Y-axis.

	  1. Rotate the cone's axis around X-local by a random value.
	  
	  2. Rotate that vector around Y-local by another random value.
	 */
	
	// Step 1
	const float pitch = dgRandomf(0, e->theta);
	float rot[] = { dgQuaternionComponents(e->rotation) };	
	dgQuaternionApplyEuler(rot, pitch, 0, 0);
	dgQuaternionToForward(rot, p->direction);	
	
	// Step 2
	float rot2[] = { dgQuaternionComponents(e->rotation) };
	float aarot[4];
	dgQuaternionToForward(rot2, aarot);
	aarot[3] = dgRandomf(0, 2 * M_PI);
	dgQuaternionFromAxisAngle(aarot, aarot);
	dgQuaternionRotate(aarot, p->direction, p->direction);
	
	// Assign particle properties randomly within bounds.
	p->radius = dgRandomf(e->minSize, e->maxSize);
	p->speed = dgRandomf(e->minSpeed, e->maxSpeed);
	p->acceleration = dgRandomf(e->minAccel, e->maxAccel);
	p->life = dgRandomf(e->minLife, e->maxLife);	
	for (int i = 0; i < 4; ++i) p->color[i] = dgRandomf(e->minColor[i], 
							    e->maxColor[i]);
}

void *dgParticleEmitterNew()
{
	dgDeclMalloc(e, dgEmitter, 1);		
	dgParticleEmitterSetTexture(e, -1);
	dgParticleEmitterSetNumParticles(e, 100);
	dgParticleEmitterSetType(e, DG_EMITTER_POINT);
	dgParticleEmitterSetRadius(e, 1);
	dgParticleEmitterSetTheta(e, M_PI);
	dgQuaternionIdentity(e->rotation);
	dgParticleEmitterSetFrequency(e, 1);
	dgParticleEmitterSetLife(e, 100, 100);
	dgParticleEmitterSetSize(e, 0.6, 0.6);
	dgParticleEmitterSetColor(e,
				  0, 1,
				  0, 1,
				  0, 1,
				  1, 1);
	dgParticleEmitterSetSpeed(e, 0.1, 0.1);
	dgParticleEmitterSetAcceleration(e, 0, 0);	

	return e;
}

void dgParticleEmitterDelete(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	
	dgCondFree(e->particles);
	dgCondFree(e->particlesAlive);
	dgFree(e);
}

void dgParticleEmitterRender(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	
	// XXX Draw a sphere at the center of the emitter.
	/* glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT); */
	/* glPushMatrix(); */
	/* glTranslatef(e->position[0], e->position[1], e->position[2]); */
	/* glEnable(GL_LIGHTING); */
	/* glEnable(GL_LIGHT0); */
	/* glutSolidSphere(0.5, 16, 16); */
	/* glPopMatrix(); */
	/* glPopAttrib(); */

	// XXX Draw the direction the emitter is facing.
	/* float dir[3]; */
	/* dgQuaternionToForward(e->rotation, dir); */
	/* dgVectorScale(dir, 10); */
	/* glPushMatrix(); */
	/* glTranslatef(e->position[0], e->position[1], e->position[2]); */
	/* glColor3f(1, 0, 0); */
	/* glBegin(GL_LINES); */
	/* glVertex3f(0, 0, 0); */
	/* glVertex3fv(dir); */
	/* glEnd(); */
	/* glPopMatrix(); */

	glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
	
	// Bind particle texture if there is one.
	if (e->texture != -1)
	{
		const GLuint texGL = dgTextureGetRefGL(e->texture);		
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texGL);
	}

	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	
	glPushMatrix();	
	dgBillboardSpherical();
	
	// Translate to emitter's position in particles are
	// in local coords.
	if (e->local) glTranslatef(e->position[0], 
				   e->position[1], 
				   e->position[2]);
	
	glBegin(GL_QUADS);
	
	for (int i = 0; i < e->numParticlesAlive; ++i)
	{
		dgParticle *p = e->particlesAlive[i];
		
		float size = p->radius;

		float verts[] = {
			p->position[0]-size,
			p->position[1]-size,
			p->position[2],
			
			p->position[0]+size,
			p->position[1]-size,
			p->position[2],
			
			p->position[0]+size,
			p->position[1]+size,
			p->position[2],
			
			p->position[0]-size,
			p->position[1]+size,
			p->position[2],
		};
		
		glColor4fv(p->color);
		
		glTexCoord2f(0, 1);
		glVertex3fv(verts);
		glTexCoord2f(0, 0);
		glVertex3fv(verts+3);
		glTexCoord2f(1, 0);
		glVertex3fv(verts+6);
		glTexCoord2f(1, 1);
		glVertex3fv(verts+9);
	}
	
	glEnd();
	
	glPopMatrix();
	glPopAttrib();

	glDepthMask(GL_TRUE);
}

void dgParticleEmitterUpdate(void *emitter, double scale)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	
	// Reset count of living particles
	e->numParticlesAlive = 0;
	e->emissionDelta += scale;

	const double emissionPeriodScaled = e->emissionPeriod * scale;
	
	for (int i = 0; i < e->numParticles; ++i)
	{
		dgParticle *p = &e->particles[i];
		
		// If particle is dead
		if (p->life <= 0)
		{
			// No more particles to create
			if (e->emissionDelta < emissionPeriodScaled) continue;
			
			_dgParticleEmitterNewParticle(e, p);
			e->emissionDelta -= emissionPeriodScaled;
		}
		
		// add particle to list of living
		e->particlesAlive[e->numParticlesAlive++] = p;		
		
		// custom particle logic
		if (e->callback) (*e->callback)(e, p, scale);

		// acceleration
		p->speed += p->acceleration * scale;
		
		// velocity = direction * speed		
		float velocity[3] = { 
			p->direction[0] * p->speed,
			p->direction[1] * p->speed,
			p->direction[2] * p->speed
		};
		
		// positon += velocity
		dgVectorAdd(p->position, velocity, p->position);
		
		// age
		p->life -= scale;				
	}
}

void dgParticleEmitterGetPosition(void *emitter, float *position)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;	
	dgCopyArray(position, e->position, float, 3);
}

void dgParticleEmitterGetRotation(void *emitter, float *rotation)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	dgCopyArray(rotation, e->rotation, float, 4);
}

void dgParticleEmitterGetVelocity(void *emitter, float *velocity)
{
	assert(emitter);
	assert(velocity);
	dgEmitter *e = (dgEmitter *) emitter;
	dgCopyArray(velocity, e->velocity, float, 3);
}

int dgParticleEmitterGetTexture(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return e->texture;
}

int dgParticleEmitterGetNumParticles(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return e->numParticles;
}

int dgParticleEmitterGetType(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return e->type;
}

float dgParticleEmitterGetRadius(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return e->radius;
}

double dgParticleEmitterGetFrequency(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return 1.0 / e->emissionPeriod;
}

int dgParticleEmitterGetNumParticlesAlive(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return e->numParticlesAlive;
}

float dgParticleEmitterGetTheta(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return e->theta;
}

void dgParticleEmitterSetPosition(void *emitter, float *position)
{
	assert(emitter);
	assert(position);
	dgEmitter *e = (dgEmitter *) emitter;
	dgCopyArray(e->position, position, float, 3);
}

void dgParticleEmitterSetRotation(void *emitter, float *rotation)
{
	assert(emitter);
	assert(rotation);
	dgEmitter *e = (dgEmitter *) emitter;
	dgCopyArray(e->rotation, rotation, float, 4);
}

void dgParticleEmitterSetVelocity(void *emitter, float *velocity)
{
	assert(emitter);
	assert(velocity);
	dgEmitter *e = (dgEmitter *) emitter;
	dgCopyArray(e->velocity, velocity, float, 3);
}

void dgParticleEmitterSetTexture(void *emitter, int texture)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	e->texture = texture;
}

void dgParticleEmitterSetNumParticles(void *emitter, int numParticles)
{
	assert(emitter);
	assert(numParticles > 0);
	dgEmitter *e = (dgEmitter *) emitter;
			
	e->numParticles = numParticles;

	// Kill all existing particles (sorry) and realloc both arrays.
	e->numParticlesAlive = 0;
	dgCondFree(e->particles);
	dgCondFree(e->particlesAlive);
	dgMalloc(e->particles, dgParticle, e->numParticles);
	dgMalloc(e->particlesAlive, dgParticle*, e->numParticles);
}

void dgParticleEmitterSetType(void *emitter, int type)
{
	assert(emitter);
	assert(type >= DG_EMITTER_POINT && type <= DG_EMITTER_SPHERE);
	dgEmitter *e = (dgEmitter *) emitter;
	e->type = type;
}

void dgParticleEmitterSetRadius(void *emitter, float radius)
{
	assert(emitter);
	assert(radius > 0);
	dgEmitter *e = (dgEmitter *) emitter;
	e->radius = radius;
}

void dgParticleEmitterSetFrequency(void *emitter, double freq)
{
	assert(emitter);
	assert(freq > 0);
	dgEmitter *e = (dgEmitter *) emitter;
	e->emissionPeriod = 1.0 / freq;
}

void dgParticleEmitterSetTheta(void *emitter, float theta)
{
	assert(emitter);
	assert(theta >= 0);
	dgEmitter *e = (dgEmitter *) emitter;
	e->theta = theta;
}

void dgParticleEmitterSetLife(void *emitter, 
			      double minLife, double maxLife)
{
	assert(emitter);
	assert(minLife <= maxLife);
	assert(minLife > 0);
	dgEmitter *e = (dgEmitter *) emitter;
	e->minLife = minLife;
	e->maxLife = maxLife;
}

void dgParticleEmitterSetSize(void *emitter,
			      float minSize, float maxSize)
{
	assert(emitter);
	assert(minSize <= maxSize);
	assert(minSize >= 0);
	dgEmitter *e = (dgEmitter *) emitter;
	e->minSize = minSize;
	e->maxSize = maxSize;
}

void dgParticleEmitterSetColor(void *emitter,
			       float minRed, float maxRed,
			       float minGreen, float maxGreen,
			       float minBlue, float maxBlue,
			       float minAlpha, float maxAlpha)
{
	assert(emitter);
	assert(minRed <= maxRed); 
	assert(minRed >= 0 && maxRed <= 1);
	assert(minGreen <= maxGreen);
	assert(minGreen >= 0 && maxGreen <= 1);
	assert(minBlue <= maxBlue);
	assert(minBlue >= 0 && maxBlue <= 1);
	assert(minAlpha <= maxAlpha);
	assert(minAlpha >= 0 && maxAlpha <= 1);
	dgEmitter *e = (dgEmitter *) emitter;
	e->minColor[0] = minRed;
	e->maxColor[0] = maxRed;
	e->minColor[1] = minGreen;
	e->maxColor[1] = maxGreen;
	e->minColor[2] = minBlue;
	e->maxColor[2] = maxBlue;
	e->minColor[3] = minAlpha;
	e->maxColor[3] = maxAlpha;
}

void dgParticleEmitterSetSpeed(void *emitter,
			       float minSpeed, float maxSpeed)
{
	assert(emitter);
	assert(minSpeed <= maxSpeed);
	dgEmitter *e = (dgEmitter *) emitter;
	e->minSpeed = minSpeed;
	e->maxSpeed = maxSpeed;
}

void dgParticleEmitterSetAcceleration(void *emitter,
				      float minAccel, float maxAccel)
{
	assert(emitter);
	assert(minAccel <= maxAccel);
	dgEmitter *e = (dgEmitter *) emitter;
	e->minAccel = minAccel;
	e->maxAccel = maxAccel;
}

void dgParticleEmitterSetCallback(void *emitter, 
	       void (*callback)(void *emitter, void *particle, double scale))
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	e->callback = callback;
}

int dgParticleEmitterGetLocal(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	return e->local;
}

void dgParticleEmitterSetLocal(void *emitter, int local)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	
	// Kill all living particles (sorry), particles in world
	// coordinates with local transformations applied will
	// be messed up, and vice versa.
	if (e->local != local) dgParticleEmitterKill(e);

	e->local = local;
}

void dgParticleGetPosition(void *particle, float *position)
{
	assert(particle);
	assert(position);
	dgParticle *p = (dgParticle *) particle;
	dgCopyArray(position, p->position, float, 3);
}

void dgParticleGetDirection(void *particle, float *direction)
{
	assert(particle);
	assert(direction);
	dgParticle *p = (dgParticle *) particle;
	dgCopyArray(direction, p->direction, float, 3);
}

void dgParticleGetColor(void *particle, float *color)
{
	assert(particle);
	assert(color);
	dgParticle *p = (dgParticle *) particle;
	dgCopyArray(color, p->color, float, 4);
}

float dgParticleGetSpeed(void *particle)
{
	assert(particle);
	dgParticle *p = (dgParticle *) particle;
	return p->speed;
}

float dgParticleGetAcceleration(void *particle)
{
	assert(particle);
	dgParticle *p = (dgParticle *) particle;
	return p->acceleration;
}

float dgParticleGetSize(void *particle)
{
	assert(particle);
	dgParticle *p = (dgParticle *) particle;
	return p->radius;
}

double dgParticleGetLife(void *particle)
{
	assert(particle);
	dgParticle *p = (dgParticle *) particle;
	return p->life;
}

void dgParticleSetPosition(void *particle, float *position)
{
	assert(particle);
	assert(position);
	dgParticle *p = (dgParticle *) particle;
	dgCopyArray(p->position, position, float, 3);
}

void dgParticleSetDirection(void *particle, float *direction)
{
	assert(particle);
	assert(direction);
	dgParticle *p = (dgParticle *) particle;
	dgCopyArray(p->direction, direction, float, 3);
}

void dgParticleSetColor(void *particle, float *color)
{
	assert(particle);
	assert(color);
	dgParticle *p = (dgParticle *) particle;
	dgCopyArray(p->color, color, float, 4);
}

void dgParticleSetSpeed(void *particle, float speed)
{
	assert(particle);
	dgParticle *p = (dgParticle *) particle;
	p->speed = speed;
}

void dgParticleSetAcceleration(void *particle, float accel)
{
	assert(particle);
	dgParticle *p = (dgParticle *) particle;
	p->acceleration = accel;
}

void dgParticleSetSize(void *particle, float size)
{
	assert(particle);
	assert(size >= 0);
	dgParticle *p = (dgParticle *) particle;
	p->radius = size;
}

void dgParticleSetLife(void *particle, double life)
{
	assert(particle);
	assert(life >= 0);
	dgParticle *p = (dgParticle *) particle;
	p->life = life;
}

void dgParticleGetVelocity(void *particle, float *velocity)
{
	assert(particle);
	assert(velocity);
	dgParticle *p = (dgParticle *) particle;

	// Extract particle velocity.
	dgCopyArray(velocity, p->direction, float, 3);
	dgVectorScale(velocity, p->speed);
}

void dgParticleSetVelocity(void *particle, float *velocity)
{
	assert(particle);
	assert(velocity);
	dgParticle *p = (dgParticle *) particle;

	// Extract particle direction and magnitude.
	dgCopyArray(p->direction, velocity, float, 3);
	p->speed = dgVectorMag(p->direction);
	dgVectorNorm(p->direction);
}

void dgParticleEmitterKill(void *emitter)
{
	assert(emitter);
	dgEmitter *e = (dgEmitter *) emitter;
	e->numParticlesAlive = 0;
	memset(e->particles, 0, sizeof(dgParticle) * e->numParticles);
}
