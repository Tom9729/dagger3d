/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>
#include <dagger3d/gfx/window.h>
#include <dagger3d/gfx/camera.h>

static void *camera_list = NULL;
static const int defaultCamType = DG_CAMERA_5DOF;
static int activeCamera = -1;

/**
 * Set the active camera.
 * 
 * If there is an active camera, it's perspective will be
 * automatically applied with dgCameraPerspective().
 *
 * @param camera Camera reference.
 */
void dgCameraSetActive(int camera)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));
	
	// Set the active camera.
	activeCamera = camera;

	// Tell window system which camera we're using.
	_dgWindowSetActiveCam(camera);
}

/**
 * Get the active camera.
 * 
 * If there is an active camera, it's perspective will be
 * automatically applied with dgCameraPerspective().
 *
 * @return Camera reference.
 */
int dgCameraGetActive()
{	
	return activeCamera;
}

static void dgCameraCleanup()
{
//	puts("Running camera cleanup.");

	// For each camera we have...
	dgArrayListForEach(dgCamera, c, camera_list, idx,
			   dgCameraDelete(idx);
		);

	// Delete the list of cameras.
	dgArrayListDelete(camera_list);
	camera_list = NULL;
}

static void dgCameraInit()
{
	if (camera_list == NULL)
	{
		camera_list = dgArrayListNew();
		int deps[] = { };
		dgSysRegister(DG_SYS_CAMERA, dgCameraCleanup, 
			       sizeof(deps) / sizeof(int), deps);
	}
}

/**
 * Create a new camera.
 *
 * @return Dagger camera reference.
 */
int dgCameraNew()
{
	dgCamera *c;

	dgCameraInit();

	// Create and zero the camera structure.
	dgMalloc(c, dgCamera, 1);
	
	// Set some sensible defaults.
	c->type = defaultCamType;
	dgQuaternionIdentity(c->rot);
	c->frustum.attribs.fov = 60;
	c->frustum.attribs.nearDist = 0.4f;
	c->frustum.attribs.farDist = 500000;

	// Plane equations used to be stored in separate arrays
	// but now we store them all in frustum.planeqs.eqs. So 
	// we don't have to change anything, keep frustum.planeqs.X
	// around as pointers into eqs.
	c->frustum.planeqs.left = c->frustum.planeqs.eqs + 0;
	c->frustum.planeqs.right = c->frustum.planeqs.eqs + 4;
	c->frustum.planeqs.front = c->frustum.planeqs.eqs + 8;
	c->frustum.planeqs.back = c->frustum.planeqs.eqs + 12;
	c->frustum.planeqs.top = c->frustum.planeqs.eqs + 16;
	c->frustum.planeqs.bottom = c->frustum.planeqs.eqs + 20;

	return dgArrayListAddItem(camera_list, c);
}

/**
 * Delete a camera.
 *
 * @param camera Dagger camera reference.
 */
void dgCameraDelete(int camera)
{
	dgCamera *c;

	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));

	// Remove the camera from the list.
	c = dgArrayListDeleteItem(camera_list, camera);

	// Delete the camera.
	dgFree(c);
}

/**
 * Set a camera's position {x,y,z}.
 *
 * @param camera Dagger camera reference.
 * @param pos Position of the camera {x,y,z}.
 */
void dgCameraSetPosition(int camera, float *pos)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));
	assert(pos);

	dgCamera *c = dgArrayListGetItem(camera_list, camera);
	
	// Copy over the new position.
	memcpy(c->pos, pos, sizeof(float) * 3);
}

/**
 * Get a camera's position.
 *
 * @param camera Dagger camera reference.
 * @param pos Vector to store position {x,y,z}.
 */
void dgCameraGetPosition(int camera, float *pos)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));
	assert(pos);
	
	dgCamera *c = dgArrayListGetItem(camera_list, camera);
	
	// Copy over the position.
	memcpy(pos, c->pos, sizeof(float) * 3);
}

/**
 * Apply a rotation to a camera.
 *
 * Rotations are measured in radians.
 *
 * @param camera Dagger camera reference.
 * @param x Rotation around x-axis.
 * @param y Rotation around y-axis.
 * @param z Rotation around z-axis.
 */
void dgCameraRotate(int camera, float x, float y, float z)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));

	dgCamera *c = dgArrayListGetItem(camera_list, camera);
		
	if (c->type == DG_CAMERA_6DOF)
	{
		dgQuaternionApplyEuler(c->rot, x, y, z);
	}

	else if (c->type == DG_CAMERA_5DOF)
	{
		// Construct quaternions for pitch/yaw.
		float qx[4] = { 1, 0, 0, x };
		float qy[4] = { 0, 1, 0, y };

		// Convert AA to quaternions.
		dgQuaternionFromAxisAngle(qx, qx);
		dgQuaternionFromAxisAngle(qy, qy);
		
		// Rotate around global y-axis.
		dgQuaternionMult(c->rot, qy, c->rot);
		// Rotate around local x-axis.
		dgQuaternionMult(qx, c->rot, c->rot);
		/* dgQuaternionMult(c->rot, qx, c->rot); */
	}
}

/**
 * Get which type of camera we are using.
 * 
 * DG_CAMERA_5DOF = pitch, yaw
 * DG_CAMERA_6DOF = pitch, yaw, roll
 *
 * @param camera Dagger camera reference.
 * @param type Type of camera.
 */
void dgCameraSetType(int camera, int type)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));
	assert(type == DG_CAMERA_5DOF || type == DG_CAMERA_6DOF);

	((dgCamera *) dgArrayListGetItem(camera_list, camera))->type = type;	
}

/**
 * Get which type of camera we are using.
 * 
 * DG_CAMERA_5DOF = pitch, yaw
 * DG_CAMERA_6DOF = pitch, yaw, roll
 *
 * @param camera Dagger camera reference.
 * @return Type of camera.
 */
int dgCameraGetType(int camera)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));

	return ((dgCamera *) dgArrayListGetItem(camera_list, camera))->type;	
}

/**
 * Apply a camera perspective upon the current scene.
 *
 * @param camera Dagger camera reference.
 */
void dgCameraPerspective(int camera)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));
	
	dgCamera *c = dgArrayListGetItem(camera_list, camera);

	// Identity must be reset first.
	glLoadIdentity();

	float a[4];
	
	// Convert orientation to axis/angle for gl.
	dgQuaternionToAxisAngle(c->rot, a);
	// Perform the rotation in one step.
	glRotatef(dgMathRadiansToDegrees(a[3]), a[0], a[1], a[2]);

	// Move to correct position.
	glTranslatef(-(c->pos[0]), -(c->pos[1]), -(c->pos[2]));
}

/**
 * Get the forward vector for this camera. Basically
 * this gets the direction the camera is looking towards
 * as a unit vector.
 *
 * @param camera Dagger camera reference.
 * @param v Vector to store the foreward vector.
 */
void dgCameraGetForward(int camera, float *v)
{
	assert(camera_list != NULL);
	assert(v);
	assert(dgArrayListItemExists(camera_list, camera));
	
	dgCamera *c = dgArrayListGetItem(camera_list, camera);
	
	// Get the "forward" vector of the orientation.
	dgQuaternionToForward(c->rot, v);
}

/**
 * Get the up vector for this camera. Basically
 * this gets the direction the camera is looking towards
 * as a unit vector, rotated -90 degrees around the local x-axis.
 *
 * @param camera Dagger camera reference.
 * @param v Vector to store the vector.
 */
void dgCameraGetUp(int camera, float *v)
{
	assert(camera_list != NULL);
	assert(v);
	assert(dgArrayListItemExists(camera_list, camera));
	
	dgCamera *c = dgArrayListGetItem(camera_list, camera);
	
	// Get the "up" vector of the orientation.
	dgQuaternionToUp(c->rot, v);
}

/**
 * Get the right vector for this camera. Basically
 * this gets the direction the camera is looking towards
 * as a unit vector, rotated -90 degrees around the local y-axis.
 *
 * @param camera Dagger camera reference.
 * @param v Vector to store the vector.
 */
void dgCameraGetRight(int camera, float *v)
{
	assert(camera_list != NULL);
	assert(v);
	assert(dgArrayListItemExists(camera_list, camera));
	
	dgCamera *c = dgArrayListGetItem(camera_list, camera);
	
	// Get the "right" vector of the orientation.
	dgQuaternionToRight(c->rot, v);
}

/**
 * Get this camera's rotation.
 *
 * @param camera Dagger camera reference.
 * @param rot Rotation { w, x, y, z }.
 */
void dgCameraGetRotation(int camera, float *rot)
{
	assert(rot);
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));

	dgCamera *c = dgArrayListGetItem(camera_list, camera);
	memcpy(rot, c->rot, sizeof(float) * 4);
}

/**
 * Set this camera's rotation.
 *
 * @param camera Dagger camera reference.
 * @param rot Rotation { w, x, y, z }.
 */
void dgCameraSetRotation(int camera, float *rot)
{
	assert(rot);
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));

	dgCamera *c = dgArrayListGetItem(camera_list, camera);
	memcpy(c->rot, rot, sizeof(float) * 4);
	// Normalise the rotation to fix rounding errors
	// that would cause weird anomalies.
	dgQuaternionNorm(c->rot);	
}

// @cond
dgCamera *_dgCameraGetPtr(int camera)
{
	assert(camera_list != NULL);
	assert(dgArrayListItemExists(camera_list, camera));
	return dgArrayListGetItem(camera_list, camera);
}
// @endcond

/**
 * Check if a particular camera reference is still good.
 * 
 * @param[in] camera The camera reference.
 * @return True if the reference is still good, otherwise false.
 */
int _dgCameraIsValid(int camera)
{
	return dgArrayListItemExists(camera_list, camera);
}

/**
 * Get camera frustum attributes.
 *
 * @param camera[in] The camera to query.
 * @param fov[out] Vertical field of view.
 * @param nearWidth[out] Width of the near plane.
 * @param nearHeight[out] Height of the near plane.
 * @param farWidth[out] Width of the far plane.
 * @param farHeight[out] Height of the far plane.
 * @param nearDist[out] Distance to the near plane.
 * @param farDist[out] Distance to the far plane.
 */
void dgCameraGetFrustumAttributes(int camera, 
				  float *fov,
				  float *nearWidth, float *nearHeight,
				  float *farWidth, float *farHeight,
				  float *nearDist, float *farDist)
{
	dgCamera *c = _dgCameraGetPtr(camera);

	dgCopy(fov, &c->frustum.attribs.fov);
	dgCopy(nearWidth, &c->frustum.attribs.nearWidth);
	dgCopy(nearHeight, &c->frustum.attribs.nearHeight);
	dgCopy(farWidth, &c->frustum.attribs.farWidth);
	dgCopy(farHeight, &c->frustum.attribs.farHeight);
	dgCopy(nearDist, &c->frustum.attribs.nearDist);
	dgCopy(farDist, &c->frustum.attribs.farDist);
}

/**
 * Set camera frustum attributes.
 *
 * @param camera[in] The camera to query.
 * @param fov[in] Vertical field of view.
 * @param nearWidth[in] Width of the near plane.
 * @param nearHeight[in] Height of the near plane.
 * @param farWidth[in] Width of the far plane.
 * @param farHeight[in] Height of the far plane.
 * @param nearDist[in] Distance to the near plane.
 * @param farDist[in] Distance to the far plane.
 */
void dgCameraSetFrustumAttributes(int camera, 
				  float *fov,
				  float *nearWidth, float *nearHeight,
				  float *farWidth, float *farHeight,
				  float *nearDist, float *farDist)
{
	dgCamera *c = _dgCameraGetPtr(camera);
	
	dgCopy(&c->frustum.attribs.fov, fov);
	dgCopy(&c->frustum.attribs.nearWidth, nearWidth);
	dgCopy(&c->frustum.attribs.nearHeight, nearHeight);
	dgCopy(&c->frustum.attribs.farWidth, farWidth);
	dgCopy(&c->frustum.attribs.farHeight, farHeight);
	dgCopy(&c->frustum.attribs.nearDist, nearDist);
	dgCopy(&c->frustum.attribs.farDist, farDist);
}

/**
 * Get this camera's frustum quads.
 *
 * Note:
 * - Quads go CCW, eg.
 *
 * <pre>
 *   3---2
 *   |   |
 *   0---1
 * </pre>
 *
 * @param[in] camera Camera reference.
 * @param[out] nearq Pointer to 12 floats to store near-quad.
 * @param[out] farq Pointer to 12 floats to store far-quad.
 */
void dgCameraFrustumQuads(int camera, float *nearq, float *farq)
{
	// Get the active camera.
	dgCamera *c = _dgCameraGetPtr(camera);
	
	dgCopyArray(nearq, c->frustum.quads.nearp, float, 12);
	dgCopyArray(farq, c->frustum.quads.farp, float, 12);
}

/**
 * Recalculate a camera's frustum quads and plane equations.
 *
 * This is expensive so it should be done as little as needed.
 *
 * @param[in] camera Camera reference.
 */
void dgCameraFrustumUpdate(int camera)
{
	// Get the active camera.
	dgCamera *c = _dgCameraGetPtr(camera);

  	// Convert FOV to radians.
	float fovy = dgMathDegreesToRadians(c->frustum.attribs.fov);
	// Calculate 2 * tan(fovy / 2)
	float tanFov = 2 * tan(fovy / 2.0f);
	// Get the window ratio.
	float ratio = dgWindowRatio();
	
	// Recalculate frustum dimensions.
	// For near plane.
	c->frustum.attribs.nearHeight = c->frustum.attribs.nearDist * tanFov;
	c->frustum.attribs.nearWidth = c->frustum.attribs.nearHeight * ratio;
	// For far plane.
	c->frustum.attribs.farHeight = c->frustum.attribs.farDist * tanFov;
	c->frustum.attribs.farWidth = c->frustum.attribs.farHeight * ratio;
	
	// Regenerate frustum planes.
	dgGeometryFrustumQuads(c->frustum.quads.nearp, 
			       c->frustum.quads.farp, 
			       c->pos, c->rot,
			       c->frustum.attribs.nearDist, 
			       c->frustum.attribs.nearWidth, 
			       c->frustum.attribs.nearHeight,
			       c->frustum.attribs.farDist, 
			       c->frustum.attribs.farWidth, 
			       c->frustum.attribs.farHeight);

	float *nearQuad = c->frustum.quads.nearp;
	float *farQuad = c->frustum.quads.farp;

	// Calculate dimensions of the bounding cone.

	// Get forward vector.
	float a[3];
	dgCameraGetForward(camera, a);       
	dgVectorNorm(a);

	// Get vector to near corner.
	// XXX This should be done twice, once for the near plane and once for the far plane.
	// XXX The largest angle should be used.
	float b[3];
	memcpy(b, nearQuad, sizeof(float) * 3);
	dgVectorSub(b, c->pos, b);
	dgVectorNorm(b);
	
	// Calculate dotprod of the two vectors which should be the angle
	// between the vectors.
	c->frustum.cone.angle = dgVectorDotProd(a, b);
	
	// Length is merely farDist.
	c->frustum.cone.len = c->frustum.attribs.farDist;

	// Now we need to generate plane equations for each quad
	// of the frustum. We can use the normal of each plane
	// to do tests to determine if things are inside or intersecting
	// the frustum.

	// Near plane.
	dgGeometryPlaneFromPoints2(&nearQuad[0],
				   &nearQuad[3],
				   &nearQuad[6],
				   c->frustum.planeqs.front);
	// Far plane.
	dgGeometryPlaneFromPoints2(&farQuad[6],
				   &farQuad[3],
				   &farQuad[0],
				   c->frustum.planeqs.back);
	// Top plane.
	dgGeometryPlaneFromPoints2(&farQuad[9],
				   &nearQuad[9],
				   &nearQuad[6],
				   c->frustum.planeqs.top);
	// Bottom plane.
	dgGeometryPlaneFromPoints2(&farQuad[3],
				   &nearQuad[3],
				   &nearQuad[0],
				   c->frustum.planeqs.bottom);

	// Right plane.
	dgGeometryPlaneFromPoints2(&nearQuad[3],
				   &farQuad[3],
				   &farQuad[6],
				   c->frustum.planeqs.right);

	// Left plane.
	dgGeometryPlaneFromPoints2(&farQuad[0], 
				   &nearQuad[0], 
				   &nearQuad[9],
				   c->frustum.planeqs.left);

	/* // Right plane. */
	/* dgGeometryPlaneFromPoints2(&nearQuad[3], */
	/* 			   &farQuad[9], */
	/* 			   &farQuad[0], */
	/* 			   c->frustum.planeqs.right); */
	/* // Left plane. */
	/* dgGeometryPlaneFromPoints2(&nearQuad[6],  */
	/* 			   &nearQuad[3],  */
	/* 			   &farQuad[3], */
	/* 			   c->frustum.planeqs.left); */
}

/**
 * Get a copy of the plane equations for each face of this camera's frustum.
 *
 * Note:
 * - Remember to do dgCameraFrustumUpdate() for recent results.
 * - Order of planes is LRFBTB (left right front back top bottom).
 *
 * @param[in] camera Camera reference.
 * @param[out] planeqs 24 float array to contain plane equations.
 */
void dgCameraFrustumPlanes(int camera, float *planeqs)
{
	assert(planeqs);

	dgCamera *c = _dgCameraGetPtr(camera);

	memcpy(planeqs, c->frustum.planeqs.eqs, sizeof(float) * 24);
}

/**
 * Get an approximating cone surrounding the frustum.
 *
 * @param[in] camera Camera reference.
 * @param[out] angle Angle of cone.
 * @param[out] len Distance to far plane.
 */
void dgCameraFrustumCone(int camera, float *angle, float *len)
{
	dgCamera *c = _dgCameraGetPtr(camera);

	dgCopy(angle, &(c->frustum.cone.angle));
	dgCopy(len, &(c->frustum.cone.len));
}
