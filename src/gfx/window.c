/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>
#include <dagger3d/input/keyboard.h>
#include <dagger3d/gfx/window.h>
#include <dagger3d/gfx/camera.h>

static dgWindow window;

static void *frameTimer;
static void *fpsTimer;

static int fps = 0;
static const int defaultMaxFPS = 60;

static void dgWindowRenderScene();
static void dgWindowDisplay();
static void dgWindowReshape(int w, int h);
static int dgWindowGameModePossible();
static void dgWindowGLInit();
static void dgWindowCleanup();

static void dgWindowDisplayCallbackDefault()
{
	
}

static void dgWindowInputCallbackDefault(double scale)
{
		if (dgWindowKeyPressedOnce(DG_KEY_ESCAPE))
		{
			exit(0);
		}
}

static void dgWindowLogicCallbackDefault(double scale)
{
	
}

static void dgWindowSelectCallbackDefault(int hits, unsigned int *buf)
{
	
}

static void (*dgWindowInitCallback)() = NULL;
static void (*dgWindowDisplayCallback)() = dgWindowDisplayCallbackDefault;
static void (*dgWindowInputCallback)(double scale) = dgWindowInputCallbackDefault;
static void (*dgWindowLogicCallback)(double scale) = dgWindowLogicCallbackDefault;
static void (*dgWindowSelectCallback)(int hits, unsigned int *buf) = dgWindowSelectCallbackDefault;

static int init = false;

void dgWindowOpenOverrideWindow(int b)
{
	window.override.window = b;
}

void dgWindowOpenOverrideFullscreen(int b)
{
	window.override.fullscreen = b;
}

void dgWindowOpenOverrideResolution(int w, int h)
{
	window.override.width = w;
	window.override.height = h;
}

/**
 * Attempts to get the currently active camera. If this somehow fails
 * (eg. if the user has deleted all of their cameras and we were using one)
 * then fallback to the default camera.
 *
 * @return Camera to use for this window.
 */
dgCamera *_dgWindowCamera()
{
	if (!_dgCameraIsValid(window.camera)) 
	{
		window.camera = window.cameraDefault;
	}
	
	return _dgCameraGetPtr(window.camera);
}

void _dgWindowSetActiveCam(int camera)
{
	window.camera = camera;
}

/**
 * Get dimensions for the near/far quads of the viewing frustum.
 *
 * @param nearWidth Width of near plane quad.
 * @param nearHeight Height of near plane quad.
 * @param farWidth Width of far plane quad.
 * @param farHeight Height of far plane quad.
 */
void dgWindowFrustumDimensions(float *nearWidth, float *nearHeight,
			       float *farWidth, float *farHeight)
{
	assert(init);

	dgCamera *c = _dgWindowCamera();

	if (nearWidth) *nearWidth = c->frustum.attribs.nearWidth;
	if (nearHeight) *nearHeight = c->frustum.attribs.nearHeight;
	if (farWidth) *farWidth = c->frustum.attribs.farWidth;
	if (farHeight) *farHeight = c->frustum.attribs.farHeight;
}

void dgWindowDrawFPS(int b)
{		
	assert(init);
	window.renderFPS = b;
}

/**
 * Get the current FPS (frames-per-second) value.
 *
 * @return Current FPS.
 */
int dgWindowGetFPS()
{	
	assert(init);
	return fps;
}

/**
 * Check an area for named objects, generally used for picking.
 *
 * To name an object, use glPushName() and glPopName().
 *
 * Typically (x,y) will be the mouse coordinates (obtained via
 * dgMouseGetX() and dgMouseGetY()), and (w,h) will be a rather small
 * value like 0.1.
 *
 * To do anything useful with this function you should register a
 * function with dgWindowSetSelectFunc().
 *
 * <b>References:</b>
 * <ul>
 *  <li> http://www.lighthouse3d.com/opengl/picking/index.php3?openglway </li>
 *  <li> http://gpwiki.org/index.php/OpenGL:Tutorials:Picking </li>
 * </ul>
 *
 * @param x X-coordinate of the center of the box.
 * @param y Y-coordinate of the center of the box.
 * @param w Width of the box.
 * @param h Height of the box.
 */
void dgWindowSelect(float x, float y, float w, float h)
{	
	// XXX When the scene is rendered here, things like
	// XXX textures/shadows/etc should be turned off.
	
	assert(init);
	if (dgWindowSelectCallback)
	{
		const int bufSize = 64;
		GLuint buf[bufSize];
		GLint hits, viewport[4];
		
		glSelectBuffer(bufSize, buf);
		glGetIntegerv(GL_VIEWPORT, viewport);
		glRenderMode(GL_SELECT);
		glInitNames();
				
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		
		gluPickMatrix(x, y, w, h, viewport);
		gluPerspective(60.0f, viewport[2] / 
			       (float) viewport[3], 0.2f, 10000.0f);
		glMatrixMode(GL_MODELVIEW);
		glutSwapBuffers();
		
		glPushAttrib(GL_ENABLE_BIT);
		
		glDisable(GL_LIGHTING);
		glDisable(GL_BLEND);
		glDisable(GL_FOG);
		glDisable(GL_TEXTURE_2D);
		
		dgWindowRenderScene();

		glPopAttrib();
		
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		
		hits = glRenderMode(GL_RENDER);
		/* printf("dgWindowSelect(): Got %d hits.\n", hits); */
		(*dgWindowSelectCallback)(hits - 1, (unsigned int *) buf);
		glMatrixMode(GL_MODELVIEW);
	}
}

/**
 * Get the height of the window in pixels.
 *
 * This is not always the same as the height of the viewport.
 *
 * @return Height of the window.
 */
int dgWindowGetHeight()
{	
	assert(init);
	return window.height;
}

/**
 * Get the width of the window in pixels.
 *
 * This is not always the same as the width of the viewport.
 *
 * @return Width of the window.
 */
int dgWindowGetWidth()
{	
	assert(init);
	return window.width;
}

/**
 * Set the coordinate mode used for drawing in this window.
 *
 * <b>Options</b>
 * <ul>
 * <li>DG_WINDOW_MODE_SCREEN - Screen or window coordinates. Bounds
 are from the origin (0,0) in the lower left corner of the window,
 to (width,height) in the upper right corner.</li>
 * <li>DG_WINDOW_MODE_WORLD - World or object coordinates, the default setting.</li>
 * </ul>
 *
 * @param mode New coordinate mode to use.
 */
void dgWindowCoordinateMode(int mode)
{	
	assert(init);

	// Switch to screen coordinates.
	if (window.mode != mode && mode == DG_WINDOW_MODE_SCREEN)
	{
		GLint viewport[4];

		glPushAttrib(GL_LIGHTING_BIT);
		glPushAttrib(GL_TRANSFORM_BIT);
		glGetIntegerv(GL_VIEWPORT, viewport);
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		gluOrtho2D(viewport[0], viewport[2],
			   viewport[1], viewport[3]);
		glPopAttrib();
		glDisable(GL_DEPTH_TEST);
		glPushMatrix();
		glLoadIdentity();

		// Disable lighting, why would we want lights when
		// drawing in screen coordinates?!?
		glDisable(GL_LIGHTING);

		window.mode = DG_WINDOW_MODE_SCREEN;
	}

	// Switch to world coordinates.
	else if (mode != window.mode && mode == DG_WINDOW_MODE_WORLD)
	{
		glPopMatrix();
		glPushAttrib(GL_TRANSFORM_BIT);
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glPopAttrib();
		glEnable(GL_DEPTH_TEST);
		glPopAttrib();

		window.mode = DG_WINDOW_MODE_WORLD;
	}
}

/**
 * Get the window system's default camera.
 *
 * @return Camera reference.
 */
int dgWindowGetSystemCamera()
{
	return window.cameraDefault;
}

/**
 * Toggle wireframe rendering mode.
 *
 * Wireframe mode only shows edges! As long as backface culling is disabled,
 * it will also enable the viewer to see through solid surfaces.
 *
 * @param b True to enable wireframe mode, false to disable.
 */
void dgWindowSetWireframe(int b)
{	
	assert(init);

	if (b)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

/**
 * Set the select/picking function.
 *
 * This function will be used by dgWindowSelect().
 *
 * @param callback Function pointer.
 */
void dgWindowSetSelectFunc(void (*callback)(int hits, unsigned int *buf))
{	
	assert(init);
	dgWindowSelectCallback = callback;
}

/**
 * Set the display function.
 *
 * @param callback Function pointer.
 */
void dgWindowSetDisplayFunc(void (*callback)())
{
	dgWindowDisplayCallback = callback;
}

/**
 * Set a callback to be run AFTER the window has been
 * created but BEFORE rendering has begun. This is
 * useful if there are things we can't do until
 * glutMainLoop has been entered.
 *
 * @param callback Function pointer.
 */
void dgWindowSetInitFunc(void (*callback)())
{
	dgWindowInitCallback = callback;
}

/**
 * Register a callback to be run before any other cleanup functions.
 *
 * @param[in] callback Cleanup function.
 */
void dgWindowSetCleanupFunc(void (*callback)())
{
	static int cleanupFuncSet = false;
	assert(!cleanupFuncSet);
	
	// We already setup DG_SYS_USER to depend on all
	// Dagger systems. Just attach a cleanup function
	// and we should be good to go.
	dgSysRegister(DG_SYS_USER, callback, 0, NULL);
	cleanupFuncSet = true;
}

static void dgWindowInit()
{
	if (!init)
	{
		int argc;
		char **argv;

		dgInitGetArgs(&argc, &argv);

		//printf("Initializing window system.\n");
		
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | 
				    GLUT_RGBA | 
				    GLUT_DEPTH | 
				    GLUT_MULTISAMPLE |
				    GLUT_STENCIL);
		
		// Create a default camera and activate it.
		window.cameraDefault = dgCameraNew();
		dgCameraSetActive(window.camera);

		// Anything that uses GL resources needs to be cleaned up first!
		int deps[] = { DG_SYS_CAMERA };
		dgSysRegister(DG_SYS_WINDOW, dgWindowCleanup, 
			      sizeof(deps) / sizeof(int), deps);

		init = 1;
	}
}

static void dgWindowCleanup()
{
	//printf("Running window cleanup.\n");
	
	// Win32 freeglut does not like us calling glutGetWindow()
	// if there is no GL context established. Sadly the GL context
	// gets destroyed when the window is closed by clicking the "x".
	// Since we have no way of knowing whether the "x" was clicked or
	// not, we do not know if we still have an active GL context.
	#ifndef MINGW
	
	// Close the window and destroy the OpenGL context
	// if we have one.
	if (/*glutGetWindow() || */window.fullscreen)
	{
		dgWindowClose();
	}

	#endif
		
	// Delete camera.
	dgCameraDelete(window.cameraDefault);

	// Delete timers.
	if (frameTimer) dgTimerDelete(frameTimer);
	if (fpsTimer) dgTimerDelete(fpsTimer);

	_dgKeyboardCleanup();
	init = false;
}

/**
 * Close the currently open window, and destroy it's associated 
 * OpenGL context.
 */
void dgWindowClose()
{	
	assert(init);
	dgPrintf("msg=closing window\n");

	if (window.fullscreen)
	{
		glutLeaveGameMode();
	}
	
	else
	{
		glutDestroyWindow(window.id);
	}
}

static int dgWindowGameModePossible()
{
	char *str = NULL;

	dgStringAppend(&str, "%dx%d:32@%d", window.width, 
		      window.height, window.refresh);
	glutGameModeString(str);
	dgFree(str);

	return glutGameModeGet(GLUT_GAME_MODE_POSSIBLE);
}

/**
 * Get the current window ratio (width/height).
 *
 * @return Window ratio.
 */
float dgWindowRatio()
{
	return window.ratio;
}

/**
 * Check if textures with non-power-of-two dimensions are
 * supported.
 *
 * @return True if they are, otherwise false.
 */
int dgWindowNPOTSupported()
{	
	assert(init);
	return window.npot_supported;
}

static void dgWindowGLInit()
{
	glutDisplayFunc(dgWindowDisplay);
	glutIdleFunc(dgWindowDisplay);
	glutReshapeFunc(dgWindowReshape);

	glEnable(GL_DEPTH_TEST);
	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Enable image transparency.
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0);	
			
	// Grab the GL_MAX_TEXTURE_SIZE.
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &window.max_texture_size);

	// Check for VBO support.
	#ifdef NO_GLEXT
	window.vbo_supported = false;
	#else
	window.vbo_supported = glutExtensionSupported(
		"GL_ARB_vertex_buffer_object");
	#endif

	// Check for NPOT texture support.
	window.npot_supported = glutExtensionSupported(
		"GL_ARB_texture_non_power_of_two");
	
	// Initialize keyboard.
	_dgKeyboardInit();
}

/**
 * Open a new window with an OpenGL rendering context.
 *
 * Refresh rate generally should be 60 (that's what most/all LCD monitors use).
 * If the refresh rate is incorrect, this will fail.
 *
 * @param title Title for the new window.
 * @param w Width of the window in pixels.
 * @param h Height of the window in pixels.
 * @param rr Refresh rate of the window.
 * @param fullscreen True for fullscreen mode.
 */
void dgWindowOpen(char *title, int w, int h, int rr, int fullscreen)
{
	dgWindowInit();
	
	assert(title != NULL);
	assert(w > 0);
	assert(h > 0);

	fullscreen = (fullscreen || window.override.fullscreen) && !window.override.window;
	w = (window.override.width ? window.override.width : w);
	h = (window.override.height ? window.override.height : h);

	// Copy over window properties...
	window.title = strdup(title);
	window.width = w;
	window.height = h;
	window.refresh = rr;
	window.maxFPS = defaultMaxFPS;

	// Update frustum dimensions.
	/* dgWindowRecalcPlaneDims(); */

	dgPrintf("msg=opening window, w=%d, h=%d, rr=%d\n", w, h, rr);

	// Attempt to enter game mode (for fullscreen).
	if (fullscreen && dgWindowGameModePossible())
	{
		dgPrintf("msg=entering fullscreen\n");
		window.id = glutEnterGameMode();
		window.fullscreen = true;
	}
	
	// Otherwise just do windowed mode.
	else
	{
		glutInitWindowSize(window.width, window.height);
		window.id = glutCreateWindow(window.title);
		dgWindowReshape(window.width, window.height);
		window.fullscreen = false;
	}
	
	dgWindowGLInit();

	// Default behaviour is to activate the mouse and show cursor.
	dgMouseEnabled(true);
	dgMouseShowCursor(true);
}

static void dgWindowRenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT |
		GL_DEPTH_BUFFER_BIT |
		GL_STENCIL_BUFFER_BIT);
	/* glClear(GL_DEPTH_BUFFER_BIT |  */
	/* 	GL_STENCIL_BUFFER_BIT); */
	glPushMatrix();
	glLoadIdentity();

	if (dgWindowDisplayCallback)
	{
		int cam = dgCameraGetActive();
		
		// Reset color to white.
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		
		// If there is an active camera.
		if (cam != -1)
		{
			// Apply it's perspective.
			dgCameraPerspective(cam);
		}
		
		// XXX Use glPushAttrib()/glPopAttrib() here so
		// XXX the user can't screw anything up.
		
		
		// Call render function.
		glPushMatrix();
		(*dgWindowDisplayCallback)();
		glPopMatrix();
	}
	
	// Render FPS.
	if (window.renderFPS || true)
	{
		const int font = dgFontGetSystemFont();
		const long fps = dgWindowGetFPS();
		char *fpsStr = NULL;
		
		glPushAttrib(GL_COLOR_BUFFER_BIT);
		glColor4f(1, 1, 1, 1);
		dgStringAppend(&fpsStr, "%ld", fps);
		dgFontPrint(font, 15, dgWindowGetHeight() - 20, 1000, fpsStr);
		glPopAttrib();
		
		dgFree(fpsStr);
	}
	
	// Render mouse cursor.
	dgMouseRender();
	
	/* // XXX For some reason picking is broken */
	/* // XXX without this. */
	/* dgWindowCoordinateMode(DG_WINDOW_MODE_SCREEN); */
	/* glBegin(GL_TRIANGLES); */
	/* glVertex2i(0, 0); */
	/* glVertex2i(0, 0); */
	/* glVertex2i(0, 0); */
	/* glEnd(); */
	/* dgWindowCoordinateMode(DG_WINDOW_MODE_WORLD); */
	
	glPopMatrix();		
	//glFlush();
	glutSwapBuffers();
}

static void dgWindowDisplay()
{
	static int init = false;
	const double logicFPS = 60;
	const double targetFPS = 100;
	static int framesRendered = 0;
	
	if (!init)
	{
		if (dgWindowInitCallback)
		{
			(*dgWindowInitCallback)();
			
			// Unset this because we only want
			// to run it once!
			dgWindowInitCallback = NULL;
		}
		
		frameTimer = dgTimerNew(1.0 / targetFPS);
		fpsTimer = dgTimerNew(1);
		fps = targetFPS;
		init = true;
	}
	
	const double scale = (targetFPS / (double) fps) / 
		(targetFPS / logicFPS);
	//printf("%f\n", scale);
	
	if (dgTimerReady(frameTimer))
	{
		(*dgWindowInputCallback)(scale);
		(*dgWindowLogicCallback)(scale);
		
		dgWindowRenderScene();
		++framesRendered;

		if (dgTimerReady(fpsTimer))
		{
			fps = (framesRendered + fps) / 2.0;
			framesRendered = 0;
		}
	}
	
	//usleep(dgTimerRemaining(frameTimer) * 1e6);
}

static void dgWindowReshape(int w, int h)
{
	dgCamera *c = _dgWindowCamera();

	// Prevent a divide by zero error.
	h = (h < 1) ? 1 : h;
    
	// New window dimensions.
	window.width = w;
	window.height = h;

	// XXX Ignore new coordinates.
//	w = dgWindowGetWidth();
//	h = dgWindowGetHeight();
	
	// View ratio is always automatically
	// window width / height.
	window.ratio = w / (float) h;
	/* printf("%f %f %f %f\n", */
	/*        c->frustum.attribs.fov,  */
	/* 	       c->frustum.attribs.ratio,  */
	/* 	       c->frustum.attribs.nearDist,  */
	/* 	       c->frustum.attribs.farDist); */

	// Set up viewing frustum.
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(c->frustum.attribs.fov,
		       window.ratio,
		       c->frustum.attribs.nearDist,
		       c->frustum.attribs.farDist);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	// XXX Force window to keep original dimensions.
//	glutReshapeWindow(dgWindowGetWidth(), dgWindowGetHeight());

	// Recalculate frustum attribs.
	/* dgWindowRecalcPlaneDims(); */
}

/**
 * Set the input function where keyboard/mouse logic should go.
 *
 * @param callback Function pointer.
 */
void dgWindowSetInputFunc(void (*callback)(double scale))
{
	assert(init);
	dgWindowInputCallback = callback;
}

/**
 * Set the logic callback.
 *
 * @param callback Function pointer.
 */
void dgWindowSetLogicFunc(void (*callback)(double scale))
{	
	assert(init);
	dgWindowLogicCallback = callback;
}

/**
 * Get GL names from the select buffer.
 *
 * @param i Index of the name.
 * @param buf The select buffer.
 * @return GL name.
 */
int dgWindowSelectGetName(int i, unsigned int *buf)
{
	assert(buf != NULL);
	assert(i >= 0);
	assert(init);
	
	return buf[(i * 4 + 3)];
}

/**
 * Enter the main loop (wrapper around glutMainLoop()).
 */
void dgWindowMainLoop()
{
	dgPrintf("msg=entering main loop\n");
	glutMainLoop();
}

/**
 * Dump the contents of the OpenGL window to
 * an array of BGRA pixel data (width * height * 4).
 *
 * @return Pointer to array of pixel data.
 */
unsigned char *dgWindowScreenDump()
{
	const int width = dgWindowGetWidth();
	const int height = dgWindowGetHeight();
	unsigned char *pdata;
	
	dgMalloc(pdata, unsigned char, width * height * 4);
	glReadPixels(0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid *) pdata);
	
	return pdata;	
}

int dgWindowScreenDumpToFile(char *fn)
{	
	unsigned char *pdata = dgWindowScreenDump();
	const int ret = dgTGASave(fn,
				  pdata, 4,
				  (short int) dgWindowGetWidth(),
				  (short int) dgWindowGetHeight(),
				  DG_TEXTURE_BGRA);
	dgFree(pdata);	
	return ret;
}

int dgWindowVBOSupported()
{
	assert(init);
	return window.vbo_supported;
}
