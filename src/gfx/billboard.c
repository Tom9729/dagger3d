#include <dagger3d.h>

// http://www.lighthouse3d.com/opengl/billboarding/index.php3?billCheat
void dgBillboardSpherical()
{
	/*
	  mv = a0 a4 a8  a12
	       a1 a5 a9  a13
	       a2 a6 a10 a14
	       a3 a7 a11 a15
	*/

	float mv[16];
	
	glGetFloatv(GL_MODELVIEW_MATRIX, mv);

	mv[0] = 1;
	mv[1] = 0;
	mv[2] = 0;
	
	mv[4] = 0;
	mv[5] = 1;
	mv[6] = 0;
	
	mv[8] = 0;
	mv[9] = 0;
	mv[10] = 1;

	glLoadMatrixf(mv);
}
