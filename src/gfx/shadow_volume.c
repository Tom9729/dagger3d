/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>
#include <dagger3d/gfx/shadow_volume.h>
#include <dagger3d/gfx/light.h>
#include <dagger3d/mesh/mesh.h>

// XXX Test performance of using VBOs to store shadows.
// XXX http://nehe.gamedev.net/data/lessons/lesson.asp?lesson=27
// XXX http://www.gamedev.net/reference/articles/article1873.asp

/*
  XXX Some of this needs to be revised, mesh data is no longer stored.
  XXX The only thing stored is the quadrilaterals of the volume itself.

  A shadow volume consists of a region created by the extrapolation
  of an object's silhouette from the perspective of a single point-light
  source.

  Essentially, place yourself in the position of the light and look at
  the object. The borders/edges between the object and what is behind the
  object form a silhouette. The shadow volume consists of the object created
  by connecting the silhouette to the point-light. This object will generally
  resemble a pyramid.

  We then slide the silhouette away from the point-light along the ray from
  the point-light cast through the center of the object. At the same time we increase
  the "radius" of the silhouette. We do this until a finite "infinity" is reached.

  Then we do some magic with the stencil buffer to actually draw the shadows.

  The most expensive operation in this process appears to be the calculations involved 
  in generating the shadow volumes. For static lights/objects we could greatly increase 
  performance by ONLY regenerating volumes when it is necessary. Perhaps in extreme cases
  we could regenerate shadow volumes for moving objects every-other draw, or something similar.

  This is one way to abstract the shadow code out of the mesh class, where it currently resides.

  dgShadowVolumeGenerate() takes a volume reference, a mesh reference, and a light reference, and
  the position/orientation of the mesh instance.
  - Mesh data is only duplicated if the mesh this volume was linked to has changed.

  Perhaps in the future some sort of (intelligent) system like this would be nice.
  - Volume is only recalculated if...
     - The position of the light or object has changed.
     - The orientation of the object has changed.
     - The mesh or light has changed.
  - Add the ability to force a regeneration.
  - Add the ability to specify an epsilon value, or in other words the amount the
    pos/rot has changed before triggering a regeneration (remember, floating point
    math is imprecise and perhaps the user will not notice a slight lag in shadows?).

  Why do we pass light/mesh references instead of raw data?
  - So we can transparently add special logic later. For example shadow opacity and
    color could be a property of the light casting the shadow, or the mesh acting
    as the shadower (eg. a semi-transparent object should not necessarily cast
    a solid shadow).
 */

static void *volume_list = NULL;

static void dgShadowVolumeCleanup()
{
//	printf("Running shadow volume cleanup.\n");

	dgArrayListForEach(dgShadowVolume, vol, volume_list, idx,
			   dgShadowVolumeDelete(idx);
		);

	dgArrayListDelete(volume_list);
	volume_list = NULL;
}

static void dgShadowVolumeInit()
{
	if (volume_list == NULL)
	{
//		printf("Initializing shadow volume system.\n");
		
		volume_list = dgArrayListNew();
		
		int deps[] = { };
		dgSysRegister(DG_SYS_SHADOW_VOLUME, dgShadowVolumeCleanup, 
			      sizeof(deps) / sizeof(int), deps);
	}
}

/**
 * Create a new shadow volume.
 *
 * @return Reference to the new volume.
 */
int dgShadowVolumeNew()
{
	dgShadowVolumeInit();

	dgShadowVolume *sv;

	// Allocate memory for shadow volume object.
	dgMalloc(sv, dgShadowVolume, 1);
	
	// Add to volume list and return a reference.
	return dgArrayListAddItem(volume_list, sv);
}

/**
 * Delete a shadow volume.
 *
 * @param vol Reference to the volume.
 */
void dgShadowVolumeDelete(int vol)
{
	assert(volume_list != NULL);
	assert(dgArrayListItemExists(volume_list, vol));
	
	// Remove the item from the list but get back a pointer
	// so we can do some additional cleanup.
	dgShadowVolume *sv = dgArrayListDeleteItem(volume_list, vol);

	// Delete stored volume if it exists.
	dgCondFree(sv->quads);
	dgCondFree(sv->nearCap);
	dgCondFree(sv->farCap);
	
	// Delete memory used by item.
	dgFree(sv);
}

static int drawShadowVol = false;
static int drawLights = false;

void dgShadowVolumeDrawVolumes(int b)
{
	drawShadowVol = b;
}

void dgShadowVolumeDrawLights(int b)
{
	drawLights = b;
}

void dgShadowVolumeDoPass(
	void **lights, int numLights,
	int *meshes, int numMeshes, int *frames,
	int **shadows,
	float **objPos, float **objRot,
	void (*renderScene)(),
	int regenShadows)
{	
	// Render scene without lighting.
	glEnable(GL_LIGHTING);
	dgLightConfigure(NULL, 0);
	renderScene();
	
	// For each light.
	for (int i = 0; i < numLights; ++i)
	{
		// Draw a sphere representing this light.
		if (drawLights)
		{
			dgLightDraw(lights[i], 2);			
		}
		
		// For each mesh.
		for (int j = 0; j < numMeshes; ++j)
		{
			// Regenerate this shadow volume
			if (regenShadows)
			{
				dgShadowVolumeGenerate(shadows[i][j], meshes[j], 
						       frames[j], lights[i],
						       objPos[j], objRot[j]);
			}
			
			if (drawShadowVol)
			{
				glEnable(GL_CULL_FACE);
				glCullFace(GL_BACK);
				dgShadowVolumeRender(shadows[i][j]);
				glDisable(GL_CULL_FACE);
			}
		}

		glClear(GL_STENCIL_BUFFER_BIT);
		glPushAttrib(GL_CURRENT_BIT |
			     GL_COLOR_BUFFER_BIT | 
			     GL_DEPTH_BUFFER_BIT |
			     GL_ENABLE_BIT | 
			     GL_POLYGON_BIT |
			     GL_STENCIL_BUFFER_BIT);
		
		glDisable(GL_LIGHTING);
		glDisable(GL_FOG);
		glDisable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glEnable(GL_STENCIL_TEST);
		glColorMask(0, 0, 0, 0);
		glStencilFunc(GL_ALWAYS, 1, ~0);
		
		// Z-pass.
		/* glFrontFace(GL_CCW); */
		/* glStencilOp(GL_KEEP, GL_KEEP, GL_INCR); */
		/* dgShadowVolumeRender(shadows[i]); */
		/* glFrontFace(GL_CW); */
		/* glStencilOp(GL_KEEP, GL_KEEP, GL_DECR); */
		/* dgShadowVolumeRender(shadows[i]); */
		
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(0, 1);
		
		// Z-fail.
		glFrontFace(GL_CW);
		glStencilOp(GL_KEEP, GL_INCR, GL_KEEP);
		for (int j = 0; j < numMeshes; ++j) dgShadowVolumeRender(shadows[i][j]);
		glFrontFace(GL_CCW);
		glStencilOp(GL_KEEP, GL_DECR, GL_KEEP);
		for (int j = 0; j < numMeshes; ++j) dgShadowVolumeRender(shadows[i][j]);
		
		glPolygonOffset(0, 0);
		glDisable(GL_POLYGON_OFFSET_FILL);
		
		glFrontFace(GL_CCW);
		glColorMask(1, 1, 1, 1);
		glStencilFunc(GL_EQUAL, 0, ~0);
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		glEnable(GL_LIGHTING);
		glDepthMask(GL_TRUE);
		dgLightConfigure(lights + i, 1);
		renderScene();
		
		glPopAttrib();
	}
	
}

void dgShadowVolumeRender(int vol)
{
	assert(volume_list != NULL);
	assert(dgArrayListItemExists(volume_list, vol));
	
	// Get the shadow volume we are working with.
	dgShadowVolume *s = dgArrayListGetItem(volume_list, vol);

	// Looks like this volume hasn't been generated yet, abort.
	if (!s->quads)
	{
		return;
	}
	
	glPushMatrix();
	glTranslatef(s->pos[0], s->pos[1], s->pos[2]);
	glRotatef(dgMathRadiansToDegrees(s->rot[3]), s->rot[0],
		  s->rot[1], s->rot[2]);
		       	
	// Sides of the volume.
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, s->quads);
	glDrawArrays(GL_QUADS, 0, s->numQuads * 4);
	glDisableClientState(GL_VERTEX_ARRAY);
	
	// Near cap.
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, s->nearCap);
	glDrawArrays(GL_TRIANGLES, 0, s->numNearFaces * 3);
	glDisableClientState(GL_VERTEX_ARRAY);
	
	// Far cap.
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, s->farCap);
	glDrawArrays(GL_TRIANGLES, 0, s->numFarFaces * 3);
	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();
}

static void dgShadowVolumeUpdateFaceVisibility(dgMesh *m, int frame, float *lightPos,
					       int *numVisible, int *numHidden)
{
	assert(m);
	assert(lightPos);
	
	// Face offset.
	const int offset = frame * m->num.faces;
	// Offset + faces.
	const dgMeshFace *end = m->faces + offset + m->num.faces;
	// Count of visible faces.
	int numVis = 0;
	
	// Update face visibility status
	// for each face.
	for (dgMeshFace *face = m->faces + offset; face != end; face++)
	{
		// Take the dot product of the plane coefficients and
		// the light's position, basically {A,B,C,D} . {x,y,z,w}.
		const float dp = dgVectorDotProd4(face->planeEq, lightPos);
		
		if (dp > 0)
		{
			++numVis;
			face->visible = true;
		}
		
		else
		{
			face->visible = false;
		}
	}
	
	*numVisible = numVis;
	*numHidden = m->num.faces - numVis;
}

/**
 * Generate a shadow volume.
 * 
 * @param vol Reference to the volume.
 * @param mesh Reference to the mesh casting the shadow.
 * @param frame Current frame in the mesh's animation.
 * @param light Reference to the light casting the shadow.
 * @param objPos Position of the mesh.
 * @param objRot Orientation of the mesh.
 */
void dgShadowVolumeGenerate(int vol, int mesh, int frame, 
			    void *light, float *objPos, float *objRot)
{
	assert(mesh >= 0);
	assert(light);
	assert(volume_list != NULL);
	assert(dgArrayListItemExists(volume_list, vol));

	// Use naughty internal methods to get pointers to the
	// mesh and light objects we are working with.
	dgMesh *m = _dgMeshGetPtr(mesh);
	const dgLight *l = (dgLight *) light;
	
	assert(m);
	assert(l);

	// Get the shadow volume we are working with.
	dgShadowVolume *s = dgArrayListGetItem(volume_list, vol);

	// Delete stored volume if it exists.
	dgCondFree(s->quads);
	dgCondFree(s->nearCap);
	dgCondFree(s->farCap);
	
	// Get the position of the light.
	float lightPos[4];
	dgLightGetfv(light, DG_LIGHT_POSITION, lightPos);
	lightPos[3] = 1;
	
	// Convert orientation to axis/angle for gl.
	float aarot[4];
	dgQuaternionToAxisAngle(objRot, aarot);

	// Need to determine light's local position.
	// Get conjugate of mesh's rotation.
	float conj[4];
	dgQuaternionConjugate(objRot, conj);
	// Apply this rotation to the light's position.
	dgVectorSub(lightPos, objPos, lightPos);
	// Get the magnitude of the light's position so
	// we can rescale it later.
	float mag = dgVectorMag(lightPos);
	// Normalize the light's position so we can rotate
	// it around the origin.
	dgVectorNorm(lightPos);
	// Rotate light's position around origin.
	dgQuaternionRotate(conj, lightPos, lightPos);
	// Rescale so light is original distance away
	// from origin.
	dgVectorScale(lightPos, mag);
	
	// Save volume orientation.
	memcpy(s->pos, objPos, sizeof(float) * 3);
	memcpy(s->rot, aarot, sizeof(float) * 4);

	// Update face visibility status from this light.
	dgShadowVolumeUpdateFaceVisibility(m, frame, lightPos,
					   &(s->numNearFaces), &(s->numFarFaces));

	// Now we know how many faces are a part of both caps.
	dgMalloc(s->nearCap, float, s->numNearFaces * 3 * 3);
	dgMalloc(s->farCap, float, s->numFarFaces * 3 * 3);	
	
	// Insertion pointers into volume caps.
	float *nearCapPtr = s->nearCap;
	float *farCapPtr = s->farCap;
	
	const float infinity = 10;
	// Face offset.
	const int faceOffset = frame * m->num.faces;
	// Vertex offset.
	const int vertexOffset = frame * 3 * m->num.verts;
	// Quads array tail pointer.
	float *quadsEnd;

	// Reset number of quads.
	s->numQuads = 0;

	// Each edge we use means 1 quad (or 4 vertices).
	// We don't know how many edges we will use right now,
	// so just assume that we are using every edge.
	// Remember 3 quads per face, 4 verts per quad, 3 components
	// per vert. So basically we need numFaces * 36 components.
	dgMalloc(s->quads, float, 36 * m->num.faces);
	quadsEnd = s->quads;

	// For each face.
	for (int fi = 0; fi < m->num.faces; ++fi)
	{
		// Get the current face.
		const dgMeshFace *face = m->faces + faceOffset + fi;
		// Offset from beginning of vertex array.
		float *offset = m->coords.verts + vertexOffset + (9 * fi);

		// If this face is visible.
		if (face->visible)
		{
			// This face  is part of the near cap.
			memcpy(nearCapPtr, offset, sizeof(float) * 3 * 3);
			
			/* memcpy(nearCapPtr + 6, offset, sizeof(float) * 3); */
			/* memcpy(nearCapPtr + 3, offset + 3, sizeof(float) * 3); */
			/* memcpy(nearCapPtr, offset + 6, sizeof(float) * 3); */

			nearCapPtr += 3 * 3;

			// For each edge of this face.
			for (int ei = 0; ei < 3; ++ei)
			{
				// If this edge has no neighbor or it's
				// neighbor is not visible.
				if ((m->faceConnInfo[fi].neighbors[ei] == -1) ||
				    !m->faces[faceOffset + m->faceConnInfo[fi].neighbors[ei]].visible)
				{									
					// Now get the actual vertices.
					float *v1 = offset + 3 * ei;
					float *v2 = offset + 3 * ((ei + 1) % 3);
					
					// Other two points of a quad.
					// Extended vertices.
					float *v3 = quadsEnd + 3;
					float *v4 = quadsEnd + 6;

					// Now project these two vectors
					// to 'infinity' to form the other
					// two points of a quad.
					dgVectorSub(v1, lightPos, v3);
					//dgVectorNorm(v3);
					dgVectorScale(v3, infinity);
					//dgVectorAdd(v3, lightPos, v3);

					dgVectorSub(v2, lightPos, v4);
					//dgVectorNorm(v4);
					dgVectorScale(v4, infinity);
					//dgVectorAdd(v4, lightPos, v4);
					
					memcpy(quadsEnd, v1, sizeof(float) * 3);
					memcpy(quadsEnd + 9, v2, sizeof(float) * 3);
					
					// Add one more quad to the list.
					quadsEnd += 12;
					++(s->numQuads);
				}
			}
		}
		
		else
		{
			// This face is part of the far cap
			memcpy(farCapPtr, offset, sizeof(float) * 3 * 3);

			// and scale it to "infinity".
			dgVectorSub(farCapPtr, lightPos, farCapPtr);
			dgVectorScale(farCapPtr, infinity);
			dgVectorSub(farCapPtr + 3, lightPos, farCapPtr + 3);
			dgVectorScale(farCapPtr + 3, infinity);
			dgVectorSub(farCapPtr + 6, lightPos, farCapPtr + 6);
			dgVectorScale(farCapPtr + 6, infinity);
			farCapPtr += 3 * 3;
		}
	}
}
