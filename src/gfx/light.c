/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d/gfx/light.h>
#include <dagger3d.h>

// http://www.opengl.org/resources/code/samples/mjktips/VirtualizedLights/VirtualizedLights.html
// http://en.wikipedia.org/wiki/Lambertian_reflectance

static const int maxActiveLights = 8;

static dgLight defaultLight = {
	{ 0.0f, 0.0f, 1.0f, 0.0f }, // Position (XYZW)
	{ 0.0f, 0.0f, -1.0f }, // Direction (XYZ)
	
	// Color (RGBA)
	{
		{ 0.0f, 0.0f, 0.0f, 1.0f }, // Ambient
		{ 0.0f, 0.0f, 0.0f, 1.0f }, // Diffuse
		{ 0.0f, 0.0f, 0.0f, 1.0f } // Specular
	},
	
	// Spotlight
	{
		0.0f, // Exponent		
		180.0f // Cutoff
	},
	
	// Attenuation factors
	{
		1.0f, // Constant
		0.0f, // Linear
		0.0f  // Quadratic
	}
};

void *dgLightNew()
{
	dgLight *lt;
	dgMalloc(lt, dgLight, 1);
	memcpy(lt, &defaultLight, sizeof(dgLight));
	
	return lt;
}

void dgLightDelete(void *light)
{
	assert(light);
	dgFree((dgLight *) light);
}

/**
 * Draw a simple representation of a light.
 *
 * <i>This does not need to be called to light
 * a scene.</i>
 *
 * This function merely draws a sphere in
 * the light's current position and is mainly meant
 * for debugging.
 *
 * @param[in] light Light to draw.
 * @param[in] radius Radius of sphere.
 */
void dgLightDraw(void *light, float radius)
{
	assert(light);
	assert(radius > 0);
	
	const dgLight *lt = (dgLight *) light;

	glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT |
		     GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT);
	
	// Disable lighting.
	glDisable(GL_LIGHTING);

	// Draw a ball using the light's diffuse color.
	glPushMatrix();
	glTranslatef(lt->position[0], lt->position[1], lt->position[2]);
	glColor3fv(lt->color.diffuse);
	glutSolidSphere(radius, 16, 16);
	glPopMatrix();
	
	glPopAttrib();
}

static void _dgLightActivate(int i, dgLight *light)
{
	assert(light);
	
	glLightfv(i, GL_POSITION, light->position);
	glLightfv(i, GL_SPOT_DIRECTION, light->direction);
	
	glLightfv(i, GL_AMBIENT, light->color.ambient);
	glLightfv(i, GL_DIFFUSE, light->color.diffuse);
	glLightfv(i, GL_SPECULAR, light->color.specular);
	
	glLightf(i, GL_SPOT_EXPONENT, light->spotlight.exponent);
	glLightf(i, GL_SPOT_CUTOFF, light->spotlight.cutoff);
	
	glLightf(i, GL_CONSTANT_ATTENUATION, light->attenuation.constant);
	glLightf(i, GL_LINEAR_ATTENUATION, light->attenuation.linear);
	glLightf(i, GL_QUADRATIC_ATTENUATION, light->attenuation.quadratic);
	
	glEnable(i);
}

void dgLightConfigure(void **lights, int nlights)
{
	assert(nlights >= 0 && nlights <= maxActiveLights);
	
	for (int i = 0; i < maxActiveLights; ++i)
	{
		if (i < nlights)
		{
			_dgLightActivate(GL_LIGHT0 + i, (dgLight *) lights[i]);
		}

		else
		{
			glDisable(GL_LIGHT0 + i);
		}
	}
}

static float _dgLightIntensity(float *lightPos, float *objPos, float *eyePos)
{
	assert(lightPos);
	assert(objPos);
	assert(eyePos);

	// Assume surface normal points to camera.
	float surfaceNormal[3];
	dgVectorSub(eyePos, objPos, surfaceNormal);
	
	// Calculate light's direction relative to camera.
	float lightDir[3];
	dgVectorSub(lightPos, objPos, lightDir);
	
	// Calculate light intensity.
	return dgVectorDotProd(lightDir, surfaceNormal) / dgVectorMag(lightDir);
}

static int _dgLightIntensityCompare(const void *a, const void *b)
{
	return ((dgLight *) *(void**) a)->tmp.intensity - ((dgLight *) *(void**) b)->tmp.intensity;
}

void dgLightSortByIntensity(void **lights, int nlights, float *objPos, float *eyePos)
{
	assert(lights);
	assert(nlights > 0);
	assert(objPos);
	assert(eyePos);
	
	for (int i = 0; i < nlights; ++i)
	{
		dgLight *light = (dgLight *) lights[i];

		// Calculate intensity for this light.
		light->tmp.intensity = _dgLightIntensity(light->position, objPos, eyePos);
	}
	
	// Sort lights descending.
	qsort(lights, nlights, sizeof(void*), _dgLightIntensityCompare);
}

void dgLightSetf(void *light, int pname, float param)
{
	assert(light);

	dgLight *lt = (dgLight *) light;
	
	switch (pname)
	{
		dgCaseSet(DG_LIGHT_EXPONENT, lt->spotlight.exponent, param);
		dgCaseSet(DG_LIGHT_CUTOFF, lt->spotlight.cutoff, param);

		dgCaseSet(DG_LIGHT_CONSTANT, lt->attenuation.constant, param);
		dgCaseSet(DG_LIGHT_LINEAR, lt->attenuation.linear, param);
		dgCaseSet(DG_LIGHT_QUADRATIC, lt->attenuation.quadratic, param);
	}
}

void dgLightSetfv(void *light, int pname, float *params)
{
	assert(light);

	dgLight *lt = (dgLight *) light;

	switch (pname)
	{
		dgCaseSet4fv(DG_LIGHT_POSITION, lt->position, params);
		dgCaseSet3fv(DG_LIGHT_DIRECTION, lt->direction, params);

		dgCaseSet4fv(DG_LIGHT_AMBIENT, lt->color.ambient, params);
		dgCaseSet4fv(DG_LIGHT_DIFFUSE, lt->color.diffuse, params);
		dgCaseSet4fv(DG_LIGHT_SPECULAR, lt->color.specular, params);
	}
}

void dgLightGetfv(void *light, int pname, float *params)
{
	assert(light);
	assert(params);

	const dgLight *lt = (dgLight *) light;

	switch (pname)
	{
		dgCaseSet4fv(DG_LIGHT_POSITION, params, lt->position);
		dgCaseSet3fv(DG_LIGHT_DIRECTION, params, lt->direction);

		dgCaseSet4fv(DG_LIGHT_AMBIENT, params, lt->color.ambient);
		dgCaseSet4fv(DG_LIGHT_DIFFUSE, params, lt->color.diffuse);
		dgCaseSet4fv(DG_LIGHT_SPECULAR, params, lt->color.specular);

		dgCaseSet(DG_LIGHT_EXPONENT, *params, lt->spotlight.exponent);
		dgCaseSet(DG_LIGHT_CUTOFF, *params, lt->spotlight.cutoff);
		
		dgCaseSet(DG_LIGHT_CONSTANT, *params, lt->attenuation.constant);
		dgCaseSet(DG_LIGHT_LINEAR, *params, lt->attenuation.linear);
		dgCaseSet(DG_LIGHT_QUADRATIC, *params, lt->attenuation.quadratic);
	}
}

static float globalAmbient[] = { 0.2, 0.2, 0.2, 1 };

void dgLightSetGlobalAmbient(float *fv)
{
	assert(fv);
	memcpy(globalAmbient, fv, sizeof(float) * 4);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbient);
}

void dgLightGetGlobalAmbient(float *fv)
{
	assert(fv);
	memcpy(fv, globalAmbient, sizeof(float) * 4);
}
