/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dagger3d.h>

// http://sidvind.com/wiki/Skybox_tutorial

static int skyboxTexture[6] = { -1, -1, -1, 
				-1, -1, -1 };
static float skyboxSize = 100;

void dgSkyboxSize(float size)
{
	assert(size > 0);
	skyboxSize = size;
}

void dgSkyboxSide(int sname, int tex)
{
	assert(sname >= 0 && sname < 6);
	skyboxTexture[sname] = tex;
}

void dgSkyboxRender()
{
	const float front[] = {
		skyboxSize, -skyboxSize, -skyboxSize,
		-skyboxSize, -skyboxSize, -skyboxSize,
		-skyboxSize, skyboxSize, -skyboxSize,
		skyboxSize, skyboxSize, -skyboxSize,
		0, 0,
		1, 0,
		1, 1,
		0, 1
	};
	const float left[] = {
		-skyboxSize, -skyboxSize, -skyboxSize,
		-skyboxSize, -skyboxSize, skyboxSize,
		-skyboxSize, skyboxSize, skyboxSize,
		-skyboxSize, skyboxSize, -skyboxSize,
		0, 0,
		1, 0,
		1, 1,
		0, 1
	};
	const float back[] = {
		-skyboxSize, -skyboxSize, skyboxSize,
		skyboxSize, -skyboxSize, skyboxSize,
		skyboxSize, skyboxSize, skyboxSize,
		-skyboxSize, skyboxSize, skyboxSize,
		0, 0,
		1, 0,
		1, 1,
		0, 1
	};
	const float right[] = {
		skyboxSize, -skyboxSize, skyboxSize,
		skyboxSize, -skyboxSize, -skyboxSize,
		skyboxSize, skyboxSize, -skyboxSize,
		skyboxSize, skyboxSize, skyboxSize,
		0, 0,
		1, 0,
		1, 1,
		0, 1
	};
	const float top[] = {
		-skyboxSize, skyboxSize, -skyboxSize,
		-skyboxSize, skyboxSize, skyboxSize,
		skyboxSize, skyboxSize, skyboxSize,
		skyboxSize, skyboxSize, -skyboxSize,
		1, 1,
		0, 1,
		0, 0,
		1, 0,
	};
	const float bottom[] = {
		-skyboxSize, -skyboxSize, -skyboxSize,
		-skyboxSize, -skyboxSize, skyboxSize,
		skyboxSize, -skyboxSize, skyboxSize,
		skyboxSize, -skyboxSize, -skyboxSize,
		0, 0,
		0, 1,
		1, 1,
		1, 0
	};

	glPushMatrix();     
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_FOG);
	
	glCullFace(GL_FRONT);
	
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);

	// Front
	if (skyboxTexture[DG_SKYBOX_FRONT] >= 0)
	{
		glBindTexture(GL_TEXTURE_2D, 
			      dgTextureGetRefGL(skyboxTexture[DG_SKYBOX_FRONT]));
		glTexCoordPointer(2, GL_FLOAT, 0, front + 12);
		glVertexPointer(3, GL_FLOAT, 0, front);
		glDrawArrays(GL_QUADS, 0, 4);
	}

	// Left
	if (skyboxTexture[DG_SKYBOX_LEFT] >= 0)
	{
		glBindTexture(GL_TEXTURE_2D, 
			      dgTextureGetRefGL(skyboxTexture[DG_SKYBOX_LEFT]));
		glTexCoordPointer(2, GL_FLOAT, 0, left + 12);
		glVertexPointer(3, GL_FLOAT, 0, left);
		glDrawArrays(GL_QUADS, 0, 4);
	}
	
	// Back
	if (skyboxTexture[DG_SKYBOX_BACK] >= 0)
	{
		glBindTexture(GL_TEXTURE_2D, 
			      dgTextureGetRefGL(skyboxTexture[DG_SKYBOX_BACK]));
		glTexCoordPointer(2, GL_FLOAT, 0, back + 12);
		glVertexPointer(3, GL_FLOAT, 0, back);	
		glDrawArrays(GL_QUADS, 0, 4);
	}

	// Right
	if (skyboxTexture[DG_SKYBOX_RIGHT] >= 0)
	{
		glBindTexture(GL_TEXTURE_2D, 
			      dgTextureGetRefGL(skyboxTexture[DG_SKYBOX_RIGHT]));
		glTexCoordPointer(2, GL_FLOAT, 0, right + 12);
		glVertexPointer(3, GL_FLOAT, 0, right);	
		glDrawArrays(GL_QUADS, 0, 4);
	}

	// Top
	if (skyboxTexture[DG_SKYBOX_TOP] >= 0)
	{
		glBindTexture(GL_TEXTURE_2D, 
			      dgTextureGetRefGL(skyboxTexture[DG_SKYBOX_TOP]));
		glTexCoordPointer(2, GL_FLOAT, 0, top + 12);
		glVertexPointer(3, GL_FLOAT, 0, top);	
		glDrawArrays(GL_QUADS, 0, 4);
	}

	// Bottom
	if (skyboxTexture[DG_SKYBOX_BOTTOM] >= 0)
	{
		glBindTexture(GL_TEXTURE_2D, 
			      dgTextureGetRefGL(skyboxTexture[DG_SKYBOX_BOTTOM]));
		glTexCoordPointer(2, GL_FLOAT, 0, bottom + 12);
		glVertexPointer(3, GL_FLOAT, 0, bottom);	
		glDrawArrays(GL_QUADS, 0, 4);
	}

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	glPopAttrib();
	glPopMatrix();
}
