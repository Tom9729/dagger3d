include config.mk

SVN=svn
DOXYGEN=doxygen
DOXYFILE=doc/dagger3d.cfg
API_DOCS=doc/api

.PHONY: all clean

all: Makefile $(LIB) $(ANAME) $(EXAMPLES) $(TESTS)

%.a: $(OBJS)
	$(AR) r $@ $(OBJS)

%.so: $(OBJS)
	$(CXX) $(LDFLAGS) $(OBJS) -shared -Wl,-soname,$@ -o $@

%.dll: $(OBJS)
	$(DLLWRAP) --dllname $@ $(OBJS) --export-all-symbols $(LDFLAGS)

%.dylib: $(OBJS)
	$(CXX) $(LDFLAGS) -dynamiclib -install_name $@ -o $@ $(OBJS)

docs: $(API_DOCS) $(OBJS) $(DOXYFILE)
	$(DOXYGEN) $(DOXYFILE)
	$(SVN) add $(API_DOCS)/html/*
	$(SVN) add $(API_DOCS)/html/search/*

$(API_DOCS):
	mkdir $@

examples/%: examples/%.c
	$(CC) -o $@ $< $(CFLAGS) $(EXAMPLES_LDFLAGS)	

tests/%: tests/%.c
	$(CC) -o $@ $< $(CFLAGS) $(EXAMPLES_LDFLAGS)

tests/%: tests/%.cc
	$(CXX) -o $@ $< $(CXXFLAGS) $(EXAMPLES_LDFLAGS)

examples/%.exe: examples/%.c
	$(CC) -o $@ $< $(CFLAGS) $(EXAMPLES_LDFLAGS)

tests/%.exe: tests/%.c
	$(CC) -o $@ $< $(CFLAGS) $(EXAMPLES_LDFLAGS)

tests/%.exe: tests/%.cc
	$(CXX) -o $@ $< $(XXCFLAGS) $(EXAMPLES_LDFLAGS)

clean:
	rm -rf $(OBJS) $(LIB) $(EXAMPLES) $(TESTS) $(ANAME)
