#!/bin/sh
# Very simple build script.
# Author: Tom Arnold
##

## Linux32 build.
cc -m32 -o glExtChecker glExtChecker.c -lglut

## Win32 build using Mingw xcompiler.
i386-mingw32-gcc -o glExtChecker.exe glExtChecker.c -lglut32 -lopengl32
