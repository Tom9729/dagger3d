#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef DARWIN
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

static const char *filename = "extensions.txt";
static int window;

int dgFileWrite(const char *fn, char *str, int ovrw)
{
	int fd, fg;
	
	fd = 0;
	fg = O_WRONLY | O_CREAT | (ovrw ? 0 : O_APPEND);

	if (ovrw)
	{
		unlink(fn);
	}

	// Windows doesn't have group permissions in
	// it's filesystems
	#ifdef WIN32
	fd = open(fn, fg, S_IRUSR | S_IWUSR);
	#else
	// open file, create with permissions set to 660
	// (rw by owner/group) if file does not exist
	fd = open(fn, fg, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
	#endif

	if (fd == -1)
	{
		fprintf(stderr, "Error: couldn't open file: %s\n", fn);
		return -1;
	}

	// Reuse our flags variable to store the number
	// of bytes written...
	fg = write(fd, str, strlen(str));

	close(fd);

	return fg;
}

void fauxDisplay()
{
	const char *extensions = glGetString(GL_EXTENSIONS);
	const char *version = glGetString(GL_VERSION);
	const char *vendor = glGetString(GL_VENDOR);
	const char *renderer = glGetString(GL_RENDERER);
	
	dgFileWrite(filename, "Version: ", 1);
	dgFileWrite(filename, version, 0);
	
	dgFileWrite(filename, "\nVendor: ", 0);
	dgFileWrite(filename, vendor, 0);
	
	dgFileWrite(filename, "\nRenderer: ", 0);
	dgFileWrite(filename, renderer, 0);

	dgFileWrite(filename, "\nExtensions: ", 0);
	dgFileWrite(filename, extensions, 0);

	dgFileWrite(filename, "\n", 0);
	
	printf("Wrote <%s>.\n", filename);
	
	glutDestroyWindow(window);
	exit(0);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(10, 10);
	window = glutCreateWindow("glExtChecker");
	glutDisplayFunc(fauxDisplay);	
	glutMainLoop();

	return 0;
}
