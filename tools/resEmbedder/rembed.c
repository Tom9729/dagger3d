#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <memory.h>

void readFile(char *fn, unsigned char **data, long *size)
{
	FILE* f = NULL;

	// Attempt to open the file.
	if (!(f = fopen(fn, "rb")))
	{
		// Error.
		*data = NULL;
		return;
	}

	// Determine file size.
	fseek(f, 0, SEEK_END);
	*size = ftell(f);

	// Go back to beginning.
	fseek(f, 0, SEEK_SET);

	// Allocate memory to hold all data.
	*data = malloc(*size);

	// Read data into array.
	fread(*data, sizeof(char), *size, f);

	// Done reading.
	fclose(f);
}

void writeHeaderFile(char *fn, unsigned char *data, long size)
{
	FILE* f = NULL;

	// Attempt to open the file.
	if (!(f = fopen(fn, "wb")))
	{
		// Error.
		return;
	}

	printf("Wrote <%s>.\n", fn);   

	// For each character in filename...
	for (char *c = fn; *c != '\0'; ++c)
	{
		// Replace non-alphanumeric characters with
		// underscores.
		if (!(isalpha(*c) || isdigit(*c)))
		{
			*c = '_';
		}
	}

	// Write data byte by byte here as unsigned 8bit integers.
	for (long i = 0; i < size; ++i)
	{
		fprintf(f, "%d, ", (unsigned char) data[i]);
	}
    
	// Done writing.
	fclose(f);
}

int main(int argc, char **argv)
{
	if (argc == 1)
	{
		fprintf(stderr, "Usage: %s [FILES]\n", argv[0]);
		return EXIT_FAILURE;
	}

	// For each file...
	while (argc-- > 1)
	{
		// Size of the file in bytes.
		long size;
		// File's data.
		unsigned char *data;

		// Read in file data.
		readFile(argv[argc], &data, &size);

		// If data appears invalid, go to next file.
		if (!data)
		{
			continue;
		}

		// Length of the original file name.
		int len = strlen(argv[argc]) + 1;
		
		// New "mangled" filename + 2 bytes
		// for '.' + 'h'.
		char *mangledfn = malloc(len + 2);
		strcpy(mangledfn, argv[argc]);

		// Add extension and nul.
		mangledfn[len - 1] = '.';
		mangledfn[len] = 'h';
		mangledfn[len + 1] = '\0';
		
		// Write a header file containing
		// this file's data.
		writeHeaderFile(mangledfn, data, size);

		// Done using this data.
		free(mangledfn);
		free(data);
	}

	return 0;
}
