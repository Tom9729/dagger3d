#!BPY
"""
Name: 'CDM2 (.cdm)...'
Blender: 2.43
Group: 'Export'
Tooltip: 'Exports to the Compiled Dagger Mesh format (.cdm).'
"""
from struct import *
import Blender
from Blender import NMesh
from Blender import sys as bsys

VERSION = "CDM2"

def gen_magic(text):
    magic = 0
    count = 0
    for c in text:
        magic += (ord(c) << count)
        count += 8
    return magic

def write_obj(filepath):
   
    print("Beginning CDM export.")
    print("Opening file (" + filepath + ").")

    out = file(filepath, 'wb')
    
    ## Get a list of objects from Blender (even though
    ## we are only going to use the first one).
    objs = Blender.Object.GetSelected()
    
    if len(objs) != 1:
        Blender.Draw.PupMenu('Error: Please only select one object.')
        return

    obj = objs[0]
    mesh = obj.getData(False, True)
    mesh = mesh.__copy__()
    matrix = obj.getMatrix('worldspace')

    ## Grab some info from Blender.
    frameStart = Blender.Get('staframe')
    frameStop = Blender.Get('endframe')
    frameCount = frameStop - frameStart + 1
    vertCount = len(mesh.faces) * 3

    ## Write the version number as a 3-byte string (no null terminator).
    print("Writing version (" + VERSION + ")")
    #for c in VERSION: out.write(pack('c', c))
    out.write(pack('i', gen_magic(VERSION)))

    ## Write the vertex count as an unsigned integer.
    print("Writing vertex count (" + str(vertCount) + ").")
    out.write(pack('I', vertCount))

    ## Write the frame count as an unsigned integer.
    print("Writing frame count (" + str(frameCount) + ").")
    out.write(pack('I', frameCount))

    ## Write all of the vertices for each frame.
    for frame in range(frameStart, frameStart + frameCount):
        Blender.Set('curframe', frame)
        mesh.getFromObject(obj.name)
        for tri in mesh.faces:       
            for vert in tri.v:
                rv = mesh.verts[vert.index]                
                for com in rv.co:
                    out.write(pack('f', com))
        #print("")

    ## Write all of the normals for each frame.
    for frame in range(frameStart, frameStart + frameCount):
        Blender.Set('curframe', frame)
        for tri in mesh.faces:       
            for vert in tri.v:
                rv = mesh.verts[vert.index]
                for com in rv.no:
                    out.write(pack('f', com))

    ## Now write texture coordinates.
    for tri in mesh.faces:
        for coord in tri.uv:
            for com in coord:
                out.write(pack('f', com))

    ## Done writing. 
    out.close()

def fs_callback(filename):
    if not filename.endswith('.cdm'):
        filename = '%s.cdm' % filename
    write_obj(filename)

objs = Blender.Object.GetSelected()
if not objs:
    Blender.Draw.PupMenu('Error: No objects selected')
else:
    fname = bsys.makename(ext=".cdm")
    Blender.Window.FileSelector(fs_callback, "Export CDM2", fname)
