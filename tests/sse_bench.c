#include <dagger3d.h>

int main()
{
	double start = dgTime();
	
	for (int i = 0; i < 10000; ++i)
	{
		float v1[4] = {
			1.2, 3.4, 5.6, 2.3
		};

		float v2[4] = {
			3.6, 45.2, 5.6, 7.1
		};
		
		float dp = dgVectorDotProd4(v1, v2);
	}
	
	printf("%f\n", dgTime() - start);
	
	return 0;
}
