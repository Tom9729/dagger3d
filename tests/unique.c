#include <dagger3d.h>

int main()
{
	int gen = dgUniqueSourceNew();

	int id[10];
	
	for (int i = 0; i < 10; ++i)
	{
		id[i] = dgUniqueGetId(gen);
	}

	for (int i = 4; i >= 0; --i)
	{
		dgUniqueReleaseId(gen, id[i]);
	}

	for (int i = 0; i < 5; ++i)
	{
		id[i] = dgUniqueGetId(gen);
	}
	
	assert(id[0] == 5);
	assert(id[1] == 4);
	assert(id[2] == 3);
	assert(id[3] == 2);
	assert(id[4] == 1);
	assert(id[5] == 6);
	assert(id[6] == 7);
	assert(id[7] == 8);
	assert(id[8] == 9);
	assert(id[9] == 10);
	
	dgUniqueSourceDelete(gen);
}
