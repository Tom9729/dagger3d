// #include <dagger3d.h>
// #include <vector>
// #include <list>

// using std::list;
// using std::vector;

// const int elementCount = 100000;
// const int holes = 10000;
// double start, stop;

// void benchStart(const char *name)
// {
// 	printf("Running: %s\n", name);
// 	start = dgTime();
// }

// void benchStop()
// {
// 	stop = dgTime();
// 	printf("Time Elapsed: %f\n", stop - start);
// }

// void arrayListFill(void *arrayList, const int n)
// {
// 	for (int i = 0; i < n; ++i)
// 	{
// 		// Allocate memory for a random number.
// 		int *rnum = new int;
// 		*rnum = dgRandomInt(0, 100);
		
// 		// Add pointer.
// 		dgArrayListAddItem(arrayList, rnum);
// 	}
// }

// void vectorFill(vector<int*> *v, const int n)
// {
// 	for (int i = 0; i < elementCount; ++i)
// 	{
// 		// Allocate memory for a random number.
// 		int *rnum = new int;
// 		*rnum = dgRandomInt(0, 100);
		
// 		// Add pointer.
// 		v->push_back(rnum);
// 	}
// }

// void arrayListEmpty(void *arrayList)
// {
// 	dgArrayListForEach(int, rnum, arrayList, 
// 			   delete rnum;
// 		);
// }

// void vectorEmpty(vector<int*> *v)
// {
// 	for (int i = 0; i < elementCount; ++i)
// 	{
// 		delete (*v)[i];
// 	}
// }

// void arrayListMakeHoles(void *arrayList)
// {
// 	for (int i = 0; i < holes; ++i)
// 	{
// 		int rpos = dgRandomInt(0, dgArrayListCapacity(arrayList) - 1);
		
// 		do
// 		{
// 			rpos = dgRandomInt(0, 
// 					   dgArrayListCapacity(arrayList) - 1);
// 		} while (!dgArrayListItemExists(arrayList, rpos));

// 		delete dgArrayListDeleteItem(arrayList, rpos);
// 	}
// }

// void vectorMakeHoles(vector<int*> *v)
// {
// 	for (int i = 0; i < holes; ++i)
// 	{
// 		int rpos = dgRandomInt(0, v->size() - 1);
// 		v->erase(v->begin() + rpos);
// 	}
// }

// void arrayListAdd()
// {
// 	void *arrayList = dgArrayListNew();
// 	arrayListFill(arrayList, elementCount);
// 	arrayListMakeHoles(arrayList);
// 	benchStart("arrayListAdd");
// 	arrayListFill(arrayList, holes);
// 	benchStop();
// 	arrayListEmpty(arrayList);
// 	dgArrayListDelete(arrayList);
// }

// void vectorAdd()
// {
// 	vector<int*> v;
// 	vectorFill(&v, elementCount);
// 	vectorMakeHoles(&v);
// 	benchStart("vectorAdd");
// 	vectorFill(&v, holes);
// 	benchStop();
// 	vectorEmpty(&v);
// }

int main()
{
	// vectorAdd();
	// arrayListAdd();

	return 0;
}
