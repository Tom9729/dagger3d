// #include <map>
// #include <list>
// #include <utility>
// #include <ctype.h>

// using std::map;
// using std::list;
// using std::pair;
// using std::make_pair;

// #include <dagger3d.h>

// const char *alphas = "abcdefghijklmnopqrstuvwxyz"
// 	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
// 	"0123456789_";
// const int alphaSize = 26 + 26 + 11;

// void randomString(char *ptr, int len)
// {
// 	// Generate len-random characters.
// 	for (int i = 0; i < len; ++i)
// 	{
// 		ptr[i] = alphas[dgRandomInt(0, alphaSize - 1)];
// 	}

// 	ptr[len] = 0;
// }

// void benchmark_strcmp(int stringLen, int numStrings, int iterations)
// {
// 	// Filename => reference count
// 	list< pair<char *, int> > refMap;

// 	// List of "filenames"
// 	char fnList[numStrings][stringLen];
	
// 	// Generate a list of random "filenames"
// 	for (int i = 0; i < numStrings; ++i)
// 	{
// 		randomString(fnList[i], stringLen);
// 	}

// 	const double start = dgTime();

// 	for (int i = 0; i < iterations; ++i)
// 	{
// 		bool found = false;
// 		// Grab a random filename.
// 		char *fn = fnList[dgRandomInt(0, numStrings - 1)];
		
// 		// Search list for filename.
// 		for (list< pair<char *, int> >::iterator j = refMap.begin(); j != refMap.end(); ++j)
// 		{
// 			// Found it!
// 			if (!strcmp(fn, (*j).first))
// 			{
// 				// Increment reference count and break.
// 				++((*j).second);
// 				found = true;
// 				break;
// 			}
// 		}

// 		// Otherwise we need to insert a new entry.
// 		if (!found)
// 		{
// 			refMap.push_back(make_pair(fn, 1));
// 		}
// 	}

// 	const double stop = dgTime();
// 	const double elapsed = stop - start;
// 	printf("time elapsed\t%f\t%s(%d, %d, %d)\n", elapsed, __FUNCTION__, stringLen, numStrings, iterations);
// }

// void benchmark_hash(int stringLen, int numStrings, int iterations)
// {
// 	// Hash code => reference count
// 	map<long, int> refMap;

// 	// List of "filenames"
// 	char fnList[numStrings][stringLen];
	
// 	// Generate a list of random "filenames"
// 	for (int i = 0; i < numStrings; ++i)
// 	{
// 		randomString(fnList[i], stringLen);
// 	}
	
// 	const double start = dgTime();
	
// 	for (int i = 0; i < iterations; ++i)
// 	{
// 		// Grab a random filename.
// 		char *fn = fnList[dgRandomInt(0, numStrings - 1)];
		
// 		// Generate hash code for filename.
// 		unsigned long hashCode = dgStringHash((unsigned char *) fn);
		
// 		// Increment reference count.
// 		++refMap[hashCode];
// 	}
       
// 	const double stop = dgTime();
// 	const double elapsed = stop - start;
// 	printf("time elapsed\t%f\t%s(%d, %d, %d)\n", elapsed, __FUNCTION__, stringLen, numStrings, iterations);
// }

int main()
{
	// benchmark_strcmp(5, 100, 1000000);
	// benchmark_hash(5, 100, 1000000);
	// puts("FIXME");
	return 0;
}
