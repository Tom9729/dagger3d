#include <dagger3d.h>

int main()
{
	struct foo {
		int a, b, c;
	};

	void *list = dgArrayListNew();
	
	struct foo foo1, foo2;

	int ref1 = dgArrayListAddItem(list, &foo1);
	int ref2 = dgArrayListAddItem(list, &foo2);

	assert(dgArrayListSize(list) == 2);

	dgArrayListDeleteItem(list, ref1);
	
	assert(dgArrayListSize(list) == 1);

	dgArrayListDeleteItem(list, ref2);
	
	assert(dgArrayListSize(list) == 0);

	return 0;
}
