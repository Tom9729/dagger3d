#include <dagger3d.h>

int main(int argc, char **argv)
{
	dgInit(argc, argv);

	void *list = dgArrayListNew();
	
	// random adds
	for (int i = 0; i < 90; ++i)
	{
		dgArrayListAddItem(list, (void *) dgRandomInt(1, 100));
	}
	
	printf("frag ratio: %f\n", dgArrayListFragRatio(list));
	
	// random removes
	for (int i = 0; i < 60; ++i)
	{
		int remIdx;
		for (remIdx = dgRandomInt(0, dgArrayListCapacity(list));
		     !dgArrayListItemExists(list, remIdx);
		     remIdx = dgRandomInt(0, dgArrayListCapacity(list)));
		//printf("deleting %d\n", remIdx);
		dgArrayListDeleteItem(list, remIdx);
	}
	
	printf("frag ratio: %f\n", dgArrayListFragRatio(list));
	dgArrayListSweep(list);
	printf("frag ratio: %f\n", dgArrayListFragRatio(list));
	
	
	printf("array capacity: %d\n", dgArrayListCapacity(list));
	printf("num items: %d\n", dgArrayListSize(list));
	dgArrayListTrim(list);
	printf("array capacity: %d\n", dgArrayListCapacity(list));

	// random adds
	for (int i = 0; i < 31; ++i)
	{
		dgArrayListAddItem(list, (void *) dgRandomInt(1, 100));
	}

	printf("frag ratio: %f\n", dgArrayListFragRatio(list));
	printf("array capacity: %d\n", dgArrayListCapacity(list));
	printf("num items: %d\n", dgArrayListSize(list));

	dgArrayListDelete(list);
	
	return 0;
}
