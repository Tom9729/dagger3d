#!/bin/sh

#### BEGIN CONFIG ####

## General configuration.
TESTS_DIR="tests"
EXAMPLES_DIR="examples"
SRC_DIR="src"
INCLUDE_DIR="include"
CONFIG="config.mk"
FIND="find"

## Configuration options for all targets.
CFLAGS="$CFLAGS -DMEMPROF -std=gnu99 -fno-inline -g3 -ggdb -Wall -I$INCLUDE_DIR "
CXXFLAGS="$CXXFLAGS -DMEMPROF -g3 -ggdb -Wall -I$INCLUDE_DIR"
LDFLAGS="$LDFLAGS -lm"
ANAME="libdagger.a"

## Examples/tests flags.
LDFLAGS2="$LDFLAGS2 -L."

## Configuration for Linux native.
CC_LINUX="gcc"
AR_LINUX="ar"
CXX_LINUX="g++"
CFLAGS_LINUX="-fPIC -I/usr/include/freetype2 -DLINUX -fPIC"
CXXFLAGS_LINUX="-fPIC -I/usr/include/freetype2 -DLINUX -fPIC"
LDFLAGS_LINUX="-lglut -lGL -lGLU -lopenal -lvorbisfile\
 -lfreetype -lpng -ljpeg"
LIB_LINUX="libdagger3d.so"

## Configuration for Linux-Win32 xcompile.
CC_LINWIN32="i386-mingw32-gcc"
AR_LINWIN32="i386-mingw32-ar"
CXX_LINWIN32="i386-mingw32-g++"
CFLAGS_LINWIN32="-DWIN32 -DMINGW"
CXXFLAGS_LINWIN32="-DWIN32 -DMINGW"
LDFLAGS_LINWIN32="-lGLee -lglut32 -lopengl32 -lglu32 -lopenal32\
 -lvorbisfile -lfreetype -lpng -ljpeg -lz -lwinmm"
LIB_LINWIN32="libdagger3d.dll"
DLLWRAP_LINWIN32="i386-mingw32-dllwrap"

## Configuration for Win native.
CC_WIN="gcc"
AR_WIN="ar"
CXX_WIN="g++"
CFLAGS_WIN="-DWIN32 -DMINGW"
CXXFLAGS_WIN="-DWIN32 -DMINGW"
LDFLAGS_WIN="-lGLee -lfreeglut -lopengl32 -lglu32 -lopenal32\
 -lvorbisfile -lfreetype -lpng -ljpeg -lz -lwinmm -lstdc++ -lpthreadGC2"
LIB_WIN="libdagger3d.dll"
DLLWRAP_WIN="dllwrap"

## Configuration for Darwin (10.2.0).
CC_DARWIN="gcc"
AR_DARWIN="ar"
CXX_DARWIN="g++"
CFLAGS_DARWIN="-DDARWIN -I/usr/local/include/freetype2 -fno-common -fPIC -fnested-functions"
CXXFLAGS_DARWIN="-DDARWIN -I/usr/local/include/freetype2 -fno-common -fPIC"
LDFLAGS_DARWIN="-framework GLUT -framework OpenGL -framework Cocoa -framework OpenAL -framework Foundation -lvorbisfile -lfreetype -lpng -ljpeg -lz"
LIB_DARWIN="libdagger3d.dylib"

#### END CONFIG ####

## If a config file exists, run 'make clean'.
if [ -e $CONFIG ];
then
    echo "Cleaning old build environment!"
    echo
    make clean
    echo ""
fi

## Determine the build environment from uname.
BUILD_ENV="$(uname)"

## Determine if this is a big endian machine.
if [ "$(uname -p)" = "powerpc" ];
then
    CFLAGS="$CFLAGS -DDG_BIG_ENDIAN"
fi

# Xcompile targets.
if [ "$TARGET" = "" ] || [ "$TARGET" = "MINGW32" ] || [ "$TARGET" = "Linux" ];
then
    echo ""
else
    echo "Unknown target, options are: Linux MINGW32"
    exit 1
fi

## If no target environment is specified...
if [ "$TARGET" = "" ];
then    
    ## Then use the build environment.
    TARGET_ENV="$BUILD_ENV"
    echo "No target specified, using build environment."
## Otherwise use the target specified.
else
    TARGET_ENV="$TARGET"
fi

## If the host is Linux.
if [ "$BUILD_ENV" = "Linux" ];
then
    ## If the target is Linux.
    if [ "$TARGET_ENV" = "Linux" ];
    then
	CC="$CC_LINUX"
	AR="$AR_LINUX"
	CXX="$CXX_LINUX"
	CFLAGS="$CFLAGS $CFLAGS_LINUX"
	CXXFLAGS="$CXXFLAGS $CXXFLAGS_LINUX"
	LDFLAGS="$LDFLAGS $LDFLAGS_LINUX"
	LIB="$LIB_LINUX"
	DLLWRAP=""

    ## If the target is Win32.
    elif [ "$(echo $TARGET_ENV | grep MINGW32)" != "" ];
    then
	CC="$CC_LINWIN32"
	AR="$AR_LINWIN32"
	CXX="$CXX_LINWIN32"
	CFLAGS="$CFLAGS $CFLAGS_LINWIN32"
	CXXFLAGS="$CXXFLAGS $CXXFLAGS_LINWIN32"
	LDFLAGS="$LDFLAGS $LDFLAGS_LINWIN32"
	LIB="$LIB_LINWIN32"
	DLLWRAP="$DLLWRAP_LINWIN32"
    fi
## If the host is Win.
elif [ "$(echo $TARGET_ENV | grep MINGW32)" != "" ];
then
    CC="$CC_WIN"
    CXX="$CXX_WIN"
    AR="$AR_WIN"
    CFLAGS="$CFLAGS $CFLAGS_WIN"
    CXXFLAGS="$CXXFLAGS $CXXFLAGS_WIN"
    LDFLAGS="$LDFLAGS $LDFLAGS_WIN"
    LIB="$LIB_WIN"
    DLLWRAP="$DLLWRAP_WIN"
    
    ## Use a different version of find
    ## since 'find' is a MSDOS command.
    FIND="unix_find"
## If the host is Darwin.
elif [ "$BUILD_ENV" = "Darwin" ];
then
    CC="$CC_DARWIN"
    AR="$AR_DARWIN"
    CXX="$CXX_DARWIN"
    CFLAGS="$CFLAGS $CFLAGS_DARWIN"
    CXXFLAGS="$CXXFLAGS $CXXFLAGS_DARWIN"
    LDFLAGS="$LDFLAGS $LDFLAGS_DARWIN"
    LIB="$LIB_DARWIN"
    DLLWRAP=""
fi

## Add library build flags to examples build flags.
LDFLAGS2="-ldagger3d $LDFLAGS $LDFLAGS2"

## Build a list of examples.
for FILE in $("$FIND" "$EXAMPLES_DIR" -name "*.c")
do
    ## Strip file extension.
    FILE="$(echo $FILE | sed -e 's/\.c//g')"
    
    ## If we are building for Windows, add
    ## an exe extension.
    if [ "$(echo $TARGET_ENV | grep MINGW32)" != "" ];
    then
	FILE="$FILE.exe"
    fi

    ## Add file to the list.
    EXAMPLES="$EXAMPLES $FILE"
done

## Build a list of tests.
for FILE in $("$FIND" "$TESTS_DIR" -name "*.c" -o -name "*.cc")
do
    ## Strip file extension.
    FILE="$(echo $FILE | sed -e 's/\.cc$//g' -e 's/\.c//g')"
    
    ## If we are building for Windows, add
    ## an exe extension.
    if [ "$(echo $TARGET_ENV | grep MINGW32)" != "" ];
    then
	FILE="$FILE.exe"
    fi

    ## Add file to the list.
    TESTS="$TESTS $FILE"
done

## Build a list of objects.
for FILE in $("$FIND" "$SRC_DIR" -name "*.c" -o -name "*.cc")
do
    ## Strip file extension and replace with '.o'.
    FILE="$(echo $FILE | sed -e 's/\.cc$/\.o/g' -e 's/\.c$/\.o/g')"
    
    ## Add file to the list.
    OBJS="$OBJS $FILE"
done

## Print build information.
echo
echo "Target: $TARGET_ENV"
echo
echo "CC: $CC"
echo
echo "CFLAGS: $CFLAGS"
echo
echo "LDFLAGS: $LDFLAGS"
echo
echo "LIB: $LIB"
echo
echo "DLLWRAP: $DLLWRAP"
echo
echo "EXAMPLES: $(echo $EXAMPLES | sed -e 's/examples\///g')"
echo
echo "TESTS: $(echo $TESTS | sed -e 's/tests\///g')"
echo
echo "OBJECTS: $(echo $OBJS | sed -e 's/src\///g')"

## If an old config exists, delete it!
if [ -e "$CONFIG" ];
then
    rm "$CONFIG"
fi

## Write a new config.
echo "# Dagger3D configuration, generated on $(date)." >> $CONFIG
echo "# Generated on $(uname -a)." >> $CONFIG
echo "# Build environment: $BUILD_ENV" >> $CONFIG
echo "# Target environment: $TARGET_ENV" >> $CONFIG
echo "CC=$CC" >> $CONFIG
echo "CXX=$CXX" >> $CONFIG
echo "CFLAGS=$CFLAGS" >> $CONFIG
echo "CXXFLAGS=$CXXFLAGS" >> $CONFIG
echo "LDFLAGS=$LDFLAGS" >> $CONFIG
echo "LIB=$LIB" >> $CONFIG
echo "DLLWRAP=$DLLWRAP" >> $CONFIG
echo "EXAMPLES=$EXAMPLES" >> $CONFIG
echo "EXAMPLES_LDFLAGS=$LDFLAGS2" >> $CONFIG
echo "TESTS=$TESTS" >> $CONFIG
echo "OBJS=$OBJS" >> $CONFIG
echo "AR=$AR" >> $CONFIG
echo "ANAME=$ANAME" >> $CONFIG

## Done!
echo 
echo "Done, now type 'make'."
