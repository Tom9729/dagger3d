/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _DAGGER3D_H_
#define _DAGGER3D_H_

#ifdef __cplusplus
extern "C" {
#endif

// @cond
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <assert.h>
#include <memory.h>
#include <math.h>
#include <unistd.h>
#include <strings.h>
#include <stdarg.h>

#ifndef NO_PTHREAD
#include <pthread.h>
#endif

#ifdef DARWIN  
  #ifndef NO_OPENAL
    // Darwin puts these in weird places...
    #include <OpenAL/al.h>
    #include <OpenAL/alc.h>
  #endif
  #include <GLUT/glut.h>
#else
  #ifndef NO_OPENAL
    // For everyone else...
    #include <AL/al.h>
    #include <AL/alc.h>
  #endif
  #include <GL/glut.h>
  #include <GL/glext.h>
  #include <GL/gl.h>
#endif
// @endcond

// Define these for anyone used to Java or C++.
#ifndef true
#define true 1
#endif
#ifndef false
#define false 0
#endif

// @cond
#if !defined(MINGW) && !defined(DARWIN)
extern int usleep (__useconds_t __useconds);
#endif
// @endcond

/**
 * Converts row/column to an index, eg. for use
 * when treating a flat array as if it were two-dimensional.
 *
 * @param[in] X Row.
 * @param[in] Y Column.
 * @param[in] W Number of columns wide.
 * @return Linear position.
 */
#define dgIdx(X,Y,W) (((Y) * (W)) + (X))

#define dgDeclMalloc(PTR,TYPE,COUNT); TYPE *PTR; dgMalloc(PTR, TYPE, COUNT);

/**
 * Allocate and zero memory.
 *
 * <b>Example:</b>
 * <pre>
 * int *k;
 *
 * dgMalloc(k, int, 10);
 * // Is the same as
 * k = malloc(sizeof(int) * 10);
 * memset(k, 0, sizeof(int) * 10);
 * </pre>
 *
 * @param PTR A Pointer to sizeof(TYPE) * COUNT bytes of memory.
 * @param TYPE Data type.
 * @param COUNT How many of TYPE to allocate memory for.
 */

/**
 * Free memory.
 *
 * @param PTR Pointer to free. If pointer is null, this
 * reports the file and line number and then DOES NOT
 * try to free() the memory.
 */

#define dgCondFree(X) if (X) { dgFree(X); }

#ifdef MEMPROF
#define dgMalloc(PTR,TYPE,COUNT); (PTR) = (TYPE*) dgMemProfAlloc(sizeof(TYPE) * (COUNT), __FILE__, __FUNCTION__, __LINE__);
#define dgRealloc(PTR,TYPE,COUNT); (PTR) = (TYPE*) dgMemProfRealloc(PTR, sizeof(TYPE) * (COUNT), __FILE__, __FUNCTION__ ,__LINE__);
#define dgFree(PTR) if (!PTR) { fprintf(stderr, "dgFree(): Attempt to free null pointer in " __FILE__ " on line %d.\n", __LINE__); } else { dgMemProfFree(PTR, __FILE__, __FUNCTION__, __LINE__); }
#else
#define dgMalloc(PTR,TYPE,COUNT); {(PTR) = (TYPE*) malloc(sizeof(TYPE) * (COUNT)); memset(PTR, 0, sizeof(TYPE) * (COUNT));}
#define dgRealloc(PTR,TYPE,COUNT); (PTR) = (TYPE*) realloc(PTR, sizeof(TYPE) * (COUNT));
#define dgFree(PTR) if (!(PTR)) { fprintf(stderr, "dgFree(): Attempt to free null pointer in " __FILE__ " on line %d.\n", __LINE__); } else { free(PTR); }
#endif

extern void *dgMemProfAlloc(size_t bytes, const char *file, const char *func, const int line);
extern void *dgMemProfRealloc(void *orig_ptr, size_t bytes, const char *file, const char *func, const int line);
extern void dgMemProfFree(void *ptr, const char *file, const char *func, const int line);
extern size_t dgMemProfBytesAllocated();
extern void dgMemProfPrintRecords();

enum {
	DG_BUTTON_LEFT,
	DG_BUTTON_MIDDLE,
	DG_BUTTON_RIGHT
};

enum {
	DG_KEY_UP,
	DG_KEY_DOWN,
	DG_KEY_LEFT,
	DG_KEY_RIGHT,
	DG_KEY_F1,
	DG_KEY_F2,
	DG_KEY_F3,
	DG_KEY_F4,
	DG_KEY_F5,
	DG_KEY_F6,
	DG_KEY_F7,
	DG_KEY_F8,
	DG_KEY_F9,
	DG_KEY_F10,
	DG_KEY_F11,
	DG_KEY_F12,
	DG_KEY_INSERT,
	DG_KEY_HOME,
	DG_KEY_END,
	DG_KEY_PAGEUP,
	DG_KEY_PAGEDOWN,
	DG_KEY_UPPER_A,
	DG_KEY_UPPER_B,
	DG_KEY_UPPER_C,
	DG_KEY_UPPER_D,
	DG_KEY_UPPER_E,
	DG_KEY_UPPER_F,
	DG_KEY_UPPER_G,
	DG_KEY_UPPER_H,
	DG_KEY_UPPER_I,
	DG_KEY_UPPER_J,
	DG_KEY_UPPER_K,
	DG_KEY_UPPER_L,
	DG_KEY_UPPER_M,
	DG_KEY_UPPER_N,
	DG_KEY_UPPER_O,
	DG_KEY_UPPER_P,
	DG_KEY_UPPER_Q,
	DG_KEY_UPPER_R,
	DG_KEY_UPPER_S,
	DG_KEY_UPPER_T,
	DG_KEY_UPPER_U,
	DG_KEY_UPPER_V,
	DG_KEY_UPPER_W,
	DG_KEY_UPPER_X,
	DG_KEY_UPPER_Y,
	DG_KEY_UPPER_Z,
	DG_KEY_0,
	DG_KEY_1,
	DG_KEY_2,
	DG_KEY_3,
	DG_KEY_4,
	DG_KEY_5,
	DG_KEY_6,
	DG_KEY_7,
	DG_KEY_8,
	DG_KEY_9,
	DG_KEY_EXCLAMATION,
	DG_KEY_ASPERAND,
	DG_KEY_HASH,
	DG_KEY_DOLLAR,
	DG_KEY_PERCENT,
	DG_KEY_CARET,
	DG_KEY_AMPERSAND,
	DG_KEY_STAR,
	DG_KEY_PARENLEFT,
	DG_KEY_PARENRIGHT,
	DG_KEY_MINUS,
	DG_KEY_PLUS,
	DG_KEY_UNDERSCORE,
	DG_KEY_EQUALS,
	DG_KEY_BRACELEFT,
	DG_KEY_BRACERIGHT,
	DG_KEY_BRACKETLEFT,
	DG_KEY_BRACKETRIGHT,
	DG_KEY_COLON,
	DG_KEY_SEMICOLON,
	DG_KEY_PIPE,
	DG_KEY_SLASHFORWARD,
	DG_KEY_SLASHBACKWARD,
	DG_KEY_QUESTION,
	DG_KEY_ANGLELEFT,
	DG_KEY_ANGLERIGHT,
	DG_KEY_QUOTEDOUBLE,
	DG_KEY_QUOTESINGLE,
	DG_KEY_ESCAPE,
	DG_KEY_SPACE,
	DG_KEY_TAB,
	DG_KEY_RETURN,
	DG_KEY_TILDE,
	DG_KEY_APOSTROPHE,
	DG_KEY_BACKSPACE,
	DG_KEY_DELETE,
	DG_KEY_LOWER_A,
	DG_KEY_LOWER_B,
	DG_KEY_LOWER_C,
	DG_KEY_LOWER_D,
	DG_KEY_LOWER_E,
	DG_KEY_LOWER_F,
	DG_KEY_LOWER_G,
	DG_KEY_LOWER_H,
	DG_KEY_LOWER_I,
	DG_KEY_LOWER_J,
	DG_KEY_LOWER_K,
	DG_KEY_LOWER_L,
	DG_KEY_LOWER_M,
	DG_KEY_LOWER_N,
	DG_KEY_LOWER_O,
	DG_KEY_LOWER_P,
	DG_KEY_LOWER_Q,
	DG_KEY_LOWER_R,
	DG_KEY_LOWER_S,
	DG_KEY_LOWER_T,
	DG_KEY_LOWER_U,
	DG_KEY_LOWER_V,
	DG_KEY_LOWER_W,
	DG_KEY_LOWER_X,
	DG_KEY_LOWER_Y,
	DG_KEY_LOWER_Z
};

enum {
	DG_KEY_CTRL,
	DG_KEY_ALT,
	DG_KEY_SHIFT
};
	
extern int dgActorNew();
extern void dgActorDelete(int actor);
extern int dgActorInstanceNew(int actor);
extern void dgActorInstanceDelete(int inst);
extern void dgActorInstanceRender(int inst, float *pos, float *rot);
extern int dgActorInstanceSetAnimation(int inst, char *name);
extern int dgActorGetMesh(int actor);
extern int dgActorGetTexture(int actor);
extern void dgActorSetMesh(int actor, int mesh);
extern void dgActorSetTexture(int actor, int tex);

extern char *dgStringCat(char *str1, char *str2);
extern void dgStringAppend(char **str, const char *fmt, ...);
extern void dgStringAppend2(char **str, const char *fmt, va_list ap);
extern int dgStringCountChar(char *str, char ch);
extern unsigned long dgStringHash(unsigned char *str);
extern char *dgStringDup(const char *s);
extern char *dgStringDup2(const char *s, size_t n);

extern double dgTime();
extern char* dgTimeOfDayStr();

extern void dgRandomSeed();
extern int dgRandomInt(unsigned int min, unsigned int max);
extern int dgRandomSign();
extern float dgRandomf(float min, float max);

#define dgMathRound dgMathRoundf
#define dgMathAbs dgMathAbsf

extern float dgMathRoundf(float n);
extern float dgMathAbsf(float v);
extern float dgMathMaxf(float v1, float v2);
extern void dgMathFixAngleDeg(float *ang);
extern void dgMathFixAngleRad(float *ang);
extern int dgMathIsPowerOfTwo(long n);
extern long dgMathNextPowerOfTwo(long v);
extern float dgMathDegreesToRadians(float deg);
extern float dgMathRadiansToDegrees(float rad);
extern int dgMathFloatEquals(float a, float b, float es);
extern int dgMathQuadrant(float x, float y);
extern float dgMathGetPolarAngle(float x, float y);

extern void dgMatrixPrint16(float *m);
extern void dgMatrixIdentity16(float *m);

extern int dgGeometryFrustumContainsPoint(float *pt, float *frustum);
extern float dgGeometryPointPlaneDistance(float *pt, float *pl);
extern void dgGeometryPlaneFromPoints2(float *u, float *v, float *w, float *plane);
extern int dgGeometryPointInRectangle(float *point, float *rect);
extern void dgGeometryPlaneFromPoints(float *points, float *plane);
extern void dgGeometryFrustumQuads(float *nearQuad, float *farQuad,
			     float *pos, float *rot,
			     float nearDist, float nearWidth, float nearHeight,
			     float farDist, float farWidth, float farHeight);
extern int dgGeometryFrustumCollidesSphere(float *pt, float rad, float *frustum);
extern int dgGeometryFrustumCollidesAABB(float *aabb_verts, float *frustum_planeqs);

#define DG_GEOMETRY_NO_COLLISION 0
#define DG_GEOMETRY_CONTAINS 1
#define DG_GEOMETRY_INTERSECTS 2

extern void dgQuaternionToForward(float *q, float *v);
extern void dgQuaternionToRight(float *q, float *v);
extern void dgQuaternionToUp(float *q, float *v);
extern float dgQuaternionLength(float *q);
extern void dgQuaternionNorm(float *q);
extern void dgQuaternionFromEuler(float *q, float *e);
extern void dgQuaternionConjugate(float *q, float *qp);
extern void dgQuaternionRotate(float *q, float *vector, float *result);
extern void dgQuaternionToMatrix(float *q, float *m);
extern void dgQuaternionMult(float *a, float *b, float *c);
extern void dgQuaternionIdentity(float *q);
extern void dgQuaternionFromAxisAngle(float *q, float *a);
extern void dgQuaternionToAxisAngle(float *q, float *a);
extern void dgQuaternionApplyEuler(float *q, float x, float y, float z);
#define dgQuaternionComponents(V) (V)[0], (V)[1], (V)[2], (V)[3]
extern void dgQuaternionApply(float *q);

extern void dgVectorAdd(float *a, float *b, float *c);
extern void dgVectorSub(float *a, float *b, float *c);
extern void dgVectorMult(float *a, float *b, float *c);
extern void dgVectorScale(float *a, float sf);
extern float dgVectorMag(float *a);
extern void dgVectorNorm(float *a);
extern float *dgVectorClone(float *a);
extern void dgVectorSet(float *a, float x, float y, float z);
extern float dgVectorDotProd(float *a, float *b);
extern void dgVectorCrossProd(float *a, float *b, float *c);
extern float dgVectorDotProd4(float *a, float *b);
#define dgVectorComponents3(VEC) (VEC)[0], (VEC)[1], (VEC)[2]

extern int dgMagicGen(const char *str);

#define DG_WINDOW_MODE_WORLD 0
#define DG_WINDOW_MODE_SCREEN 1

extern void dgBillboardSpherical();

extern void dgWindowOpenOverrideWindow(int b);
extern  void dgWindowOpenOverrideFullscreen(int b);
extern void dgWindowOpenOverrideResolution(int w, int h);
extern int dgWindowScreenDumpToFile(char *fn);
extern unsigned char *dgWindowScreenDump();
extern int dgWindowGetSystemCamera();
extern void dgWindowFrustumDimensions(float *nearWidth, 
				      float *nearHeight,
				      float *farWidth, 
				      float *farHeight);
extern float dgWindowRatio();
extern void dgWindowDrawFPS(int b);
extern int dgWindowSelectGetName(int i, unsigned int *buf);
extern void dgWindowSelect(float x, float y, float w, float h);
extern void dgWindowOpen(char *title, int w, int h, int rr, 
			 int fullscreen);
extern void dgWindowClose();
extern void dgWindowSetCleanupFunc(void (*callback)());
extern void dgWindowSetInitFunc(void (*callback)());
extern void dgWindowSetDisplayFunc(void (*callback)());
extern void dgWindowSetInputFunc(void (*callback)(double scale));
extern void dgWindowSetSelectFunc(void (*callback)(int hits, unsigned int *buf));
extern void dgWindowSetLogicFunc(void (*callback)(double scale));
extern void dgWindowCoordinateMode(int mode);
extern void dgWindowSetWireframe(int b);
extern int dgWindowGetHeight();
extern int dgWindowGetWidth();
extern int dgWindowGetFPS();
extern int dgWindowVBOSupported();
extern int dgWindowNPOTSupported();
extern void dgWindowMainLoop();

#define DG_EMITTER_POINT 0
#define DG_EMITTER_PLANE 1
#define DG_EMITTER_RING 2
#define DG_EMITTER_BOX 3
#define DG_EMITTER_SPHERE 4

extern void *dgParticleEmitterNew();
extern void dgParticleEmitterDelete(void *emitter);
extern void dgParticleEmitterRender(void *emitter);
extern void dgParticleEmitterUpdate(void *emitter, double scale);
extern void dgParticleEmitterKill(void *emitter);

extern void dgParticleEmitterSetCallback(void *emitter, 
		void (*callback)(void *emitter, void *particle, double scale));

extern void dgParticleEmitterGetPosition(void *emitter, float *position);
extern void dgParticleEmitterGetRotation(void *emitter, float *rotation);
extern void dgParticleEmitterGetVelocity(void *emitter, float *velocity);
extern int  dgParticleEmitterGetNumParticles(void *emitter);
extern int  dgParticleEmitterGetType(void *emitter);
extern float dgParticleEmitterGetRadius(void *emitter);
extern double dgParticleEmitterGetFrequency(void *emitter);
extern int  dgParticleEmitterGetNumParticlesAlive(void *emitter);
extern float dgParticleEmitterGetTheta(void *emitter);
extern int  dgParticleEmitterGetTexture(void *emitter);
extern int  dgParticleEmitterGetLocal(void *emitter);

extern void dgParticleEmitterSetPosition(void *emitter, float *position);
extern void dgParticleEmitterSetRotation(void *emitter, float *rotation);
extern void dgParticleEmitterSetVelocity(void *emitter, float *velocity);
extern void dgParticleEmitterSetNumParticles(void *emitter, int numParticles);
extern void dgParticleEmitterSetType(void *emitter, int type);
extern void dgParticleEmitterSetRadius(void *emitter, float radius);
extern void dgParticleEmitterSetFrequency(void *emitter, double freq);
extern void dgParticleEmitterSetTheta(void *emitter, float theta);
extern void dgParticleEmitterSetTexture(void *emitter, int texture);
extern void dgParticleEmitterSetLocal(void *emitter, int local);

extern void dgParticleEmitterSetLife(void *emitter, 
				     double minLife, double maxLife);
extern void dgParticleEmitterSetSize(void *emitter,
				     float minSize, float maxSize);
extern void dgParticleEmitterSetColor(void *emitter,
				      float minRed, float maxRed,
				      float minGreen, float maxGreen,
				      float minBlue, float maxBlue,
				      float minAlpha, float maxAlpha);
extern void dgParticleEmitterSetSpeed(void *emitter,
				     float minSpeed, float maxSpeed);
extern void dgParticleEmitterSetAcceleration(void *emitter,
				     float minAccel, float maxAccel);

extern void dgParticleGetPosition(void *particle, float *position);
extern void dgParticleGetDirection(void *particle, float *direction);
extern void dgParticleGetColor(void *particle, float *color);
extern float dgParticleGetSpeed(void *particle);
extern float dgParticleGetAcceleration(void *particle);
extern float dgParticleGetSize(void *particle);
extern double dgParticleGetLife(void *particle);
extern void dgParticleGetVelocity(void *particle, float *velocity);

extern void dgParticleSetPosition(void *particle, float *position);
extern void dgParticleSetDirection(void *particle, float *direction);
extern void dgParticleSetColor(void *particle, float *color);
extern void dgParticleSetSpeed(void *particle, float speed);
extern void dgParticleSetAcceleration(void *particle, float accel);
extern void dgParticleSetSize(void *particle, float size);
extern void dgParticleSetLife(void *particle, double life);
extern void dgParticleSetVelocity(void *particle, float *velocity);

#define dgWindowKeyModifier dgKeyboardModifier
#define dgWindowKeyPressed dgKeyboardPressed
#define dgWindowKeyPressedOnce dgKeyboardPressedOnce
extern int dgKeyboardModifier(int mod);
extern int dgKeyboardPressed(int key);
extern int dgKeyboardPressedOnce(int key);

#define DG_AUDIO_MONO_8    0
#define DG_AUDIO_STEREO_8  1
#define DG_AUDIO_MONO_16   2
#define DG_AUDIO_STEREO_16 3

#define DG_AUDIO_OGG 0
#define DG_AUDIO_WAV 1

#define DG_AUDIO_PLAYING 0
#define DG_AUDIO_PAUSED 1
#define DG_AUDIO_FINISHED 2
#define DG_AUDIO_INTERRUPTED 3

extern int dgAudioLoadOGG(char *fn, int bref);
extern int dgAudioLoadWAV(char *fn, int bref);
extern void dgAudioData(int bref, char *data, long bytes, 
			int format, int freq);
extern int dgAudioNew(char *fn);
extern int dgAudioLoad(char *fn);
extern void dgAudioDelete(int bref);

extern int dgAudioPlay(int bref, int priority, float *pos, float *vel);
/* extern int dgAudioStream(char *fn, int priority, float *pos, float *vel); */
extern void dgAudioPlayNoHandle(int bref, int priority,
				float *pos, float *vel);
extern int dgAudioGetState(int pref);
extern int dgAudioPausePlay(int pref);
extern void dgAudioEnd(int pref);

extern int dgAudioSetSource(int pref, float *pos, float *vel);
extern void dgAudioSetListener(float *pos, float *vel, float *ori);

// Improved syntax now allows you to specify
// the name of the index variable.
#define dgArrayListForEach(TYPE,VAR,LIST,IDX,...)	\
	{\
		TYPE *VAR;\
		for (int IDX = 0; (LIST) && (IDX < dgArrayListCapacity(LIST)); ++IDX) \
		{\
			if ((VAR = (TYPE *) dgArrayListGetItem(LIST, IDX))) \
			{\
				__VA_ARGS__\
			}\
                }\
        }

extern void *dgArrayListNew();
extern void *dgArrayListNew2(int initialCapacity);
extern void dgArrayListDelete(void *list);
extern int dgArrayListSize(void *list);
extern void dgArrayListSort(void *list, int (*compare)(const void *, const void *));
extern int dgArrayListAddItem(void *list, void *ptr);
extern void *dgArrayListDeleteItem(void *list, int ref);
extern void *dgArrayListGetItem(void *list, int ref);
extern int dgArrayListFindItem(void *list, void *ptr);
extern int dgArrayListCapacity(void *list);
extern void *dgArrayListPopItem(void *list);
extern int dgArrayListItemExists(void *list, int ref);
extern void dgArrayListSweep(void *list);
extern void *dgArrayListSetItem(void *list, int ref, void *ptr);
extern float dgArrayListFragRatio(void *list);
extern void dgArrayListTrim(void *list);


extern void dgJoystickDelete(int joystick);
extern int dgJoystickDetectDevices(int **devices);
extern int dgJoystickPoll(int device);
extern int dgJoystickNumButtons(int device);
extern int dgJoystickNumAxes(int device);
extern void dgJoystickButtons(int device, char *buttons);
extern void dgJoystickAxes(int device, int *axes);
extern void dgJoystickName(int device, char *name);

extern void *dgTimerNew(double sleep);
extern void dgTimerDelete(void *timer);
extern int dgTimerReady(void *timer);
extern double dgTimerRemaining(void *timer);

extern void dgPrintf(const char *fmt, ...);

/**
 * @brief Map a local enum so that it can be used without conflicts.
 *
 * @description This is provided for cases where enum types are being used
 * as names. For example, Dagger has enums like DG_INIT_FOO that are used as
 * names of subsystems. Because C enums are basically just integers, we have
 * situations where two different enums are "equivalent" because their integer
 * values overlap.
 *
 * What this function does is offset the integer value of an enum E by 
 * 1000*K, where K is a unique integer determined by the three-character namespace.
 *
 * @example
 * enum { A, B, C };
 * enum { D, E, F };
 * 
 * // A == D is true
 * // A == dgEnumNS("DAG", D) is false
 * 
 * @param[in] S
 * @param[in] E 
 */
#define dgEnumNS(S,E)\
	((((((tolower((S)[0]) - 'a') * 26 * 26) + \
	    ((tolower((S)[1]) - 'a') * 26) + \
	    ((tolower((S)[2]) - 'a') )) + 1) * 1000) + E)

extern void dgInit(int argc, char **argv);
extern void dgInitGetArgs(int *argc, char ***argv);

enum {
	// User should place dep on this so that their
	// cleanup code always runs first!
	DG_SYS_USER,
	
	// Don't forget to update 'util/sys.cc'!
	DG_SYS_ACTOR,
	DG_SYS_AUDIO,
	DG_SYS_BSP,
	DG_SYS_CAMERA,
	DG_SYS_FONT,
	DG_SYS_JOYSTICK,
	DG_SYS_MESH,
	DG_SYS_RES,
	DG_SYS_SHADOW_VOLUME,
	DG_SYS_SOCKET,
	DG_SYS_TEXTURE,
	DG_SYS_UNIQUE,
	DG_SYS_WINDOW,
	DG_SYS_WORKER
};

extern void dgSysRegister(int name, void (*cleanup)(), int ndeps, int *deps);
extern void dgSysCleanup();
extern void dgSysBinCwd(int b);

extern int dgFontLoad(char *fname, unsigned int height);
extern void dgFontDelete(int font);
extern int dgFontPrint(int font, float x, float y, unsigned int wrap, char *str);
extern float dgFontHeight(int font);
extern int dgFontLoadMemory(unsigned char *buf, unsigned int height, int size);
extern int dgFontGetSystemFont();

#define DG_MOUSE_DEFAULT 1
#define DG_MOUSE_WAIT 2

extern void dgMouseCursor(int cursor);
extern void dgMouseGetDelta(int *x, int *y);
extern int dgMouseButtonIsDown(int button);
extern void dgMouseEnabled(int enabled);
extern int dgMouseGetX();
extern int dgMouseGetY();
extern void dgMouseSetX(int x);
extern void dgMouseSetY(int y);
extern void dgMouseShowCursor(int show);
extern void dgMouseSetCaptured(int cap);
extern int dgMouseGetCaptured();
extern void dgMouseRender();
extern int dgMouseGetAutoCapture();
extern void dgMouseSetAutoCapture(int enabled);

#define DG_TEXTURE_RGB  0
#define DG_TEXTURE_RGBA 1
#define DG_TEXTURE_BGR  2
#define DG_TEXTURE_BGRA 3
#define DG_TEXTURE_GREY 4

#define DG_TEXTURE_TGA 0
#define DG_TEXTURE_PNG 1
#define DG_TEXTURE_JPG 2

extern GLuint dgTextureGetRefGL(int ref);
extern void dgTextureDelete(int ref);
extern int dgTextureLoad(char *fn);
extern int dgTextureLoadTGA(char *fn, int ref);
extern int dgTGASave(char *fn, unsigned char *data, int mode,
		     short int width, short int height, int format);
extern int dgTGALoad(char *fn, unsigned char **data, int *mode,
			      short int *width, short int *height, int *format);
#ifndef NO_PNG
extern int dgTextureLoadPNG(char *fn, int ref);
#endif
extern int dgTextureLoadJPG(char *fn, int ref);
extern void dgTextureData(int ref, unsigned char *data, int mode, 
			  int w, int h, int format);

extern int dgBspMapLoad(char *fn);
extern void dgBspMapDelete(int map);
extern void dgBspMapRender(int map);

#define DG_MESH_CDM 0

#define DG_MESH_RENDER_NORMALS 1
#define DG_MESH_RENDER_NO_VBO 2

#define DG_MESH_NORM_INTERP 1
#define DG_MESH_VBO 2
#define DG_MESH_NO_FACE_INFO 4

#ifdef NO_GLEXT // No VBO support compiled in, eek.
#undef DG_MESH_VBO
#define DG_MESH_VBO 0
#endif

extern int dgMeshNew();
extern void dgMeshDelete(int ref);

extern int dgMeshLoad(char *fn);
extern int dgMeshLoadOpts(char *fn, int opts);
extern int dgMeshLoadFromDef(char *fn);
extern int dgMeshLoadCDM(char *fn, int ref);
extern int dgMeshSaveCDM(char *fn, int ref);
extern void dgMeshData(int ref, float *data, unsigned int nverts, 
		       unsigned int nframes);
extern void dgMeshDataOpts(int ref, float *data, unsigned int nverts, 
		       unsigned int nframes, int opts);
extern void dgMeshGetData(int ref, float **data, size_t *num, 
			  unsigned int *nverts, unsigned int *nframes);

extern void dgMeshRender(int mesh, int tex, int cframe);
extern void dgMeshRenderOpts(int mesh, int tex, int cframe, int opts);

extern void dgMeshSetFilename(int mesh, char *fn);
extern char *dgMeshGetFilename(int mesh);

extern int dgMeshAddAnimation(int mesh, char *name, int start, int end, 
			      float duration, int loop);
extern void dgMeshDeleteAnimation(int mesh, int animation);
extern int dgMeshGetAnimation(int mesh, char *name);
extern void dgMeshGetAnimationData(int mesh, int anim, int *start, 
				   int *end, float *duration, int *loop);

#define DG_SKYBOX_FRONT 0
#define DG_SKYBOX_LEFT 1
#define DG_SKYBOX_BACK 2
#define DG_SKYBOX_RIGHT 3
#define DG_SKYBOX_TOP 4
#define DG_SKYBOX_BOTTOM 5

extern void dgSkyboxSize(float size);
extern void dgSkyboxSide(int sname, int tex);
extern void dgSkyboxRender();

#define DG_CAMERA_5DOF 0
#define DG_CAMERA_6DOF 1

extern void dgCameraFrustumCone(int camera, float *angle, float *len);
extern void dgCameraFrustumPlanes(int camera, float *planeqs);
extern void dgCameraFrustumQuads(int camera, float *nearq, float *farq);
extern void dgCameraFrustumUpdate(int camera);
extern int dgCameraNew();
extern void dgCameraDelete(int camera);
extern void dgCameraSetPosition(int camera, float *pos);
extern void dgCameraGetPosition(int camera, float *pos);
extern void dgCameraRotate(int camera, float x, float y, float z);
extern void dgCameraSetType(int camera, int type);
extern int dgCameraGetType(int camera);
extern void dgCameraPerspective(int camera);
extern void dgCameraGetForward(int camera, float *v);
extern void dgCameraGetUp(int camera, float *v);
extern void dgCameraGetRight(int camera, float *v);
extern void dgCameraGetRotation(int camera, float *rot);
extern void dgCameraSetRotation(int camera, float *rot);
extern void dgCameraSetActive(int camera);
extern int dgCameraGetActive();
extern void dgCameraGetFrustumAttributes(int camera, 
		   float *fov,
		   float *nearWidth, float *nearHeight,
		   float *farWidth, float *farHeight,
		   float *nearDist, float *farDist);
extern void dgCameraSetFrustumAttributes(int camera, 
		   float *fov,
		   float *nearWidth, float *nearHeight,
		   float *farWidth, float *farHeight,
		   float *nearDist, float *farDist);

enum {
	DG_LIGHT_POSITION = 1,
	DG_LIGHT_DIRECTION = 2,
	DG_LIGHT_AMBIENT = 3,
	DG_LIGHT_DIFFUSE = 4,
	DG_LIGHT_SPECULAR = 5,
	DG_LIGHT_EXPONENT = 6,
	DG_LIGHT_CUTOFF = 7,
	DG_LIGHT_CONSTANT = 8,
	DG_LIGHT_LINEAR = 9,
	DG_LIGHT_QUADRATIC = 10
};

extern void *dgLightNew();
extern void dgLightDelete(void *light);
extern void dgLightDraw(void *light, float radius);
extern void dgLightConfigure(void **lights, int nlights);
extern void dgLightSetf(void *light, int pname, float param);
extern void dgLightSetfv(void *light, int pname, float *params);
extern void dgLightGetfv(void *light, int pname, float *params);
extern void dgLightSortByIntensity(void **lights, int nlights, 
				   float *objPos, float *eyePos);
extern void dgLightSetGlobalAmbient(float *fv);
extern void dgLightGetGlobalAmbient(float *fv);

extern int dgShadowVolumeNew();
extern void dgShadowVolumeDelete(int vol);
extern void dgShadowVolumeRender(int vol);
extern void dgShadowVolumeGenerate(int vol, int mesh, int frame, 
				   void *light, float *objPos, float *objRot);
extern void dgShadowVolumeDoPass(
	void **lights, int numLights,
	int *meshes, int numMeshes, int *frames,
	int **shadows,
	float **objPos, float **objRot,
	void (*renderScene)(),
	int regenShadows);
extern void dgShadowVolumeDrawVolumes(int b);
extern void dgShadowVolumeDrawLights(int b);

extern int dgUniqueSourceNew();
extern void dgUniqueSourceDelete(int src);
extern int dgUniqueGetId(int src);
extern void dgUniqueReleaseId(int src, int id);

extern void dgDrawRect(float x, float y, float width, float height);
extern void dgDrawRectTextured(float x, float y, float width, 
			       float height, int texture);
extern void dgDrawArc(float radius, float theta, int sub);

#define DG_MESH 1
#define DG_TEXTURE 2
#define DG_FONT 3
#define DG_AUDIO 4

extern int dgResLoad(int type, char *fn, int opts);
extern void dgResDelete(int type, int ref);
extern int dgResExists(int type, char *fn, int opts);

/* extern void dgResPrefetch(int type, char *fn, int opts); */
/* extern void *dgResPrefetchRetrieve(int type, char *fn, int opts); */
/* extern int dgResPrefetchProgress(); */

/* #define DG_WORKER_MAX_DATA 16 */

/* enum { */
/* 	DG_WORKER_SIGNAL_NONE, */
/* 	DG_WORKER_SIGNAL_PAUSE, */
/* 	DG_WORKER_SIGNAL_STOP, */
/* 	DG_WORKER_SIGNAL_DONE */
/* }; */

/* extern void *dgWorkerNew(void (*func)(void *worker)); */
/* extern void dgWorkerDelete(void *worker); */
/* extern void dgWorkerStart(void *worker); */
/* extern void dgWorkerSetSignal(void *worker, int signal); */
/* extern int dgWorkerGetSignal(void *worker); */
/* extern void dgWorkerSetData(void *worker, int slot, void *data); */
/* extern void dgWorkerSetDataInt(void *worker, int slot, int data); */
/* extern void dgWorkerSetDataFloat(void *worker, int slot, float data); */
/* extern void *dgWorkerGetData(void *worker, int slot); */
/* extern int dgWorkerGetDataInt(void *worker, int slot); */
/* extern float dgWorkerGetDataFloat(void *worker, int slot); */
/* extern void dgWorkerWait(void *worker); */
/* extern void dgWorkerNotify(void *worker); */
/* extern void dgWorkerSetEvent(void *worker, int event, void (*handler)(void *worker)); */
/* extern void dgWorkerTriggerEvent(void *worker, int event); */

/* extern void *dgWorkerGroupNew(void (*func)(void *worker), int num); */
/* extern void dgWorkerGroupDelete(void *group); */
/* extern void dgWorkerGroupStart(void *group); */
/* extern void dgWorkerGroupWait(void *group); */
/* extern void dgWorkerGroupNotify(void *group); */
/* extern void dgWorkerGroupSetSignal(void *group, int signal); */
/* extern void dgWorkerGroupGetSignal(void *group); */
/* extern void dgWorkerGroupSetEvent(void *group, int event,  */
/* 				  void (*handler)(void *worker)); */
/* extern void dgWorkerGroupTriggerEvent(void *group, int event); */
/* extern void dgWorkerGroupSetData(void *group, int slot, void *data); */
/* extern void dgWorkerGroupSetDataInt(void *group, int slot, int data); */
/* extern void dgWorkerGroupSetDataFloat(void *group, int slot, float data); */
/* extern void *dgWorkerGroupGetData(void *group, int slot); */
/* extern int dgWorkerGroupGetDataInt(void *group, int slot); */
/* extern float dgWorkerGroupGetDataFloat(void *group, int slot); */
/* extern void dgWorkerGroupPushData(void *group); */
/* extern void dgWorkerGroupPopData(void *group); */

/** M_PI doesn't get defined for some bizarre reason... */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/** Two times M_PI. */
#define M_2PI 6.28318531

/**
 * Make a copy from SRC to DEST if both are non-NULL.
 *
 * Useful for implementing mass-getter functions where the user
 * can provide NULL pointers as arguments to indicate which values
 * they could care less about. :)
 *
 * @param[in/out] DEST Destination (pointer) to copy to.
 * @param[in] SRC Source (pointer) to copy from.
 */
#define dgCopy(DEST,SRC)\
	if ((DEST) && (SRC)) {*(DEST) = *(SRC);}

/**
 * Make a copy from SRC to DEST if both are non-NULL.
 *
 * Useful for implementing mass-getter functions where the user
 * can provide NULL pointers as arguments to indicate which values
 * they could care less about. :)
 *
 * Same as dgCopy except this deals with arrays.
 *
 * @param[in/out] DEST Destination (pointer) to copy to.
 * @param[in] SRC Source (value) to copy from.
 * @param[in] TYPE Type of array (eg. int, char, ...).
 * @param[in] LEN Length of array in items.
 */
#define dgCopyArray(DEST,SRC,TYPE,LEN)	\
	if ((DEST) && (SRC)) memcpy(DEST, SRC, sizeof(TYPE) * (LEN));

// @cond
	
#define dgCaseSet(A,DEST,SRC) case A: DEST = SRC; break;	
#define dgCaseSet3fv(A, DEST, SRC) case A: memcpy(DEST, SRC, sizeof(float) * 3); break;
#define dgCaseSet4fv(A, DEST, SRC) case A: memcpy(DEST, SRC, sizeof(float) * 4); break;

#if !defined(strdup) && !defined(__cplusplus)
extern char *strdup(const char *s);
#endif

#if !defined(strcasecmp) && !defined(__cplusplus)
extern int strcasecmp(const char*, const char *);
#endif
// @endcond

#ifdef __cplusplus
}
#endif

#endif
