/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _DAGGER3D_INPUT_JOYSTICK_H_
#define _DAGGER3D_INPUT_JOYSTICK_H_

// @cond

#if defined(LINUX)
#include <dagger3d/input/joystick_unix.h>
#endif

#if defined(WIN32)
#include <dagger3d/input/joystick_win32.h>
#endif

typedef struct
{
	/** Number of axes on this device. */
	int numAxes;
	/** Number of buttons on this device. */
	int numButtons;
	/** Name as reported by device. */
	char joyName[80];
	/** Array of device buttons. */
	char *buttons;
	/** Array of device axes. */
	int *axes;
	/** True if this device is still connected. */
	int connected;
	
	#if defined(LINUX)
	char joyPath[80];
	struct js_event joyEvent;
	int joyDesc;
	#endif
	
	#if defined(WIN32)
	int joyDesc;
	/** Axes center values (6 axes supported)*/
	UINT axesCenters[6];
	#endif
} dgJoystick;

extern int _dgJoystickNew();
extern dgJoystick *_dgJoystickGetPtr(int joystick);
extern void *_dgJoystickGetList();
#endif

extern void dgJoystickDelete(int joystick);

// @endcond
