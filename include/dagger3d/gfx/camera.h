/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _DAGGER3D_GFX_CAMERA_H_
#define _DAGGER3D_GFX_CAMERA_H_
// @cond

typedef struct
{
	// Position {x,y,z} of this camera.
	float pos[3];
	// Camera orientation {w,x,y,z}.
	float rot[4];
	// Camera type. Each camera has 3 translational degrees of
	// freedom, and either 2 or 3 degrees of rotational freedom.
	// 5DOF = pitch, yaw
	// 6DOF = pitch, yaw, roll
	int type;
	
	// Information related to this camera's view frustum.
	struct
	{	
		// An approximating cone containing the frustum.
		struct
		{
			// Angle between the forward-vector and
			// a vector from the camera's position to
			// one of the far corners.
			float angle;
			// Distance from apex (camera position)
			// to the end of the cone.
			float len;
		} cone;
	
		// Properties related to the viewing
		// frustum.
		struct
		{
			/* Angle in the y direction that is visible to the user. */
			float fov;
			// Dimensions of the near plane.
			float nearWidth, nearHeight;
			// Dimensions of the far plane.
			float farWidth, farHeight;
			// Distance from camera to near plane.
			float nearDist;
			// Distance from camera to far plane.
			float farDist;
			// True if this camera has been updated
			// and the viewing frustum needs to be
			// regenerated.
			int changed;
		} attribs;

		// Vertices (CCW) making up the near and far
		// plane quadrilaterals.
		// 0 = bottom left
		// 3 = bottom right
		// 6 = upper right
		// 9 = upper left
		struct
		{
			// Near plane.
			float nearp[12];
			// Far plane.
			float farp[12];
		} quads;
	
		// Plane equations for each quad of the viewing
		// frustum.
		struct
		{
			// All equations LRFBTB.
			float eqs[24];
			// Left quad (0).
			float *left;
			// Right quad (4).
			float *right;
			// Front quad (8).
			float *front;
			// Back quad (12).
			float *back;
			// Top quad (16).
			float *top;
			// Bottom quad (20).
			float *bottom;
		} planeqs;
	} frustum;
} dgCamera;

extern int _dgCameraIsValid(int camera);
extern dgCamera *_dgCameraGetPtr(int camera);


//extern void dgCameraCalcFrustum(int camera);

// @endcond
#endif
