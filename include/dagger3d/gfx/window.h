/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _DAGGER3D_GFX_WINDOW_H_
#define _DAGGER3D_GFX_WINDOW_H_
// @cond

#include <dagger3d/gfx/camera.h>

typedef struct
{
	int fullscreen;
	int max_texture_size;
	/* True if we have support for vertex buffer objects. */
	int vbo_supported;
	/* True if VBOs are enabled, default=1 if we have support. */
	int vbo_enabled;
	/* True if non-power-of-two sized textures are supported by hardware. */
	int npot_supported;
	/* Window properties (dimension and refresh rate). */
	int width, height, refresh;
	/* GL window id. */
	int id;
	/* Coordinate mode. */
	int mode;
	/* Window title. */
	char *title;
	/* Maximum FPS. */
	int maxFPS;
	/* Backface culling. */
	int backfaceCull;
	/* Viewing ratio W:H (eg. 4:3, 16:9). */
	float ratio;
	/* Currently active camera. */
	int camera;
	/* Reference to default system camera. */
	int cameraDefault;
	/* Render FPS display if true. */
	int renderFPS;
	
	struct
	{
		int window;
		int fullscreen;
		int width, height;
	} override;
} dgWindow;

extern void _dgWindowSetActiveCam(int camera);
extern dgCamera *_dgWindowCamera();

// @endcond
#endif
