/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _DAGGER3D_GFX_LIGHT_H_
#define _DAGGER3D_GFX_LIGHT_H_

/* See http://www.opengl.org/sdk/docs/man/xhtml/glLight.xml */
typedef struct
{
	/* GL_POSITION */
	float position[4];
	/* GL_SPOT_DIRECTION */
	float direction[3];
	
	struct
	{
		/* GL_AMBIENT */
		float ambient[4];
		/* GL_DIFFUSE */
		float diffuse[4];
		/* GL_SPECULAR */
		float specular[4];		
	} color;

	struct
	{
		/* GL_SPOT_EXPONENT */
		float exponent;
		/* GL_SPOT_CUTOFF */
		float cutoff;
	} spotlight;

	struct
	{
		/* GL_CONSTANT_ATTENUATION */
		float constant;
		/* GL_LINEAR_ATTENUATION */
		float linear;
		/* GL_QUADRATIC_ATTENUATION */
		float quadratic;
	} attenuation;

	struct
	{
		float intensity;
	} tmp;
	
} dgLight;

#endif
