/*
 * Copyright (c) 2008, Tom Arnold
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 3. Neither the name of this project nor the names of its 
 *     contributors may be used to endorse or promote products derived 
 *     from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _DAGGER3D_MESH_MESH_H_
#define _DAGGER3D_MESH_MESH_H_

#include <dagger3d.h>

typedef struct
{
	// Each face is a triangle which means it should
	// have at most 3 "neighboring" adjacent polygons.
	int neighbors[3];
} dgMeshFaceConnectivity;

// Mesh face information.
typedef struct
{
	// Eqn of a plane containing this face {a,b,c,d}.
	float planeEq[4];
	// True if this face is visible.
	int visible;
} dgMeshFace;

typedef struct
{
	struct
	{
		char *mesh;
	} fn;
	
	struct
	{
		int verts;
		int frames; 
		int anims;
		int faces;
	} num;
	
	struct
	{
		float *tex;
		float *verts; 
		float *norms;
	} coords;
	
	struct 
	{
		GLuint tex;
		GLuint *verts;
		GLuint *norms;
	} vbo;
	
	// List of animations associated with this mesh.
	void *animations;
	// Array of faces for each frame.
	dgMeshFace *faces;
	// Array of connectivity info for each face of one
	// frame (the frame isn't important, as a face's adjacent
	// polygons should never change in between frames).
	dgMeshFaceConnectivity *faceConnInfo;
	// Options used when loading this mesh.
	int opts;
} dgMesh;

// @cond
extern dgMesh *_dgMeshGetPtr(int mesh);
extern void *_dgMeshGetList();
// @endcond

#endif
